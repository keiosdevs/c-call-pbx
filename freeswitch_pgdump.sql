--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aliases; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE aliases (
    sticky integer,
    alias character varying(128),
    command character varying(4096),
    hostname character varying(256)
);


ALTER TABLE public.aliases OWNER TO fusionpbx;

--
-- Name: calls; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE calls (
    call_uuid character varying(255),
    call_created character varying(128),
    call_created_epoch integer,
    caller_uuid character varying(256),
    callee_uuid character varying(256),
    hostname character varying(256)
);


ALTER TABLE public.calls OWNER TO fusionpbx;

--
-- Name: channels; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE channels (
    uuid character varying(256),
    direction character varying(32),
    created character varying(128),
    created_epoch integer,
    name character varying(1024),
    state character varying(64),
    cid_name character varying(1024),
    cid_num character varying(256),
    ip_addr character varying(256),
    dest character varying(1024),
    application character varying(128),
    application_data character varying(4096),
    dialplan character varying(128),
    context character varying(128),
    read_codec character varying(128),
    read_rate character varying(32),
    read_bit_rate character varying(32),
    write_codec character varying(128),
    write_rate character varying(32),
    write_bit_rate character varying(32),
    secure character varying(64),
    hostname character varying(256),
    presence_id character varying(4096),
    presence_data character varying(4096),
    callstate character varying(64),
    callee_name character varying(1024),
    callee_num character varying(256),
    callee_direction character varying(5),
    call_uuid character varying(256),
    sent_callee_name character varying(1024),
    sent_callee_num character varying(256),
    initial_cid_name character varying(1024),
    initial_cid_num character varying(256),
    initial_ip_addr character varying(256),
    initial_dest character varying(1024),
    initial_dialplan character varying(128),
    initial_context character varying(128)
);


ALTER TABLE public.channels OWNER TO fusionpbx;

--
-- Name: basic_calls; Type: VIEW; Schema: public; Owner: fusionpbx
--

CREATE VIEW basic_calls AS
 SELECT a.uuid,
    a.direction,
    a.created,
    a.created_epoch,
    a.name,
    a.state,
    a.cid_name,
    a.cid_num,
    a.ip_addr,
    a.dest,
    a.presence_id,
    a.presence_data,
    a.callstate,
    a.callee_name,
    a.callee_num,
    a.callee_direction,
    a.call_uuid,
    a.hostname,
    a.sent_callee_name,
    a.sent_callee_num,
    b.uuid AS b_uuid,
    b.direction AS b_direction,
    b.created AS b_created,
    b.created_epoch AS b_created_epoch,
    b.name AS b_name,
    b.state AS b_state,
    b.cid_name AS b_cid_name,
    b.cid_num AS b_cid_num,
    b.ip_addr AS b_ip_addr,
    b.dest AS b_dest,
    b.presence_id AS b_presence_id,
    b.presence_data AS b_presence_data,
    b.callstate AS b_callstate,
    b.callee_name AS b_callee_name,
    b.callee_num AS b_callee_num,
    b.callee_direction AS b_callee_direction,
    b.sent_callee_name AS b_sent_callee_name,
    b.sent_callee_num AS b_sent_callee_num,
    c.call_created_epoch
   FROM ((channels a
     LEFT JOIN calls c ON ((((a.uuid)::text = (c.caller_uuid)::text) AND ((a.hostname)::text = (c.hostname)::text))))
     LEFT JOIN channels b ON ((((b.uuid)::text = (c.callee_uuid)::text) AND ((b.hostname)::text = (c.hostname)::text))))
  WHERE (((a.uuid)::text = (c.caller_uuid)::text) OR (NOT ((a.uuid)::text IN ( SELECT calls.callee_uuid
           FROM calls))));


ALTER TABLE public.basic_calls OWNER TO fusionpbx;

--
-- Name: complete; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE complete (
    sticky integer,
    a1 character varying(128),
    a2 character varying(128),
    a3 character varying(128),
    a4 character varying(128),
    a5 character varying(128),
    a6 character varying(128),
    a7 character varying(128),
    a8 character varying(128),
    a9 character varying(128),
    a10 character varying(128),
    hostname character varying(256)
);


ALTER TABLE public.complete OWNER TO fusionpbx;

--
-- Name: detailed_calls; Type: VIEW; Schema: public; Owner: fusionpbx
--

CREATE VIEW detailed_calls AS
 SELECT a.uuid,
    a.direction,
    a.created,
    a.created_epoch,
    a.name,
    a.state,
    a.cid_name,
    a.cid_num,
    a.ip_addr,
    a.dest,
    a.application,
    a.application_data,
    a.dialplan,
    a.context,
    a.read_codec,
    a.read_rate,
    a.read_bit_rate,
    a.write_codec,
    a.write_rate,
    a.write_bit_rate,
    a.secure,
    a.hostname,
    a.presence_id,
    a.presence_data,
    a.callstate,
    a.callee_name,
    a.callee_num,
    a.callee_direction,
    a.call_uuid,
    a.sent_callee_name,
    a.sent_callee_num,
    b.uuid AS b_uuid,
    b.direction AS b_direction,
    b.created AS b_created,
    b.created_epoch AS b_created_epoch,
    b.name AS b_name,
    b.state AS b_state,
    b.cid_name AS b_cid_name,
    b.cid_num AS b_cid_num,
    b.ip_addr AS b_ip_addr,
    b.dest AS b_dest,
    b.application AS b_application,
    b.application_data AS b_application_data,
    b.dialplan AS b_dialplan,
    b.context AS b_context,
    b.read_codec AS b_read_codec,
    b.read_rate AS b_read_rate,
    b.read_bit_rate AS b_read_bit_rate,
    b.write_codec AS b_write_codec,
    b.write_rate AS b_write_rate,
    b.write_bit_rate AS b_write_bit_rate,
    b.secure AS b_secure,
    b.hostname AS b_hostname,
    b.presence_id AS b_presence_id,
    b.presence_data AS b_presence_data,
    b.callstate AS b_callstate,
    b.callee_name AS b_callee_name,
    b.callee_num AS b_callee_num,
    b.callee_direction AS b_callee_direction,
    b.call_uuid AS b_call_uuid,
    b.sent_callee_name AS b_sent_callee_name,
    b.sent_callee_num AS b_sent_callee_num,
    c.call_created_epoch
   FROM ((channels a
     LEFT JOIN calls c ON ((((a.uuid)::text = (c.caller_uuid)::text) AND ((a.hostname)::text = (c.hostname)::text))))
     LEFT JOIN channels b ON ((((b.uuid)::text = (c.callee_uuid)::text) AND ((b.hostname)::text = (c.hostname)::text))))
  WHERE (((a.uuid)::text = (c.caller_uuid)::text) OR (NOT ((a.uuid)::text IN ( SELECT calls.callee_uuid
           FROM calls))));


ALTER TABLE public.detailed_calls OWNER TO fusionpbx;

--
-- Name: interfaces; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE interfaces (
    type character varying(128),
    name character varying(1024),
    description character varying(4096),
    ikey character varying(1024),
    filename character varying(4096),
    syntax character varying(4096),
    hostname character varying(256)
);


ALTER TABLE public.interfaces OWNER TO fusionpbx;

--
-- Name: nat; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE nat (
    sticky integer,
    port integer,
    proto integer,
    hostname character varying(256)
);


ALTER TABLE public.nat OWNER TO fusionpbx;

--
-- Name: recovery; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE recovery (
    runtime_uuid character varying(255),
    technology character varying(255),
    profile_name character varying(255),
    hostname character varying(255),
    uuid character varying(255),
    metadata text
);


ALTER TABLE public.recovery OWNER TO fusionpbx;

--
-- Name: registrations; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE registrations (
    reg_user character varying(256),
    realm character varying(256),
    token character varying(256),
    url text,
    expires integer,
    network_ip character varying(256),
    network_port character varying(256),
    network_proto character varying(256),
    hostname character varying(256),
    metadata character varying(256)
);


ALTER TABLE public.registrations OWNER TO fusionpbx;

--
-- Name: tasks; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE tasks (
    task_id integer,
    task_desc character varying(4096),
    task_group character varying(1024),
    task_sql_manager integer,
    hostname character varying(256)
);


ALTER TABLE public.tasks OWNER TO fusionpbx;

--
-- Data for Name: aliases; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY aliases (sticky, alias, command, hostname) FROM stdin;
\.


--
-- Data for Name: calls; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY calls (call_uuid, call_created, call_created_epoch, caller_uuid, callee_uuid, hostname) FROM stdin;
\.


--
-- Data for Name: channels; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY channels (uuid, direction, created, created_epoch, name, state, cid_name, cid_num, ip_addr, dest, application, application_data, dialplan, context, read_codec, read_rate, read_bit_rate, write_codec, write_rate, write_bit_rate, secure, hostname, presence_id, presence_data, callstate, callee_name, callee_num, callee_direction, call_uuid, sent_callee_name, sent_callee_num, initial_cid_name, initial_cid_num, initial_ip_addr, initial_dest, initial_dialplan, initial_context) FROM stdin;
\.


--
-- Data for Name: complete; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY complete (sticky, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, hostname) FROM stdin;
0	conference	::conference::list_conferences	list								sigil
0	conference	::conference::list_conferences	xml_list								sigil
0	conference	::conference::list_conferences	energy								sigil
0	conference	::conference::list_conferences	volume_in								sigil
0	conference	::conference::list_conferences	volume_out								sigil
0	conference	::conference::list_conferences	position								sigil
0	conference	::conference::list_conferences	auto-3d-position								sigil
0	conference	::conference::list_conferences	play								sigil
0	conference	::conference::list_conferences	pause								sigil
0	conference	::conference::list_conferences	file_seek								sigil
0	conference	::conference::list_conferences	say								sigil
0	conference	::conference::list_conferences	saymember								sigil
0	conference	::conference::list_conferences	stop								sigil
0	conference	::conference::list_conferences	dtmf								sigil
0	conference	::conference::list_conferences	kick								sigil
0	conference	::conference::list_conferences	hup								sigil
0	conference	::conference::list_conferences	mute								sigil
0	conference	::conference::list_conferences	tmute								sigil
0	conference	::conference::list_conferences	unmute								sigil
0	conference	::conference::list_conferences	deaf								sigil
0	conference	::conference::list_conferences	undeaf								sigil
0	conference	::conference::list_conferences	relate								sigil
0	conference	::conference::list_conferences	lock								sigil
0	conference	::conference::list_conferences	unlock								sigil
0	conference	::conference::list_conferences	agc								sigil
0	conference	::conference::list_conferences	dial								sigil
0	conference	::conference::list_conferences	bgdial								sigil
0	conference	::conference::list_conferences	transfer								sigil
0	conference	::conference::list_conferences	record								sigil
0	conference	::conference::list_conferences	chkrecord								sigil
0	conference	::conference::list_conferences	norecord								sigil
0	conference	::conference::list_conferences	pause								sigil
0	conference	::conference::list_conferences	resume								sigil
0	sofia	help									sigil
0	sofia	status									sigil
0	sofia	xmlstatus									sigil
0	sofia	loglevel	::[all:default:tport:iptsec:nea:nta:nth_client:nth_server:nua:soa:sresolv:stun	::[0:1:2:3:4:5:6:7:8:9							sigil
0	sofia	tracelevel	::[console:alert:crit:err:warning:notice:info:debug								sigil
0	sofia	global	siptrace	::[on:off							sigil
0	sofia	global	standby	::[on:off							sigil
0	sofia	global	capture	::[on:off							sigil
0	sofia	global	watchdog	::[on:off							sigil
0	sofia	global	debug	::[presence:sla:none							sigil
0	sofia	profile									sigil
0	sofia	profile	restart	all							sigil
0	sofia	profile	::sofia::list_profiles	start							sigil
0	sofia	profile	::sofia::list_profiles	stop	wait						sigil
0	sofia	profile	::sofia::list_profiles	rescan							sigil
0	sofia	profile	::sofia::list_profiles	restart							sigil
0	sofia	profile	::sofia::list_profiles	flush_inbound_reg							sigil
0	sofia	profile	::sofia::list_profiles	check_sync							sigil
0	sofia	profile	::sofia::list_profiles	register	::sofia::list_profile_gateway						sigil
0	sofia	profile	::sofia::list_profiles	unregister	::sofia::list_profile_gateway						sigil
0	sofia	profile	::sofia::list_profiles	killgw	::sofia::list_profile_gateway						sigil
0	sofia	profile	::sofia::list_profiles	siptrace	on						sigil
0	sofia	profile	::sofia::list_profiles	siptrace	off						sigil
0	sofia	profile	::sofia::list_profiles	capture	on						sigil
0	sofia	profile	::sofia::list_profiles	capture	off						sigil
0	sofia	profile	::sofia::list_profiles	watchdog	on						sigil
0	sofia	profile	::sofia::list_profiles	watchdog	off						sigil
0	sofia	profile	::sofia::list_profiles	gwlist	up						sigil
0	sofia	profile	::sofia::list_profiles	gwlist	down						sigil
0	sofia	status	profile	::sofia::list_profiles							sigil
0	sofia	status	profile	::sofia::list_profiles	reg						sigil
0	sofia	status	gateway	::sofia::list_gateways							sigil
0	sofia	xmlstatus	profile	::sofia::list_profiles							sigil
0	sofia	xmlstatus	profile	::sofia::list_profiles	reg						sigil
0	sofia	xmlstatus	gateway	::sofia::list_gateways							sigil
0	conference	::conference::list_conferences	recording								sigil
0	conference	::conference::list_conferences	exit_sound								sigil
0	conference	::conference::list_conferences	enter_sound								sigil
0	conference	::conference::list_conferences	pin								sigil
0	conference	::conference::list_conferences	nopin								sigil
0	conference	::conference::list_conferences	get								sigil
0	conference	::conference::list_conferences	set								sigil
0	conference	::conference::list_conferences	file-vol								sigil
0	conference	::conference::list_conferences	floor								sigil
0	conference	::conference::list_conferences	vid-floor								sigil
0	conference	::conference::list_conferences	clear-vid-floor								sigil
0	hash	insert									sigil
0	hash	delete									sigil
0	hash	select									sigil
0	hash_remote	list									sigil
0	hash_remote	kill									sigil
0	hash_remote	rescan									sigil
0	start_tone_detect	::console::list_uuid									sigil
0	stop_tone_detect	::console::list_uuid									sigil
0	uuid_send_tdd	::console::list_uuid									sigil
0	callcenter_config	agent	add								sigil
0	callcenter_config	agent	del								sigil
0	callcenter_config	agent	reload								sigil
0	callcenter_config	agent	set	status							sigil
0	callcenter_config	agent	set	state							sigil
0	callcenter_config	agent	set	uuid							sigil
0	callcenter_config	agent	set	contact							sigil
0	callcenter_config	agent	set	ready_time							sigil
0	callcenter_config	agent	set	reject_delay_time							sigil
0	callcenter_config	agent	set	busy_delay_time							sigil
0	callcenter_config	agent	set	no_answer_delay_time							sigil
0	callcenter_config	agent	get	status							sigil
0	callcenter_config	agent	list								sigil
0	callcenter_config	tier	add								sigil
0	callcenter_config	tier	del								sigil
0	callcenter_config	tier	reload								sigil
0	callcenter_config	tier	set	state							sigil
0	callcenter_config	tier	set	level							sigil
0	callcenter_config	tier	set	position							sigil
0	callcenter_config	tier	list								sigil
0	callcenter_config	queue	load								sigil
0	callcenter_config	queue	unload								sigil
0	callcenter_config	queue	reload								sigil
0	callcenter_config	queue	list								sigil
0	callcenter_config	queue	list	agents							sigil
0	callcenter_config	queue	list	members							sigil
0	callcenter_config	queue	list	tiers							sigil
0	callcenter_config	queue	count								sigil
0	callcenter_config	queue	count	agents							sigil
0	callcenter_config	queue	count	members							sigil
0	callcenter_config	queue	count	tiers							sigil
0	alias	add									sigil
0	alias	stickyadd									sigil
0	alias	del									sigil
0	coalesce										sigil
0	complete	add									sigil
0	complete	del									sigil
0	db_cache	status									sigil
0	fsctl	debug_level									sigil
0	fsctl	debug_pool									sigil
0	fsctl	debug_sql									sigil
0	fsctl	last_sps									sigil
0	fsctl	default_dtmf_duration									sigil
0	fsctl	hupall									sigil
0	fsctl	loglevel									sigil
0	fsctl	loglevel	console								sigil
0	fsctl	loglevel	alert								sigil
0	fsctl	loglevel	crit								sigil
0	fsctl	loglevel	err								sigil
0	fsctl	loglevel	warning								sigil
0	fsctl	loglevel	notice								sigil
0	fsctl	loglevel	info								sigil
0	fsctl	loglevel	debug								sigil
0	fsctl	max_dtmf_duration									sigil
0	fsctl	max_sessions									sigil
0	fsctl	min_dtmf_duration									sigil
0	fsctl	pause									sigil
0	fsctl	pause	inbound								sigil
0	fsctl	pause	outbound								sigil
0	fsctl	reclaim_mem									sigil
0	fsctl	resume									sigil
0	fsctl	resume	inbound								sigil
0	fsctl	resume	outbound								sigil
0	fsctl	calibrate_clock									sigil
0	fsctl	crash									sigil
0	fsctl	verbose_events									sigil
0	fsctl	save_history									sigil
0	fsctl	pause_check									sigil
0	fsctl	pause_check	inbound								sigil
0	fsctl	pause_check	outbound								sigil
0	fsctl	ready_check									sigil
0	fsctl	recover									sigil
0	fsctl	shutdown_check									sigil
0	fsctl	shutdown									sigil
0	fsctl	shutdown	asap								sigil
0	fsctl	shutdown	now								sigil
0	fsctl	shutdown	asap	restart							sigil
0	fsctl	shutdown	cancel								sigil
0	fsctl	shutdown	elegant								sigil
0	fsctl	shutdown	elegant	restart							sigil
0	fsctl	shutdown	reincarnate	now							sigil
0	fsctl	shutdown	restart								sigil
0	fsctl	shutdown	restart	asap							sigil
0	fsctl	shutdown	restart	elegant							sigil
0	fsctl	sps									sigil
0	fsctl	sync_clock									sigil
0	fsctl	flush_db_handles									sigil
0	fsctl	min_idle_cpu									sigil
0	fsctl	send_sighup									sigil
0	interface_ip	auto	::console::list_interfaces								sigil
0	interface_ip	ipv4	::console::list_interfaces								sigil
0	interface_ip	ipv6	::console::list_interfaces								sigil
0	load	::console::list_available_modules									sigil
0	nat_map	reinit									sigil
0	nat_map	republish									sigil
0	nat_map	status									sigil
0	reload	::console::list_loaded_modules									sigil
0	reloadacl	reloadxml									sigil
0	show	aliases									sigil
0	show	api									sigil
0	show	application									sigil
0	show	calls									sigil
0	show	channels									sigil
0	show	channels	count								sigil
0	show	chat									sigil
0	show	codec									sigil
0	show	complete									sigil
0	show	dialplan									sigil
0	show	detailed_calls									sigil
0	show	bridged_calls									sigil
0	show	detailed_bridged_calls									sigil
0	show	endpoint									sigil
0	show	file									sigil
0	show	interfaces									sigil
0	show	interface_types									sigil
0	show	tasks									sigil
0	show	management									sigil
0	show	modules									sigil
0	show	nat_map									sigil
0	show	registrations									sigil
0	show	say									sigil
0	show	status									sigil
0	show	timer									sigil
0	shutdown										sigil
0	sql_escape										sigil
0	unload	::console::list_loaded_modules									sigil
0	uuid_audio	::console::list_uuid	start	read	mute						sigil
0	uuid_audio	::console::list_uuid	start	read	level						sigil
0	uuid_audio	::console::list_uuid	start	write	mute						sigil
0	uuid_audio	::console::list_uuid	start	write	level						sigil
0	uuid_audio	::console::list_uuid	stop								sigil
0	uuid_break	::console::list_uuid	all								sigil
0	uuid_break	::console::list_uuid	both								sigil
0	uuid_bridge	::console::list_uuid	::console::list_uuid								sigil
0	uuid_broadcast	::console::list_uuid									sigil
0	uuid_buglist	::console::list_uuid									sigil
0	uuid_chat	::console::list_uuid									sigil
0	uuid_debug_media	::console::list_uuid									sigil
0	uuid_deflect	::console::list_uuid									sigil
0	uuid_displace	::console::list_uuid									sigil
0	uuid_display	::console::list_uuid									sigil
0	uuid_dump	::console::list_uuid									sigil
0	uuid_answer	::console::list_uuid									sigil
0	uuid_ring_ready	::console::list_uuid	queued								sigil
0	uuid_pre_answer	::console::list_uuid									sigil
0	uuid_early_ok	::console::list_uuid									sigil
0	uuid_exists	::console::list_uuid									sigil
0	uuid_fileman	::console::list_uuid									sigil
0	uuid_flush_dtmf	::console::list_uuid									sigil
0	uuid_getvar	::console::list_uuid									sigil
0	uuid_hold	::console::list_uuid									sigil
0	uuid_send_info	::console::list_uuid									sigil
0	uuid_jitterbuffer	::console::list_uuid									sigil
0	uuid_kill	::console::list_uuid									sigil
0	uuid_outgoing_answer	::console::list_uuid									sigil
0	uuid_limit	::console::list_uuid									sigil
0	uuid_limit_release	::console::list_uuid									sigil
0	uuid_loglevel	::console::list_uuid	console								sigil
0	uuid_loglevel	::console::list_uuid	alert								sigil
0	uuid_loglevel	::console::list_uuid	crit								sigil
0	uuid_loglevel	::console::list_uuid	err								sigil
0	uuid_loglevel	::console::list_uuid	warning								sigil
0	uuid_loglevel	::console::list_uuid	notice								sigil
0	uuid_loglevel	::console::list_uuid	info								sigil
0	uuid_loglevel	::console::list_uuid	debug								sigil
0	uuid_media	::console::list_uuid									sigil
0	uuid_media	off	::console::list_uuid								sigil
0	uuid_park	::console::list_uuid									sigil
0	uuid_media_reneg	::console::list_uuid									sigil
0	uuid_phone_event	::console::list_uuid	talk								sigil
0	uuid_phone_event	::console::list_uuid	hold								sigil
0	uuid_preprocess	::console::list_uuid									sigil
0	uuid_record	::console::list_uuid	::[start:stop								sigil
0	uuid_recovery_refresh	::console::list_uuid									sigil
0	uuid_recv_dtmf	::console::list_uuid									sigil
0	uuid_send_dtmf	::console::list_uuid									sigil
0	uuid_session_heartbeat	::console::list_uuid									sigil
0	uuid_setvar_multi	::console::list_uuid									sigil
0	uuid_setvar	::console::list_uuid									sigil
0	uuid_simplify	::console::list_uuid									sigil
0	uuid_transfer	::console::list_uuid									sigil
0	uuid_dual_transfer	::console::list_uuid									sigil
0	uuid_video_refresh	::console::list_uuid									sigil
0	version										sigil
0	uuid_warning	::console::list_uuid									sigil
0	...										sigil
0	file_exists										sigil
0	db	insert									sigil
0	db	delete									sigil
0	db	select									sigil
0	db	exists									sigil
0	db	count									sigil
0	db	list									sigil
0	group	insert									sigil
0	group	delete									sigil
0	group	call									sigil
0	fifo	list									sigil
0	fifo	list_verbose									sigil
0	fifo	count									sigil
0	fifo	debug									sigil
0	fifo	status									sigil
0	fifo	has_outbound									sigil
0	fifo	importance									sigil
0	fifo	reparse									sigil
0	fifo_check_bridge	::console::list_uuid									sigil
\.


--
-- Data for Name: interfaces; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY interfaces (type, name, description, ikey, filename, syntax, hostname) FROM stdin;
dialplan	enum		mod_enum	/usr/local/freeswitch/mod/mod_enum.so		sigil
application	enum	Perform an ENUM lookup	mod_enum	/usr/local/freeswitch/mod/mod_enum.so	[reload | <number> [<root>]]	sigil
api	enum	ENUM	mod_enum	/usr/local/freeswitch/mod/mod_enum.so		sigil
api	enum_auto	ENUM	mod_enum	/usr/local/freeswitch/mod/mod_enum.so		sigil
endpoint	error		mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
endpoint	group		mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
endpoint	user		mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
endpoint	pickup		mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
dialplan	inline		mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	blind_transfer_ack		mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[true|false]	sigil
application	bind_digit_action	bind a key sequence or regex to an action	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<realm>,<digits|~regex>,<string>[,<value>][,<dtmf target leg>][,<event target leg>]	sigil
application	capture	capture data into a var	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<varname>|<data>|<regex>	sigil
endpoint	sofia		mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so		sigil
endpoint	rtp		mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so		sigil
application	sofia_sla	private sofia sla function	mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so	<uuid>	sigil
api	sofia	Sofia Controls	mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so	<cmd> <args>	sigil
api	sofia_gateway_data	Get data from a sofia gateway	mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so	<gateway_name> [ivar|ovar|var] <name>	sigil
api	sofia_username_of	Sofia Username Lookup	mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so	[profile/]<user>@<domain>	sigil
api	sofia_contact	Sofia Contacts	mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so	[profile/]<user>@<domain>	sigil
api	sofia_count_reg	Count Sofia registration	mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so	[profile/]<user>@<domain>	sigil
api	sofia_dig	SIP DIG	mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so	<url>	sigil
api	sofia_presence_data	Sofia Presence Data	mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so	[list|status|rpid|user_agent] [profile/]<user>@domain	sigil
chat	sip		mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so		sigil
management	1001		mod_sofia	/usr/local/freeswitch/mod/mod_sofia.so		sigil
application	clear_digit_action	clear all digit bindings	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<realm>|all[,target]	sigil
application	digit_action_set_realm	change binding realm	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<realm>[,<target>]	sigil
application	privacy	Set privacy on calls	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	off|on|name|full|number	sigil
application	set_audio_level	set volume	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	set_mute	set mute	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	flush_dtmf	flush any queued dtmf	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	hold	Send a hold message	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[<display message>]	sigil
application	unhold	Send a un-hold message	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	mutex	block on a call flow only allowing one at a time	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<keyname>[ on|off]	sigil
application	page		mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<var1=val1,var2=val2><chan1>[:_:<chanN>]	sigil
application	transfer	Transfer a channel	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<exten> [<dialplan> <context>]	sigil
application	check_acl	Check an ip against an ACL list	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<ip> <acl | cidr> [<hangup_cause>]	sigil
application	verbose_events	Make ALL Events verbose.	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	novideo	Refuse Inbound Video	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	cng_plc	Do PLC on CNG frames	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	early_hangup	Enable early hangup	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	sleep	Pause a channel	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<pausemilliseconds>	sigil
application	delay_echo	echo audio at a specified delay	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<delay ms>	sigil
application	strftime	strftime	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[<epoch>|]<format string>	sigil
application	phrase	Say a Phrase	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<macro_name>,<data>	sigil
application	eval	Do Nothing	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	set_media_stats	Set Media Stats	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	stop	Do Nothing	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	set_zombie_exec	Enable Zombie Execution	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	pre_answer	Pre-Answer the call	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	answer	Answer the call	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	wait_for_answer	Wait for call to be answered	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	hangup	Hangup the call	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[<cause>]	sigil
application	set_name	Name the channel	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<name>	sigil
application	presence	Send Presence	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<rpid> <status> [<id>]	sigil
application	log	Logs to the logger	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<log_level> <log_string>	sigil
application	info	Display Call Info	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	event	Fire an event	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	sound_test	Analyze Audio	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	export	Export a channel variable across a bridge	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<varname>=<value>	sigil
application	bridge_export	Export a channel variable across a bridge	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<varname>=<value>	sigil
application	set	Set a channel variable	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<varname>=<value>	sigil
application	multiset	Set many channel variables	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[^^<delim>]<varname>=<value> <var2>=<val2>	sigil
application	push	Set a channel variable	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<varname>=<value>	sigil
application	unshift	Set a channel variable	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<varname>=<value>	sigil
application	set_global	Set a global variable	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<varname>=<value>	sigil
application	set_profile_var	Set a caller profile variable	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<varname>=<value>	sigil
application	unset	Unset a channel variable	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<varname>	sigil
application	multiunset	Unset many channel variables	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[^^<delim>]<varname> <var2> <var3>	sigil
application	ring_ready	Indicate Ring_Ready	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	remove_bugs	Remove media bugs	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[<function>]	sigil
application	break	Break	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	detect_speech	Detect speech	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<mod_name> <gram_name> <gram_path> [<addr>] OR grammar <gram_name> [<path>] OR nogrammar <gram_name> OR grammaron/grammaroff <gram_name> OR grammarsalloff OR pause OR resume OR start_input_timers OR stop OR param <name> <value>	sigil
application	play_and_detect_speech	Play and do speech recognition	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<file> detect:<engine> {param1=val1,param2=val2}<grammar>	sigil
application	ivr	Run an ivr menu	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<menu_name>	sigil
application	redirect	Send session redirect	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<redirect_data>	sigil
application	video_refresh	Send video refresh.	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	send_info	Send info	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<info>	sigil
application	jitterbuffer	Send session jitterbuffer	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<jitterbuffer_data>	sigil
application	send_display	Send session a new display	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<text>	sigil
application	respond	Send session respond	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<respond_data>	sigil
application	deflect	Send call deflect	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<deflect_data>	sigil
application	recovery_refresh	Send call recovery_refresh	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	queue_dtmf	Queue dtmf to be sent	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<dtmf_data>	sigil
application	send_dtmf	Send dtmf to be sent	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<dtmf_data>	sigil
application	sched_cancel	cancel scheduled tasks	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[group]	sigil
application	sched_hangup	Schedule a hangup in the future	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[+]<time> [<cause>]	sigil
application	sched_broadcast	Schedule a broadcast in the future	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[+]<time> <path> [aleg|bleg|both]	sigil
application	sched_transfer	Schedule a transfer in the future	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[+]<time> <extension> <dialplan> <context>	sigil
application	execute_extension	Execute an extension	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<extension> <dialplan> <context>	sigil
application	sched_heartbeat	Enable Scheduled Heartbeat	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[0|<seconds>]	sigil
application	enable_heartbeat	Enable Media Heartbeat	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[0|<seconds>]	sigil
application	enable_keepalive	Enable Keepalive	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[0|<seconds>]	sigil
application	media_reset	Reset all bypass/proxy media flags	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	mkdir	Create a directory	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<path>	sigil
application	rename	Rename file	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<from_path> <to_path>	sigil
application	soft_hold	Put a bridged channel on hold	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<unhold key> [<moh_a>] [<moh_b>]	sigil
application	bind_meta_app	Bind a key to an application	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<key> [a|b|ab] [a|b|o|s|i|1] <app>	sigil
application	unbind_meta_app	Unbind a key from an application	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[<key>]	sigil
application	block_dtmf	Block DTMF	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	unblock_dtmf	Stop blocking DTMF	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	intercept	intercept	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[-bleg] <uuid>	sigil
application	eavesdrop	eavesdrop on a uuid	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[all | <uuid>]	sigil
application	three_way	three way call with a uuid	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<uuid>	sigil
application	set_user	Set a User	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<user>@<domain> [prefix]	sigil
application	stop_dtmf	stop inband dtmf	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	start_dtmf	Detect dtmf	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	stop_dtmf_generate	stop inband dtmf generation	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[write]	sigil
application	start_dtmf_generate	Generate dtmf	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	stop_tone_detect	stop detecting tones	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	fax_detect	Detect faxes	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	tone_detect	Detect tones	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	echo	Echo	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	park	Park	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	park_state	Park State	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	gentones	Generate Tones	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<tgml_script>[|<loops>]	sigil
application	playback	Playback File	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<path>	sigil
application	endless_playback	Playback File Endlessly	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<path>	sigil
application	loop_playback	Playback File looply	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[+loops] <path>	sigil
application	att_xfer	Attended Transfer	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<channel_url>	sigil
application	read	Read Digits	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<min> <max> <file> <var_name> <timeout> <terminators> <digit_timeout>	sigil
application	play_and_get_digits	Play and get Digits	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	\n\t<min> <max> <tries> <timeout> <terminators> <file> <invalid_file> <var_name> <regexp> [<digit_timeout>] ['<failure_ext> [failure_dp [failure_context]]']	sigil
application	stop_record_session	Stop Record Session	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<path>	sigil
application	record_session	Record Session	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<path> [+<timeout>]	sigil
application	record_session_mask	Mask audio in recording	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<path>	sigil
application	record_session_unmask	Resume recording	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<path>	sigil
application	record	Record File	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<path> [<time_limit_secs>] [<silence_thresh>] [<silence_hits>]	sigil
application	preprocess	pre-process	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	stop_displace_session	Stop Displace File	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<path>	sigil
application	displace_session	Displace File	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<path> [<flags>] [+time_limit_ms]	sigil
application	speak	Speak text	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<engine>|<voice>|<text>	sigil
application	clear_speech_cache	Clear Speech Handle Cache	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	bridge	Bridge Audio	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<channel_url>	sigil
application	system	Execute a system command	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<command>	sigil
application	bgsystem	Execute a system command in the background	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<command>	sigil
application	say	say	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<module_name>[:<lang>] <say_type> <say_method> [<say_gender>] <text>	sigil
application	wait_for_silence	wait_for_silence	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<silence_thresh> <silence_hits> <listen_hits> <timeout_ms> [<file>]	sigil
application	session_loglevel	session_loglevel	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<level>	sigil
application	limit	Limit	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<backend> <realm> <id> [<max>[/interval]] [number [dialplan [context]]]	sigil
application	limit_hash	Limit	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<realm> <id> [<max>[/interval]] [number [dialplan [context]]]	sigil
application	limit_execute	Limit	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<backend> <realm> <id> <max>[/interval] <application> [application arguments]	sigil
application	limit_hash_execute	Limit	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<realm> <id> <max>[/interval] <application> [application arguments]	sigil
application	pickup	Pickup	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[<key>]	sigil
api	strepoch	Convert a date string into epoch time	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<string>	sigil
api	page	Send a file as a page	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	(var1=val1,var2=val2)<var1=val1,var2=val2><chan1>[:_:<chanN>]	sigil
api	strmicroepoch	Convert a date string into micoepoch time	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<string>	sigil
api	chat	chat	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<proto>|<from>|<to>|<message>|[<content-type>]	sigil
api	strftime	strftime	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	<format_string>	sigil
api	presence	presence	mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so	[in|out] <user> <rpid> <message>	sigil
file	file_string		mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
file	file		mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
chat	event		mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
chat	api		mod_dptools	/usr/local/freeswitch/mod/mod_dptools.so		sigil
application	conference	conference	mod_conference	/usr/local/freeswitch/mod/mod_conference.so		sigil
application	conference_set_auto_outcall	conference_set_auto_outcall	mod_conference	/usr/local/freeswitch/mod/mod_conference.so		sigil
api	conference	Conference module commands	mod_conference	/usr/local/freeswitch/mod/mod_conference.so	\t\tlist [delim <string>]|[count]\n\t\txml_list\n\t\tenergy <member_id|all|last|non_moderator> [<newval>]\n\t\tvolume_in <member_id|all|last|non_moderator> [<newval>]\n\t\tvolume_out <member_id|all|last|non_moderator> [<newval>]\n\t\tposition <member_id> <x>,<y>,<z>\n\t\tauto-3d-position [on|off]\n\t\tplay <file_path> [async|<member_id> [nomux]]\n\t\tpause\n\t\tfile_seek [+-]<val>\n\t\tsay <text>\n\t\tsaymember <member_id> <text>\n\t\tstop <[current|all|async|last]> [<member_id>]\n\t\tdtmf <[member_id|all|last|non_moderator]> <digits>\n\t\tkick <[member_id|all|last|non_moderator]> [<optional sound file>]\n\t\thup <[member_id|all|last|non_moderator]>\n\t\tmute <[member_id|all]|last|non_moderator> [<quiet>]\n\t\ttmute <[member_id|all]|last|non_moderator> [<quiet>]\n\t\tunmute <[member_id|all]|last|non_moderator> [<quiet>]\n\t\tdeaf <[member_id|all]|last|non_moderator>\n\t\tundeaf <[member_id|all]|last|non_moderator>\n\t\trelate <member_id> <other_member_id> [nospeak|nohear|clear]\n\t\tlock\n\t\tunlock\n\t\tagc\n\t\tdial <endpoint_module_name>/<destination> <callerid number> <callerid name>\n\t\tbgdial <endpoint_module_name>/<destination> <callerid number> <callerid name>\n\t\ttransfer <conference_name> <member id> [...<member id>]\n\t\trecord <filename>\n\t\tchkrecord <confname>\n\t\tnorecord <[filename|all]>\n\t\tpause <filename>\n\t\tresume <filename>\n\t\trecording [start|stop|check|pause|resume] [<filename>|all]\n\t\texit_sound on|off|none|file <filename>\n\t\tenter_sound on|off|none|file <filename>\n\t\tpin <pin#>\n\t\tnopin\n\t\tget <parameter-name>\n\t\tset <max_members|sound_prefix|caller_id_name|caller_id_number|endconf_grace_time> <value>\n\t\tfile-vol <vol#>\n\t\tfloor <member_id|last>\n\t\tvid-floor <member_id|last> [force]\n\t\tclear-vid-floor	sigil
chat	conf		mod_conference	/usr/local/freeswitch/mod/mod_conference.so		sigil
application	esf_page_group		mod_esf	/usr/local/freeswitch/mod/mod_esf.so		sigil
application	info	Display Call Info	mod_sms	/usr/local/freeswitch/mod/mod_sms.so		sigil
application	reply	reply to a message	mod_sms	/usr/local/freeswitch/mod/mod_sms.so		sigil
application	stop	stop execution	mod_sms	/usr/local/freeswitch/mod/mod_sms.so		sigil
application	set	set a variable	mod_sms	/usr/local/freeswitch/mod/mod_sms.so		sigil
application	send	send the message as-is	mod_sms	/usr/local/freeswitch/mod/mod_sms.so		sigil
application	fire	fire the message	mod_sms	/usr/local/freeswitch/mod/mod_sms.so		sigil
application	system	execute a system command	mod_sms	/usr/local/freeswitch/mod/mod_sms.so		sigil
chat	GLOBAL_SMS		mod_sms	/usr/local/freeswitch/mod/mod_sms.so		sigil
application	hash	Insert into the hashtable	mod_hash	/usr/local/freeswitch/mod/mod_hash.so	[insert|insert_ifempty|delete|delete_ifmatch]/<realm>/<key>/<val>	sigil
api	hash	hash get/set	mod_hash	/usr/local/freeswitch/mod/mod_hash.so	[insert|delete|select]/<realm>/<key>/<value>	sigil
api	hash_dump	dump hash/limit_hash data (used for synchronization)	mod_hash	/usr/local/freeswitch/mod/mod_hash.so	all|limit|db [<realm>]	sigil
api	hash_remote	hash remote	mod_hash	/usr/local/freeswitch/mod/mod_hash.so	list|kill [name]|rescan	sigil
limit	hash		mod_hash	/usr/local/freeswitch/mod/mod_hash.so		sigil
endpoint	modem		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	ADPCM (IMA)		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	G.726 16k (AAL2)		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	G.726 16k		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	G.726 24k (AAL2)		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	G.726 24k		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	G.726 32k (AAL2)		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	G.726 32k		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	G.726 40k (AAL2)		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	G.726 40k		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	G.722		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	GSM		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
codec	LPC-10		mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
application	t38_gateway	Convert to T38 Gateway if tones are heard	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
application	rxfax	FAX Receive Application	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so	<filename>	sigil
application	txfax	FAX Transmit Application	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so	<filename>	sigil
application	stopfax	Stop FAX Application	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
application	spandsp_stop_dtmf	stop inband dtmf	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
application	spandsp_start_dtmf	Detect dtmf	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
application	spandsp_stop_inject_tdd	stop sending tdd	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
application	spandsp_inject_tdd	Send TDD data	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
application	spandsp_stop_detect_tdd	stop sending tdd	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
application	spandsp_detect_tdd	Detect TDD data	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
application	spandsp_send_tdd	Send TDD data	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
application	spandsp_start_fax_detect	start fax detect	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so	<app>[ <arg>][ <timeout>][ <tone_type>]	sigil
application	spandsp_stop_fax_detect	stop fax detect	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
application	start_tone_detect	Start background tone detection with cadence	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so	<name>	sigil
application	stop_tone_detect	Stop background tone detection with cadence	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so		sigil
api	start_tone_detect	Start background tone detection with cadence	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so	<uuid> <name>	sigil
api	stop_tone_detect	Stop background tone detection with cadence	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so	<uuid>	sigil
api	start_tdd_detect	Start background tdd detection	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so	<uuid>	sigil
api	stop_tdd_detect	Stop background tdd detection	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so	<uuid>	sigil
api	uuid_send_tdd	send tdd data to a uuid	mod_spandsp	/usr/local/freeswitch/mod/mod_spandsp.so	<uuid> <text>	sigil
application	callcenter	CallCenter	mod_callcenter	/usr/local/freeswitch/mod/mod_callcenter.so	queue_name	sigil
api	callcenter_config	Config of callcenter	mod_callcenter	/usr/local/freeswitch/mod/mod_callcenter.so	callcenter_config <target> <args>,\n\tcallcenter_config agent add [name] [type] | \n\tcallcenter_config agent del [name] | \n\tcallcenter_config agent reload [name] | \n\tcallcenter_config agent set status [agent_name] [status] | \n\tcallcenter_config agent set state [agent_name] [state] | \n\tcallcenter_config agent set contact [agent_name] [contact] | \n\tcallcenter_config agent set ready_time [agent_name] [wait till epoch] | \n\tcallcenter_config agent set reject_delay_time [agent_name] [wait second] | \n\tcallcenter_config agent set busy_delay_time [agent_name] [wait second] | \n\tcallcenter_config agent set no_answer_delay_time [agent_name] [wait second] | \n\tcallcenter_config agent get status [agent_name] | \n\tcallcenter_config agent get state [agent_name] | \n\tcallcenter_config agent get uuid [agent_name] | \n\tcallcenter_config agent list [[agent_name]] | \n\tcallcenter_config tier add [queue_name] [agent_name] [level] [position] | \n\tcallcenter_config tier set state [queue_name] [agent_name] [state] | \n\tcallcenter_config tier set level [queue_name] [agent_name] [level] | \n\tcallcenter_config tier set position [queue_name] [agent_name] [position] | \n\tcallcenter_config tier del [queue_name] [agent_name] | \n\tcallcenter_config tier reload [queue_name] [agent_name] | \n\tcallcenter_config tier list | \n\tcallcenter_config queue load [queue_name] | \n\tcallcenter_config queue unload [queue_name] | \n\tcallcenter_config queue reload [queue_name] | \n\tcallcenter_config queue list | \n\tcallcenter_config queue list agents [queue_name] [status] | \n\tcallcenter_config queue list members [queue_name] | \n\tcallcenter_config queue list tiers [queue_name] | \n\tcallcenter_config queue count | \n\tcallcenter_config queue count agents [queue_name] [status] | \n\tcallcenter_config queue count members [queue_name] | \n\tcallcenter_config queue count tiers [queue_name]	sigil
api	acl	Compare an ip to an acl list	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<ip> <list_name>	sigil
api	alias	Alias	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[add|stickyadd] <alias> <command> | del [<alias>|*]	sigil
api	coalesce	Return first nonempty parameter	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[^^<delim>]<value1>,<value2>,...	sigil
api	banner	Return the system banner	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
api	bgapi	Execute an api command in a thread	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<command>[ <arg>]	sigil
api	bg_system	Execute a system command in the background	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<command>	sigil
api	break	uuid_break	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> [all]	sigil
api	complete	Complete	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	add <word>|del [<word>|*]	sigil
api	cond	Evaluate a conditional	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<expr> ? <true val> : <false val>	sigil
api	console_complete		mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<line>	sigil
api	console_complete_xml		mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<line>	sigil
api	create_uuid	Create a uuid	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <other_uuid>	sigil
api	db_cache	Manage db cache	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	status	sigil
api	domain_exists	Check if a domain exists	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<domain>	sigil
api	echo	Echo	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<data>	sigil
api	escape	Escape a string	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<data>	sigil
api	eval	eval (noop)	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[uuid:<uuid> ]<expression>	sigil
api	expand	Execute an api with variable expansion	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[uuid:<uuid> ]<cmd> <args>	sigil
api	find_user_xml	Find a user	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<key> <user> <domain>	sigil
api	fsctl	FS control messages	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[recover|send_sighup|hupall|pause [inbound|outbound]|resume [inbound|outbound]|shutdown [cancel|elegant|asap|now|restart]|sps|sps_peak_reset|sync_clock|sync_clock_when_idle|reclaim_mem|max_sessions|min_dtmf_duration [num]|max_dtmf_duration [num]|default_dtmf_duration [num]|min_idle_cpu|loglevel [level]|debug_level [level]]	sigil
api	...	Shutdown	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
api	shutdown	Shutdown	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
api	version	Version	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[short]	sigil
api	global_getvar	Get global var	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<var>	sigil
api	global_setvar	Set global var	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<var>=<value> [=<value2>]	sigil
api	group_call	Generate a dial string to call a group	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<group>[@<domain>]	sigil
api	help	Show help for all the api commands	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
api	host_lookup	Lookup host	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<hostname>	sigil
api	hostname	Return the system hostname	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
api	interface_ip	Return the primary IP of an interface	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[auto|ipv4|ipv6] <ifname>	sigil
api	switchname	Return the switch name	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
api	gethost	gethostbyname	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
api	hupall	hupall	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<cause> [<var> <value>]	sigil
api	in_group	Determine if a user is in a group	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<user>[@<domain>] <group_name>	sigil
api	is_lan_addr	See if an ip is a lan addr	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<ip>	sigil
api	limit_usage	Get the usage count of a limited resource	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<backend> <realm> <id>	sigil
api	limit_hash_usage	Deprecated: gets the usage count of a limited resource	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<realm> <id>	sigil
api	limit_status	Get the status of a limit backend	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<backend>	sigil
api	limit_reset	Reset the counters of a limit backend	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<backend>	sigil
api	limit_interval_reset	Reset the interval counter for a limited resource	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<backend> <realm> <resource>	sigil
api	list_users	List Users configured in Directory	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[group <group>] [domain <domain>] [user <user>] [context <context>]	sigil
api	load	Load Module	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<mod_name>	sigil
api	log	Log	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<level> <message>	sigil
api	md5	Return md5 hash	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<data>	sigil
api	module_exists	Check if module exists	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<module>	sigil
api	msleep	Sleep N milliseconds	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<milliseconds>	sigil
api	nat_map	Manage NAT	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[status|republish|reinit] | [add|del] <port> [tcp|udp] [static]	sigil
api	originate	Originate a call	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<call url> <exten>|&<application_name>(<app_args>) [<dialplan>] [<context>] [<cid_name>] [<cid_num>] [<timeout_sec>]	sigil
api	pause	Pause media on a channel	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <on|off>	sigil
api	quote_shell_arg	Quote/escape a string for use on shell command line	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<data>	sigil
api	regex	Evaluate a regex	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<data>|<pattern>[|<subst string>][n|b]	sigil
api	reloadacl	Reload XML	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
api	reload	Reload module	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[-f] <mod_name>	sigil
api	reloadxml	Reload XML	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
api	replace	Replace a string	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<data>|<string1>|<string2>	sigil
api	say_string		mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<module_name>[.<ext>] <lang>[.<ext>] <say_type> <say_method> [<say_gender>] <text>	sigil
api	sched_api	Schedule an api command	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[+@]<time> <group_name> <command_string>[&]	sigil
api	sched_broadcast	Schedule a broadcast event to a running call	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[[+]<time>|@time] <uuid> <path> [aleg|bleg|both]	sigil
api	sched_del	Delete a scheduled task	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<task_id>|<group_id>	sigil
api	sched_hangup	Schedule a running call to hangup	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[+]<time> <uuid> [<cause>]	sigil
api	sched_transfer	Schedule a transfer for a running call	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[+]<time> <uuid> <extension> [<dialplan>] [<context>]	sigil
api	show	Show various reports	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	codec|endpoint|application|api|dialplan|file|timer|calls [count]|channels [count|like <match string>]|calls|detailed_calls|bridged_calls|detailed_bridged_calls|aliases|complete|chat|management|modules|nat_map|say|interfaces|interface_types|tasks|limits|status	sigil
api	sql_escape	Escape a string to prevent sql injection	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<string>	sigil
api	status	Show current status	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
api	strftime_tz	Display formatted time of timezone	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<timezone_name> [<epoch>|][format string]	sigil
api	stun	Execute STUN lookup	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<stun_server>[:port] [<source_ip>[:<source_port]]	sigil
api	system	Execute a system command	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<command>	sigil
api	time_test	Show time jitter	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<mss> [count]	sigil
api	timer_test	Exercise FS timer	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<10|20|40|60|120> [<1..200>] [<timer_name>]	sigil
api	tone_detect	Start tone detection on a channel	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <key> <tone_spec> [<flags> <timeout> <app> <args> <hits>]	sigil
api	unload	Unload module	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[-f] <mod_name>	sigil
api	unsched_api	Unschedule an api command	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<task_id>	sigil
api	reg_url		mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<user>@<realm>	sigil
api	url_decode	Url decode a string	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<string>	sigil
api	url_encode	Url encode a string	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<string>	sigil
api	user_data	Find user data	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<user>@<domain> [var|param|attr] <name>	sigil
api	uuid_early_ok	stop ignoring early media	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	user_exists	Find a user	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<key> <user> <domain>	sigil
api	uuid_answer	answer	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_audio	uuid_audio	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> [start [read|write] [mute|level <level>]|stop]	sigil
api	uuid_break	Break out of media sent to channel	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> [all]	sigil
api	uuid_bridge	Bridge call legs	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
api	uuid_broadcast	Execute dialplan application	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <path> [aleg|bleg|holdb|both]	sigil
api	uuid_buglist	List media bugs on a session	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_chat	Send a chat message	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <text>	sigil
api	uuid_debug_media	Debug media	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <read|write|both|vread|vwrite|vboth|all> <on|off>	sigil
api	uuid_deflect	Send a deflect	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <uri>	sigil
api	uuid_displace	Displace audio	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> [start|stop] <path> [<limit>] [mux]	sigil
api	uuid_display	Update phone display	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <display>	sigil
api	uuid_dump	Dump session vars	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> [format]	sigil
api	uuid_exists	Check if a uuid exists	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_fileman	Manage session audio	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <cmd>:<val>	sigil
api	uuid_flush_dtmf	Flush dtmf on a given uuid	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_getvar	Get a variable from a channel	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <var>	sigil
api	uuid_hold	Place call on hold	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[off|toggle] <uuid> [<display>]	sigil
api	uuid_kill	Kill channel	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> [cause]	sigil
api	uuid_send_message	Send MESSAGE to the endpoint	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <message>	sigil
api	uuid_send_info	Send info to the endpoint	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_set_media_stats	Set media stats	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_video_refresh	Send video refresh.	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_outgoing_answer	Answer outgoing channel	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_limit	Increase limit resource	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <backend> <realm> <resource> [<max>[/interval]] [number [dialplan [context]]]	sigil
api	uuid_limit_release	Release limit resource	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <backend> [realm] [resource]	sigil
api	uuid_limit_release	Release limit resource	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <backend> [realm] [resource]	sigil
api	uuid_loglevel	Set loglevel on session	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <level>	sigil
api	uuid_media	Reinvite FS in or out of media path	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[off] <uuid>	sigil
api	uuid_media_reneg	Media negotiation	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>[ <codec_string>]	sigil
api	uuid_park	Park channel	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_pause	Pause media on a channel	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <on|off>	sigil
api	uuid_phone_event	Send an event to the phone	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_ring_ready	Sending ringing to a channel	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> [queued]	sigil
api	uuid_pre_answer	pre_answer	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_preprocess	Pre-process Channel	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<>	sigil
api	uuid_record	Record session audio	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> [start|stop|mask|unmask] <path> [<limit>]	sigil
api	uuid_recovery_refresh	Send a recovery_refresh	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <uri>	sigil
api	uuid_recv_dtmf	Receive dtmf digits	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <dtmf_data>	sigil
api	uuid_send_dtmf	Send dtmf digits	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <dtmf_data>	sigil
api	uuid_session_heartbeat	uuid_session_heartbeat	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> [sched] [0|<seconds>]	sigil
api	uuid_setvar_multi	Set multiple variables	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <var>=<value>;<var>=<value>...	sigil
api	uuid_setvar	Set a variable	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <var> [value]	sigil
api	uuid_transfer	Transfer a session	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> [-bleg|-both] <dest-exten> [<dialplan>] [<context>]	sigil
api	uuid_dual_transfer	Transfer a session and its partner	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> <A-dest-exten>[/<A-dialplan>][/<A-context>] <B-dest-exten>[/<B-dialplan>][/<B-context>]	sigil
api	uuid_simplify	Try to cut out of a call path / attended xfer	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	uuid_jitterbuffer	uuid_jitterbuffer	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid> [0|<min_msec>[:<max_msec>]]	sigil
api	uuid_zombie_exec	Set zombie_exec flag on the specified uuid	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<uuid>	sigil
api	xml_flush_cache	Clear xml cache	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<id> <key> <val>	sigil
api	xml_locate	Find some xml	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	[root | <section> <tag> <tag_attr_name> <tag_attr_val>]	sigil
api	xml_wrap	Wrap another api command in xml	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<command> <args>	sigil
api	file_exists	Check if a file exists on server	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	<file>	sigil
api	json	JSON API	mod_commands	/usr/local/freeswitch/mod/mod_commands.so	JSON	sigil
json_api	mediaStats	JSON Media Stats	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
json_api	status	JSON status API	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
json_api	fsapi	JSON FSAPI Gateway	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
json_api	execute	JSON session execute application	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
json_api	channelData	JSON channel data application	mod_commands	/usr/local/freeswitch/mod/mod_commands.so		sigil
application	db	Insert to the db	mod_db	/usr/local/freeswitch/mod/mod_db.so	[insert|delete]/<realm>/<key>/<val>	sigil
application	group	Manage a group	mod_db	/usr/local/freeswitch/mod/mod_db.so	[insert|delete]:<group name>:<val>	sigil
api	db	db get/set	mod_db	/usr/local/freeswitch/mod/mod_db.so	[insert|delete|select|exists|count|list]/<realm>/<key>/<value>	sigil
api	group	group [insert|delete|call]	mod_db	/usr/local/freeswitch/mod/mod_db.so	[insert|delete|call]:<group name>:<url>	sigil
limit	db		mod_db	/usr/local/freeswitch/mod/mod_db.so		sigil
api	expr	Eval an expression	mod_expr	/usr/local/freeswitch/mod/mod_expr.so	<expr>	sigil
application	fifo	Park with FIFO	mod_fifo	/usr/local/freeswitch/mod/mod_fifo.so	<fifo name>[!<importance_number>] [in [<announce file>|undef] [<music file>|undef] | out [wait|nowait] [<announce file>|undef] [<music file>|undef]]	sigil
application	fifo_track_call	Count a call as a fifo call in the manual_calls queue	mod_fifo	/usr/local/freeswitch/mod/mod_fifo.so	<fifo_outbound_uuid>	sigil
api	fifo	Return data about a fifo	mod_fifo	/usr/local/freeswitch/mod/mod_fifo.so	list|list_verbose|count|debug|status|has_outbound|importance [<fifo name>]|reparse [del_all]	sigil
api	fifo_member	Add members to a fifo	mod_fifo	/usr/local/freeswitch/mod/mod_fifo.so	[add <fifo_name> <originate_string> [<simo_count>] [<timeout>] [<lag>] [<expires>] [<taking_calls>] | del <fifo_name> <originate_string>]	sigil
api	fifo_add_outbound	Add outbound members to a fifo	mod_fifo	/usr/local/freeswitch/mod/mod_fifo.so	<node> <url> [<priority>]	sigil
api	fifo_check_bridge	check if uuid is in a bridge	mod_fifo	/usr/local/freeswitch/mod/mod_fifo.so	<uuid>|<outbound_id>	sigil
codec	H.264 Video (passthru)		mod_h26x	/usr/local/freeswitch/mod/mod_h26x.so		sigil
codec	H.263 Video (passthru)		mod_h26x	/usr/local/freeswitch/mod/mod_h26x.so		sigil
codec	H.263+ Video (passthru)		mod_h26x	/usr/local/freeswitch/mod/mod_h26x.so		sigil
codec	H.263++ Video (passthru)		mod_h26x	/usr/local/freeswitch/mod/mod_h26x.so		sigil
codec	H.261 Video (passthru)		mod_h26x	/usr/local/freeswitch/mod/mod_h26x.so		sigil
codec	BroadVoice16 (BV16)		mod_bv	/usr/local/freeswitch/mod/mod_bv.so		sigil
codec	BroadVoice32 (BV32)		mod_bv	/usr/local/freeswitch/mod/mod_bv.so		sigil
codec	AMR		mod_amr	/usr/local/freeswitch/mod/mod_amr.so		sigil
codec	G.729		mod_g729	/usr/local/freeswitch/mod/mod_g729.so		sigil
codec	G.723.1 6.3k		mod_g723_1	/usr/local/freeswitch/mod/mod_g723_1.so		sigil
dialplan	XML		mod_dialplan_xml	/usr/local/freeswitch/mod/mod_dialplan_xml.so		sigil
endpoint	loopback		mod_loopback	/usr/local/freeswitch/mod/mod_loopback.so		sigil
application	unloop	Tell loopback to unfold	mod_loopback	/usr/local/freeswitch/mod/mod_loopback.so		sigil
application	socket	Connect to a socket	mod_event_socket	/usr/local/freeswitch/mod/mod_event_socket.so	<ip>[:<port>]	sigil
api	event_sink	event_sink	mod_event_socket	/usr/local/freeswitch/mod/mod_event_socket.so	<web data>	sigil
file	aiff		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	au		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	avr		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	caf		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	htk		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	iff		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	mat		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	mpc		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	paf		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	pvf		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	raw		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	rf64		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	sd2		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	sds		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	sf		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	voc		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	w64		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	wav		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	wve		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	xi		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	r8		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	r16		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	r24		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	r32		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	gsm		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	ul		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	ulaw		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	al		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	alaw		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	adpcm		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	vox		mod_sndfile	/usr/local/freeswitch/mod/mod_sndfile.so		sigil
file	AAL2-G726-32		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	DVI4		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	PCMU		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	G722		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	AAL2-G726-24		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	SPEEX		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	AAL2-G726-40		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	L16		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	BV32		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	G726-16		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	PCMA		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	PROXY		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	G726-24		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	G729		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	AAL2-G726-16		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	G726-32		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	AMR		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	GSM		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	G726-40		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	BV16		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	G723		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	H263-1998		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	H263		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	LPC		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	H261		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	H264		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	H263-2000		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
file	PROXY-VID		mod_native_file	/usr/local/freeswitch/mod/mod_native_file.so		sigil
say	en		mod_say_en	/usr/local/freeswitch/mod/mod_say_en.so		sigil
say	es		mod_say_es	/usr/local/freeswitch/mod/mod_say_es.so		sigil
asr	pocketsphinx		mod_pocketsphinx	/usr/local/freeswitch/mod/mod_pocketsphinx.so		sigil
file	tone_stream		mod_tone_stream	/usr/local/freeswitch/mod/mod_tone_stream.so		sigil
file	silence_stream		mod_tone_stream	/usr/local/freeswitch/mod/mod_tone_stream.so		sigil
api	hup_local_stream	Skip to next file in local_stream	mod_local_stream	/usr/local/freeswitch/mod/mod_local_stream.so	<local_stream_name>	sigil
api	reload_local_stream	Reloads a local_stream	mod_local_stream	/usr/local/freeswitch/mod/mod_local_stream.so	<local_stream_name>	sigil
api	stop_local_stream	Stops and unloads a local_stream	mod_local_stream	/usr/local/freeswitch/mod/mod_local_stream.so	<local_stream_name>	sigil
api	start_local_stream	Starts a new local_stream	mod_local_stream	/usr/local/freeswitch/mod/mod_local_stream.so	<local_stream_name>	sigil
api	show_local_stream	Shows a local stream	mod_local_stream	/usr/local/freeswitch/mod/mod_local_stream.so	[local_stream_name [xml]]	sigil
file	local_stream		mod_local_stream	/usr/local/freeswitch/mod/mod_local_stream.so		sigil
generic	mod_xml_cdr		mod_xml_cdr	/usr/local/freeswitch/mod/mod_xml_cdr.so		sigil
\.


--
-- Data for Name: nat; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY nat (sticky, port, proto, hostname) FROM stdin;
\.


--
-- Data for Name: recovery; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY recovery (runtime_uuid, technology, profile_name, hostname, uuid, metadata) FROM stdin;
\.


--
-- Data for Name: registrations; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY registrations (reg_user, realm, token, url, expires, network_ip, network_port, network_proto, hostname, metadata) FROM stdin;
118	sip.c-call.eu	NGRmMzc3ZTgwZjBiMDgxOTg2ZWMyMmQ4N2RlODYwMmY.	sofia/internal/sip:118@83.10.238.161:6302;transport=UDP;rinstance=5d92de42e4fe1c2e	1409297729	83.4.58.171	6319	udp	sigil	\N
\.


--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY tasks (task_id, task_desc, task_group, task_sql_manager, hostname) FROM stdin;
4	limit_hash_cleanup	mod_hash	0	sigil
\.


--
-- Name: alias1; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX alias1 ON aliases USING btree (alias);


--
-- Name: calls1; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX calls1 ON calls USING btree (hostname);


--
-- Name: callsidx1; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX callsidx1 ON calls USING btree (hostname);


--
-- Name: channels1; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX channels1 ON channels USING btree (hostname);


--
-- Name: chidx1; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX chidx1 ON channels USING btree (hostname);


--
-- Name: complete1; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX complete1 ON complete USING btree (a1, hostname);


--
-- Name: complete10; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX complete10 ON complete USING btree (a10, hostname);


--
-- Name: complete11; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX complete11 ON complete USING btree (a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, hostname);


--
-- Name: complete2; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX complete2 ON complete USING btree (a2, hostname);


--
-- Name: complete3; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX complete3 ON complete USING btree (a3, hostname);


--
-- Name: complete4; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX complete4 ON complete USING btree (a4, hostname);


--
-- Name: complete5; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX complete5 ON complete USING btree (a5, hostname);


--
-- Name: complete6; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX complete6 ON complete USING btree (a6, hostname);


--
-- Name: complete7; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX complete7 ON complete USING btree (a7, hostname);


--
-- Name: complete8; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX complete8 ON complete USING btree (a8, hostname);


--
-- Name: complete9; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX complete9 ON complete USING btree (a9, hostname);


--
-- Name: eeuuindex; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX eeuuindex ON calls USING btree (callee_uuid);


--
-- Name: eeuuindex2; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX eeuuindex2 ON calls USING btree (call_uuid);


--
-- Name: eruuindex; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX eruuindex ON calls USING btree (caller_uuid, hostname);


--
-- Name: nat_map_port_proto; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX nat_map_port_proto ON nat USING btree (port, proto, hostname);


--
-- Name: recovery1; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX recovery1 ON recovery USING btree (technology);


--
-- Name: recovery2; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX recovery2 ON recovery USING btree (profile_name);


--
-- Name: recovery3; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX recovery3 ON recovery USING btree (uuid);


--
-- Name: regindex1; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX regindex1 ON registrations USING btree (reg_user, realm, hostname);


--
-- Name: tasks1; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX tasks1 ON tasks USING btree (hostname, task_id);


--
-- Name: uuindex; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX uuindex ON channels USING btree (uuid, hostname);


--
-- Name: uuindex2; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX uuindex2 ON channels USING btree (call_uuid);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

