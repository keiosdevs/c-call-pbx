--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: v_apps; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_apps (
    app_uuid uuid NOT NULL,
    app_category text,
    app_version text,
    app_enabled text
);


ALTER TABLE public.v_apps OWNER TO fusionpbx;

--
-- Name: v_call_block; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_call_block (
    domain_uuid uuid,
    call_block_uuid uuid NOT NULL,
    call_block_name text,
    call_block_number text,
    call_block_count numeric,
    call_block_action text,
    date_added text,
    call_block_enabled text
);


ALTER TABLE public.v_call_block OWNER TO fusionpbx;

--
-- Name: v_call_broadcasts; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_call_broadcasts (
    call_broadcast_uuid uuid NOT NULL,
    domain_uuid uuid,
    broadcast_name text,
    broadcast_description text,
    broadcast_timeout numeric,
    broadcast_concurrent_limit numeric,
    recording_uuid uuid,
    broadcast_caller_id_name text,
    broadcast_caller_id_number text,
    broadcast_destination_type text,
    broadcast_phone_numbers text,
    broadcast_avmd text,
    broadcast_destination_data text
);


ALTER TABLE public.v_call_broadcasts OWNER TO fusionpbx;

--
-- Name: v_call_center_agents; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_call_center_agents (
    call_center_agent_uuid uuid NOT NULL,
    domain_uuid uuid,
    agent_name text,
    agent_type text,
    agent_call_timeout numeric,
    agent_contact text,
    agent_status text,
    agent_logout text,
    agent_max_no_answer numeric,
    agent_wrap_up_time numeric,
    agent_reject_delay_time numeric,
    agent_busy_delay_time numeric,
    agent_no_answer_delay_time text
);


ALTER TABLE public.v_call_center_agents OWNER TO fusionpbx;

--
-- Name: v_call_center_logs; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_call_center_logs (
    cc_uuid uuid NOT NULL,
    domain_uuid uuid,
    cc_queue text,
    cc_action text,
    cc_count numeric,
    cc_agent text,
    cc_agent_system text,
    cc_agent_status text,
    cc_agent_state text,
    cc_agent_uuid uuid,
    cc_selection text,
    cc_cause text,
    cc_wait_time text,
    cc_talk_time text,
    cc_total_time text,
    cc_epoch numeric,
    cc_date timestamp without time zone,
    cc_agent_type text,
    cc_member_uuid text,
    cc_member_session_uuid text,
    cc_member_cid_name text,
    cc_member_cid_number text,
    cc_agent_called_time numeric,
    cc_agent_answered_time numeric,
    cc_member_joined_time numeric,
    cc_member_leaving_time numeric,
    cc_bridge_terminated_time numeric,
    cc_hangup_cause text
);


ALTER TABLE public.v_call_center_logs OWNER TO fusionpbx;

--
-- Name: v_call_center_queues; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_call_center_queues (
    call_center_queue_uuid uuid NOT NULL,
    domain_uuid uuid,
    dialplan_uuid uuid,
    queue_name text,
    queue_extension text,
    queue_strategy text,
    queue_moh_sound text,
    queue_record_template text,
    queue_time_base_score text,
    queue_max_wait_time numeric,
    queue_max_wait_time_with_no_agent numeric,
    queue_max_wait_time_with_no_agent_time_reached numeric,
    queue_tier_rules_apply text,
    queue_tier_rule_wait_second numeric,
    queue_tier_rule_no_agent_no_wait text,
    queue_timeout_action text,
    queue_discard_abandoned_after numeric,
    queue_abandoned_resume_allowed text,
    queue_tier_rule_wait_multiply_level text,
    queue_cid_prefix text,
    queue_announce_sound text,
    queue_announce_frequency numeric,
    queue_description text
);


ALTER TABLE public.v_call_center_queues OWNER TO fusionpbx;

--
-- Name: v_call_center_tiers; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_call_center_tiers (
    call_center_tier_uuid uuid NOT NULL,
    domain_uuid uuid,
    agent_name text,
    queue_name text,
    tier_level numeric,
    tier_position numeric
);


ALTER TABLE public.v_call_center_tiers OWNER TO fusionpbx;

--
-- Name: v_call_flows; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_call_flows (
    domain_uuid uuid,
    call_flow_uuid uuid NOT NULL,
    dialplan_uuid uuid,
    call_flow_name text,
    call_flow_extension text,
    call_flow_feature_code text,
    call_flow_context text,
    call_flow_status text,
    call_flow_pin_number text,
    call_flow_label text,
    call_flow_app text,
    call_flow_data text,
    call_flow_anti_label text,
    call_flow_anti_app text,
    call_flow_anti_data text,
    call_flow_description text
);


ALTER TABLE public.v_call_flows OWNER TO fusionpbx;

--
-- Name: v_clips; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_clips (
    clip_uuid uuid NOT NULL,
    clip_name text,
    clip_folder text,
    clip_text_start text,
    clip_text_end text,
    clip_order text,
    clip_desc text
);


ALTER TABLE public.v_clips OWNER TO fusionpbx;

--
-- Name: v_conference_centers; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_conference_centers (
    domain_uuid uuid,
    conference_center_uuid uuid NOT NULL,
    dialplan_uuid uuid,
    conference_center_name text,
    conference_center_extension text,
    conference_center_pin_length numeric,
    conference_center_greeting text,
    conference_center_description text,
    conference_center_enabled text
);


ALTER TABLE public.v_conference_centers OWNER TO fusionpbx;

--
-- Name: v_conference_rooms; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_conference_rooms (
    domain_uuid uuid,
    conference_room_uuid uuid NOT NULL,
    conference_center_uuid uuid,
    meeting_uuid uuid,
    profile text,
    record text,
    max_members numeric,
    wait_mod text,
    announce text,
    sounds text,
    mute text,
    created text,
    created_by text,
    enabled text,
    description text
);


ALTER TABLE public.v_conference_rooms OWNER TO fusionpbx;

--
-- Name: v_conference_session_details; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_conference_session_details (
    domain_uuid uuid,
    conference_session_detail_uuid uuid NOT NULL,
    conference_session_uuid uuid,
    meeting_uuid uuid,
    username text,
    caller_id_name text,
    caller_id_number text,
    uuid uuid,
    moderator text,
    network_addr text,
    start_epoch numeric,
    end_epoch numeric
);


ALTER TABLE public.v_conference_session_details OWNER TO fusionpbx;

--
-- Name: v_conference_sessions; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_conference_sessions (
    domain_uuid uuid,
    conference_session_uuid uuid NOT NULL,
    meeting_uuid uuid,
    profile text,
    recording text,
    start_epoch numeric,
    end_epoch numeric
);


ALTER TABLE public.v_conference_sessions OWNER TO fusionpbx;

--
-- Name: v_conference_users; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_conference_users (
    conference_user_uuid uuid NOT NULL,
    domain_uuid uuid,
    conference_uuid uuid,
    user_uuid uuid
);


ALTER TABLE public.v_conference_users OWNER TO fusionpbx;

--
-- Name: v_conferences; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_conferences (
    domain_uuid uuid,
    conference_uuid uuid NOT NULL,
    dialplan_uuid uuid,
    conference_name text,
    conference_extension text,
    conference_pin_number text,
    conference_profile text,
    conference_flags text,
    conference_order numeric,
    conference_description text,
    conference_enabled text
);


ALTER TABLE public.v_conferences OWNER TO fusionpbx;

--
-- Name: v_contact_addresses; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_contact_addresses (
    contact_address_uuid uuid NOT NULL,
    domain_uuid uuid,
    contact_uuid uuid,
    address_type text,
    address_street text,
    address_extended text,
    address_locality text,
    address_region text,
    address_postal_code text,
    address_country text,
    address_latitude text,
    address_longitude text,
    address_description text
);


ALTER TABLE public.v_contact_addresses OWNER TO fusionpbx;

--
-- Name: v_contact_groups; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_contact_groups (
    contact_groups_uuid uuid NOT NULL,
    domain_uuid uuid,
    contact_uuid uuid,
    group_name text
);


ALTER TABLE public.v_contact_groups OWNER TO fusionpbx;

--
-- Name: v_contact_notes; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_contact_notes (
    contact_note_uuid uuid NOT NULL,
    domain_uuid uuid,
    contact_uuid uuid,
    contact_note text,
    last_mod_date text,
    last_mod_user text
);


ALTER TABLE public.v_contact_notes OWNER TO fusionpbx;

--
-- Name: v_contact_phones; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_contact_phones (
    contact_phone_uuid uuid NOT NULL,
    domain_uuid uuid,
    contact_uuid uuid,
    phone_type text,
    phone_number text,
    phone_extension text,
    phone_description text
);


ALTER TABLE public.v_contact_phones OWNER TO fusionpbx;

--
-- Name: v_contacts; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_contacts (
    contact_uuid uuid NOT NULL,
    domain_uuid uuid,
    contact_parent_uuid uuid,
    contact_type text,
    contact_organization text,
    contact_name_given text,
    contact_name_family text,
    contact_nickname text,
    contact_title text,
    contact_role text,
    contact_category text,
    contact_email text,
    contact_url text,
    contact_time_zone text,
    contact_note text,
    created text,
    created_by text
);


ALTER TABLE public.v_contacts OWNER TO fusionpbx;

--
-- Name: v_databases; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_databases (
    database_uuid uuid NOT NULL,
    database_driver text,
    database_type text,
    database_host text,
    database_port text,
    database_name text,
    database_username text,
    database_password text,
    database_path text,
    database_description text
);


ALTER TABLE public.v_databases OWNER TO fusionpbx;

--
-- Name: v_default_settings; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_default_settings (
    default_setting_uuid uuid NOT NULL,
    default_setting_category text,
    default_setting_subcategory text,
    default_setting_name text,
    default_setting_value text,
    default_setting_order numeric,
    default_setting_enabled text,
    default_setting_description text
);


ALTER TABLE public.v_default_settings OWNER TO fusionpbx;

--
-- Name: v_destinations; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_destinations (
    domain_uuid uuid,
    destination_uuid uuid NOT NULL,
    dialplan_uuid uuid,
    fax_uuid uuid,
    destination_type text,
    destination_number text,
    destination_caller_id_name text,
    destination_caller_id_number text,
    destination_cid_name_prefix text,
    destination_context text,
    destination_app text,
    destination_data text,
    destination_enabled text,
    destination_description text,
    destination_accountcode text
);


ALTER TABLE public.v_destinations OWNER TO fusionpbx;

--
-- Name: v_device_keys; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_device_keys (
    domain_uuid uuid,
    device_key_uuid uuid NOT NULL,
    device_uuid uuid,
    device_key_id numeric,
    device_key_category text,
    device_key_type text,
    device_key_line text,
    device_key_value text,
    device_key_extension text,
    device_key_label text
);


ALTER TABLE public.v_device_keys OWNER TO fusionpbx;

--
-- Name: v_device_lines; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_device_lines (
    domain_uuid uuid,
    device_line_uuid uuid NOT NULL,
    device_uuid uuid,
    line_number text,
    server_address text,
    outbound_proxy text,
    display_name text,
    user_id text,
    auth_id text,
    password text,
    sip_port numeric,
    sip_transport text,
    register_expires numeric
);


ALTER TABLE public.v_device_lines OWNER TO fusionpbx;

--
-- Name: v_device_settings; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_device_settings (
    device_setting_uuid uuid NOT NULL,
    device_uuid uuid,
    domain_uuid uuid,
    device_setting_category text,
    device_setting_subcategory text,
    device_setting_name text,
    device_setting_value text,
    device_setting_enabled text,
    device_setting_description text
);


ALTER TABLE public.v_device_settings OWNER TO fusionpbx;

--
-- Name: v_devices; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_devices (
    device_uuid uuid NOT NULL,
    domain_uuid uuid,
    device_mac_address text,
    device_label text,
    device_vendor text,
    device_model text,
    device_firmware_version text,
    device_provision_enable text,
    device_template text,
    device_description text
);


ALTER TABLE public.v_devices OWNER TO fusionpbx;

--
-- Name: v_dialplan_details; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_dialplan_details (
    domain_uuid uuid,
    dialplan_uuid uuid,
    dialplan_detail_uuid uuid NOT NULL,
    dialplan_detail_tag text,
    dialplan_detail_type text,
    dialplan_detail_data text,
    dialplan_detail_break text,
    dialplan_detail_inline text,
    dialplan_detail_group numeric,
    dialplan_detail_order numeric
);


ALTER TABLE public.v_dialplan_details OWNER TO fusionpbx;

--
-- Name: v_dialplans; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_dialplans (
    domain_uuid uuid,
    dialplan_uuid uuid NOT NULL,
    app_uuid uuid,
    dialplan_context text,
    dialplan_name text,
    dialplan_number text,
    dialplan_continue text,
    dialplan_order numeric,
    dialplan_enabled text,
    dialplan_description text
);


ALTER TABLE public.v_dialplans OWNER TO fusionpbx;

--
-- Name: v_domain_settings; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_domain_settings (
    domain_uuid uuid,
    domain_setting_uuid uuid NOT NULL,
    domain_setting_category text,
    domain_setting_subcategory text,
    domain_setting_name text,
    domain_setting_value text,
    domain_setting_order numeric,
    domain_setting_enabled text,
    domain_setting_description text
);


ALTER TABLE public.v_domain_settings OWNER TO fusionpbx;

--
-- Name: v_domains; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_domains (
    domain_uuid uuid NOT NULL,
    domain_name text,
    domain_description text
);


ALTER TABLE public.v_domains OWNER TO fusionpbx;

--
-- Name: v_emails; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_emails (
    email_uuid uuid NOT NULL,
    call_uuid uuid,
    domain_uuid uuid,
    sent_date timestamp without time zone,
    type text,
    status text,
    email text
);


ALTER TABLE public.v_emails OWNER TO fusionpbx;

--
-- Name: v_extension_users; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_extension_users (
    extension_user_uuid uuid NOT NULL,
    domain_uuid uuid,
    extension_uuid uuid,
    user_uuid uuid
);


ALTER TABLE public.v_extension_users OWNER TO fusionpbx;

--
-- Name: v_extensions; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_extensions (
    extension_uuid uuid NOT NULL,
    domain_uuid uuid,
    extension text,
    number_alias text,
    password text,
    accountcode text,
    effective_caller_id_name text,
    effective_caller_id_number text,
    outbound_caller_id_name text,
    outbound_caller_id_number text,
    emergency_caller_id_name text,
    emergency_caller_id_number text,
    directory_full_name text,
    directory_visible text,
    directory_exten_visible text,
    limit_max numeric,
    limit_destination text,
    user_context text,
    toll_allow text,
    call_timeout numeric,
    call_group text,
    user_record text,
    hold_music text,
    auth_acl text,
    cidr text,
    sip_force_contact text,
    nibble_account numeric,
    sip_force_expires numeric,
    mwi_account text,
    sip_bypass_media text,
    unique_id numeric,
    dial_string text,
    dial_user text,
    dial_domain text,
    do_not_disturb text,
    forward_all_destination text,
    forward_all_enabled text,
    forward_busy_destination text,
    forward_busy_enabled text,
    follow_me_uuid uuid,
    enabled text,
    description text
);


ALTER TABLE public.v_extensions OWNER TO fusionpbx;

--
-- Name: v_fax; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_fax (
    fax_uuid uuid NOT NULL,
    domain_uuid uuid,
    dialplan_uuid uuid,
    fax_extension text,
    fax_destination_number text,
    fax_name text,
    fax_email text,
    fax_pin_number text,
    fax_caller_id_name text,
    fax_caller_id_number text,
    fax_forward_number text,
    fax_description text
);


ALTER TABLE public.v_fax OWNER TO fusionpbx;

--
-- Name: v_fax_logs; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_fax_logs (
    domain_uuid uuid,
    domain_name text,
    fax_success text,
    fax_result_code numeric,
    fax_result_text text,
    fax_file text,
    uuid uuid,
    fax_ecm_used text,
    fax_local_station_id text,
    fax_document_transferred_pages numeric,
    fax_document_total_pages numeric,
    fax_image_resolution text,
    fax_image_size numeric,
    fax_bad_rows numeric,
    fax_transfer_rate numeric,
    fax_retry_attempts numeric,
    fax_retry_limit numeric,
    fax_retry_sleep numeric,
    fax_uri text
);


ALTER TABLE public.v_fax_logs OWNER TO fusionpbx;

--
-- Name: v_fax_users; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_fax_users (
    fax_user_uuid uuid NOT NULL,
    domain_uuid uuid,
    fax_uuid uuid,
    user_uuid uuid
);


ALTER TABLE public.v_fax_users OWNER TO fusionpbx;

--
-- Name: v_follow_me; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_follow_me (
    domain_uuid uuid,
    follow_me_uuid uuid NOT NULL,
    cid_name_prefix text,
    cid_number_prefix text,
    call_prompt text,
    dial_string text,
    follow_me_enabled text
);


ALTER TABLE public.v_follow_me OWNER TO fusionpbx;

--
-- Name: v_follow_me_destinations; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_follow_me_destinations (
    domain_uuid uuid,
    follow_me_uuid uuid,
    follow_me_destination_uuid uuid NOT NULL,
    follow_me_destination text,
    follow_me_delay text,
    follow_me_timeout text,
    follow_me_prompt text,
    follow_me_order numeric
);


ALTER TABLE public.v_follow_me_destinations OWNER TO fusionpbx;

--
-- Name: v_gateways; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_gateways (
    gateway_uuid uuid NOT NULL,
    domain_uuid uuid,
    gateway text,
    username text,
    password text,
    distinct_to text,
    auth_username text,
    realm text,
    from_user text,
    from_domain text,
    proxy text,
    register_proxy text,
    outbound_proxy text,
    expire_seconds numeric,
    register text,
    register_transport text,
    retry_seconds numeric,
    extension text,
    ping text,
    caller_id_in_from text,
    supress_cng text,
    sip_cid_type text,
    codec_prefs text,
    channels numeric,
    extension_in_contact text,
    context text,
    profile text,
    enabled text,
    description text
);


ALTER TABLE public.v_gateways OWNER TO fusionpbx;

--
-- Name: v_group_permissions; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_group_permissions (
    group_permission_uuid uuid NOT NULL,
    domain_uuid uuid,
    permission_name text,
    group_name text
);


ALTER TABLE public.v_group_permissions OWNER TO fusionpbx;

--
-- Name: v_group_users; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_group_users (
    group_user_uuid uuid NOT NULL,
    domain_uuid uuid,
    group_name text,
    user_uuid uuid
);


ALTER TABLE public.v_group_users OWNER TO fusionpbx;

--
-- Name: v_groups; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_groups (
    group_uuid uuid NOT NULL,
    domain_uuid uuid,
    group_name text,
    group_protected text,
    group_description text
);


ALTER TABLE public.v_groups OWNER TO fusionpbx;

--
-- Name: v_ivr_menu_options; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_ivr_menu_options (
    ivr_menu_option_uuid uuid NOT NULL,
    ivr_menu_uuid uuid,
    domain_uuid uuid,
    ivr_menu_option_digits text,
    ivr_menu_option_action text,
    ivr_menu_option_param text,
    ivr_menu_option_order numeric,
    ivr_menu_option_description text
);


ALTER TABLE public.v_ivr_menu_options OWNER TO fusionpbx;

--
-- Name: v_ivr_menus; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_ivr_menus (
    ivr_menu_uuid uuid NOT NULL,
    domain_uuid uuid,
    dialplan_uuid uuid,
    ivr_menu_name text,
    ivr_menu_extension text,
    ivr_menu_greet_long text,
    ivr_menu_greet_short text,
    ivr_menu_invalid_sound text,
    ivr_menu_exit_sound text,
    ivr_menu_confirm_macro text,
    ivr_menu_confirm_key text,
    ivr_menu_tts_engine text,
    ivr_menu_tts_voice text,
    ivr_menu_confirm_attempts numeric,
    ivr_menu_timeout numeric,
    ivr_menu_exit_app text,
    ivr_menu_exit_data text,
    ivr_menu_inter_digit_timeout numeric,
    ivr_menu_max_failures numeric,
    ivr_menu_max_timeouts numeric,
    ivr_menu_digit_len numeric,
    ivr_menu_direct_dial text,
    ivr_menu_ringback text,
    ivr_menu_cid_prefix text,
    ivr_menu_enabled text,
    ivr_menu_description text
);


ALTER TABLE public.v_ivr_menus OWNER TO fusionpbx;

--
-- Name: v_meeting_users; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_meeting_users (
    domain_uuid uuid,
    meeting_user_uuid uuid NOT NULL,
    meeting_uuid uuid,
    user_uuid uuid
);


ALTER TABLE public.v_meeting_users OWNER TO fusionpbx;

--
-- Name: v_meetings; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_meetings (
    domain_uuid uuid,
    meeting_uuid uuid,
    moderator_pin numeric,
    participant_pin numeric,
    enabled text,
    description text
);


ALTER TABLE public.v_meetings OWNER TO fusionpbx;

--
-- Name: v_menu_item_groups; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_menu_item_groups (
    menu_uuid uuid,
    menu_item_uuid uuid,
    group_name text
);


ALTER TABLE public.v_menu_item_groups OWNER TO fusionpbx;

--
-- Name: v_menu_items; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_menu_items (
    menu_item_uuid uuid,
    menu_uuid uuid,
    menu_item_parent_uuid uuid,
    menu_item_title text,
    menu_item_link text,
    menu_item_category text,
    menu_item_protected text,
    menu_item_order numeric,
    menu_item_description text,
    menu_item_add_user text,
    menu_item_add_date text,
    menu_item_mod_user text,
    menu_item_mod_date text
);


ALTER TABLE public.v_menu_items OWNER TO fusionpbx;

--
-- Name: v_menu_languages; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_menu_languages (
    menu_language_uuid uuid,
    menu_uuid uuid,
    menu_item_uuid uuid,
    menu_language text,
    menu_item_title text
);


ALTER TABLE public.v_menu_languages OWNER TO fusionpbx;

--
-- Name: v_menus; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_menus (
    menu_uuid uuid NOT NULL,
    menu_name text,
    menu_language text,
    menu_description text
);


ALTER TABLE public.v_menus OWNER TO fusionpbx;

--
-- Name: v_modules; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_modules (
    module_uuid uuid NOT NULL,
    module_label text,
    module_name text,
    module_category text,
    module_enabled text,
    module_default_enabled text,
    module_description text
);


ALTER TABLE public.v_modules OWNER TO fusionpbx;

--
-- Name: v_notifications; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_notifications (
    notification_uuid uuid NOT NULL,
    project_notifications text
);


ALTER TABLE public.v_notifications OWNER TO fusionpbx;

--
-- Name: v_recordings; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_recordings (
    recording_uuid uuid NOT NULL,
    domain_uuid uuid,
    recording_filename text,
    recording_name text,
    recording_description text
);


ALTER TABLE public.v_recordings OWNER TO fusionpbx;

--
-- Name: v_ring_group_destinations; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_ring_group_destinations (
    ring_group_destination_uuid uuid NOT NULL,
    domain_uuid uuid,
    ring_group_uuid uuid,
    destination_number text,
    destination_delay numeric,
    destination_timeout numeric,
    destination_prompt numeric
);


ALTER TABLE public.v_ring_group_destinations OWNER TO fusionpbx;

--
-- Name: v_ring_group_users; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_ring_group_users (
    ring_group_user_uuid uuid NOT NULL,
    domain_uuid uuid,
    ring_group_uuid uuid,
    user_uuid uuid
);


ALTER TABLE public.v_ring_group_users OWNER TO fusionpbx;

--
-- Name: v_ring_groups; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_ring_groups (
    domain_uuid uuid,
    ring_group_uuid uuid NOT NULL,
    ring_group_name text,
    ring_group_extension text,
    ring_group_context text,
    ring_group_forward_destination text,
    ring_group_forward_enabled text,
    ring_group_cid_name_prefix text,
    ring_group_strategy text,
    ring_group_timeout_app text,
    ring_group_timeout_data text,
    ring_group_ringback text,
    ring_group_skip_active text,
    ring_group_enabled text,
    ring_group_description text,
    dialplan_uuid uuid
);


ALTER TABLE public.v_ring_groups OWNER TO fusionpbx;

--
-- Name: v_rss; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_rss (
    rss_uuid uuid NOT NULL,
    domain_uuid uuid,
    rss_language text,
    rss_category text,
    rss_sub_category text,
    rss_title text,
    rss_link text,
    rss_description text,
    rss_img bytea,
    rss_optional_1 text,
    rss_optional_2 text,
    rss_optional_3 text,
    rss_optional_4 text,
    rss_optional_5 text,
    rss_add_date text,
    rss_add_user text,
    rss_del_date text,
    rss_del_user text,
    rss_order numeric,
    rss_content text,
    rss_group text
);


ALTER TABLE public.v_rss OWNER TO fusionpbx;

--
-- Name: v_rss_sub; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_rss_sub (
    rss_sub_uuid uuid NOT NULL,
    domain_uuid uuid,
    rss_uuid uuid,
    rss_sub_language text,
    rss_sub_title text,
    rss_sub_link text,
    rss_sub_description text,
    rss_sub_optional_1 text,
    rss_sub_optional_2 text,
    rss_sub_optional_3 text,
    rss_sub_optional_4 text,
    rss_sub_optional_5 text,
    rss_sub_add_date text,
    rss_sub_add_user text,
    rss_sub_del_user text,
    rss_sub_del_date text
);


ALTER TABLE public.v_rss_sub OWNER TO fusionpbx;

--
-- Name: v_rss_sub_category; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_rss_sub_category (
    rss_sub_category_uuid uuid NOT NULL,
    domain_uuid uuid,
    rss_sub_category_language text,
    rss_category text,
    rss_sub_category text,
    rss_sub_category_description text,
    rss_sub_add_user text,
    rss_sub_add_date text
);


ALTER TABLE public.v_rss_sub_category OWNER TO fusionpbx;

--
-- Name: v_schema_data; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_schema_data (
    schema_data_uuid uuid NOT NULL,
    domain_uuid uuid,
    schema_uuid uuid,
    data_row_uuid text,
    field_name text,
    data_field_value text,
    data_add_user text,
    data_add_date text,
    data_del_user text,
    data_del_date text,
    schema_parent_uuid uuid,
    data_parent_row_uuid text
);


ALTER TABLE public.v_schema_data OWNER TO fusionpbx;

--
-- Name: v_schema_fields; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_schema_fields (
    schema_field_uuid uuid NOT NULL,
    domain_uuid uuid,
    schema_uuid uuid,
    field_label text,
    field_name text,
    field_type text,
    field_list_hidden text,
    field_search_by text,
    field_column text,
    field_required text,
    field_order numeric,
    field_order_tab numeric,
    field_value text,
    field_description text
);


ALTER TABLE public.v_schema_fields OWNER TO fusionpbx;

--
-- Name: v_schema_name_values; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_schema_name_values (
    schema_name_value_uuid uuid NOT NULL,
    domain_uuid uuid,
    schema_uuid uuid,
    schema_field_uuid uuid,
    data_types_name text,
    data_types_value text
);


ALTER TABLE public.v_schema_name_values OWNER TO fusionpbx;

--
-- Name: v_schemas; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_schemas (
    schema_uuid uuid NOT NULL,
    domain_uuid uuid,
    schema_category text,
    schema_label text,
    schema_name text,
    schema_auth text,
    schema_captcha text,
    schema_parent_uuid uuid,
    schema_description text
);


ALTER TABLE public.v_schemas OWNER TO fusionpbx;

--
-- Name: v_services; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_services (
    service_uuid uuid NOT NULL,
    domain_uuid uuid,
    service_name text,
    service_type text,
    service_data text,
    service_cmd_start text,
    service_cmd_stop text,
    service_cmd_restart text,
    service_description text
);


ALTER TABLE public.v_services OWNER TO fusionpbx;

--
-- Name: v_settings; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_settings (
    numbering_plan text,
    event_socket_ip_address text,
    event_socket_port text,
    event_socket_password text,
    xml_rpc_http_port text,
    xml_rpc_auth_realm text,
    xml_rpc_auth_user text,
    xml_rpc_auth_pass text,
    admin_pin numeric,
    smtp_host text,
    smtp_secure text,
    smtp_auth text,
    smtp_username text,
    smtp_password text,
    smtp_from text,
    smtp_from_name text,
    mod_shout_decoder text,
    mod_shout_volume text
);


ALTER TABLE public.v_settings OWNER TO fusionpbx;

--
-- Name: v_sip_profile_settings; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_sip_profile_settings (
    sip_profile_setting_uuid uuid NOT NULL,
    sip_profile_uuid uuid,
    sip_profile_setting_name text,
    sip_profile_setting_value text,
    sip_profile_setting_enabled text,
    sip_profile_setting_description text
);


ALTER TABLE public.v_sip_profile_settings OWNER TO fusionpbx;

--
-- Name: v_sip_profiles; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_sip_profiles (
    sip_profile_uuid uuid NOT NULL,
    sip_profile_name text,
    sip_profile_hostname text,
    sip_profile_description text
);


ALTER TABLE public.v_sip_profiles OWNER TO fusionpbx;

--
-- Name: v_software; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_software (
    software_uuid uuid NOT NULL,
    software_name text,
    software_url text,
    software_version text
);


ALTER TABLE public.v_software OWNER TO fusionpbx;

--
-- Name: v_user_settings; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_user_settings (
    user_setting_uuid uuid NOT NULL,
    user_uuid uuid,
    domain_uuid uuid,
    user_setting_category text,
    user_setting_subcategory text,
    user_setting_name text,
    user_setting_value text,
    user_setting_order numeric,
    user_setting_enabled text,
    user_setting_description text
);


ALTER TABLE public.v_user_settings OWNER TO fusionpbx;

--
-- Name: v_users; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_users (
    user_uuid uuid NOT NULL,
    domain_uuid uuid,
    username text,
    password text,
    salt text,
    contact_uuid uuid,
    user_status text,
    api_key uuid,
    user_enabled text,
    add_user text,
    add_date text
);


ALTER TABLE public.v_users OWNER TO fusionpbx;

--
-- Name: v_vars; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_vars (
    var_uuid uuid NOT NULL,
    var_name text,
    var_value text,
    var_cat text,
    var_enabled text,
    var_order numeric,
    var_description text
);


ALTER TABLE public.v_vars OWNER TO fusionpbx;

--
-- Name: v_voicemail_destinations; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_voicemail_destinations (
    voicemail_destination_uuid uuid NOT NULL,
    voicemail_uuid uuid,
    voicemail_uuid_copy uuid
);


ALTER TABLE public.v_voicemail_destinations OWNER TO fusionpbx;

--
-- Name: v_voicemail_greetings; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_voicemail_greetings (
    voicemail_greeting_uuid uuid NOT NULL,
    domain_uuid uuid,
    voicemail_id text,
    greeting_name text,
    greeting_description text
);


ALTER TABLE public.v_voicemail_greetings OWNER TO fusionpbx;

--
-- Name: v_voicemail_messages; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_voicemail_messages (
    domain_uuid uuid,
    voicemail_message_uuid uuid NOT NULL,
    voicemail_uuid uuid,
    created_epoch numeric,
    read_epoch numeric,
    caller_id_name text,
    caller_id_number text,
    message_length numeric,
    message_status text,
    message_priority text
);


ALTER TABLE public.v_voicemail_messages OWNER TO fusionpbx;

--
-- Name: v_voicemails; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_voicemails (
    domain_uuid uuid,
    voicemail_uuid uuid NOT NULL,
    voicemail_id text,
    voicemail_password text,
    greeting_id numeric,
    voicemail_mail_to text,
    voicemail_attach_file text,
    voicemail_local_after_email text,
    voicemail_enabled text,
    voicemail_description text
);


ALTER TABLE public.v_voicemails OWNER TO fusionpbx;

--
-- Name: v_xml_cdr; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_xml_cdr (
    uuid uuid NOT NULL,
    domain_uuid uuid,
    extension_uuid uuid,
    domain_name text,
    accountcode text,
    direction text,
    default_language text,
    context text,
    xml text,
    json json,
    caller_id_name text,
    caller_id_number text,
    destination_number text,
    start_epoch numeric,
    start_stamp timestamp without time zone,
    answer_stamp timestamp without time zone,
    answer_epoch numeric,
    end_epoch numeric,
    end_stamp text,
    duration numeric,
    mduration numeric,
    billsec numeric,
    billmsec numeric,
    bridge_uuid text,
    read_codec text,
    read_rate text,
    write_codec text,
    write_rate text,
    remote_media_ip text,
    network_addr text,
    recording_file text,
    leg character(1),
    pdd_ms numeric,
    rtp_audio_in_mos numeric,
    last_app text,
    last_arg text,
    cc_side text,
    cc_member_uuid uuid,
    cc_queue_joined_epoch text,
    cc_queue text,
    cc_member_session_uuid uuid,
    cc_agent text,
    cc_agent_type text,
    waitsec numeric,
    conference_name text,
    conference_uuid uuid,
    conference_member_id text,
    digits_dialed text,
    pin_number text,
    hangup_cause text,
    hangup_cause_q850 numeric,
    sip_hangup_disposition text
);


ALTER TABLE public.v_xml_cdr OWNER TO fusionpbx;

--
-- Name: v_xmpp; Type: TABLE; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE TABLE v_xmpp (
    xmpp_profile_uuid uuid NOT NULL,
    domain_uuid uuid,
    profile_name text,
    username text,
    password text,
    dialplan text,
    context text,
    rtp_ip text,
    ext_rtp_ip text,
    auto_login text,
    sasl_type text,
    xmpp_server text,
    tls_enable text,
    use_rtp_timer text,
    default_exten text,
    vad text,
    avatar text,
    candidate_acl text,
    local_network_acl text,
    enabled text,
    description text
);


ALTER TABLE public.v_xmpp OWNER TO fusionpbx;

--
-- Data for Name: v_apps; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_apps (app_uuid, app_category, app_version, app_enabled) FROM stdin;
\.


--
-- Data for Name: v_call_block; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_call_block (domain_uuid, call_block_uuid, call_block_name, call_block_number, call_block_count, call_block_action, date_added, call_block_enabled) FROM stdin;
\.


--
-- Data for Name: v_call_broadcasts; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_call_broadcasts (call_broadcast_uuid, domain_uuid, broadcast_name, broadcast_description, broadcast_timeout, broadcast_concurrent_limit, recording_uuid, broadcast_caller_id_name, broadcast_caller_id_number, broadcast_destination_type, broadcast_phone_numbers, broadcast_avmd, broadcast_destination_data) FROM stdin;
\.


--
-- Data for Name: v_call_center_agents; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_call_center_agents (call_center_agent_uuid, domain_uuid, agent_name, agent_type, agent_call_timeout, agent_contact, agent_status, agent_logout, agent_max_no_answer, agent_wrap_up_time, agent_reject_delay_time, agent_busy_delay_time, agent_no_answer_delay_time) FROM stdin;
\.


--
-- Data for Name: v_call_center_logs; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_call_center_logs (cc_uuid, domain_uuid, cc_queue, cc_action, cc_count, cc_agent, cc_agent_system, cc_agent_status, cc_agent_state, cc_agent_uuid, cc_selection, cc_cause, cc_wait_time, cc_talk_time, cc_total_time, cc_epoch, cc_date, cc_agent_type, cc_member_uuid, cc_member_session_uuid, cc_member_cid_name, cc_member_cid_number, cc_agent_called_time, cc_agent_answered_time, cc_member_joined_time, cc_member_leaving_time, cc_bridge_terminated_time, cc_hangup_cause) FROM stdin;
\.


--
-- Data for Name: v_call_center_queues; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_call_center_queues (call_center_queue_uuid, domain_uuid, dialplan_uuid, queue_name, queue_extension, queue_strategy, queue_moh_sound, queue_record_template, queue_time_base_score, queue_max_wait_time, queue_max_wait_time_with_no_agent, queue_max_wait_time_with_no_agent_time_reached, queue_tier_rules_apply, queue_tier_rule_wait_second, queue_tier_rule_no_agent_no_wait, queue_timeout_action, queue_discard_abandoned_after, queue_abandoned_resume_allowed, queue_tier_rule_wait_multiply_level, queue_cid_prefix, queue_announce_sound, queue_announce_frequency, queue_description) FROM stdin;
\.


--
-- Data for Name: v_call_center_tiers; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_call_center_tiers (call_center_tier_uuid, domain_uuid, agent_name, queue_name, tier_level, tier_position) FROM stdin;
\.


--
-- Data for Name: v_call_flows; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_call_flows (domain_uuid, call_flow_uuid, dialplan_uuid, call_flow_name, call_flow_extension, call_flow_feature_code, call_flow_context, call_flow_status, call_flow_pin_number, call_flow_label, call_flow_app, call_flow_data, call_flow_anti_label, call_flow_anti_app, call_flow_anti_data, call_flow_description) FROM stdin;
\.


--
-- Data for Name: v_clips; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_clips (clip_uuid, clip_name, clip_folder, clip_text_start, clip_text_end, clip_order, clip_desc) FROM stdin;
\.


--
-- Data for Name: v_conference_centers; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_conference_centers (domain_uuid, conference_center_uuid, dialplan_uuid, conference_center_name, conference_center_extension, conference_center_pin_length, conference_center_greeting, conference_center_description, conference_center_enabled) FROM stdin;
\.


--
-- Data for Name: v_conference_rooms; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_conference_rooms (domain_uuid, conference_room_uuid, conference_center_uuid, meeting_uuid, profile, record, max_members, wait_mod, announce, sounds, mute, created, created_by, enabled, description) FROM stdin;
\.


--
-- Data for Name: v_conference_session_details; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_conference_session_details (domain_uuid, conference_session_detail_uuid, conference_session_uuid, meeting_uuid, username, caller_id_name, caller_id_number, uuid, moderator, network_addr, start_epoch, end_epoch) FROM stdin;
\.


--
-- Data for Name: v_conference_sessions; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_conference_sessions (domain_uuid, conference_session_uuid, meeting_uuid, profile, recording, start_epoch, end_epoch) FROM stdin;
\.


--
-- Data for Name: v_conference_users; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_conference_users (conference_user_uuid, domain_uuid, conference_uuid, user_uuid) FROM stdin;
\.


--
-- Data for Name: v_conferences; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_conferences (domain_uuid, conference_uuid, dialplan_uuid, conference_name, conference_extension, conference_pin_number, conference_profile, conference_flags, conference_order, conference_description, conference_enabled) FROM stdin;
\.


--
-- Data for Name: v_contact_addresses; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_contact_addresses (contact_address_uuid, domain_uuid, contact_uuid, address_type, address_street, address_extended, address_locality, address_region, address_postal_code, address_country, address_latitude, address_longitude, address_description) FROM stdin;
\.


--
-- Data for Name: v_contact_groups; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_contact_groups (contact_groups_uuid, domain_uuid, contact_uuid, group_name) FROM stdin;
\.


--
-- Data for Name: v_contact_notes; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_contact_notes (contact_note_uuid, domain_uuid, contact_uuid, contact_note, last_mod_date, last_mod_user) FROM stdin;
\.


--
-- Data for Name: v_contact_phones; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_contact_phones (contact_phone_uuid, domain_uuid, contact_uuid, phone_type, phone_number, phone_extension, phone_description) FROM stdin;
\.


--
-- Data for Name: v_contacts; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_contacts (contact_uuid, domain_uuid, contact_parent_uuid, contact_type, contact_organization, contact_name_given, contact_name_family, contact_nickname, contact_title, contact_role, contact_category, contact_email, contact_url, contact_time_zone, contact_note, created, created_by) FROM stdin;
cae1bea5-18f1-4cae-9c51-a46239da8e9b	60d6731b-e548-4175-ab3e-3fbb261e60f5	\N	user	\N	ccalleu	\N	ccalleu	\N	\N	\N	\N	\N	\N	\N	\N	\N
9f808502-6109-410d-958c-9f7e445ce190	60d6731b-e548-4175-ab3e-3fbb261e60f5	\N	user				ophiel	\N	\N	\N	jaub@c-call.eu	\N	\N	\N	\N	\N
49b0e27c-182d-4e35-8682-0e872feed5df	60d6731b-e548-4175-ab3e-3fbb261e60f5	\N	user	Panthos	Lukasz	Berezowski	lukaszb	\N	\N	\N	berezowski@panthos.net	\N	\N	\N	\N	\N
6fc080d2-3c08-433a-8894-ea085f6af093	60d6731b-e548-4175-ab3e-3fbb261e60f5	\N	user				lukaszb	\N	\N	\N	berezowski@panthos.net	\N	\N	\N	\N	\N
\.


--
-- Data for Name: v_databases; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_databases (database_uuid, database_driver, database_type, database_host, database_port, database_name, database_username, database_password, database_path, database_description) FROM stdin;
\.


--
-- Data for Name: v_default_settings; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_default_settings (default_setting_uuid, default_setting_category, default_setting_subcategory, default_setting_name, default_setting_value, default_setting_order, default_setting_enabled, default_setting_description) FROM stdin;
9899ddeb-4ed2-4a3b-8caf-11190afd4707	domain	menu	uuid	b4750c3f-2a86-b00d-b7d0-345c14eca286	\N	true	\N
da4adb5b-c503-4bbb-a727-c1f05ecc0706	domain	time_zone	name		\N	true	\N
167efe71-d8ef-4cfa-a4b7-a50689e2fe2a	domain	language	code	en-us	\N	true	\N
3592fe80-3200-409a-846f-390eb20a7f35	domain	template	name	enhanced	\N	true	\N
8f692454-116f-43f2-ace6-77a2e980321c	server	temp	dir	/tmp	\N	true	\N
4eb1bafb-5f08-4b47-be37-bf7f02aaca27	server	startup_script	dir	/etc/init.d	\N	true	\N
51ade02c-e33c-4478-a77c-f11788c2536d	server	backup	dir	/tmp	\N	true	\N
093d4c69-2c2a-4bb3-baeb-02d7840c3439	switch	bin	dir	/usr/bin	\N	true	\N
78e000af-045a-438a-a987-ac322bb298c0	switch	base	dir	/usr/local/freeswitch	\N	true	\N
6e3c56ba-0fd2-4fa7-8ee7-67f46affa4ee	switch	conf	dir	/usr/local/freeswitch/conf	\N	true	\N
d23d3075-25c9-4966-b34d-054aad938f5d	switch	db	dir	/usr/local/freeswitch/db	\N	true	\N
fec6d878-b8ab-4fc7-bf2f-5bf4ff4ffc1b	switch	log	dir	/var/log/freeswitch	\N	true	\N
bbafdc60-8d94-4a93-9f20-eb0d85272d63	switch	extensions	dir	/usr/local/freeswitch/conf/directory	\N	true	\N
56d0600b-21b3-4558-bcd9-9c85bb6a9fcd	switch	sip_profiles	dir	/usr/local/freeswitch/conf/sip_profiles	\N	true	\N
0c4b9ff9-d20a-42d0-9259-0fe2592dc929	switch	dialplan	dir	/usr/local/freeswitch/conf/dialplan	\N	true	\N
5b5059e9-0a7f-457d-9af5-da8523a0a3b8	switch	mod	dir	/usr/local/freeswitch/mod	\N	true	\N
3bfcee7d-41bd-4bda-92a7-26d9b2c94f90	switch	scripts	dir	/usr/local/freeswitch/scripts	\N	true	\N
ff4b5265-9de5-43ac-8fa2-214d5c9b28fe	switch	grammar	dir	/usr/local/freeswitch/grammar	\N	true	\N
15472ce3-14f9-4dd5-8639-4fcc280f71cf	switch	storage	dir	/usr/local/freeswitch/storage	\N	true	\N
283131bb-7701-4bbc-903d-427a3b3ad006	switch	voicemail	dir	/usr/local/freeswitch/storage/voicemail	\N	true	\N
ca03913d-0738-4726-8de0-ec85c88c70a4	switch	recordings	dir	/usr/local/freeswitch/recordings	\N	true	\N
66f6180b-a257-4936-acde-cd5bbed8539c	switch	sounds	dir	/usr/local/freeswitch/sounds	\N	true	\N
ac28ac7d-573a-4148-94f7-8328aef67aff	switch	provision	dir		\N	false	\N
d7ec4dc6-9ecb-454b-9364-de08a667f738	provision	enabled	text	\N	\N	false	\N
1aa60fc3-338a-439b-a095-574d1231dcd1	provision	http_auth_username	text	\N	\N	false	\N
22a66f14-8510-4f0b-b9b3-9a3e052f9581	provision	http_auth_password	text	\N	\N	false	\N
8e0aad46-d451-4d7f-a4a3-82e964be21a6	provision	cidr	array	\N	\N	false	\N
bc30bb80-6fa6-4b36-a7fe-374a73a62a14	provision	admin_name	text	\N	\N	false	\N
35f14314-060c-41db-ad9e-3ffc656a52c8	provision	admin_password	text	\N	\N	false	\N
71c04cb5-a59e-444b-9c20-1cba5687fe85	provision	voicemail_number	text	*97	\N	false	\N
51358187-0d54-4082-aa30-a556585888b6	email	smtp_host	var		\N	true	
fe318479-3007-4117-bd5e-318bb5c2747b	email	smtp_secure	var	none	\N	true	
0beb6fd8-0a87-487b-9a61-f2a0e3cd1ab8	email	smtp_auth	var		\N	true	
10341672-deb6-40ff-9ad0-9b4f7543fdc5	email	smtp_username	var		\N	true	
90e8f95f-e31b-412b-a04b-0dce7b6541c8	email	smtp_password	var		\N	true	
8cb0c3cf-4547-4f10-a032-13a1212b9082	email	smtp_from	var		\N	true	
1ba795ad-c952-4d6a-a14f-92b8a60447b1	email	smtp_from_name	var	Voicemail	\N	true	
dc7f1b17-ec09-48f3-ae70-f51412a5e26d	cdr	format	text	json	\N	true	\N
19d5fea4-34df-4f47-b03a-45f5c89d0148	cdr	storage	text	db	\N	true	\N
318c55fa-90af-494c-b006-a52d0987648b	cdr	limit	numeric	800	\N	true	\N
3bd3ae39-05e9-4102-9df6-bd46d0c42262	provision	cidr	array	\N	\N	false	\N
3ee44972-af9e-4ab2-8334-dd72811f99ca	provision	admin_name	text	\N	\N	false	\N
3c388fc8-947d-4057-bd10-927719d05018	provision	admin_password	text	\N	\N	false	\N
81542567-ee64-401a-aeb0-d7d556b5b425	provision	voicemail_number	text	*97	\N	false	\N
40de090d-51b3-46f8-935d-1c769fcf5311	security	password_length	var	10	\N	true	Sets the default length for system generated passwords.
75b13474-7492-42ba-a96b-52a496b3791a	security	password_strength	var	4	\N	true	Sets the default strength for system generated passwords.  Valid Options: 1 - Numeric Only, 2 - Include Lower Apha, 3 - Include Upper Alpha, 4 - Include Special Characters
8f502dd6-3cb4-4b0d-a197-af6c9f7a4849	login	message	text		\N	true	
af293f39-d503-4622-8b30-42d37bb08134	theme	login_opacity	text	0.35	\N	true	Set the opacity of the login box (decimal).
a06b3398-e5a6-4b72-a39c-5d413d256078	theme	login_background_color	text	#ffffff	\N	true	Set a background color (HTML compatible) for the login box.
0ce9162b-df80-49b4-a8cb-2c30cc8e4af5	theme	footer_background_color	text	#000000	\N	true	Set a background color (HTML compatible) for the footer bar.
764556f1-7a67-46f7-a485-00ffd7730a80	theme	footer_color	text	#ffffff	\N	true	Set a foreground color (HTML compatible) for the footer bar.
275d3098-e085-413a-85fe-6a1351dbf1ca	theme	footer_opacity	text	0.2	\N	true	Set the opacity of the footer bar (decimal).
10cfb6ea-2c79-40f8-b753-b039eb9ded6c	theme	message_default_background_color	text	#ccffcc	\N	true	Set the background color (HTML compatible) for the positive (default) message bar.
244682e5-3fe7-4886-bbee-e03b3b61d1e7	theme	message_default_color	text	#004200	\N	true	Set the foreground color (HTML compatible) for the positive (default) message bar text.
eae403cc-7f7e-4397-8f01-2124ff90def9	theme	message_negative_background_color	text	#ffcdcd	\N	true	Set the background color (HTML compatible) for the negative message bar.
987f32dd-fe81-4fee-a4f1-a58cd7c5d34a	theme	message_negative_color	text	#670000	\N	true	Set the foreground color (HTML compatible) for the negative message bar text.
2f7125b0-c99a-4a9d-8ca4-5e9aba92d81f	theme	message_alert_background_color	text	#ffe585	\N	true	Set the background color (HTML compatible) for the alert message bar.
89b5b0c2-3b12-439b-a57a-56ce75dd8b79	theme	message_alert_color	text	#d66721	\N	true	Set the foreground color (HTML compatible) for the alert message bar text.
41a9d83f-b2f3-4e09-a1ff-1d570d89d626	theme	message_opacity	text	0.9	\N	true	Set the opacity of the message bar (decimal).
522356ec-e485-48a6-84a8-8eb51e8d9c20	theme	message_delay	text	1.75	\N	true	Set the hide delay of the message bar (seconds).
cba9605a-277f-4db8-8e43-d1a3843ae6e9	theme	background_color	array	#ffffff	0	true	Set a background (HTML compatible) color.
446ef40d-4805-41f5-a463-fcf984fd80cf	theme	background_color	array	#e7ebf1	1	true	Set a secondary background (HTML compatible) color, for a gradient effect.
356096fe-81c6-42f7-988a-8f78ebab081c	theme	background_image	array	/themes/enhanced/images/backgrounds/sand.jpg	\N	false	Set a relative path or URL within a selected compatible template.
a397be9d-ef33-4cd3-a783-ba2e6aadaf06	theme	background_image	array	/themes/enhanced/images/backgrounds/yellowstone_4.jpg	\N	false	Set a relative path or URL within a selected compatible template.
75116fbf-e09a-4b80-a744-0d31f7458357	theme	background_image	array	/themes/enhanced/images/backgrounds/sand_sea.jpg	\N	false	Set a relative path or URL within a selected compatible template.
95a64c82-2a91-4b6f-815d-db04f85c0191	theme	background_image	array	/themes/enhanced/images/backgrounds/yellowstone_3.jpg	\N	false	Set a relative path or URL within a selected compatible template.
09f22637-df93-477f-934e-23b20815ac5f	theme	background_image	array	/themes/enhanced/images/backgrounds/wave.jpg	\N	false	Set a relative path or URL within a selected compatible template.
\.


--
-- Data for Name: v_destinations; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_destinations (domain_uuid, destination_uuid, dialplan_uuid, fax_uuid, destination_type, destination_number, destination_caller_id_name, destination_caller_id_number, destination_cid_name_prefix, destination_context, destination_app, destination_data, destination_enabled, destination_description, destination_accountcode) FROM stdin;
60d6731b-e548-4175-ab3e-3fbb261e60f5	1c80521e-6f7b-4a98-9f99-69188d0c8842	709af092-f796-4de2-a779-d7bdd9acc279	\N	inbound	48598881023	\N	\N	\N	public	\N	\N	true	\N	\N
\.


--
-- Data for Name: v_device_keys; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_device_keys (domain_uuid, device_key_uuid, device_uuid, device_key_id, device_key_category, device_key_type, device_key_line, device_key_value, device_key_extension, device_key_label) FROM stdin;
\.


--
-- Data for Name: v_device_lines; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_device_lines (domain_uuid, device_line_uuid, device_uuid, line_number, server_address, outbound_proxy, display_name, user_id, auth_id, password, sip_port, sip_transport, register_expires) FROM stdin;
\.


--
-- Data for Name: v_device_settings; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_device_settings (device_setting_uuid, device_uuid, domain_uuid, device_setting_category, device_setting_subcategory, device_setting_name, device_setting_value, device_setting_enabled, device_setting_description) FROM stdin;
\.


--
-- Data for Name: v_devices; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_devices (device_uuid, domain_uuid, device_mac_address, device_label, device_vendor, device_model, device_firmware_version, device_provision_enable, device_template, device_description) FROM stdin;
\.


--
-- Data for Name: v_dialplan_details; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_dialplan_details (domain_uuid, dialplan_uuid, dialplan_detail_uuid, dialplan_detail_tag, dialplan_detail_type, dialplan_detail_data, dialplan_detail_break, dialplan_detail_inline, dialplan_detail_group, dialplan_detail_order) FROM stdin;
60d6731b-e548-4175-ab3e-3fbb261e60f5	832a2be2-fe70-493c-a3aa-87a4c6554370	5cfde8a0-ed12-44e5-b904-0cecd81ef4bd	condition			\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	832a2be2-fe70-493c-a3aa-87a4c6554370	601360f1-1567-49d7-9596-b08f1678b534	action	set	user_exists=${user_exists id ${destination_number} ${domain_name}}	\N	true	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	832a2be2-fe70-493c-a3aa-87a4c6554370	6144e306-0040-4f76-b8b1-22567cb138b8	condition	${user_exists}	^true$	\N	\N	1	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	832a2be2-fe70-493c-a3aa-87a4c6554370	88606229-a777-454b-889d-40aaf8633d32	action	set	extension_uuid=${user_data ${destination_number}@${domain_name} var extension_uuid}	\N	\N	1	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	86a8e20c-325d-4eec-8b14-7642bbcc41b5	aaed656c-1c8e-48c2-82df-252c32983207	condition	${call_direction}	^(inbound|outbound|local)$	never	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	86a8e20c-325d-4eec-8b14-7642bbcc41b5	e25b8eb6-5dbc-4419-b2a6-e01c52cbbb79	anti-action	set	call_direction=local	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	86a8e20c-325d-4eec-8b14-7642bbcc41b5	f8635d30-2e7c-4fd7-b332-7d59c50ce931	condition	${user_exists}	^false$	\N	\N	1	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	86a8e20c-325d-4eec-8b14-7642bbcc41b5	033eb0c5-8887-4b71-9670-28038e8010c1	condition	destination_number	^\\d{7,20}$	\N	\N	1	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	86a8e20c-325d-4eec-8b14-7642bbcc41b5	43715cf4-af67-4d49-b402-5d2fd3616041	action	set	call_direction=outbound	\N	\N	1	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	3c2507d5-d6e9-4d95-93f1-11ec718fc350	a5dfab15-5132-43f3-aa8c-0d862802dd87	condition			\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	3c2507d5-d6e9-4d95-93f1-11ec718fc350	16610446-2fe7-44df-8015-d88eacf8c5fc	action	export	origination_callee_id_name=${destination_number}	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	3c2507d5-d6e9-4d95-93f1-11ec718fc350	e425b2e7-c6fa-4007-817a-d84aca0375d7	action	set	RFC2822_DATE=${strftime(%a, %d %b %Y %T %z)}	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	db204b4d-fd08-41bf-84d6-e34e3e038916	74f6b00a-1fb3-46b2-98c0-a9169f3b6846	condition			\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	db204b4d-fd08-41bf-84d6-e34e3e038916	8141573c-7a3d-4d2c-a18c-5a50c884b6c3	action	lua	app.lua is_local	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	33c104e2-9cb9-4d0d-b6c9-2579b8d97249	ae4c96e3-e74e-41fc-bb67-1b51aaeecab2	condition			\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	33c104e2-9cb9-4d0d-b6c9-2579b8d97249	8f92c99b-1d26-46a8-b167-97914563da7e	action	lua	app.lua call_block	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	4f4afc6a-a6ac-443b-8331-b6aad05e0816	condition			\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	880c6819-2cef-4b73-9d0d-e9df0f766935	action	set	user_record=${user_data ${destination_number}@${domain_name} var user_record}	\N	true	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	2a403f5a-951b-4c5a-a6e0-cfc6b5a83b50	action	set	from_user_exists=${user_exists id ${sip_from_user} ${sip_from_host}}	\N	true	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	47934a5c-7a4f-4d75-bd70-b933c591310c	condition	${user_exists}	^true$	never	\N	1	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	cacc819b-1d57-44d0-ba5e-927c4a28d95a	condition	${user_record}	^all$	never	\N	1	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	a424342b-d6ee-40fd-8dd1-f1a28de5e94e	action	set	record_session=true	\N	true	1	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	76b8a166-5028-4d84-ac44-edf4c2e89222	condition	${user_exists}	^true$	never	\N	2	45
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	5617da15-b0d2-4d14-9c33-d19552105a0f	condition	${call_direction}	^inbound$	never	\N	2	50
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	235296bd-85ce-4803-ac56-43964185c2d5	condition	${user_record}	^inbound$	never	\N	2	55
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	e063f535-5128-4994-ac36-b198bd448780	action	set	record_session=true	\N	true	2	60
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	41076056-276f-42fc-9818-a0b0b8971f24	condition	${user_exists}	^true$	never	\N	3	70
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	0e8eae1b-4bba-4fb5-b3dd-b63b6b629237	condition	${call_direction}	^outbound$	never	\N	3	75
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	7388c709-2746-4869-aa3c-f5eb4a57ee63	condition	${user_record}	^outbound$	never	\N	3	80
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	35bd9aa5-8aa4-46cb-bc40-d3a6d04f499d	action	set	record_session=true	\N	true	3	85
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	2b9ad1e8-4a91-4a9a-9ec5-3b5a02b60a01	condition	${user_exists}	^true$	never	\N	4	95
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	4ec01fb4-a1c3-4466-8b16-edd3daa0c0bd	condition	${call_direction}	^local$	never	\N	4	100
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	b1787ddd-ea25-4f74-a377-815044bbe472	condition	${user_record}	^local$	never	\N	4	105
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	0feb4eee-1dce-4af4-a501-32841aa67bcb	action	set	record_session=true	\N	true	4	110
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	53aee607-b9f1-43c0-a9c7-d1207eeabd2f	condition	${from_user_exists}	^true$	never	\N	5	120
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	3d556033-08b3-4c0a-a4c2-236cd7e2840c	action	set	from_user_record=${user_data ${sip_from_user}@${sip_from_host} var user_record}	\N	true	5	125
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	a1b51e60-45ce-4a27-b638-3d77eca7803b	condition	${from_user_exists}	^true$	never	\N	6	135
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	3cab00b2-9dd1-4bcd-b5e7-42059c513c2a	condition	${from_user_record}	^all$	never	\N	6	140
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	a5fbbd93-041c-4ba2-acfd-fe0e7d5d64bd	action	set	record_session=true	\N	true	6	145
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	7645b763-c0c2-4846-b06f-db613a301e68	condition	${from_user_exists}	^true$	never	\N	7	155
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	8578d259-0c0a-48c3-82b2-102d5341e5a9	condition	${call_direction}	^inbound$	never	\N	7	160
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	3c4ed826-d610-4e79-85d3-5914558acd82	condition	${from_user_record}	^inbound$	never	\N	7	165
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	ee871607-93a1-4fef-af3a-17065cb2fa96	action	set	record_session=true	\N	true	7	170
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	5abc51d7-0027-4e0a-be8b-f1f030f36ce5	condition	${from_user_exists}	^true$	never	\N	8	180
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	92fceb88-ef3d-4f57-b618-f9669b2e78ec	condition	${call_direction}	^outbound$	never	\N	8	185
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	6ab9080e-a569-4590-8df8-f08508302623	condition	${from_user_record}	^outbound$	never	\N	8	190
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	56b6758e-61b7-4707-91bc-932ebf48fc02	action	set	record_session=true	\N	true	8	195
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	b4b48033-2c47-489b-b4c2-b2e6af2ffb03	condition	${from_user_exists}	^true$	never	\N	9	205
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	4f5eec87-107f-496f-870c-e6db179d60ca	condition	${call_direction}	^local$	never	\N	9	210
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	44c0a139-ef3c-4f97-89dc-9a550e15f0a7	condition	${from_user_record}	^local$	never	\N	9	215
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	16ce16ed-937c-40ad-97ab-18545dc350d4	action	set	record_session=true	\N	true	9	220
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	2f204179-1a82-4559-b076-1af34e5c3db2	condition	${record_session}	^true$	\N	\N	10	230
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	3e52aff6-83f0-4e14-8523-64e7df64e542	action	set	api_on_answer=uuid_record ${uuid} start ${recordings_dir}/${domain_name}/archive/${strftime(%Y)}/${strftime(%b)}/${strftime(%d)}/${uuid}.${record_ext}	\N	\N	10	235
60d6731b-e548-4175-ab3e-3fbb261e60f5	cf3f8ed6-740b-42f4-91e6-5794d59ed981	55c1cb7a-c374-4366-8d49-7770b551a507	condition	destination_number	^\\*22$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	cf3f8ed6-740b-42f4-91e6-5794d59ed981	016fa93e-0907-4f70-bee4-f4f64f7f2241	action	lua	app.lua user_status	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	8afcc1af-0e57-45cf-a036-f33914a14f99	4788b38c-d2f5-41d6-8171-187a54a5cb3a	condition	destination_number	^\\*886$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	8afcc1af-0e57-45cf-a036-f33914a14f99	cd2adfda-93d3-49e8-bd83-abe5b461492c	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	8afcc1af-0e57-45cf-a036-f33914a14f99	8c0b768a-a003-4a97-8702-0e3d4721c95f	action	lua	intercept.lua	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	b199391e-8909-4555-aad3-9722b0ee2c0f	2fe3e5c8-bb42-44ba-9762-7b8f2093bc06	condition	destination_number	^\\*8$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	b199391e-8909-4555-aad3-9722b0ee2c0f	72a1dc30-2153-4a6e-8e03-2170ac719f3e	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	b199391e-8909-4555-aad3-9722b0ee2c0f	c3c8d751-0cbc-43f2-ab2f-b699fd44d61c	action	lua	intercept_group.lua	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	9901adea-b483-4253-8d86-ae4d8b6209c1	5e84eb02-3cf8-4b17-b0ec-32c70cf51234	condition	destination_number	^(redial|\\*870)$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	9901adea-b483-4253-8d86-ae4d8b6209c1	910a4b2d-d786-4579-819c-b5caac95a5a4	action	transfer	${hash(select/${domain_name}-last_dial/${caller_id_number})}	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	38679a59-eb22-4532-9b18-2a125d01c5c4	condition	destination_number	^\\*724$	never	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	fe60f8be-c4b6-4d8e-a450-7ce4287c78c0	condition	${sip_h_Referred-By}	sip:(.*)@.*	never	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	887784d0-ea2b-480f-ab61-0d946ae87f8b	action	set	referred_by_user=$1	\N	true	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	79b37c7d-2c2e-4a99-b738-6b7129aa8f5e	action	log	INFO referred_by_user is [${referred_by_user}]	\N	true	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	99931fd8-1d96-4b82-a0c4-41c2bb50f396	action	log	INFO sip_h_Referred-By is [${sip_h_Referred-By}]	\N	true	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	e990d8b0-1a50-4cbc-9be9-e78185b6b682	anti-action	set	referred_by_user=false	\N	true	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	35375f37-c524-4fce-b8b6-fbe68499883f	anti-action	log	INFO referred_by_user is [${referred_by_user}]	\N	true	0	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	295441bd-fd6c-4037-9b82-e4d954b87e31	anti-action	log	INFO sip_h_Referred-By is [${sip_h_Referred-By}]	\N	true	0	40
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	013924c4-d426-4241-82b2-166a4f239c08	condition	destination_number	^\\*724$	\N	\N	1	50
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	58f8c05b-3692-4f3a-a9d3-26d71ed0665d	condition	${referred_by_user}	false	never	\N	1	55
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	9518892d-a231-44a0-906c-5b27cf42f361	action	set	was_transfered_to_page=false	\N	true	1	60
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	20d34f2e-38b9-48aa-9360-c6c6ae81ae16	action	log	INFO was_transfered_to_page is [${was_transfered_to_page}]	\N	true	1	65
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	82383e21-512a-4d95-815e-5597b8c4e554	anti-action	set	was_transfered_to_page=true	\N	true	1	70
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	ee681d3a-2dd2-4d4c-a781-d15082a121ca	anti-action	log	INFO was_transfered_to_page is [${was_transfered_to_page}]	\N	true	1	75
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	89899717-0559-4a1e-955f-669966509c6b	condition	destination_number	^\\*724$	never	\N	2	85
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	b0d5e1a5-696e-4254-9d12-2fa1bf0d66d3	action	log	INFO was_transfered_to_page is [${was_transfered_to_page}]	\N	\N	2	90
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	faa0837d-cbe0-4ec1-bd0c-ad220afc97d1	condition	${was_transfered_to_page}	true	never	\N	3	100
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	1a7dcea5-a3f0-4b9d-90ed-8a8dbc14ddf8	action	transfer	${referred_by_user} XML ${context}	\N	\N	3	105
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	9d0230d5-0751-49b2-b173-237d96dfdc3e	anti-action	set	caller_id_name=Page	\N	\N	3	110
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	b3ede013-9849-42e1-b031-a23b6f15a537	anti-action	set	caller_id_number=	\N	\N	3	115
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	efee6921-a91a-4262-b7a2-00276944f9c0	anti-action	set	pin_number=	\N	\N	3	120
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	a13c89d5-fa0b-40d9-a46c-8bef0f6a6fc4	anti-action	set	extension_list=2000-2002	\N	\N	3	125
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	c68596c5-4d07-4c05-83cf-c66548afb949	anti-action	lua	page.lua	\N	\N	3	130
60d6731b-e548-4175-ab3e-3fbb261e60f5	ef20d627-dc21-4a1f-82a5-1cbefc08846e	10f2e32b-95d4-4797-869e-6aa151d5e828	condition	destination_number	^\\*88(\\d{2,7})$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	ef20d627-dc21-4a1f-82a5-1cbefc08846e	07810e98-7593-4a40-84be-c1d433b3351c	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	ef20d627-dc21-4a1f-82a5-1cbefc08846e	d8d6d671-cf9f-445f-bfd6-658931dafdba	action	set	pin_number=60071087	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	ef20d627-dc21-4a1f-82a5-1cbefc08846e	b644d99e-c029-403b-b242-e3bd483c1fc2	action	lua	eavesdrop.lua $1	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	ce999264-89f0-4baf-9741-26037935276a	4d3331dd-a38d-4699-aca2-364b1e4676c1	condition	destination_number	^\\*67(\\d+)$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	ce999264-89f0-4baf-9741-26037935276a	6c8e6842-75c2-4344-955e-2ce2f69543c5	action	privacy	full	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	ce999264-89f0-4baf-9741-26037935276a	70087133-95c5-4a84-85a7-b9e80ae28a1b	action	set	sip_h_Privacy=id	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	ce999264-89f0-4baf-9741-26037935276a	03cc0e4a-0b0e-4a77-a573-91d66cf4f48a	action	set	privacy=yes	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	ce999264-89f0-4baf-9741-26037935276a	5d00d241-ef2f-4ac1-8dfa-a019760182b8	action	transfer	$1 XML ${context}	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	7c7232bb-f386-461c-96cf-6c89029939c8	c8eee4cb-d256-48c9-b3df-99b5c76240f6	condition	destination_number	^\\*69$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	7c7232bb-f386-461c-96cf-6c89029939c8	22c1e92e-5784-431b-88b4-a6ad4a7ff075	action	transfer	${hash(select/${domain_name}-call_return/${caller_id_number})}	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	0b35b102-7acf-420a-8b45-729bcdad6490	4106776e-dcbb-4fe0-8ff8-ba63c424db4d	condition	destination_number	^\\*\\*(\\d+)$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	0b35b102-7acf-420a-8b45-729bcdad6490	de1ae9d4-8ad4-41ed-9f3f-306d10ffcf4d	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	0b35b102-7acf-420a-8b45-729bcdad6490	8f320c8f-e973-4295-864a-ff534e9f351f	action	lua	intercept.lua $1	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	04600444-9f69-427b-a108-3a3bca0751eb	388328ee-ff34-46ed-bc9a-39bd4b255e98	condition	destination_number	^dx$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	04600444-9f69-427b-a108-3a3bca0751eb	021aa7b9-7ebe-4704-bb01-fe4aba46e5d5	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	04600444-9f69-427b-a108-3a3bca0751eb	a0802fc1-c813-46e1-8d0d-e7e35e2966ad	action	read	11 11 'tone_stream://%(10000,0,350,440)' digits 5000 #	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	04600444-9f69-427b-a108-3a3bca0751eb	533f2b36-866c-4256-9b11-7ff1669fb211	action	transfer	-bleg ${digits}	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	aed96a7a-7a7a-45e3-b5fc-885adcb8f101	5f011b67-bfcc-4ea6-a1eb-8abe9528ba56	condition	destination_number	^\\*8(\\d{2,7})$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	aed96a7a-7a7a-45e3-b5fc-885adcb8f101	5e841ddf-44ae-4c3f-b4e9-5450ce048ab4	action	set	extension_list=$1	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	aed96a7a-7a7a-45e3-b5fc-885adcb8f101	472e04c6-fd11-4177-a147-f9ecaf2360c8	action	set	pin_number=	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	aed96a7a-7a7a-45e3-b5fc-885adcb8f101	90085353-e37c-431e-962a-0733bc1b06a0	action	set	mute=true	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	aed96a7a-7a7a-45e3-b5fc-885adcb8f101	194bfd12-a69e-4039-abeb-03494fe8a5c0	action	lua	page.lua	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	e4e6b6a6-65d3-463b-98ec-f738a7949477	c861f74a-5b80-4fa1-b1d0-7dad7c67d656	condition	destination_number	^att_xfer$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	e4e6b6a6-65d3-463b-98ec-f738a7949477	df032945-2a99-49af-bb80-97fd499a47e4	action	read	2 6 'tone_stream://%(10000,0,350,440)' digits 30000 #	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	e4e6b6a6-65d3-463b-98ec-f738a7949477	9bb5b3a7-a9c0-4ffe-aefb-5d1473e16448	action	set	origination_cancel_key=#	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	e4e6b6a6-65d3-463b-98ec-f738a7949477	89380b4a-c0a7-4ba5-98c3-2ab933d9afb3	action	set	domain_name=${transfer_context}	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	e4e6b6a6-65d3-463b-98ec-f738a7949477	563f4feb-2926-4c44-992b-23b71587c891	action	att_xfer	user/${digits}@${transfer_context}	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	80917642-03da-4136-98bc-aea43b97b2d1	4574c5f7-97eb-4661-a0d3-c8c7f480f0e6	condition	username	^${caller_id_number}$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	80917642-03da-4136-98bc-aea43b97b2d1	33fd90bd-80b7-4887-a4c7-a5809215c4e3	condition	destination_number	^${caller_id_number}$	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	80917642-03da-4136-98bc-aea43b97b2d1	5bd5db14-a9f4-4d86-a0ab-cab6082e5b51	action	answer		\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	80917642-03da-4136-98bc-aea43b97b2d1	484521b6-34cf-41d8-8467-28ae45cde9ea	action	sleep	1000	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	80917642-03da-4136-98bc-aea43b97b2d1	fe4c1685-07ef-4e9e-9825-0e54215603f6	action	set	voicemail_action=check	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	80917642-03da-4136-98bc-aea43b97b2d1	c0320c6f-6181-4d31-86cd-28d5b4411575	action	set	voicemail_id=${caller_id_number}	\N	\N	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	80917642-03da-4136-98bc-aea43b97b2d1	9c1479e7-6598-4c6e-89ef-c91e5e0d946e	action	set	voicemail_profile=default	\N	\N	0	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	80917642-03da-4136-98bc-aea43b97b2d1	12e47095-adf0-45aa-a76a-a19e253f1c88	action	lua	app.lua voicemail	\N	\N	0	40
60d6731b-e548-4175-ab3e-3fbb261e60f5	2b7218c5-0498-4b48-91c9-e5878a8c5069	95a198b2-0822-4b71-bdb9-8d3557076bbd	condition	destination_number	^\\*99(\\d{2,7})$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	2b7218c5-0498-4b48-91c9-e5878a8c5069	de4a46dc-8e1c-48ac-899b-b080f2688d55	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	2b7218c5-0498-4b48-91c9-e5878a8c5069	3d663470-1ec1-412d-9c90-20605aa20981	action	sleep	1000	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	2b7218c5-0498-4b48-91c9-e5878a8c5069	a66f4d58-fe08-4c9d-b386-144e87993009	action	set	voicemail_action=save	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	2b7218c5-0498-4b48-91c9-e5878a8c5069	d356ac20-bfe3-4fb2-9e0d-8edf9447f6df	action	set	voicemail_id=$1	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	2b7218c5-0498-4b48-91c9-e5878a8c5069	0ad780b4-3ce0-400e-ad0a-4b4b97cc938c	action	set	voicemail_profile=default	\N	\N	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	2b7218c5-0498-4b48-91c9-e5878a8c5069	64ceecfe-d49a-425f-9e08-101a3b40de99	action	lua	app.lua voicemail	\N	\N	0	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	59ab0756-6dff-4ca9-8df2-776676042feb	8db3366d-62f3-4c81-ac50-13272de7ddc8	condition	destination_number	^vmain$|^\\*4000$|^\\*98$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	59ab0756-6dff-4ca9-8df2-776676042feb	26c2c205-d7d9-42d0-b844-28d830f797b5	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	59ab0756-6dff-4ca9-8df2-776676042feb	219b648d-39c3-49e2-b42d-9e43e980adfb	action	sleep	1000	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	59ab0756-6dff-4ca9-8df2-776676042feb	2dd0c8ce-a204-4048-aa8d-6e72e0a7d9c5	action	set	voicemail_action=check	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	59ab0756-6dff-4ca9-8df2-776676042feb	bf0567bc-414a-45fd-b7bd-05c65a6d9fd9	action	set	voicemail_profile=default	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	59ab0756-6dff-4ca9-8df2-776676042feb	1ed3229d-501e-402c-91a7-1d7740ea7011	action	lua	app.lua voicemail	\N	\N	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	3cdb7409-f0b6-4e09-b055-923a878595e4	6960a1f1-177e-4210-8799-9986f84ca7ba	condition	destination_number	^xfer_vm$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	3cdb7409-f0b6-4e09-b055-923a878595e4	a68b3132-8a4e-4736-853b-f6afe138bea3	action	read	2 6 'tone_stream://%(10000,0,350,440)' digits 30000 #	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	3cdb7409-f0b6-4e09-b055-923a878595e4	c0834bad-f54d-420b-8a60-20d8eb9e9b31	action	set	origination_cancel_key=#	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	3cdb7409-f0b6-4e09-b055-923a878595e4	d63f19d8-b670-4bc3-bee5-c880f23e4306	action	set	domain_name=${transfer_context}	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	3cdb7409-f0b6-4e09-b055-923a878595e4	09663de9-0973-4a2c-a8cb-8a9de2947208	action	export	domain_name=${transfer_context}	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	3cdb7409-f0b6-4e09-b055-923a878595e4	21ee6ee3-5174-4462-ab46-bdec83bf4a85	action	transfer	-bleg *99${digits} XML ${transfer_context}	\N	\N	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	ac8513db-39fa-4c0a-a337-a29afcb0f568	7cd73071-4a7d-4072-924d-f0aec14c3b74	condition	destination_number	^is_transfer$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	ac8513db-39fa-4c0a-a337-a29afcb0f568	b0096c06-560c-43eb-80d6-feeb8ccb748f	condition	${digits}	^(\\d+)$	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	ac8513db-39fa-4c0a-a337-a29afcb0f568	2d7a77ae-7c55-45a7-88b3-38b67ac933be	action	transfer	-aleg ${digits} XML ${context}	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	ac8513db-39fa-4c0a-a337-a29afcb0f568	0429bd8c-8491-4168-a6a9-465d337910f0	anti-action	eval	cancel transfer	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	5aac9fad-64b5-4fc4-ac31-f6b83c7acfd8	a6fac499-445b-441d-8a01-e4b3c01b97de	condition	destination_number	^\\*97$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	5aac9fad-64b5-4fc4-ac31-f6b83c7acfd8	3661b420-d643-4a64-8b70-2a38db7b1944	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	5aac9fad-64b5-4fc4-ac31-f6b83c7acfd8	790f7b55-ee62-483d-a9ce-18810c0d853d	action	sleep	1000	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	5aac9fad-64b5-4fc4-ac31-f6b83c7acfd8	a214030c-a051-433d-b778-c27efb2e0f22	action	set	voicemail_action=check	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	5aac9fad-64b5-4fc4-ac31-f6b83c7acfd8	90d3d7e6-4bd9-4656-bbb0-6110876cceaf	action	set	voicemail_id=${caller_id_number}	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	5aac9fad-64b5-4fc4-ac31-f6b83c7acfd8	1b1eec3a-43df-49bf-ae3e-5eb56f42d45e	action	set	voicemail_profile=default	\N	\N	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	5aac9fad-64b5-4fc4-ac31-f6b83c7acfd8	6f492501-7362-41c1-9c61-ce7e3ae55c3b	action	lua	app.lua voicemail	\N	\N	0	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	f0e49bed-4dc7-4628-ab70-094b16585e16	e19e2f38-96e4-41df-befe-a2974841efb1	condition	destination_number	^cf$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	f0e49bed-4dc7-4628-ab70-094b16585e16	52e89beb-3ab5-4df6-8cf9-9bb9e5fa1b42	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	f0e49bed-4dc7-4628-ab70-094b16585e16	805bcc7f-0655-4797-8da6-a79054d498a6	action	transfer	-both 30${dialed_extension:2} XML ${context}	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	4c7f90b6-b099-4f3b-8a46-4ea123a04c02	1bdad2c2-5b53-4ba9-b286-42b60e68404d	condition	destination_number	^\\*9195$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	4c7f90b6-b099-4f3b-8a46-4ea123a04c02	e8ef18c3-e780-4028-be23-7e5de1de0f2d	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	4c7f90b6-b099-4f3b-8a46-4ea123a04c02	cb08e256-a1c4-4410-b370-81e7162df27f	action	delay_echo	5000	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	b616b00e-e9ee-4f3a-8b59-2e0f8ea481d1	90515466-fcd4-4a1f-99f0-f1442451c1ea	condition	destination_number	^\\*9196$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	b616b00e-e9ee-4f3a-8b59-2e0f8ea481d1	cacae475-9a59-4154-9139-885c7572c0aa	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	b616b00e-e9ee-4f3a-8b59-2e0f8ea481d1	a1626af8-a9ca-46bc-b53a-09165f80cf53	action	echo		\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	7c753572-237c-422c-b05d-db7917dc9335	507a9c6a-a574-4bd7-a56d-369c456e9553	condition	destination_number	^(10[01][0-9])$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	7c753572-237c-422c-b05d-db7917dc9335	ed0fde27-453b-475c-8706-037626aef371	action	set	transfer_ringback=$${hold_music}	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	7c753572-237c-422c-b05d-db7917dc9335	cdaeff24-0287-455e-9d86-e74731fcfd52	action	answer		\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	7c753572-237c-422c-b05d-db7917dc9335	4ef097c3-801f-4af4-b59d-abb9a2fe1b0b	action	sleep	1500	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	7c753572-237c-422c-b05d-db7917dc9335	d63d5f49-ca19-4738-a4c0-151ed8dbbc01	action	playback	ivr/ivr-hold_connect_call.wav	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	7c753572-237c-422c-b05d-db7917dc9335	dc002aba-da82-421a-ab31-5bf96fef1698	action	transfer	$1 XML ${context}	\N	\N	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	424f79ed-c9bc-4734-bf3e-407dd883fd7d	d1c1af64-78c5-4706-8dad-584992dc32fa	condition	destination_number	^\\*9197$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	424f79ed-c9bc-4734-bf3e-407dd883fd7d	bd4c4015-a911-4698-b77d-9589bccc1fbb	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	424f79ed-c9bc-4734-bf3e-407dd883fd7d	2f6ac5a7-1d1b-41ad-b5fa-4dc99a78b4e4	action	playback	{loops=-1}tone_stream://%(251,0,1004)	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	22f0e3b2-9891-400a-a2fe-1922d6ca8d90	78477649-16c2-43c3-b712-b3dd828a0b0e	condition	destination_number	^\\*9198$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	22f0e3b2-9891-400a-a2fe-1922d6ca8d90	ebabdd1e-1ef0-462c-8325-6c2f8a01c1ba	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	22f0e3b2-9891-400a-a2fe-1922d6ca8d90	a3b45c09-ebc3-4601-805a-279b56063c3a	action	playback	{loops=10}tone_stream://path=${base_dir}/conf/tetris.ttml	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	fd4876cf-5db5-4b2c-8132-73bcd150434e	7032ff83-5101-421b-a6d6-5ef49d2fa2c1	condition	destination_number	^\\*9664$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	fd4876cf-5db5-4b2c-8132-73bcd150434e	154513ef-e0b3-4c9a-960b-4f156df5c338	condition	${sip_has_crypto}	^(AES_CM_128_HMAC_SHA1_32|AES_CM_128_HMAC_SHA1_80)$	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	fd4876cf-5db5-4b2c-8132-73bcd150434e	c629dedf-9f23-42f7-9e94-b8dbf38ab38c	action	answer		\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	fd4876cf-5db5-4b2c-8132-73bcd150434e	674bd0d5-76b0-41cf-9478-145dc306474a	action	execute_extension	is_secure XML features	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	fd4876cf-5db5-4b2c-8132-73bcd150434e	e68eb2e6-02c4-4821-a25c-e50a223101e9	action	playback	$${hold_music}	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	fd4876cf-5db5-4b2c-8132-73bcd150434e	611768a5-a597-4248-91d7-bca2e444abcc	anti-action	set	zrtp_secure_media=true	\N	\N	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	fd4876cf-5db5-4b2c-8132-73bcd150434e	dc078d54-755d-4587-b28e-1d6fae00e38a	anti-action	answer		\N	\N	0	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	fd4876cf-5db5-4b2c-8132-73bcd150434e	023c0c4a-339f-4b13-b8d0-4a6544c711aa	anti-action	playback	silence_stream://2000	\N	\N	0	40
60d6731b-e548-4175-ab3e-3fbb261e60f5	fd4876cf-5db5-4b2c-8132-73bcd150434e	f454dda5-c11e-42dd-964b-e4a33db4215f	anti-action	execute_extension	is_zrtp_secure XML features	\N	\N	0	45
60d6731b-e548-4175-ab3e-3fbb261e60f5	fd4876cf-5db5-4b2c-8132-73bcd150434e	ab592b6c-dbb5-4d42-83f9-45b6b1a1bf84	anti-action	playback	$${hold_music}	\N	\N	0	50
60d6731b-e548-4175-ab3e-3fbb261e60f5	46ea5764-9958-437c-b004-2f8b40fe667c	74be059d-eb63-41a9-82a0-409b9a615175	condition	destination_number	^\\*(732)$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	46ea5764-9958-437c-b004-2f8b40fe667c	a0e0676a-468e-46e6-9d3e-a5ea410799d9	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	46ea5764-9958-437c-b004-2f8b40fe667c	7f0459b2-e58e-4180-8e19-b17bcf8a6230	action	set	pin_number=06274507	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	46ea5764-9958-437c-b004-2f8b40fe667c	c87423e8-1d4b-4906-98ac-cf376958ec96	action	set	recording_slots=true	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	46ea5764-9958-437c-b004-2f8b40fe667c	3017d88a-5b33-4b0e-b69c-49b710d2c08d	action	set	recording_prefix=recording	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	46ea5764-9958-437c-b004-2f8b40fe667c	fa1c50cc-6a19-4e43-aebf-4883bc8c33f3	action	lua	recordings.lua	\N	\N	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	a356af9a-10c3-4d3b-a939-b7ccfb87ad07	1b77e5b7-5625-4fdd-bd71-7d05b9ecd7dd	condition	destination_number	^\\*9(888|8888|1616|3232)$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	a356af9a-10c3-4d3b-a939-b7ccfb87ad07	45d080ee-7964-4697-aa33-a38730f5cd37	action	export	hold_music=silence	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	a356af9a-10c3-4d3b-a939-b7ccfb87ad07	7f998dfc-4cfd-4d28-813c-f7b9293b3806	action	bridge	sofia/${use_profile}/$1@conference.freeswitch.org	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	0f67fd8e-031d-42dc-bdb8-6c828dafeb59	a84df8e3-7313-41bd-98d1-04a8e51e9a3a	condition	destination_number	^\\*(3472)$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	0f67fd8e-031d-42dc-bdb8-6c828dafeb59	2d7daed3-4b9c-4eff-8253-fafb96ed4184	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	0f67fd8e-031d-42dc-bdb8-6c828dafeb59	e4b687eb-3a59-47cd-adf3-a7747acf91d0	action	set	pin_number=17218018	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	0f67fd8e-031d-42dc-bdb8-6c828dafeb59	05b14f72-d30d-4a8a-b6d6-d7210316258c	action	set	dialplan_context=${context}	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	0f67fd8e-031d-42dc-bdb8-6c828dafeb59	e15a6a95-81ba-4436-9d80-a0fb8ca42104	action	lua	disa.lua	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	ef158010-9457-4f6a-9a3c-8cbdf92160cc	b089dd62-7683-4e0f-8039-9be22bcb3413	condition	destination_number	^\\*411$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	ef158010-9457-4f6a-9a3c-8cbdf92160cc	1e585195-13d9-450f-90cc-aaf28a3576c5	action	lua	directory.lua	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	657266f0-4ed5-4a2c-b31a-a6df573569ee	b149cac9-3821-4b26-bd8a-81a70b1e15d7	condition	destination_number	^\\*(925)$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	657266f0-4ed5-4a2c-b31a-a6df573569ee	cfb438cc-2993-4c5b-b2e8-b7aab09753fc	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	657266f0-4ed5-4a2c-b31a-a6df573569ee	c134f2f0-8280-42fa-be7b-f2ac56f569c2	action	set	pin_number=47177789	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	657266f0-4ed5-4a2c-b31a-a6df573569ee	c1b45704-fb8b-4b40-9fec-5f354cd36bb0	action	set	time_zone_offset=-7	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	657266f0-4ed5-4a2c-b31a-a6df573569ee	03657976-f47c-4239-8128-19f6d23e6333	action	lua	wakeup.lua	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	104cbd03-d8f5-4664-9a2f-70db92418771	a35306e8-b2a0-4297-8392-3dab3c702cb0	condition	destination_number	^\\*5900$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	104cbd03-d8f5-4664-9a2f-70db92418771	bfaee461-5f5a-459c-977e-864ba6108ab5	action	set	park_direction=in	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	104cbd03-d8f5-4664-9a2f-70db92418771	59d488a7-1fbf-41ac-8a15-c324df507597	action	set	park_extension=\\*5901	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	104cbd03-d8f5-4664-9a2f-70db92418771	592f3f22-ffda-4bb1-bc30-8fa6a6337c8f	action	set	park_range=3	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	104cbd03-d8f5-4664-9a2f-70db92418771	98f09c63-24ed-4ee0-85d5-569f8aad202a	action	set	park_announce=true	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	104cbd03-d8f5-4664-9a2f-70db92418771	1a59ff18-c9aa-4445-b8d5-120615d51576	action	set	park_timeout_seconds=70	\N	\N	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	104cbd03-d8f5-4664-9a2f-70db92418771	eb87c72a-a8c3-43ba-821e-e12604cd8a90	action	set	park_timeout_destination=1000	\N	\N	0	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	104cbd03-d8f5-4664-9a2f-70db92418771	a47f3d8e-ea58-4811-b67a-5f307ffc1e12	action	set	park_music=$${hold_music}	\N	\N	0	40
60d6731b-e548-4175-ab3e-3fbb261e60f5	104cbd03-d8f5-4664-9a2f-70db92418771	3a90126b-394b-42cd-824b-408d99d2c203	action	set	park_caller_id_prefix=	\N	\N	0	45
60d6731b-e548-4175-ab3e-3fbb261e60f5	104cbd03-d8f5-4664-9a2f-70db92418771	a144f9c4-44cb-45a5-bab7-3e433d898dcb	action	lua	park.lua	\N	\N	0	50
60d6731b-e548-4175-ab3e-3fbb261e60f5	7f937674-97a3-45e2-a4c2-fa5bbfd44dd4	cbea1259-0132-4136-b71b-29f0bfa3359a	condition	destination_number	(^\\*59[0-9][0-9]$)	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	7f937674-97a3-45e2-a4c2-fa5bbfd44dd4	20f84e73-402b-4006-b324-b1ff9d289b94	action	set	park_direction=out	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	7f937674-97a3-45e2-a4c2-fa5bbfd44dd4	3810bf14-4e46-44b3-834b-a41a0fcb7340	action	set	park_extension=$1	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	7f937674-97a3-45e2-a4c2-fa5bbfd44dd4	4f8f6734-e2bb-45a1-ab8f-8a5b0178f656	action	lua	park.lua	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	8890ec25-7ff2-4554-bdad-e07362930f43	0fa5b3d0-9a11-4383-864e-d36ebf5134f3	condition	destination_number	(^\\*59[0-9][0-9]$|^\\*5902$|^\\*5903$)	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	8890ec25-7ff2-4554-bdad-e07362930f43	7c05cc2c-1067-4758-a012-523b5a98bbda	action	set	park_extension=$1	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	8890ec25-7ff2-4554-bdad-e07362930f43	f678cf59-7484-499d-9d39-6a98b0d9383a	action	set	park_direction=both	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	8890ec25-7ff2-4554-bdad-e07362930f43	13cdd352-678b-4288-a206-15300acce9c3	action	set	park_announce=true	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	8890ec25-7ff2-4554-bdad-e07362930f43	dd8fe12c-b6d8-425d-bc82-e9b91f2f9960	action	set	park_timeout_seconds=250	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	8890ec25-7ff2-4554-bdad-e07362930f43	0e397e15-961b-46a4-b63e-ef9c183c21b1	action	set	park_timeout_destination=1000	\N	\N	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	8890ec25-7ff2-4554-bdad-e07362930f43	4712a831-49f9-4058-8626-6972b9a2b877	action	set	park_music=$${hold_music}	\N	\N	0	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	8890ec25-7ff2-4554-bdad-e07362930f43	0ae9a17b-290e-4691-b6bc-fc289c98e4b0	action	set	park_caller_id_prefix=	\N	\N	0	40
60d6731b-e548-4175-ab3e-3fbb261e60f5	8890ec25-7ff2-4554-bdad-e07362930f43	3a49202d-33d4-4415-8348-d699e01da66f	action	lua	park.lua	\N	\N	0	45
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	1b066b56-3493-4346-aa11-c56d5a4f2c62	condition	destination_number	^(park\\+)?(\\*59[0-9][0-9])$	never	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	7d3f6429-7efe-44cd-87f5-c0fbbc40c6d7	condition	${sip_h_Referred-By}	sip:(.*)@.*	never	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	08a9e9c7-a057-4543-8c4b-f49dd1bbca3d	action	set	referred_by_user=$1	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	c03a82df-f9f8-4deb-9295-b3b7c2c54e84	condition	destination_number	^(park\\+)?(\\*59[0-9][0-9])$	never	\N	1	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	d33f98ac-9d14-4550-b75c-8341278a7243	action	set	park_in_use=false	\N	true	1	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	4cea7495-c9dd-4524-a016-2df78761955b	action	set	park_lot=$2	\N	true	1	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	3911697b-a32c-44ba-b54e-76f37a14fdc2	condition	destination_number	^(park\\+)?(\\*59[0-9][0-9])$	\N	\N	2	45
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	27b524da-b270-4a35-8a41-0efe5f94a30b	condition	${cond ${sip_h_Referred-By} == 0 ? false : true}	true	never	\N	2	50
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	44ade6bd-3d6e-4e16-8a5d-b2958e861ea4	action	set	park_in_use=${regex ${valet_info park@${domain_name}}|${park_lot}}	\N	true	2	55
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	b8722503-c487-4df5-b496-bdba9a3056c5	condition	${park_in_use}	true	never	\N	3	65
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	18cc65b6-a0c1-47a1-83ce-985a6caa2e19	action	transfer	${referred_by_user} XML ${context}	\N	\N	3	70
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	14dd8404-16c8-4a81-8a65-30750db521ec	anti-action	set	valet_parking_timeout=90	\N	\N	3	75
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	d53558b6-8ae9-48d0-919d-a463cd33d5fc	anti-action	set	valet_hold_music=${hold_music}	\N	\N	3	80
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	a9b203b7-fe3a-4307-83ac-dcb67d913573	anti-action	set	valet_parking_orbit_exten=${referred_by_user}	\N	\N	3	85
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	55cf4847-7456-42bb-bf01-cfa921155ab0	anti-action	valet_park	park@${domain_name} ${park_lot}	\N	\N	3	90
60d6731b-e548-4175-ab3e-3fbb261e60f5	862a8a6b-24dd-4ea8-a283-70467d0c7b32	ae9f43fe-da68-4570-9adf-4215508887e1	condition	destination_number	^(park\\+)?(\\*5900)$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	862a8a6b-24dd-4ea8-a283-70467d0c7b32	2df85212-6db4-4d2d-9749-57d4e33ba5a0	action	valet_park	park@${domain_name} auto in 5901 5999	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	09ea7c5a-77b5-4b7a-8a03-1b07b77c10f3	63463d82-b71a-48c0-ab57-34b7f758fb73	condition	destination_number	^(park\\+)?\\*(59[0-9][0-9])$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	09ea7c5a-77b5-4b7a-8a03-1b07b77c10f3	f9a465f5-52a8-454b-902b-3dac3334b513	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	09ea7c5a-77b5-4b7a-8a03-1b07b77c10f3	c0162283-9912-4423-b8df-1a3c576d29e2	action	valet_park	park@${domain_name} $1	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	2e7ac7bf-ea6d-4ada-b7be-9fafd97a249b	67088a49-b891-4c86-a8b0-e8420484e3ff	condition	destination_number	^\\*000$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	2e7ac7bf-ea6d-4ada-b7be-9fafd97a249b	9141026b-d28a-43f8-90fe-a01180ce2541	action	set	dial_string=loopback/operator/default/XML	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	2e7ac7bf-ea6d-4ada-b7be-9fafd97a249b	c11d3824-8bd8-4ec1-965a-515f8357df5d	action	set	direction=both	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	2e7ac7bf-ea6d-4ada-b7be-9fafd97a249b	a2dd3174-2299-4082-973c-9c0afd35c72f	action	set	extension=true	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	2e7ac7bf-ea6d-4ada-b7be-9fafd97a249b	c41e71d4-9e77-4de8-9a95-dbf2766e1bb7	action	lua	dial_string.lua	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	bf6b14a1-ed80-4122-9759-0dde8852c885	21c826ed-d826-438f-ae9a-843a0143275a	condition	destination_number	^\\*78$|\\*363$	on-true	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	bf6b14a1-ed80-4122-9759-0dde8852c885	a6e49a7d-f5ca-4322-9f8f-c668ce1ba76c	action	set	enabled=true	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	bf6b14a1-ed80-4122-9759-0dde8852c885	9c29a47a-ab91-4e1a-9d05-5e2c7141dfb0	action	lua	do_not_disturb.lua	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	bf6b14a1-ed80-4122-9759-0dde8852c885	569d897c-a319-4b3a-a022-a766be8f6e41	condition	destination_number	^\\*79$	\N	\N	1	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	bf6b14a1-ed80-4122-9759-0dde8852c885	b33b05cb-c596-4b63-b692-d6756ac2b4ce	action	set	enabled=false	\N	\N	1	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	bf6b14a1-ed80-4122-9759-0dde8852c885	48fa820e-344b-4029-ac3a-33cb3f9afb53	action	lua	do_not_disturb.lua	\N	\N	1	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	91cdaa61-862c-4ec8-b800-9cb12a2c32bd	c487905e-647d-448f-9f98-5cda558a91bd	condition	destination_number	^\\*72$	on-true	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	91cdaa61-862c-4ec8-b800-9cb12a2c32bd	bd3537a9-64cb-4af2-aa52-7a11888ac96a	action	set	enabled=true	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	91cdaa61-862c-4ec8-b800-9cb12a2c32bd	97c2ea3b-027f-4de3-8fa8-41195cab3d64	action	lua	call_forward.lua	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	91cdaa61-862c-4ec8-b800-9cb12a2c32bd	52d18d14-e410-418b-b7ff-8f0aae47e9f3	condition	destination_number	^\\*73$	on-true	\N	1	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	91cdaa61-862c-4ec8-b800-9cb12a2c32bd	cbfe6dc8-5345-4991-b4b2-dd370b08e0c5	action	set	enabled=false	\N	\N	1	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	91cdaa61-862c-4ec8-b800-9cb12a2c32bd	351a7d93-317b-4230-962a-0503df149539	action	lua	call_forward.lua	\N	\N	1	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	91cdaa61-862c-4ec8-b800-9cb12a2c32bd	5aa2d438-7b54-4590-a92d-b52a36967050	condition	destination_number	^\\*74$	on-true	\N	2	45
60d6731b-e548-4175-ab3e-3fbb261e60f5	91cdaa61-862c-4ec8-b800-9cb12a2c32bd	b61f755f-1844-4853-a028-deee8df5889a	action	set	request_id=true	\N	\N	2	50
60d6731b-e548-4175-ab3e-3fbb261e60f5	91cdaa61-862c-4ec8-b800-9cb12a2c32bd	947c6b84-7cfb-4c5f-ade8-70ec493f9dca	action	set	enabled=toggle	\N	\N	2	55
60d6731b-e548-4175-ab3e-3fbb261e60f5	91cdaa61-862c-4ec8-b800-9cb12a2c32bd	d553f696-600d-46a7-b4aa-da6d2ae8d0f8	action	lua	call_forward.lua	\N	\N	2	60
60d6731b-e548-4175-ab3e-3fbb261e60f5	b538b96f-598f-44e1-ae53-086d11951fdb	fed106d1-1417-467f-9597-32ddb2674fc9	condition	destination_number	^\\*21$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	b538b96f-598f-44e1-ae53-086d11951fdb	92429107-282c-46dc-904d-11cd8ca79889	action	answer		\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	b538b96f-598f-44e1-ae53-086d11951fdb	0967fe00-8c57-4da9-8448-9a188663b9c9	action	lua	follow_me.lua	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	879f8a1d-df73-426d-94e1-3dfaa2635ecb	condition	destination_number	(^\\d{2,7}|\\D+$)	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	507fa2a2-80f5-42cc-99b0-2e8e2b08c065	action	set	dialed_extension=$1	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	c4d3bd70-b223-4405-ac87-e093867ca7e3	action	export	dialed_extension=$1	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	c08f06ac-d249-4f60-9b87-a426542491f2	action	limit	hash ${domain_name} $1 ${limit_max} ${limit_destination}	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	0024e08c-3b28-454c-b174-9d86abf504fc	action	bind_meta_app	1 ab s execute_extension::dx XML ${context}	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	fba3d6d0-f506-4d3f-8816-ffe7d59eeec7	action	bind_meta_app	2 ab s record_session::$${recordings_dir}/${domain_name}/archive/${strftime(%Y)}/${strftime(%b)}/${strftime(%d)}/${uuid}.wav	\N	\N	0	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	d966a97c-a456-4a8e-9c67-54189e717ea8	action	bind_meta_app	3 ab s execute_extension::cf XML ${context}	\N	\N	0	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	83c6d898-4b15-4a8a-b08a-41e4f59519e1	action	bind_meta_app	4 ab s execute_extension::att_xfer XML ${context}	\N	\N	0	40
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	297f3840-0b64-43a3-9ac6-82770751e89c	action	set	hangup_after_bridge=true	\N	\N	0	45
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	8834b76c-82dd-45a7-a252-7a375913bc79	action	set	continue_on_fail=true	\N	\N	0	50
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	93b102c6-f62b-4e2c-85ae-815737ff1743	action	hash	insert/${domain_name}-call_return/${dialed_extension}/${caller_id_number}	\N	\N	0	55
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	9ff6a56e-8a70-4dbc-937e-f292668f82c4	action	hash	insert/${domain_name}-last_dial_ext/${dialed_extension}/${uuid}	\N	\N	0	60
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	3177ca4b-2f18-4331-8b4e-7bccae967d65	action	set	called_party_call_group=${user_data(${dialed_extension}@${domain_name} var call_group)}	\N	\N	0	65
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	118c436a-6d83-4f47-aba7-fee212c3c2c4	action	hash	insert/${domain_name}-last_dial/${called_party_call_group}/${uuid}	\N	\N	0	70
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	af3e9a67-e8de-4f79-80b9-cd97fae7c371	action	bridge	user/${destination_number}@${domain_name}	\N	\N	0	75
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	257dca63-95d3-47c9-82b4-7bf76015046d	action	answer		\N	\N	0	80
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	ad5ffcc0-40af-44aa-9979-783e68351993	action	sleep	1000	\N	\N	0	85
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	09377fee-7cf1-4f3d-b6aa-e75e76065540	action	set	voicemail_action=save	\N	\N	0	90
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	b39df61c-4363-4c7c-a1b0-41a25e8079d1	action	set	voicemail_id=$1	\N	\N	0	95
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	6ff47291-d24c-41dd-a894-5e5839737216	action	set	voicemail_profile=default	\N	\N	0	100
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	e6839e91-c340-4698-bc83-1a388f12928e	action	lua	app.lua voicemail	\N	\N	0	105
60d6731b-e548-4175-ab3e-3fbb261e60f5	a7712974-9f5c-4ef6-a5c5-13cfe7136af3	037fd6e2-7fa0-4af4-870a-aefa416eb094	condition	destination_number	^\\*072$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	a7712974-9f5c-4ef6-a5c5-13cfe7136af3	b58f22f8-f6b3-4292-8555-294d86874946	action	set	direction=in	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	a7712974-9f5c-4ef6-a5c5-13cfe7136af3	b4c447cb-d361-46cb-bdde-c507852ba8a6	action	lua	dial_string.lua	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	00a48ebc-e877-44da-a25e-4169bf2e37be	00e8c59d-3ada-4796-a239-1ebb6ac325d3	condition	destination_number	^\\*073$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	00a48ebc-e877-44da-a25e-4169bf2e37be	fbd21348-04d0-4ce5-8a82-9240ae948fca	action	set	direction=out	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	00a48ebc-e877-44da-a25e-4169bf2e37be	b65250cc-27cb-41c4-ade1-e95773a54385	action	lua	dial_string.lua	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	620ec152-be51-4f59-863d-4bb94259e8c8	e392a9fa-67dc-4e15-bb6e-81530f52bec5	condition	${zrtp_secure_media_confirmed}	^true$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	620ec152-be51-4f59-863d-4bb94259e8c8	fc00a92c-2ba9-4f6d-98fc-d3864dff68c9	action	sleep	1000	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	620ec152-be51-4f59-863d-4bb94259e8c8	bee41343-fa2d-494d-a110-b771e297e89d	action	playback	misc/call_secured.wav	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	620ec152-be51-4f59-863d-4bb94259e8c8	91b8de63-02b4-40e4-9801-bdabb1355591	anti-action	eval	not_secure	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	1fe7dfc6-2c74-4e7a-ad49-339c974ff6be	bc2bbbdf-312e-4d01-897d-e986d3d98a10	condition	${sip_via_protocol}	tls	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	1fe7dfc6-2c74-4e7a-ad49-339c974ff6be	401fb118-ca04-49d0-b568-f0a5aadd25b7	condition	${sip_secure_media_confirmed}	^true$	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	1fe7dfc6-2c74-4e7a-ad49-339c974ff6be	25eeb8ce-0f2a-4464-8bf0-ae4b77a6e103	action	sleep	1000	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	1fe7dfc6-2c74-4e7a-ad49-339c974ff6be	f16599f2-9959-4f8a-a8c2-f4a1cc749365	action	playback	misc/call_secured.wav	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	1fe7dfc6-2c74-4e7a-ad49-339c974ff6be	a9237ee2-d7be-420c-b851-f34811cbe95b	anti-action	eval	not_secure	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	54d55a11-9a04-45a5-86c2-dd3234487e0a	0beb7aff-6e2c-47e9-9cf4-ce535981afec	condition	destination_number	^0$|^operator$	\N	\N	0	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	54d55a11-9a04-45a5-86c2-dd3234487e0a	5ac1d731-3649-4173-bdcf-57cb4fe60418	action	export	transfer_context=default	\N	\N	0	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	54d55a11-9a04-45a5-86c2-dd3234487e0a	7ba1cce2-f631-45c4-8260-7a7a28f7ecf5	action	bind_meta_app	4 ab s execute_extension::att_xfer XML features	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	54d55a11-9a04-45a5-86c2-dd3234487e0a	54d0b87c-5a0b-4339-81c2-cd0e6b43739b	action	bind_meta_app	5 ab s execute_extension::xfer_vm XML features	\N	\N	0	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	54d55a11-9a04-45a5-86c2-dd3234487e0a	a9a555c5-3a87-4f44-bf11-7af2024681c8	action	set	domain_name=default	\N	\N	0	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	54d55a11-9a04-45a5-86c2-dd3234487e0a	a9cf1659-ba91-44a8-ae11-c7cf35a80aa8	action	transfer	1001 XML default	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	0c411464-93d8-4544-a087-7089f0313af3	action	bind_meta_app	2 ab s record_session::$${recordings_dir}/${domain_name}/archive/${strftime(%Y)}/${strftime(%b)}/${strftime(%d)}/${uuid}.wav	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	6788993a-613c-43af-a3cd-343ae18798fc	action	bind_meta_app	3 ab s execute_extension::cf XML ${context}	\N	\N	0	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	fde0ee7c-a8c3-4587-90c1-6de0fc62b888	action	bind_meta_app	4 ab s execute_extension::att_xfer XML ${context}	\N	\N	0	40
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	aaa169d8-5ff7-46ba-91da-75427036f11c	action	set	hangup_after_bridge=true	\N	\N	0	45
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	3a42ef05-0d77-452d-917e-45f8f49abc11	action	set	continue_on_fail=true	\N	\N	0	50
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	76c07125-aaf0-49fc-9fb9-6adfb002dfa3	action	hash	insert/${domain_name}-call_return/${dialed_extension}/${caller_id_number}	\N	\N	0	55
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	0317e7dd-5cf8-41c7-a5cb-ebd3ba35ef68	action	hash	insert/${domain_name}-last_dial_ext/${dialed_extension}/${uuid}	\N	\N	0	60
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	ecb6be5a-4a61-4513-a3da-a092851338dc	action	set	called_party_call_group=${user_data(${dialed_extension}@${domain_name} var call_group)}	\N	\N	0	65
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	013bad5a-2286-45d3-9ba2-5d4f903f5c27	action	hash	insert/${domain_name}-last_dial/${called_party_call_group}/${uuid}	\N	\N	0	70
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	c2408fb5-e797-4e6e-a782-b1ee24760500	action	bridge	user/${destination_number}@${domain_name}	\N	\N	0	75
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	be660003-82c1-44ce-83e8-a8a6cce23b46	action	answer		\N	\N	0	80
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	926a19b5-46ff-43cc-a6ad-2a3e0771f6a6	action	sleep	1000	\N	\N	0	85
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	77da419a-41ce-4abd-bac4-58a40cfd1cc9	action	set	voicemail_action=save	\N	\N	0	90
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	691095fc-c8fc-4992-b20a-33cd2badf906	action	set	voicemail_id=$1	\N	\N	0	95
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	a612f004-6bac-4b7d-b877-6047fddc2797	action	set	voicemail_profile=default	\N	\N	0	100
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	30d175bd-8746-404e-8e6a-6624cf21a49a	action	lua	app.lua voicemail	\N	\N	0	105
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b626cbb2-684a-4256-a978-8f57f7a47e04	c065af84-82d0-4e2d-9993-e93a6a64a3c2	condition	destination_number	^\\*072$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b626cbb2-684a-4256-a978-8f57f7a47e04	5ad11ea1-f531-4a90-957d-782de09736e7	action	set	direction=in	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b626cbb2-684a-4256-a978-8f57f7a47e04	9038772a-7803-4af7-b229-3039cc8b2aaf	action	lua	dial_string.lua	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9b4ecefb-e961-433d-93f8-2343e1d52d14	0ed21e30-fc26-48ad-b205-c7e85fe35447	condition	destination_number	^\\*073$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9b4ecefb-e961-433d-93f8-2343e1d52d14	70f6e5dd-27af-49b5-8286-2f27a227d1dc	action	set	direction=out	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9b4ecefb-e961-433d-93f8-2343e1d52d14	e5c28c7b-f7ab-4122-84c4-9e7af09936b8	action	lua	dial_string.lua	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	709af092-f796-4de2-a779-d7bdd9acc279	0b703d91-a053-4796-aa0c-637a6eeb5ef7	condition	context	public	\N	\N	\N	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	709af092-f796-4de2-a779-d7bdd9acc279	b3c438b8-37ee-4a4f-b8f7-1ba48affd730	condition	destination_number	48598881023	\N	\N	\N	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	709af092-f796-4de2-a779-d7bdd9acc279	f4e6b0b0-d47b-48c9-bf99-12c288aacdc6	action	transfer	118 XML sip.c-call.eu	\N	\N	\N	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	66ed2e61-1ae0-4c0f-938e-13a5a13f5d3b	b166cfee-353e-485d-97ef-841ed23394b9	condition			\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	66ed2e61-1ae0-4c0f-938e-13a5a13f5d3b	34a4966d-a9f1-4f2f-b42d-34960dce52d3	action	set	user_exists=${user_exists id ${destination_number} ${domain_name}}	\N	true	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	66ed2e61-1ae0-4c0f-938e-13a5a13f5d3b	723d5795-9b8e-4830-8257-2a5c5bba0d3e	condition	${user_exists}	^true$	\N	\N	1	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	66ed2e61-1ae0-4c0f-938e-13a5a13f5d3b	d89ad3a1-476d-4b8f-bc28-0e5e98795846	action	set	extension_uuid=${user_data ${destination_number}@${domain_name} var extension_uuid}	\N	\N	1	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1d5aaa24-14e8-43ea-9020-ee10bbcdeff4	b9121c80-0ced-4a22-a9d3-68a5efebf1da	condition	${call_direction}	^(inbound|outbound|local)$	never	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1d5aaa24-14e8-43ea-9020-ee10bbcdeff4	12f309a2-05a0-4693-b248-8495a23c0269	anti-action	set	call_direction=local	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1d5aaa24-14e8-43ea-9020-ee10bbcdeff4	81511015-88ff-4be2-b1ad-606bb5b69791	condition	${user_exists}	^false$	\N	\N	1	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1d5aaa24-14e8-43ea-9020-ee10bbcdeff4	bffe4a22-afd5-47c5-8f5f-a70a5ba84974	condition	destination_number	^\\d{7,20}$	\N	\N	1	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1d5aaa24-14e8-43ea-9020-ee10bbcdeff4	54808487-a4c9-410e-b2e3-638a4e3eee6b	action	set	call_direction=outbound	\N	\N	1	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	f2896ce9-6bee-46ed-8b0a-2262d6970535	5e8a3d52-e594-44a4-b911-bd5b4cf1e979	condition			\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	f2896ce9-6bee-46ed-8b0a-2262d6970535	78d25473-6383-4a21-b039-97819518ac7c	action	export	origination_callee_id_name=${destination_number}	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	f2896ce9-6bee-46ed-8b0a-2262d6970535	ec49912c-fc11-45d6-bea4-d65ad9cab686	action	set	RFC2822_DATE=${strftime(%a, %d %b %Y %T %z)}	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ea39f721-ca6c-40e5-b03e-df826c4f3dfd	8cbf7b98-4f12-4ded-8c10-24d930293b5e	condition			\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ea39f721-ca6c-40e5-b03e-df826c4f3dfd	e1cd36fc-2830-4052-b56c-dddd5ec4bc2c	action	lua	app.lua is_local	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fde54f5d-e465-46cb-a3ea-40401c74df9e	c1f27aaa-66b3-4b13-b66e-8e8f95bd59bb	condition			\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fde54f5d-e465-46cb-a3ea-40401c74df9e	c727ba1b-4d78-48a1-996f-03bd47763014	action	lua	app.lua call_block	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	48b788e7-b194-4aae-8ba6-c147516f5e6c	condition			\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	5840327a-0e46-47ff-b2c1-18605bced7e5	action	set	user_record=${user_data ${destination_number}@${domain_name} var user_record}	\N	true	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	a77d30e8-c88e-4cef-a5a8-0a6a7ef72e1a	action	set	from_user_exists=${user_exists id ${sip_from_user} ${sip_from_host}}	\N	true	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	104dd1db-c546-49e3-b92a-59d10a94ca37	condition	${user_exists}	^true$	never	\N	1	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	cd410c7d-6f98-4117-88d6-1d96b05fcae1	547d6281-6359-4152-8b5a-5f27bda9eade	condition			\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	cd410c7d-6f98-4117-88d6-1d96b05fcae1	27c947bc-832f-45ad-82c6-e6dde8bd9e18	action	set	user_exists=${user_exists id ${destination_number} ${domain_name}}	\N	true	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	cd410c7d-6f98-4117-88d6-1d96b05fcae1	c86d5938-26cc-4a55-8418-334971aa24ce	condition	${user_exists}	^true$	\N	\N	1	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	cd410c7d-6f98-4117-88d6-1d96b05fcae1	15ba3b7d-8d3e-4f6d-b69e-038442a7ba0f	action	set	extension_uuid=${user_data ${destination_number}@${domain_name} var extension_uuid}	\N	\N	1	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ca2d180c-599e-4e64-87fc-7ea36586fbc3	406882e0-68d5-46d1-8583-b5e04a9600ce	condition	${call_direction}	^(inbound|outbound|local)$	never	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ca2d180c-599e-4e64-87fc-7ea36586fbc3	2f126ad0-fd09-46a0-b3d1-7257fb1710b1	anti-action	set	call_direction=local	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ca2d180c-599e-4e64-87fc-7ea36586fbc3	81739988-ec07-4667-9624-8bd6fc5fd2c0	condition	${user_exists}	^false$	\N	\N	1	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ca2d180c-599e-4e64-87fc-7ea36586fbc3	a3dd4b32-9cb9-4c19-9229-a703ca61c0f6	condition	destination_number	^\\d{7,20}$	\N	\N	1	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ca2d180c-599e-4e64-87fc-7ea36586fbc3	c0072d7f-e0c3-4572-b934-f4600fd56e41	action	set	call_direction=outbound	\N	\N	1	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	66beee43-444e-4bb5-b351-a56483c823eb	04da1524-0e67-4b33-a349-802025bfc746	condition			\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	66beee43-444e-4bb5-b351-a56483c823eb	ae2fab96-6abb-48fa-9531-e4ebbfb23250	action	export	origination_callee_id_name=${destination_number}	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	66beee43-444e-4bb5-b351-a56483c823eb	29b419c3-6267-4548-b0b4-ccdfad0b933c	action	set	RFC2822_DATE=${strftime(%a, %d %b %Y %T %z)}	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bab0214c-e8a5-43e8-9cd9-7f523574f346	977c8309-91dc-424f-86aa-2804461e8588	condition			\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bab0214c-e8a5-43e8-9cd9-7f523574f346	e3b5e640-2461-4915-a0a0-77f979c76746	action	lua	app.lua is_local	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6f6c99c7-4289-4633-9d25-61825c963fa5	85e9b80d-882b-4e16-b08e-7492946f7751	condition			\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6f6c99c7-4289-4633-9d25-61825c963fa5	ecf0b414-b039-401f-bb4d-3c526860074c	action	lua	app.lua call_block	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	72e6e542-b13b-4ec7-a4a1-52a3c206a8c8	condition			\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	ef1b3baa-83f1-421d-b767-41be66d711c6	action	set	user_record=${user_data ${destination_number}@${domain_name} var user_record}	\N	true	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	a1b86510-27a1-445f-86ec-09b94685fc8b	action	set	from_user_exists=${user_exists id ${sip_from_user} ${sip_from_host}}	\N	true	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	62df6e4f-828c-4478-8e57-a3d1d53af769	condition	${user_exists}	^true$	never	\N	1	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	377d4613-7643-405f-a18f-b4ec9a6fabc7	condition	${user_record}	^all$	never	\N	1	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	375c3634-ecbc-4fe2-aa3c-4308cf181c45	action	set	record_session=true	\N	true	1	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	e2fb27d5-7940-41a0-b85e-b749dcd1e49d	condition	${user_exists}	^true$	never	\N	2	45
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	2fb3066a-cf7e-4aea-a5f9-96b61f99b34d	condition	${call_direction}	^inbound$	never	\N	2	50
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	07e0e22c-6214-4727-8dc0-246646649c7b	condition	${user_record}	^inbound$	never	\N	2	55
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	337693e2-665a-491a-bd6e-75083ce43012	action	set	record_session=true	\N	true	2	60
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	f8fa4dd9-9ac6-4804-b1ae-8499743d80f6	condition	${user_exists}	^true$	never	\N	3	70
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	1743d1c6-d6b6-4304-9ddd-4bc9245a2757	condition	${call_direction}	^outbound$	never	\N	3	75
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	fc79dcaa-6534-47fb-94bf-10a45e0aa2d2	condition	${user_record}	^outbound$	never	\N	3	80
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	2adfe8d8-5309-4918-976e-2e8b110e09d8	action	set	record_session=true	\N	true	3	85
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	c55f1497-7949-4501-b702-d2ef7ba5685d	condition	${user_exists}	^true$	never	\N	4	95
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	a4b47ddc-07d0-40e1-86ce-31a93bead1fe	condition	${call_direction}	^local$	never	\N	4	100
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	246d3c81-2078-4683-92b3-b43efcd07c28	condition	${user_record}	^local$	never	\N	4	105
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	6e1d2fd5-07db-4ecb-a0e5-718d3bf5a758	action	set	record_session=true	\N	true	4	110
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	e1afc576-a245-4a10-bd20-8246d33c8eab	condition	${from_user_exists}	^true$	never	\N	5	120
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	eac75b3a-4fb0-4a0f-8024-96c720baf7f7	action	set	from_user_record=${user_data ${sip_from_user}@${sip_from_host} var user_record}	\N	true	5	125
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	7413b4eb-702b-4a0e-a5ef-f1d1188c311c	condition	${from_user_exists}	^true$	never	\N	6	135
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	f1a8c23f-ed2f-4137-b520-e947e180ec21	condition	${from_user_record}	^all$	never	\N	6	140
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	329a9f2c-81d4-478a-85d7-b8ffc64df357	action	set	record_session=true	\N	true	6	145
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	80003580-0895-4454-8124-ff83615ca269	condition	${from_user_exists}	^true$	never	\N	7	155
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	0ca7b0e8-9d67-4970-8dc3-cd4b2b58db99	condition	${call_direction}	^inbound$	never	\N	7	160
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	265c4f00-0265-461d-af17-ee7da50cba94	condition	${from_user_record}	^inbound$	never	\N	7	165
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	47a1c108-c2f8-430b-a689-dbf95aa1d2ab	action	set	record_session=true	\N	true	7	170
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	076ef560-5869-4d87-a026-f331093d20d9	condition	${from_user_exists}	^true$	never	\N	8	180
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	439c5ae9-44d9-4bd7-a2b1-7f22ff8040b1	condition	${call_direction}	^outbound$	never	\N	8	185
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	dc896f68-42a5-4627-b05c-26b206121d30	condition	${from_user_record}	^outbound$	never	\N	8	190
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	86a560b6-b073-40d0-a80f-98a2f26b41fd	action	set	record_session=true	\N	true	8	195
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	b17f9605-6293-4790-aef8-2b2ea4c457f1	condition	${from_user_exists}	^true$	never	\N	9	205
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	69da4a5c-b443-43ac-adea-e6881804650f	condition	${call_direction}	^local$	never	\N	9	210
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	1658e594-8329-4a67-bd9c-fcbc0641dc24	condition	${from_user_record}	^local$	never	\N	9	215
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	93c3d43e-451d-492c-a42e-ec4641543f42	action	set	record_session=true	\N	true	9	220
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	60db1b26-034d-4628-878c-0e336955e877	condition	${record_session}	^true$	\N	\N	10	230
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	4f71eed9-aad1-44b4-a029-0c2146edf9a9	action	set	api_on_answer=uuid_record ${uuid} start ${recordings_dir}/${domain_name}/archive/${strftime(%Y)}/${strftime(%b)}/${strftime(%d)}/${uuid}.${record_ext}	\N	\N	10	235
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	27c9d813-de2f-4891-bd6d-01b75cd87551	1afa097e-3311-4f3c-a480-116b43e5976c	condition	destination_number	^\\*22$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	27c9d813-de2f-4891-bd6d-01b75cd87551	7744ac49-e0b0-4d2a-b39c-468c34a9ad2c	action	lua	app.lua user_status	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	30dfceb7-a3e3-4464-ba6e-a6b559c219cc	2c415d31-b8a9-4795-98ad-b28a4b02a088	condition	destination_number	^\\*886$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	30dfceb7-a3e3-4464-ba6e-a6b559c219cc	3a4eb741-1dd9-4703-abef-c46aac3dd565	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	30dfceb7-a3e3-4464-ba6e-a6b559c219cc	95af0ecb-d447-4977-84d3-d1efd61d7e57	action	lua	intercept.lua	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	51036f37-3403-4916-9476-4eec18b90393	23accb64-cf34-48e5-85ed-6549636a7f8c	condition	destination_number	^\\*8$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	51036f37-3403-4916-9476-4eec18b90393	bf8d7ee7-8180-4aa8-8f57-afbbd368dd22	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	51036f37-3403-4916-9476-4eec18b90393	0b536fd4-3110-49be-9d0f-6f16c03ede3a	action	lua	intercept_group.lua	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f31459d6-c0b9-4f6a-a426-27e50a5939c3	acbd8205-1672-4cf9-94cd-d65cd6e5db47	condition	destination_number	^(redial|\\*870)$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f31459d6-c0b9-4f6a-a426-27e50a5939c3	ae9cabbb-db00-4d40-9ca7-d586febb3c8e	action	transfer	${hash(select/${domain_name}-last_dial/${caller_id_number})}	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	89f73b9b-66ff-40b7-b477-45a5e294364b	condition	destination_number	^\\*724$	never	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	4959cd7b-c075-4bc5-89b8-4023c838eb34	condition	${sip_h_Referred-By}	sip:(.*)@.*	never	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	e4775b6c-68dc-42f0-9787-b00d0724ecc3	action	set	referred_by_user=$1	\N	true	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	6e2ff407-498c-49cd-9cc8-c65b9d7ec763	action	log	INFO referred_by_user is [${referred_by_user}]	\N	true	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	d0aa05f8-e170-42c1-a2d0-51d7425529b3	action	log	INFO sip_h_Referred-By is [${sip_h_Referred-By}]	\N	true	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	1225b65b-4e03-47ee-91fd-cafb1516e602	anti-action	set	referred_by_user=false	\N	true	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	e7ff0c0b-ac1c-451d-a6f1-5a8cb97e2e81	anti-action	log	INFO referred_by_user is [${referred_by_user}]	\N	true	0	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	9314c762-b1f9-4244-8d77-37ea23a25348	anti-action	log	INFO sip_h_Referred-By is [${sip_h_Referred-By}]	\N	true	0	40
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	0bbf8cc2-15bb-4632-8683-9b26bc1b973b	condition	destination_number	^\\*724$	\N	\N	1	50
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	125c6234-db4a-4c20-b0e9-9efc058834cc	condition	${referred_by_user}	false	never	\N	1	55
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	0b9983fb-008e-49cf-a599-a73c14562deb	action	set	was_transfered_to_page=false	\N	true	1	60
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	458af03f-68ff-4528-8a3f-e065dd4954e1	action	log	INFO was_transfered_to_page is [${was_transfered_to_page}]	\N	true	1	65
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	73ee5a71-b60e-43c6-9efe-59f361701f54	anti-action	set	was_transfered_to_page=true	\N	true	1	70
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	9ed5a0da-9710-4a3f-9082-c669194d10fc	anti-action	log	INFO was_transfered_to_page is [${was_transfered_to_page}]	\N	true	1	75
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	af24cbd5-dad9-452c-ad6d-ba71d1b51472	condition	destination_number	^\\*724$	never	\N	2	85
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	3b03c71b-b83e-4531-936c-42c072c9b35d	action	log	INFO was_transfered_to_page is [${was_transfered_to_page}]	\N	\N	2	90
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	bd7f6ace-49b8-470a-978c-a5c6d55371f2	condition	${was_transfered_to_page}	true	never	\N	3	100
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	4000d741-90e2-435a-8b8b-0e9615162165	action	transfer	${referred_by_user} XML ${context}	\N	\N	3	105
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	d38da5b9-3fc6-4ea4-94e7-a20c84385a6b	anti-action	set	caller_id_name=Page	\N	\N	3	110
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	744c19a3-8c12-49cf-9842-63dde1a372f0	anti-action	set	caller_id_number=	\N	\N	3	115
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	a2fcabfb-78ff-453b-87ca-1c073dc863e9	anti-action	set	pin_number=	\N	\N	3	120
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	151bec05-5161-4402-a346-24f2525e447c	anti-action	set	extension_list=2000-2002	\N	\N	3	125
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	ab2115d2-c6ee-4524-a173-670c2ad05f81	anti-action	lua	page.lua	\N	\N	3	130
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6871595a-f860-4fdb-86d5-92e818102b1f	44414bba-3c9b-457e-95df-a8d57fa002b0	condition	destination_number	^\\*88(\\d{2,7})$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6871595a-f860-4fdb-86d5-92e818102b1f	52fb2ec1-509a-4d48-b037-194132ba863c	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6871595a-f860-4fdb-86d5-92e818102b1f	e1c9d3c1-ca34-4b7e-b6dc-215a095f233a	action	set	pin_number=22776081	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6871595a-f860-4fdb-86d5-92e818102b1f	cf691a99-cdcb-40c6-9b79-3454c921bfb9	action	lua	eavesdrop.lua $1	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	7579668a-b69a-40d4-9fc1-8b53556c831b	752f762b-a332-4489-b328-9c52d931bcf5	condition	destination_number	^\\*67(\\d+)$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	7579668a-b69a-40d4-9fc1-8b53556c831b	4d3df486-8228-4754-ae23-9816bc550ea6	action	privacy	full	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	7579668a-b69a-40d4-9fc1-8b53556c831b	eccfe9e6-a086-42a2-88f7-3d515bf23f78	action	set	sip_h_Privacy=id	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	7579668a-b69a-40d4-9fc1-8b53556c831b	38ad7418-b6fc-404c-b61d-9e6700ee6300	action	set	privacy=yes	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	7579668a-b69a-40d4-9fc1-8b53556c831b	759aba2b-fc7b-4d30-8150-70b540b7abf9	action	transfer	$1 XML ${context}	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	eb7ef69a-5333-42e5-a181-021d40b7c6af	dd8f3587-4369-49a8-9d84-9b6dc08bf76a	condition	destination_number	^\\*69$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	eb7ef69a-5333-42e5-a181-021d40b7c6af	1b7c9b27-bf41-4ee3-94ac-a1d4d5e5e60f	action	transfer	${hash(select/${domain_name}-call_return/${caller_id_number})}	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fe423c2f-114d-40bf-8c7b-79c14f0dbaaa	8c3fc03b-fce7-4202-8fc1-161ae4012fb4	condition	destination_number	^\\*\\*(\\d+)$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fe423c2f-114d-40bf-8c7b-79c14f0dbaaa	7a125f4c-58ad-4b08-9ba8-7ed5875d687a	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fe423c2f-114d-40bf-8c7b-79c14f0dbaaa	15c98cd6-21e6-4874-a3af-0d37e7513ce8	action	lua	intercept.lua $1	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	38f2ab98-d586-4d5f-8813-00e59b495dcc	057e299c-000d-4c32-9bb8-c7b2d0935376	condition	destination_number	^dx$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	38f2ab98-d586-4d5f-8813-00e59b495dcc	d2b94e46-4288-46bc-a417-82ff564ddf7b	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	38f2ab98-d586-4d5f-8813-00e59b495dcc	e8c032cd-ae94-4ab3-a0af-448f297895e8	action	read	11 11 'tone_stream://%(10000,0,350,440)' digits 5000 #	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	38f2ab98-d586-4d5f-8813-00e59b495dcc	537b7460-9770-4638-9d62-1d5336dd2e24	action	transfer	-bleg ${digits}	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d5e4f1df-5af2-4f50-91c5-e0cf7069cf76	5ea466c6-6d1d-4adc-aee9-aff832497e88	condition	destination_number	^\\*8(\\d{2,7})$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d5e4f1df-5af2-4f50-91c5-e0cf7069cf76	1db7d6ec-8851-4622-8b10-940c7db0a4ea	action	set	extension_list=$1	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d5e4f1df-5af2-4f50-91c5-e0cf7069cf76	71ba8b7b-2186-4978-b8bd-990bfaab57b4	action	set	pin_number=	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d5e4f1df-5af2-4f50-91c5-e0cf7069cf76	83051299-d2fd-4d61-b707-e3566deefc85	action	set	mute=true	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d5e4f1df-5af2-4f50-91c5-e0cf7069cf76	f47643ae-01ff-48fd-b3b5-059e917817bc	action	lua	page.lua	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4e53e1c0-20d9-404d-8203-d3eda7ebce80	985bcc48-bd06-48ea-a80d-c970ad3596ba	condition	destination_number	^att_xfer$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4e53e1c0-20d9-404d-8203-d3eda7ebce80	79960c6d-3a59-4869-897e-cf41755cfa6b	action	read	2 6 'tone_stream://%(10000,0,350,440)' digits 30000 #	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4e53e1c0-20d9-404d-8203-d3eda7ebce80	1d123741-1ccc-4737-a146-6bc11dda486a	action	set	origination_cancel_key=#	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4e53e1c0-20d9-404d-8203-d3eda7ebce80	328d299f-a6e7-4d5f-9abb-947ac6fd0e77	action	set	domain_name=${transfer_context}	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4e53e1c0-20d9-404d-8203-d3eda7ebce80	0d06d2f6-b16f-4240-8e75-cfabf9505d19	action	att_xfer	user/${digits}@${transfer_context}	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bb6a3c6c-74f7-4829-8acd-083acbbb273d	4cbe9c8c-24e0-4c50-81f7-c135a6242379	condition	username	^${caller_id_number}$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bb6a3c6c-74f7-4829-8acd-083acbbb273d	2858384c-c26e-4f56-9eee-37d292bb0e25	condition	destination_number	^${caller_id_number}$	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bb6a3c6c-74f7-4829-8acd-083acbbb273d	540878ba-107b-46ad-aa0c-cd587570a5bf	action	answer		\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bb6a3c6c-74f7-4829-8acd-083acbbb273d	072018c2-deca-4c09-9f70-8f36930317ac	action	sleep	1000	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bb6a3c6c-74f7-4829-8acd-083acbbb273d	246851dd-76ee-4859-9cf3-966d2fdd4b89	action	set	voicemail_action=check	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bb6a3c6c-74f7-4829-8acd-083acbbb273d	3cbc596b-a7e2-4a6c-8de2-a00747aaa77b	action	set	voicemail_id=${caller_id_number}	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bb6a3c6c-74f7-4829-8acd-083acbbb273d	a0d5ceea-f0e5-4652-93d1-48e2ad982965	action	set	voicemail_profile=default	\N	\N	0	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bb6a3c6c-74f7-4829-8acd-083acbbb273d	eb78eb81-59f1-45b0-b796-0afa093a5184	action	lua	app.lua voicemail	\N	\N	0	40
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f3baf3fc-2691-415c-9804-61afa7e4b0b9	cbaba20c-7f43-4110-ad19-fa30246f1a4d	condition	destination_number	^\\*99(\\d{2,7})$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f3baf3fc-2691-415c-9804-61afa7e4b0b9	00ecc675-e968-420d-9343-80906b2823f1	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f3baf3fc-2691-415c-9804-61afa7e4b0b9	d0a28be6-460f-43d9-8d35-3ab5ec918fcb	action	sleep	1000	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f3baf3fc-2691-415c-9804-61afa7e4b0b9	3b996f52-4016-4883-abe3-b86a55684251	action	set	voicemail_action=save	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f3baf3fc-2691-415c-9804-61afa7e4b0b9	6de0be62-5543-46be-9586-e2d3e21f9099	action	set	voicemail_id=$1	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f3baf3fc-2691-415c-9804-61afa7e4b0b9	691a8e7c-376e-4db1-adce-1ed460b3e848	action	set	voicemail_profile=default	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f3baf3fc-2691-415c-9804-61afa7e4b0b9	1e04611c-5f7c-45da-905d-6437fdde4029	action	lua	app.lua voicemail	\N	\N	0	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	a2f8ca40-15c6-438a-a116-fb21b48670ad	151f8eaa-5eed-4060-bd91-18a725c306b8	condition	destination_number	^vmain$|^\\*4000$|^\\*98$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	a2f8ca40-15c6-438a-a116-fb21b48670ad	bd4f6a0d-bfb6-49b7-b6f9-6d4f76c20eaf	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	a2f8ca40-15c6-438a-a116-fb21b48670ad	762752fc-df68-43f9-a151-02114b7f9d15	action	sleep	1000	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	a2f8ca40-15c6-438a-a116-fb21b48670ad	43ade001-c5cf-4ab9-8c07-f9983b90f9ae	action	set	voicemail_action=check	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	a2f8ca40-15c6-438a-a116-fb21b48670ad	76f32a8f-5edd-4ef6-88d4-29c15b5ce700	action	set	voicemail_profile=default	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	a2f8ca40-15c6-438a-a116-fb21b48670ad	2bdebb2a-4c4c-45f8-b74f-ca4c3b29ee8d	action	lua	app.lua voicemail	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	0f16bb3f-cb3b-4907-8580-ebed46649a85	5fa7022f-d51b-4bb2-b1a6-88966340810e	condition	destination_number	^xfer_vm$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	0f16bb3f-cb3b-4907-8580-ebed46649a85	d0f8da4a-4fc7-4144-bedf-e1c16d25746a	action	read	2 6 'tone_stream://%(10000,0,350,440)' digits 30000 #	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	0f16bb3f-cb3b-4907-8580-ebed46649a85	20cbb323-f84e-4c8c-b89c-132bd760f73a	action	set	origination_cancel_key=#	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	0f16bb3f-cb3b-4907-8580-ebed46649a85	684bb88c-0410-4a42-af9c-ac4d089212af	action	set	domain_name=${transfer_context}	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	0f16bb3f-cb3b-4907-8580-ebed46649a85	8dd5c2b0-ed4f-4cb4-8536-bc3d08e806b3	action	export	domain_name=${transfer_context}	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	0f16bb3f-cb3b-4907-8580-ebed46649a85	c6310c72-4a2e-4d23-8143-b441ffdc372e	action	transfer	-bleg *99${digits} XML ${transfer_context}	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	14c6ab00-c40f-4000-8d07-73e9392a00b2	245882d5-49d9-4921-bf24-e646bc6857f4	condition	destination_number	^is_transfer$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	14c6ab00-c40f-4000-8d07-73e9392a00b2	7ec3654f-9f5d-4376-beca-63611fe0c395	condition	${digits}	^(\\d+)$	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	14c6ab00-c40f-4000-8d07-73e9392a00b2	2c0005f8-756f-40ab-8355-79074130d81b	action	transfer	-aleg ${digits} XML ${context}	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	14c6ab00-c40f-4000-8d07-73e9392a00b2	286cc373-1e8a-412e-ac89-e4f9df07e399	anti-action	eval	cancel transfer	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ebb1fcaf-03ec-46e0-a1a2-3989acc6cd31	2383bb7f-0f24-46b9-8781-b591a2b99869	condition	destination_number	^\\*97$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ebb1fcaf-03ec-46e0-a1a2-3989acc6cd31	2c564a30-6a0b-4ddd-8297-4d99729f32b8	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ebb1fcaf-03ec-46e0-a1a2-3989acc6cd31	42f67292-8922-4fea-98a9-53a985039408	action	sleep	1000	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ebb1fcaf-03ec-46e0-a1a2-3989acc6cd31	03cb8069-2a70-4db4-a612-4d46d3604772	action	set	voicemail_action=check	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ebb1fcaf-03ec-46e0-a1a2-3989acc6cd31	269d5662-cbc4-49ed-bb57-a4ca40455014	action	set	voicemail_id=${caller_id_number}	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ebb1fcaf-03ec-46e0-a1a2-3989acc6cd31	df9efa33-b998-4de5-8a3e-8e260da04b6c	action	set	voicemail_profile=default	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ebb1fcaf-03ec-46e0-a1a2-3989acc6cd31	4d3331d0-5213-40de-b5d8-2bace4216886	action	lua	app.lua voicemail	\N	\N	0	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4b59d1a9-6052-4c41-ae75-62b729c8559f	c978558f-0662-4dd0-8201-f48ce8322cc9	condition	destination_number	^cf$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4b59d1a9-6052-4c41-ae75-62b729c8559f	496b2f32-8c63-453e-bd12-81a1cee3b6dd	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4b59d1a9-6052-4c41-ae75-62b729c8559f	08323f25-1aa9-465c-81af-e006e2e7ce63	action	transfer	-both 30${dialed_extension:2} XML ${context}	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	2d227581-5b0e-4f35-862a-a5093b2d2c3a	d88e2516-6bc0-41f1-9835-63adb5c5f722	condition	destination_number	^\\*9195$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	2d227581-5b0e-4f35-862a-a5093b2d2c3a	13c38124-72e5-48b9-bebb-f42af2579ccd	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	2d227581-5b0e-4f35-862a-a5093b2d2c3a	587c783b-642f-4f24-ab1f-1aa211b42eb5	action	delay_echo	5000	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e4c191df-b205-4621-b559-daba14750f84	df9a67a5-70f5-47a4-9689-f6a909105907	condition	destination_number	^\\*9196$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e4c191df-b205-4621-b559-daba14750f84	11c46584-204a-4550-9d82-31106a8ebb36	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e4c191df-b205-4621-b559-daba14750f84	0617af8f-8cbd-4283-873f-2c0f8508b5b7	action	echo		\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4a3e487d-202c-48de-9d0e-e5c02c6e9199	fee90d9c-e690-4fb5-bcf7-338829c57654	condition	destination_number	^(10[01][0-9])$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4a3e487d-202c-48de-9d0e-e5c02c6e9199	c3df109b-8075-406f-87e7-e4a8e37b0859	action	set	transfer_ringback=$${hold_music}	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4a3e487d-202c-48de-9d0e-e5c02c6e9199	6b3da834-64a4-4ec1-907e-c553f91c7486	action	answer		\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4a3e487d-202c-48de-9d0e-e5c02c6e9199	b8f99c9e-21e3-4c12-b85a-3ab71c09c464	action	sleep	1500	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4a3e487d-202c-48de-9d0e-e5c02c6e9199	7b2482b9-767f-44de-aaca-eb9adf8f6f94	action	playback	ivr/ivr-hold_connect_call.wav	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4a3e487d-202c-48de-9d0e-e5c02c6e9199	6310a4e1-910e-4979-be01-9b32d45cca97	action	transfer	$1 XML ${context}	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	97640de5-c50f-42aa-bae3-e2ab77e15b2d	7dc05a2b-bdbd-4de4-a5eb-7b67755a7d08	condition	${zrtp_secure_media_confirmed}	^true$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	97640de5-c50f-42aa-bae3-e2ab77e15b2d	dcd6ca3c-0c1b-4ab9-96b0-c2fcb0ed97ec	action	sleep	1000	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	97640de5-c50f-42aa-bae3-e2ab77e15b2d	7d76b612-0806-4a03-bb04-9cb95ebb0521	action	playback	misc/call_secured.wav	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	97640de5-c50f-42aa-bae3-e2ab77e15b2d	e4aacf77-2c77-45f4-a806-e52467528e0e	anti-action	eval	not_secure	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	66761ab2-9158-4cd5-bf53-804a28d475ee	7c7f2028-9c4f-463d-80de-3a9d650d659f	condition	destination_number	^\\*9197$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	66761ab2-9158-4cd5-bf53-804a28d475ee	eaa56f9a-67ae-42ba-a124-b51449114394	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	66761ab2-9158-4cd5-bf53-804a28d475ee	9447b6af-c647-4d91-83dc-c95db62e014f	action	playback	{loops=-1}tone_stream://%(251,0,1004)	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6644feaf-af62-4862-8b4a-c697dd84c34d	d4c2b096-d239-459b-b529-ccceec26fc84	condition	${sip_via_protocol}	tls	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6644feaf-af62-4862-8b4a-c697dd84c34d	1e1fa892-57bd-474a-9a80-e74e8d034667	condition	${sip_secure_media_confirmed}	^true$	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6644feaf-af62-4862-8b4a-c697dd84c34d	9f6ea410-16e7-4874-a898-d5382f27aad8	action	sleep	1000	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6644feaf-af62-4862-8b4a-c697dd84c34d	4eadb503-1e13-404f-a0fd-ac409fed3699	action	playback	misc/call_secured.wav	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6644feaf-af62-4862-8b4a-c697dd84c34d	d95eb1d6-fda8-4d04-8c2c-2603feedff1c	anti-action	eval	not_secure	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b8ec018-a99d-4e31-81e2-3e58703afdd5	0bd048f8-ab9f-4376-ab87-c0e6fa5ef35f	condition	destination_number	^\\*9198$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b8ec018-a99d-4e31-81e2-3e58703afdd5	17353803-d6a2-48b7-b0b6-08d07865d22b	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b8ec018-a99d-4e31-81e2-3e58703afdd5	31ecdf93-0ad1-4c2d-b997-ff36c5533d69	action	playback	{loops=10}tone_stream://path=${base_dir}/conf/tetris.ttml	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	26c5e134-9773-4ab3-b52a-41d584a26daa	5353f2e1-8c8a-4938-b02c-e1dfbd1b100a	condition	destination_number	^\\*9664$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	26c5e134-9773-4ab3-b52a-41d584a26daa	7831b608-b8e1-4bdb-ac80-1f48d189f66f	condition	${sip_has_crypto}	^(AES_CM_128_HMAC_SHA1_32|AES_CM_128_HMAC_SHA1_80)$	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	26c5e134-9773-4ab3-b52a-41d584a26daa	99d8720f-90c2-4de6-abd3-951ad09e3fcd	action	answer		\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	26c5e134-9773-4ab3-b52a-41d584a26daa	36a46b07-3e3b-4753-ad56-75b7532f1706	action	execute_extension	is_secure XML features	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	26c5e134-9773-4ab3-b52a-41d584a26daa	d663b105-fae4-4f21-8a64-5e49779411c8	action	playback	$${hold_music}	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	26c5e134-9773-4ab3-b52a-41d584a26daa	e8b1d86d-b872-4532-b7a4-4846d6d8949f	anti-action	set	zrtp_secure_media=true	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	26c5e134-9773-4ab3-b52a-41d584a26daa	c56f55f8-aad1-4108-bfd8-29d227d1af4b	anti-action	answer		\N	\N	0	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	26c5e134-9773-4ab3-b52a-41d584a26daa	c3bfdb56-d096-4936-bf12-85fa1d9169ef	anti-action	playback	silence_stream://2000	\N	\N	0	40
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	26c5e134-9773-4ab3-b52a-41d584a26daa	64efd58c-7cf2-4c56-963f-8272fb7827a1	anti-action	execute_extension	is_zrtp_secure XML features	\N	\N	0	45
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	26c5e134-9773-4ab3-b52a-41d584a26daa	dd806cc6-419b-407c-bae8-33bd7c95ac19	anti-action	playback	$${hold_music}	\N	\N	0	50
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b01ee25e-973a-4487-a19e-f5880be8566c	d4dae09f-2016-4706-b9bf-f2205b006140	condition	destination_number	^\\*(732)$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b01ee25e-973a-4487-a19e-f5880be8566c	2177ef17-1e9a-4300-bc0e-096838d58628	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b01ee25e-973a-4487-a19e-f5880be8566c	afafd94c-0fa2-4b28-8b88-43f9bd551b62	action	set	pin_number=01967747	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b01ee25e-973a-4487-a19e-f5880be8566c	b6676311-f9c8-45e4-9e56-ac0ac2f9ca84	action	set	recording_slots=true	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b01ee25e-973a-4487-a19e-f5880be8566c	ee99ad1a-6b52-48a7-8770-bb7cb0013003	action	set	recording_prefix=recording	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b01ee25e-973a-4487-a19e-f5880be8566c	86ccd9e4-4e44-44e0-85fe-ffb0b4d0ddc4	action	lua	recordings.lua	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	80eaf8fe-134c-4a8d-b3dc-f42ce47012d5	a9cd5033-48cf-499e-bf6a-8ca40eb95e72	condition	destination_number	^\\*9(888|8888|1616|3232)$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	80eaf8fe-134c-4a8d-b3dc-f42ce47012d5	6491dcd0-e039-437e-bbed-708caaae2c81	action	export	hold_music=silence	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	80eaf8fe-134c-4a8d-b3dc-f42ce47012d5	01916eea-8833-4faf-82e8-3ffbcfc13776	action	bridge	sofia/${use_profile}/$1@conference.freeswitch.org	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9cb26349-f2fa-4e91-910d-94015679b89f	f7e8f0a6-91c6-4bec-843a-f8bd169fa80c	condition	destination_number	^\\*(3472)$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9cb26349-f2fa-4e91-910d-94015679b89f	c80e4133-d8ce-4e62-a358-334cccb2ccc2	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9cb26349-f2fa-4e91-910d-94015679b89f	b34a48b8-7c88-4e34-987d-e2ec50e592ed	action	set	pin_number=39269055	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9cb26349-f2fa-4e91-910d-94015679b89f	e8bcc201-0c9e-4160-b773-30b4210a9dfa	action	set	dialplan_context=${context}	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9cb26349-f2fa-4e91-910d-94015679b89f	b350e3ba-2d28-4a9a-9887-db68843d1705	action	lua	disa.lua	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	0105f5cd-d66a-4aeb-9a33-5e235ff68751	64ad2cac-e500-4b8e-ba29-23cf536a7012	condition	destination_number	^\\*411$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	0105f5cd-d66a-4aeb-9a33-5e235ff68751	5e75b8bf-7982-46b7-a129-8f8a623ec2e6	action	lua	directory.lua	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	8be784dd-139f-4106-b314-560bb32e0ee3	ce806b22-ec24-4401-85ac-297a6de79bab	condition	destination_number	^\\*(925)$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	8be784dd-139f-4106-b314-560bb32e0ee3	892247a2-40b9-4a9f-a18f-8d078a715820	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	8be784dd-139f-4106-b314-560bb32e0ee3	4677b2bc-2759-4c0e-99e8-3eb0c59815ec	action	set	pin_number=60373267	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	8be784dd-139f-4106-b314-560bb32e0ee3	ef3cbd50-b47f-4757-89a1-db0599948524	action	set	time_zone_offset=-7	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	8be784dd-139f-4106-b314-560bb32e0ee3	2bb17658-629c-4816-bdea-911016d347ff	action	lua	wakeup.lua	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d1a07b69-b4fa-44f8-9034-39f911baab21	2bfd61fb-f359-4505-b124-d73abcd1096e	condition	destination_number	^\\*5900$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d1a07b69-b4fa-44f8-9034-39f911baab21	becf37a8-b872-471d-80d6-4fd10241cebd	action	set	park_direction=in	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d1a07b69-b4fa-44f8-9034-39f911baab21	ec256644-255e-416a-b156-05e5dadff874	action	set	park_extension=\\*5901	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d1a07b69-b4fa-44f8-9034-39f911baab21	c7774378-b22d-4f58-b0cf-d2a3d53663a1	action	set	park_range=3	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d1a07b69-b4fa-44f8-9034-39f911baab21	cdd4452c-60da-4d84-974a-e2c12435daf2	action	set	park_announce=true	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d1a07b69-b4fa-44f8-9034-39f911baab21	3c22adba-f0e1-4dfe-9981-419433741fe9	action	set	park_timeout_seconds=70	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d1a07b69-b4fa-44f8-9034-39f911baab21	2e3632fa-8af8-4e5a-9be5-19207dd144d4	action	set	park_timeout_destination=1000	\N	\N	0	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d1a07b69-b4fa-44f8-9034-39f911baab21	2981317d-6539-4b59-a6df-e7c4910b0f42	action	set	park_music=$${hold_music}	\N	\N	0	40
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d1a07b69-b4fa-44f8-9034-39f911baab21	4cf62a83-8961-42f5-9219-09ff115dcb90	action	set	park_caller_id_prefix=	\N	\N	0	45
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d1a07b69-b4fa-44f8-9034-39f911baab21	e0af2c4d-3a93-47a3-a311-0b0d13f423cb	action	lua	park.lua	\N	\N	0	50
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	7f3eeb36-c66d-4ab0-9974-c0604b0268c5	adddecf3-fbc3-4a7b-9e62-888ec5ca308c	condition	destination_number	(^\\*59[0-9][0-9]$)	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	7f3eeb36-c66d-4ab0-9974-c0604b0268c5	1b3662dd-cf31-4e6b-943e-0c127e8cebe0	action	set	park_direction=out	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	7f3eeb36-c66d-4ab0-9974-c0604b0268c5	ad57da20-b784-43a4-98a6-72ee274f30fd	action	set	park_extension=$1	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	7f3eeb36-c66d-4ab0-9974-c0604b0268c5	658afae1-3e2b-44d2-8768-7b0e5a9519b3	action	lua	park.lua	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e76e9134-4392-4af0-9ae9-749dc2084fd7	ec640c46-8b42-4336-9dbe-750b560c8579	condition	destination_number	(^\\*59[0-9][0-9]$|^\\*5902$|^\\*5903$)	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e76e9134-4392-4af0-9ae9-749dc2084fd7	5b9331f6-4223-483d-9242-057c452298b0	action	set	park_extension=$1	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e76e9134-4392-4af0-9ae9-749dc2084fd7	c711765b-6c01-4493-89cb-3e999d8a1ad6	action	set	park_direction=both	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e76e9134-4392-4af0-9ae9-749dc2084fd7	40dde68e-f711-49cc-8428-e49bae0bcbc8	action	set	park_announce=true	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e76e9134-4392-4af0-9ae9-749dc2084fd7	7fc6da19-ae16-4c1f-9b35-946b05c16743	action	set	park_timeout_seconds=250	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e76e9134-4392-4af0-9ae9-749dc2084fd7	fe2aaceb-4088-40b6-bcda-5952773907df	action	set	park_timeout_destination=1000	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e76e9134-4392-4af0-9ae9-749dc2084fd7	9c605f31-01d3-4c39-bb4f-05f6174411c3	action	set	park_music=$${hold_music}	\N	\N	0	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e76e9134-4392-4af0-9ae9-749dc2084fd7	63414447-e789-4257-bfc9-ac46d5ef0923	action	set	park_caller_id_prefix=	\N	\N	0	40
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e76e9134-4392-4af0-9ae9-749dc2084fd7	24b6c838-b3e6-4778-9b2d-df7b687e4f9d	action	lua	park.lua	\N	\N	0	45
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	48bc8ba8-897a-4eaf-bb2e-038acb948041	condition	destination_number	^(park\\+)?(\\*59[0-9][0-9])$	never	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	9ed2287f-6cb6-4469-b793-719818daedd8	condition	${sip_h_Referred-By}	sip:(.*)@.*	never	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	742af9ff-4979-484e-b5af-42df438855d1	action	set	referred_by_user=$1	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	c137bd79-ac83-4b5f-9b15-944e484e1683	condition	destination_number	^(park\\+)?(\\*59[0-9][0-9])$	never	\N	1	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	a1ad0fdc-e53f-40a9-93b5-aab463571bcf	action	set	park_in_use=false	\N	true	1	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	124530c9-ffb5-40ad-977c-3d5819def1c6	action	set	park_lot=$2	\N	true	1	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	b5354382-4db0-4b27-a0d7-be284f0b8df9	condition	destination_number	^(park\\+)?(\\*59[0-9][0-9])$	\N	\N	2	45
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	6d67d05a-11bf-42c9-bede-2c04b011d425	condition	${cond ${sip_h_Referred-By} == 0 ? false : true}	true	never	\N	2	50
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	354c551d-b1ba-4273-b0df-abcb6d790065	action	set	park_in_use=${regex ${valet_info park@${domain_name}}|${park_lot}}	\N	true	2	55
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	62f5d65e-be19-4f4c-9faa-30e459c35b32	condition	${park_in_use}	true	never	\N	3	65
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	4608e774-92ca-4d56-9e3b-52c08c76c465	action	transfer	${referred_by_user} XML ${context}	\N	\N	3	70
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	839d6d94-eebe-46b7-83e5-ef76704654e4	anti-action	set	valet_parking_timeout=90	\N	\N	3	75
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	961c2adc-c58f-4ce1-aa95-ed5f5b67827d	anti-action	set	valet_hold_music=${hold_music}	\N	\N	3	80
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	e88d2590-1550-49ab-a6c4-a768e5bc6750	anti-action	set	valet_parking_orbit_exten=${referred_by_user}	\N	\N	3	85
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	770d30cd-a10b-47f1-9211-13d072394cd6	anti-action	valet_park	park@${domain_name} ${park_lot}	\N	\N	3	90
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	57289bf2-49d6-4734-b786-d4218e4bc0e5	041af46c-cf67-4853-b0de-2af7d388b19e	condition	destination_number	^(park\\+)?(\\*5900)$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	57289bf2-49d6-4734-b786-d4218e4bc0e5	2a3b5888-755e-431a-b738-a91aee666dc6	action	valet_park	park@${domain_name} auto in 5901 5999	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	2c6a4345-7759-47d7-bc63-67241d67b731	3a0f4f3b-3b53-4d6c-8e43-913368a1a40c	condition	destination_number	^(park\\+)?\\*(59[0-9][0-9])$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	2c6a4345-7759-47d7-bc63-67241d67b731	1a9238c0-d600-4300-82dc-2e75a1416cff	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	2c6a4345-7759-47d7-bc63-67241d67b731	0b4c5de6-4805-4086-b954-53ac10a8a7bd	action	valet_park	park@${domain_name} $1	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	3d574307-cd3e-4a51-a3d0-5f0fa9a616b9	1b2b1607-17d5-4ee5-9ee3-ce7c4db0652b	condition	destination_number	^0$|^operator$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	3d574307-cd3e-4a51-a3d0-5f0fa9a616b9	d9051292-75c8-4c21-ab0b-41dc68ddfbe6	action	export	transfer_context=sigil.c-call.eu	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	3d574307-cd3e-4a51-a3d0-5f0fa9a616b9	076f0770-16af-4bb1-b94a-c87f3577894d	action	bind_meta_app	4 ab s execute_extension::att_xfer XML features	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	3d574307-cd3e-4a51-a3d0-5f0fa9a616b9	c2dcb818-d772-4974-83e0-a79ce388733a	action	bind_meta_app	5 ab s execute_extension::xfer_vm XML features	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	3d574307-cd3e-4a51-a3d0-5f0fa9a616b9	fb833807-772c-46f4-89d0-aeedd7e7152e	action	set	domain_name=sigil.c-call.eu	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	3d574307-cd3e-4a51-a3d0-5f0fa9a616b9	b7f23e4f-8e83-4617-af81-7fb7e4319c3b	action	transfer	1001 XML sigil.c-call.eu	\N	\N	0	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9225e08b-320d-42b9-aa0d-9a0f77b55570	fb400936-20f5-453e-b8bc-3a886e745549	condition	destination_number	^\\*000$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9225e08b-320d-42b9-aa0d-9a0f77b55570	5e662cbd-a895-48d5-a673-0c51b66961f8	action	set	dial_string=loopback/operator/sigil.c-call.eu/XML	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9225e08b-320d-42b9-aa0d-9a0f77b55570	7ad5d240-a3a1-47e2-922c-3b6186a98132	action	set	direction=both	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9225e08b-320d-42b9-aa0d-9a0f77b55570	24bb1b76-b690-4a91-ac3c-7f408b67dffd	action	set	extension=true	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9225e08b-320d-42b9-aa0d-9a0f77b55570	444643e5-d17d-4f01-8b99-7522883bb76c	action	lua	dial_string.lua	\N	\N	0	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b27e7d9d-5512-4685-ad78-48b850748a9e	2c956356-cde2-492c-8589-304e27101c9a	condition	destination_number	^\\*78$|\\*363$	on-true	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b27e7d9d-5512-4685-ad78-48b850748a9e	df2177cf-d316-427b-a55b-a56375ce432b	action	set	enabled=true	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b27e7d9d-5512-4685-ad78-48b850748a9e	bd8e1620-ef06-4c5b-905a-23fcd7612610	action	lua	do_not_disturb.lua	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b27e7d9d-5512-4685-ad78-48b850748a9e	dd156b74-a845-4b4f-8228-cb7bf301c9ea	condition	destination_number	^\\*79$	\N	\N	1	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b27e7d9d-5512-4685-ad78-48b850748a9e	42c4d1c9-5961-447c-9386-0c2d340911dc	action	set	enabled=false	\N	\N	1	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b27e7d9d-5512-4685-ad78-48b850748a9e	a72c9cc2-d6c1-4f2f-b083-17f72da95897	action	lua	do_not_disturb.lua	\N	\N	1	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	67171a49-bdb0-4adf-8574-f2a32a45d22a	4af7fcf9-5165-4b35-9c07-b65e74f7b471	condition	destination_number	^\\*72$	on-true	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	67171a49-bdb0-4adf-8574-f2a32a45d22a	78cdb941-90df-4d1d-ad53-a8a25964a3f2	action	set	enabled=true	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	67171a49-bdb0-4adf-8574-f2a32a45d22a	5d84d631-ed34-4940-ac61-c27ae03df58a	action	lua	call_forward.lua	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	67171a49-bdb0-4adf-8574-f2a32a45d22a	91db0b50-5ea6-4ee7-847f-d402a605e2c1	condition	destination_number	^\\*73$	on-true	\N	1	25
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	67171a49-bdb0-4adf-8574-f2a32a45d22a	2945eb69-bfa0-4b86-8ffb-21fab4b79c4b	action	set	enabled=false	\N	\N	1	30
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	67171a49-bdb0-4adf-8574-f2a32a45d22a	c9a6c574-b5f9-4863-bfa1-80d954d62060	action	lua	call_forward.lua	\N	\N	1	35
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	67171a49-bdb0-4adf-8574-f2a32a45d22a	16f7bb2f-277e-4820-8ef1-fed902bd3360	condition	destination_number	^\\*74$	on-true	\N	2	45
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	67171a49-bdb0-4adf-8574-f2a32a45d22a	b7cc9ecb-5405-494d-847f-2ce3ecbb8bbb	action	set	request_id=true	\N	\N	2	50
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	67171a49-bdb0-4adf-8574-f2a32a45d22a	befaa821-bcc6-443d-9ec7-8f717313899f	action	set	enabled=toggle	\N	\N	2	55
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	67171a49-bdb0-4adf-8574-f2a32a45d22a	67b9d1db-080b-4997-bb03-794080552159	action	lua	call_forward.lua	\N	\N	2	60
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f88b3ea8-4445-4a12-a6a0-105fd91f7a7d	d41854da-e2ac-4dd9-8dbf-56327b83152f	condition	destination_number	^\\*21$	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f88b3ea8-4445-4a12-a6a0-105fd91f7a7d	f06be42b-2183-467d-b7a0-6aa5c35368dc	action	answer		\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f88b3ea8-4445-4a12-a6a0-105fd91f7a7d	535227e3-9219-4c08-bfc7-4343ba5f68b4	action	lua	follow_me.lua	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	5b3fc9f3-4e4d-4dcb-9065-9faa28b977f6	condition	destination_number	(^\\d{2,7}|\\D+$)	\N	\N	0	5
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	aaa43f14-cf31-4a16-959e-40add3b4600f	action	set	dialed_extension=$1	\N	\N	0	10
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	8870177a-7d0c-40a4-b403-dc75a4c83a0c	action	export	dialed_extension=$1	\N	\N	0	15
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	d2e7a5b1-17f1-4f9d-8349-a2c607f59fbb	action	limit	hash ${domain_name} $1 ${limit_max} ${limit_destination}	\N	\N	0	20
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	52f89c0d-18de-492a-a2a6-1816d7f68e2d	action	bind_meta_app	1 ab s execute_extension::dx XML ${context}	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	28f35467-063e-4ac9-8079-6cb4ffa0ebca	condition	${user_record}	^all$	never	\N	1	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	afb0ade5-6fa1-4de9-94e5-a570097c17ec	action	set	record_session=true	\N	true	1	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	e5868caf-fdd9-4ba5-8710-69b44b67b009	condition	${user_exists}	^true$	never	\N	2	45
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	30e77fc3-29a0-43e3-9fd3-c11183411ffe	condition	${call_direction}	^inbound$	never	\N	2	50
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	d39d45ca-f301-4f20-8f27-2a984e9fb18a	condition	${user_record}	^inbound$	never	\N	2	55
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	7c6f7370-1a30-4e52-8c49-c68867f5f8a7	action	set	record_session=true	\N	true	2	60
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	e3c0c5c1-1193-406c-a51c-b585c4a1fb4a	condition	${user_exists}	^true$	never	\N	3	70
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	24b5414f-4bba-482e-b34f-f629af0968a2	condition	${call_direction}	^outbound$	never	\N	3	75
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	701aa2fd-7a15-4a91-93a0-89642f14be48	condition	${user_record}	^outbound$	never	\N	3	80
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	03c1a80e-78db-411d-a594-63368865e439	action	set	record_session=true	\N	true	3	85
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	e0ddcebc-3624-45f6-aceb-f77957f4f05b	condition	${user_exists}	^true$	never	\N	4	95
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	9cce5709-490a-43ee-bd93-41070575dc68	condition	${call_direction}	^local$	never	\N	4	100
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	68971cf9-558c-4b27-ae72-ab376220894c	condition	${user_record}	^local$	never	\N	4	105
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	6de5a83c-9788-4709-9a34-74a38298dfe6	action	set	record_session=true	\N	true	4	110
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	21b7c9db-612c-4a37-af7d-a8627f2a9680	condition	${from_user_exists}	^true$	never	\N	5	120
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	a909785e-8c02-41df-98be-82087db74869	action	set	from_user_record=${user_data ${sip_from_user}@${sip_from_host} var user_record}	\N	true	5	125
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	0350a59c-e4d5-4c39-ae47-9417805f3f0a	condition	${from_user_exists}	^true$	never	\N	6	135
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	f3a281cb-246a-4d3d-9b3b-9f27552c0995	condition	${from_user_record}	^all$	never	\N	6	140
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	8bc86df8-ddf2-4a02-b1ff-7eaeca99df25	action	set	record_session=true	\N	true	6	145
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	711712fc-9f38-4ea2-88df-deb61f018527	condition	${from_user_exists}	^true$	never	\N	7	155
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	65370547-2e36-48c5-8edb-f2dfcdf7abb0	condition	${call_direction}	^inbound$	never	\N	7	160
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	31e12baf-5aa4-4b49-ba5c-4482b170b173	condition	${from_user_record}	^inbound$	never	\N	7	165
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	a98dc599-6411-4a10-b9f0-d2ff0b86ac4b	action	set	record_session=true	\N	true	7	170
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	88f21c64-bd70-4728-8592-29a9516c980d	condition	${from_user_exists}	^true$	never	\N	8	180
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	b7ee3a2e-cb6e-42d2-b7ad-d8c019cfa20f	condition	${call_direction}	^outbound$	never	\N	8	185
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	77571e5c-dc09-4667-b9de-f88696660283	condition	${from_user_record}	^outbound$	never	\N	8	190
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	77c2472c-f9f4-43f1-87e9-2d638c9ee1cd	action	set	record_session=true	\N	true	8	195
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	33fd1786-2930-49ba-bf1b-c4c99cea8ead	condition	${from_user_exists}	^true$	never	\N	9	205
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	33791bfd-a9f5-4cf6-823f-2dcf945fcca0	condition	${call_direction}	^local$	never	\N	9	210
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	a61e8d32-6a8c-4db7-8f29-51bc534bd4de	condition	${from_user_record}	^local$	never	\N	9	215
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	da31ce45-e3e8-41fc-b545-59071f420998	action	set	record_session=true	\N	true	9	220
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	9ea8a9c5-c5b0-4017-947b-f96d3e502984	condition	${record_session}	^true$	\N	\N	10	230
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	46e46793-b21e-481a-9767-0858e5af6fc9	action	set	api_on_answer=uuid_record ${uuid} start ${recordings_dir}/${domain_name}/archive/${strftime(%Y)}/${strftime(%b)}/${strftime(%d)}/${uuid}.${record_ext}	\N	\N	10	235
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	6215ebd1-81d3-4d61-b2a5-c38d0073cc03	23b10844-160a-4263-8fe9-01574bbfa8ce	condition	destination_number	^\\*22$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	6215ebd1-81d3-4d61-b2a5-c38d0073cc03	7a772f98-d85a-4741-b6c8-e63075db7d75	action	lua	app.lua user_status	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	555aaa34-1eb7-4af5-a1d8-fa23cd391f00	ba25f3b6-f792-4639-ac7d-e5e5c8d05f7b	condition	destination_number	^\\*886$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	555aaa34-1eb7-4af5-a1d8-fa23cd391f00	b982cc61-65ec-4184-afa3-d8fa44a90808	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	555aaa34-1eb7-4af5-a1d8-fa23cd391f00	8c24d15f-4f99-4d0f-a347-0bb5baefd549	action	lua	intercept.lua	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	cfd67640-914a-4b84-9c0b-ace4396da28f	40b6d4cc-93da-469d-8fd2-0e5ac7a5b3e2	condition	destination_number	^\\*8$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	cfd67640-914a-4b84-9c0b-ace4396da28f	df4b9d74-2699-470e-9739-439099205135	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	cfd67640-914a-4b84-9c0b-ace4396da28f	f0a2a779-dc91-41ab-a1d9-b7e56e2d184a	action	lua	intercept_group.lua	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1fb228a6-8c8c-4c1b-9417-e299dd39e0f7	a210726f-397a-428f-acb2-4be83ed2a0c5	condition	destination_number	^(redial|\\*870)$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1fb228a6-8c8c-4c1b-9417-e299dd39e0f7	426b0489-cc18-40f3-b531-d5f137dd8e8d	action	transfer	${hash(select/${domain_name}-last_dial/${caller_id_number})}	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	353f9b6a-d08a-4ea6-93df-79b1d8a8f249	condition	destination_number	^\\*724$	never	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	cdaeaa8f-d621-4bd6-ae61-2dfcac976b6c	condition	${sip_h_Referred-By}	sip:(.*)@.*	never	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	85e43926-be4b-4812-98e3-8e50ccb64e01	action	set	referred_by_user=$1	\N	true	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	cd43934f-623e-4864-8b02-ef5575fc4686	action	log	INFO referred_by_user is [${referred_by_user}]	\N	true	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	d6a31161-dd71-4cc8-866d-7a0164ffc6b8	action	log	INFO sip_h_Referred-By is [${sip_h_Referred-By}]	\N	true	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	ae1f88f0-174c-4812-be61-7f819de8ff22	anti-action	set	referred_by_user=false	\N	true	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	7eef895e-53f0-43a3-8bae-fb7c45089def	anti-action	log	INFO referred_by_user is [${referred_by_user}]	\N	true	0	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	ee05c8dc-ac76-44c5-9538-56c9e2f4992c	anti-action	log	INFO sip_h_Referred-By is [${sip_h_Referred-By}]	\N	true	0	40
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	f2433425-d410-4892-9105-0873c13ec5c0	condition	destination_number	^\\*724$	\N	\N	1	50
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	23f9569c-a106-452c-acd3-3fcaaddfb217	condition	${referred_by_user}	false	never	\N	1	55
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	644e0710-90f7-4c56-b2f3-ce1e9529550c	action	set	was_transfered_to_page=false	\N	true	1	60
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	62e1874b-6119-4027-bd05-838112a908cc	action	log	INFO was_transfered_to_page is [${was_transfered_to_page}]	\N	true	1	65
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	6fa3a00e-67df-461e-b9d7-21d83e3df5c7	anti-action	set	was_transfered_to_page=true	\N	true	1	70
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	5e9be62d-265d-4ea7-9158-bf45e7af90e5	anti-action	log	INFO was_transfered_to_page is [${was_transfered_to_page}]	\N	true	1	75
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	f58031c9-6e28-4ec9-a8ab-cb7fe196d129	condition	destination_number	^\\*724$	never	\N	2	85
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	3d80dbb9-5c2d-4944-adc7-6392c2e86d89	action	log	INFO was_transfered_to_page is [${was_transfered_to_page}]	\N	\N	2	90
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	4ff34cee-db21-48fd-9c0d-4db36fed1379	condition	${was_transfered_to_page}	true	never	\N	3	100
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	dd30ef20-22c4-4bd9-a41d-02acb5d470bf	action	transfer	${referred_by_user} XML ${context}	\N	\N	3	105
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	718e964f-f43b-4161-8b18-1199b1cd1476	anti-action	set	caller_id_name=Page	\N	\N	3	110
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	e3022747-1b88-4553-b46f-6cdcc49686aa	anti-action	set	caller_id_number=	\N	\N	3	115
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	39880b5a-1a9b-4841-a09b-564a762b7249	anti-action	set	pin_number=	\N	\N	3	120
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	75268dfc-42aa-4b11-9610-883fac743654	anti-action	set	extension_list=2000-2002	\N	\N	3	125
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	7a34fcdf-3a5f-41cb-bdbf-87afa1cabdec	anti-action	lua	page.lua	\N	\N	3	130
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	6685b141-c688-435c-806c-af6f412a3933	7be498c7-b19f-4aa1-962d-4d348e24e109	condition	destination_number	^\\*88(\\d{2,7})$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	6685b141-c688-435c-806c-af6f412a3933	47d16f6b-1c67-4e30-862f-feaa80bacf7e	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	6685b141-c688-435c-806c-af6f412a3933	5a03cb8c-d3bf-437e-b5c9-32d1836b614e	action	set	pin_number=14100265	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	6685b141-c688-435c-806c-af6f412a3933	8978eac0-4a2e-48c5-8653-2a7f1745144a	action	lua	eavesdrop.lua $1	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1993439c-f903-4270-9498-1bd06374b6c8	a922e860-ee8d-4aa4-aa01-3b5e10609f17	condition	destination_number	^\\*67(\\d+)$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1993439c-f903-4270-9498-1bd06374b6c8	06412bd2-b566-4b89-a27f-5a8d1cfe9048	action	privacy	full	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1993439c-f903-4270-9498-1bd06374b6c8	702ed4b3-d7e6-43bb-968e-74c85eb973a6	action	set	sip_h_Privacy=id	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1993439c-f903-4270-9498-1bd06374b6c8	5de7a1d8-92e3-48ec-9c9d-022197c9b7da	action	set	privacy=yes	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1993439c-f903-4270-9498-1bd06374b6c8	5dcfe61a-6be3-4deb-8be0-4ee780249fb3	action	transfer	$1 XML ${context}	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	8f02f7b0-f08e-4868-a635-697a9c6319b1	8d14a5e9-e359-4c2a-a762-17f2cd8ee2a3	condition	destination_number	^\\*69$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	8f02f7b0-f08e-4868-a635-697a9c6319b1	77c4a525-98f2-4ecd-b900-c9bae1c3ebf0	action	transfer	${hash(select/${domain_name}-call_return/${caller_id_number})}	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	88a6fa14-8aac-4be3-bab7-4b849b593881	74d314e0-bd34-4622-b728-16d1371e1025	condition	destination_number	^\\*\\*(\\d+)$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	88a6fa14-8aac-4be3-bab7-4b849b593881	a2abc7dd-ea07-4095-a956-525969165ca2	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	88a6fa14-8aac-4be3-bab7-4b849b593881	80228061-4c0c-4a05-9dc2-042828bf0a43	action	lua	intercept.lua $1	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1389aa00-b02a-48be-8054-8306d96b85b9	a5c9c397-e1b1-413c-8c68-6fa7bcee4a1e	condition	destination_number	^dx$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1389aa00-b02a-48be-8054-8306d96b85b9	af76bad8-e651-43d2-960c-69ca5f8c5c9c	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1389aa00-b02a-48be-8054-8306d96b85b9	39f41416-44be-4bd3-9557-21f225a9dbd9	action	read	11 11 'tone_stream://%(10000,0,350,440)' digits 5000 #	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1389aa00-b02a-48be-8054-8306d96b85b9	0ade3ef4-d421-4dbd-817e-fb7664e7e408	action	transfer	-bleg ${digits}	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	90877902-82d5-4b8f-8a16-a7a5ba245246	61e49807-b801-4ea4-880a-4a89c4628e30	condition	destination_number	^\\*8(\\d{2,7})$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	90877902-82d5-4b8f-8a16-a7a5ba245246	26c52d0b-9136-44ce-b9bd-c62a118375e7	action	set	extension_list=$1	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	90877902-82d5-4b8f-8a16-a7a5ba245246	e848acb0-bbdd-4d96-8e4c-4bc7aadbc9f9	action	set	pin_number=	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	90877902-82d5-4b8f-8a16-a7a5ba245246	6dcdecce-f101-4a22-975b-6ba3d3c0e8fa	action	set	mute=true	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	90877902-82d5-4b8f-8a16-a7a5ba245246	0a5f1a6e-7872-49b0-8d15-bf9ea7728133	action	lua	page.lua	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5662f5dd-3802-42c5-8ed2-2f7a84a36f93	4e09a775-f975-47ef-916b-ef0ff0b36d2f	condition	destination_number	^att_xfer$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5662f5dd-3802-42c5-8ed2-2f7a84a36f93	2ddcd474-4880-48ce-9bfc-498834effa7c	action	read	2 6 'tone_stream://%(10000,0,350,440)' digits 30000 #	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5662f5dd-3802-42c5-8ed2-2f7a84a36f93	81384c06-fd38-4a09-961f-3c38e33f3ef9	action	set	origination_cancel_key=#	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5662f5dd-3802-42c5-8ed2-2f7a84a36f93	a2d19b82-662e-4cd5-9213-9e2fc2c0e7a6	action	set	domain_name=${transfer_context}	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5662f5dd-3802-42c5-8ed2-2f7a84a36f93	3491ed33-35ae-4e73-85e2-550355b959d5	action	att_xfer	user/${digits}@${transfer_context}	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cb11b91-bca8-4cb3-8587-348e96cb5b5d	178c8238-4ddf-4564-a55d-7c89ecb3cc4a	condition	username	^${caller_id_number}$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cb11b91-bca8-4cb3-8587-348e96cb5b5d	c4513dfb-a844-49fe-ad96-3aaec3c9c36a	condition	destination_number	^${caller_id_number}$	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cb11b91-bca8-4cb3-8587-348e96cb5b5d	8d2d975a-9f4c-484e-96ef-84fa5e8ce376	action	answer		\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cb11b91-bca8-4cb3-8587-348e96cb5b5d	595abf0e-1afe-4fc3-b7ed-53fa5a41079d	action	sleep	1000	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cb11b91-bca8-4cb3-8587-348e96cb5b5d	4b247548-e08c-46c2-82a9-43a655a2adcb	action	set	voicemail_action=check	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cb11b91-bca8-4cb3-8587-348e96cb5b5d	fc36fe77-d7a4-440c-82fc-2f82eb59a87d	action	set	voicemail_id=${caller_id_number}	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cb11b91-bca8-4cb3-8587-348e96cb5b5d	b03fe52e-03ce-4c96-b240-22f2c0c94aa0	action	set	voicemail_profile=default	\N	\N	0	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cb11b91-bca8-4cb3-8587-348e96cb5b5d	17ec3429-d36f-4fd6-ad83-f1d847caab56	action	lua	app.lua voicemail	\N	\N	0	40
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	17104eb4-244b-41c6-ab4d-2f43850030f5	11d9480e-c9bb-40af-812b-7b9be6abd94a	condition	destination_number	^\\*99(\\d{2,7})$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	17104eb4-244b-41c6-ab4d-2f43850030f5	c44fddd8-d848-465e-9810-e63abbcb64d0	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	17104eb4-244b-41c6-ab4d-2f43850030f5	96f1dcc2-ceea-4eb9-9739-7254baecb7e7	action	sleep	1000	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	17104eb4-244b-41c6-ab4d-2f43850030f5	16856cc7-ae40-44fa-922a-52e42509601c	action	set	voicemail_action=save	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	17104eb4-244b-41c6-ab4d-2f43850030f5	19541e1b-7d66-4468-8b8d-f8c679527047	action	set	voicemail_id=$1	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	17104eb4-244b-41c6-ab4d-2f43850030f5	8b9dfe01-edd8-42b5-b13b-a446f3991a60	action	set	voicemail_profile=default	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	17104eb4-244b-41c6-ab4d-2f43850030f5	fa78760f-d0e4-4526-b31d-220bd0e35383	action	lua	app.lua voicemail	\N	\N	0	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	01c95137-2f78-47ba-b518-8db287f6789c	1c7a2dcb-4e90-436c-8ab5-0c4e2ae98990	condition	destination_number	^vmain$|^\\*4000$|^\\*98$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	01c95137-2f78-47ba-b518-8db287f6789c	6afa217c-4887-45a2-95e3-797ad8057d96	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	01c95137-2f78-47ba-b518-8db287f6789c	61121366-4cc9-4d70-acda-c824d378d1c0	action	sleep	1000	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	01c95137-2f78-47ba-b518-8db287f6789c	75f6adc8-a245-4bb7-ba52-3da893b36049	action	set	voicemail_action=check	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	01c95137-2f78-47ba-b518-8db287f6789c	778d6fb0-ef2d-4877-a606-f8e26abd7991	action	set	voicemail_profile=default	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	01c95137-2f78-47ba-b518-8db287f6789c	81aae9b0-826b-43a6-aae9-2dea2ebf087b	action	lua	app.lua voicemail	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	a989b32c-eefb-47cc-9aad-5d7789905aa3	6cbcd1c4-d0af-4bca-8571-95e5d69dbead	condition	destination_number	^xfer_vm$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	a989b32c-eefb-47cc-9aad-5d7789905aa3	8877e095-9357-41c2-9bed-a70012d4977e	action	read	2 6 'tone_stream://%(10000,0,350,440)' digits 30000 #	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	a989b32c-eefb-47cc-9aad-5d7789905aa3	747da99d-7a12-499c-bf13-5e3fa2d5ca24	action	set	origination_cancel_key=#	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	a989b32c-eefb-47cc-9aad-5d7789905aa3	c1b18082-f8fa-4ef9-bcba-31ec4d5d9059	action	set	domain_name=${transfer_context}	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	a989b32c-eefb-47cc-9aad-5d7789905aa3	558f9900-66cb-44a5-84ae-10f9fe2e4d2e	action	export	domain_name=${transfer_context}	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	a989b32c-eefb-47cc-9aad-5d7789905aa3	a3a7cae4-6235-498a-9cdf-cfea83f3fe8a	action	transfer	-bleg *99${digits} XML ${transfer_context}	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b3c0f873-b537-49b7-a426-bda6dd20ada5	49e70ec8-53bc-4dc3-b537-a48ba18af3dc	condition	destination_number	^is_transfer$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b3c0f873-b537-49b7-a426-bda6dd20ada5	8b4d1e0c-4e45-471d-a005-1cfc5207718c	condition	${digits}	^(\\d+)$	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b3c0f873-b537-49b7-a426-bda6dd20ada5	0b73ea7a-23dc-4581-9d1e-5d63339622f2	action	transfer	-aleg ${digits} XML ${context}	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b3c0f873-b537-49b7-a426-bda6dd20ada5	a0b09c22-a394-4586-8634-d36ba050ab26	anti-action	eval	cancel transfer	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	da91410a-1901-41df-85f9-01495a917103	ad2b3312-b775-4e96-811f-bfa2b03cf8c7	condition	destination_number	^\\*97$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	da91410a-1901-41df-85f9-01495a917103	904de682-2e19-4ecc-a66f-d4a47b0f7191	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	da91410a-1901-41df-85f9-01495a917103	38809b66-7cb5-469f-9874-5200924c20a1	action	sleep	1000	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	da91410a-1901-41df-85f9-01495a917103	38b15fee-65a4-41ce-a395-1fb95b2929b4	action	set	voicemail_action=check	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	da91410a-1901-41df-85f9-01495a917103	f1c4f06f-c44c-4c49-aa2b-835b8554a9b5	action	set	voicemail_id=${caller_id_number}	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	da91410a-1901-41df-85f9-01495a917103	55e71a50-3933-42d0-b64a-1a5d830956ef	action	set	voicemail_profile=default	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	da91410a-1901-41df-85f9-01495a917103	2d7b1b87-5534-43d8-85b0-21a0d87c6790	action	lua	app.lua voicemail	\N	\N	0	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cf1fc90-b48b-4b48-9764-271618d0bd17	98575952-a72a-4bcd-a4d6-49d74af684dd	condition	destination_number	^cf$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cf1fc90-b48b-4b48-9764-271618d0bd17	41c2ded5-0e04-4371-a0f1-e8979a123a5e	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cf1fc90-b48b-4b48-9764-271618d0bd17	dbe14561-4826-40a3-bc9b-5e143c92113a	action	transfer	-both 30${dialed_extension:2} XML ${context}	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	39139117-3c74-440d-90ff-b476a5d1cb63	eedd7e11-7b17-4e19-89a0-72003aad6293	condition	destination_number	^\\*9195$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	39139117-3c74-440d-90ff-b476a5d1cb63	f12cf38d-ade3-444f-9b64-50e5863639b7	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	39139117-3c74-440d-90ff-b476a5d1cb63	e8b7f382-d8c8-45be-abbc-48bbe81ef1d1	action	delay_echo	5000	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0695301f-3f37-4e8f-ab98-3b9f7a640a3d	5bb28707-867c-487b-af34-d706277e64bd	condition	destination_number	^\\*9196$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0695301f-3f37-4e8f-ab98-3b9f7a640a3d	6bc1700f-9293-4cfc-8ee8-d77bf66ee1c4	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0695301f-3f37-4e8f-ab98-3b9f7a640a3d	f24e25b5-eff5-4733-9d06-deccae30c1ad	action	echo		\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	33241635-82c7-4315-8f23-66d9a096710b	8f4d0837-bd07-4e82-aa02-6313dfac45e2	condition	destination_number	^(10[01][0-9])$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	33241635-82c7-4315-8f23-66d9a096710b	6638f1f1-d073-4ee4-afb8-79f0fa44cb8f	action	set	transfer_ringback=$${hold_music}	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	33241635-82c7-4315-8f23-66d9a096710b	3c76d4f0-c176-4e2a-aaf5-17d0612b419c	action	answer		\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	33241635-82c7-4315-8f23-66d9a096710b	a4cba500-a053-4dbb-9a2b-9932a817f9a5	action	sleep	1500	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	33241635-82c7-4315-8f23-66d9a096710b	d24116db-cce1-4d2d-91f6-525fd543cc51	action	playback	ivr/ivr-hold_connect_call.wav	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	33241635-82c7-4315-8f23-66d9a096710b	5e70dc19-27c8-4c51-b292-b2d259635d88	action	transfer	$1 XML ${context}	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0c561674-2b8b-4f4e-b5e2-c87f689ab9d4	bf240363-2899-424a-a390-61bae5fbc0db	condition	${zrtp_secure_media_confirmed}	^true$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0c561674-2b8b-4f4e-b5e2-c87f689ab9d4	58b1d33b-5731-44ae-960a-2187a6b7ae8a	action	sleep	1000	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0c561674-2b8b-4f4e-b5e2-c87f689ab9d4	f2f44299-f9ed-471d-b6cf-c242d0e2243a	action	playback	misc/call_secured.wav	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0c561674-2b8b-4f4e-b5e2-c87f689ab9d4	931337ef-a7ff-46df-8285-8416f265b5d5	anti-action	eval	not_secure	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	2a6d9c67-2704-4a74-b5af-c491c284d9c6	92735117-d644-40ca-a9b7-f950fdc3a19d	condition	destination_number	^\\*9197$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	2a6d9c67-2704-4a74-b5af-c491c284d9c6	ad54e4d9-b249-4737-aa7c-bd7f565065da	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	2a6d9c67-2704-4a74-b5af-c491c284d9c6	64569d88-4f8a-4825-9228-a7b08c811197	action	playback	{loops=-1}tone_stream://%(251,0,1004)	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d337fd44-f1d0-41c1-ae2b-6fbb8322a24d	e7d96cea-be39-428a-b84a-28391f0dbb34	condition	${sip_via_protocol}	tls	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d337fd44-f1d0-41c1-ae2b-6fbb8322a24d	1f1bd66a-0147-45e9-995d-582bdf80dd2a	condition	${sip_secure_media_confirmed}	^true$	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d337fd44-f1d0-41c1-ae2b-6fbb8322a24d	454942f0-5537-4ed3-a291-297f6010361c	action	sleep	1000	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d337fd44-f1d0-41c1-ae2b-6fbb8322a24d	4b9ca0e5-ebff-400d-b8c6-91db6f7a8c71	action	playback	misc/call_secured.wav	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d337fd44-f1d0-41c1-ae2b-6fbb8322a24d	a956e3e5-cbae-4aa6-b5be-56e908f541f6	anti-action	eval	not_secure	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	76dac144-5693-4e0c-9946-7bf18f0b8a3b	032c3f4e-e6d2-4ac2-ba06-fc56493d7041	condition	destination_number	^\\*9198$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	76dac144-5693-4e0c-9946-7bf18f0b8a3b	859f4c27-db0c-43fe-8e25-b08b70dc4279	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	76dac144-5693-4e0c-9946-7bf18f0b8a3b	65697637-b2aa-40bc-aab8-3bd10669f37d	action	playback	{loops=10}tone_stream://path=${base_dir}/conf/tetris.ttml	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b794a80d-60e4-4030-92d4-e50ea6f11fd4	5bed3841-92e6-42d5-bde4-fee928def627	condition	destination_number	^\\*9664$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b794a80d-60e4-4030-92d4-e50ea6f11fd4	46ee5282-c18e-49f3-95c4-b1c7922e739f	condition	${sip_has_crypto}	^(AES_CM_128_HMAC_SHA1_32|AES_CM_128_HMAC_SHA1_80)$	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b794a80d-60e4-4030-92d4-e50ea6f11fd4	4a19bbab-8c57-42b6-8602-82e2ee5d638e	action	answer		\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b794a80d-60e4-4030-92d4-e50ea6f11fd4	85e5b719-157f-4054-8279-4b4c9ca85875	action	execute_extension	is_secure XML features	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b794a80d-60e4-4030-92d4-e50ea6f11fd4	b54a34c0-091a-44c3-943d-d88636f77164	action	playback	$${hold_music}	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b794a80d-60e4-4030-92d4-e50ea6f11fd4	8f56ef1f-4222-44b6-bcbf-8cada657584c	anti-action	set	zrtp_secure_media=true	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b794a80d-60e4-4030-92d4-e50ea6f11fd4	6c28a068-0c87-4d81-b3a2-1860c636f0e0	anti-action	answer		\N	\N	0	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b794a80d-60e4-4030-92d4-e50ea6f11fd4	6bc9e68d-54c9-44b8-9d8e-bb2a28b09a51	anti-action	playback	silence_stream://2000	\N	\N	0	40
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b794a80d-60e4-4030-92d4-e50ea6f11fd4	a27ee0d2-8723-4c90-a0e5-aa9f03d310d8	anti-action	execute_extension	is_zrtp_secure XML features	\N	\N	0	45
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b794a80d-60e4-4030-92d4-e50ea6f11fd4	8a54954f-6c36-4ad5-842d-d5f7a2b1fa8e	anti-action	playback	$${hold_music}	\N	\N	0	50
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	8e2a4f36-2b53-41ea-83f9-ec71ec4a5836	bbcf698c-eac6-441e-8975-e00dfe66d827	condition	destination_number	^\\*(732)$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	8e2a4f36-2b53-41ea-83f9-ec71ec4a5836	f1bd49f2-6ccb-43af-8735-bdf54e4277b3	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	8e2a4f36-2b53-41ea-83f9-ec71ec4a5836	aefcdb1d-b771-4c6a-9487-27485dfce7fd	action	set	pin_number=22925778	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	8e2a4f36-2b53-41ea-83f9-ec71ec4a5836	bf53e072-45f3-464b-b6ea-fa6144f8f649	action	set	recording_slots=true	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	8e2a4f36-2b53-41ea-83f9-ec71ec4a5836	50a1bdbd-eb63-4ee3-a34b-4a02787f7299	action	set	recording_prefix=recording	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	8e2a4f36-2b53-41ea-83f9-ec71ec4a5836	8e915eb5-08b3-42b4-9781-35526b172d17	action	lua	recordings.lua	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	db93047f-8d8f-4874-805a-0293036885ca	c559f42c-d9c3-4481-af72-27429021e4ac	condition	destination_number	^\\*9(888|8888|1616|3232)$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	db93047f-8d8f-4874-805a-0293036885ca	c727ae7b-18e4-45da-b761-e697731251c3	action	export	hold_music=silence	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	db93047f-8d8f-4874-805a-0293036885ca	c2372fa8-1f8d-46fa-8fe7-7d477568cb52	action	bridge	sofia/${use_profile}/$1@conference.freeswitch.org	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b4fb961e-f718-4f88-a7de-f13d22df7632	509c4fc2-8dc7-4f39-bfce-dfa5371d4fae	condition	destination_number	^\\*(3472)$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b4fb961e-f718-4f88-a7de-f13d22df7632	8affa3bb-fcd5-4808-9917-6881992d0bf0	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b4fb961e-f718-4f88-a7de-f13d22df7632	3762a8c3-faea-45cc-a47a-7e67aecbeb58	action	set	pin_number=48305578	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b4fb961e-f718-4f88-a7de-f13d22df7632	31bcdf6f-3027-4978-9550-9de292e1c171	action	set	dialplan_context=${context}	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b4fb961e-f718-4f88-a7de-f13d22df7632	eac7f812-e505-4974-9ef3-33026ee0dbc3	action	lua	disa.lua	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	23803346-5926-4209-8428-c6089118e1dd	636f34ec-7677-49bf-8d8f-6cfa5d6939b9	condition	destination_number	^\\*411$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	23803346-5926-4209-8428-c6089118e1dd	c5781c5b-7449-4e61-89cc-1c5ff4fac352	action	lua	directory.lua	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	04a22f2f-3c53-4932-8a31-7e01df88304f	27139cac-2efd-4cb8-ae17-0224f093ca56	condition	destination_number	^\\*(925)$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	04a22f2f-3c53-4932-8a31-7e01df88304f	32e0dafc-7fed-464c-9a7e-53c57d2e09ed	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	04a22f2f-3c53-4932-8a31-7e01df88304f	a343cc9f-bccf-4b0d-a54e-9968755faaa7	action	set	pin_number=72181995	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	04a22f2f-3c53-4932-8a31-7e01df88304f	fec35b37-2c63-4179-9560-6865c1b185da	action	set	time_zone_offset=-7	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	04a22f2f-3c53-4932-8a31-7e01df88304f	b21e917f-1539-4733-a546-0e2ec534ecf0	action	lua	wakeup.lua	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5603932f-bc8b-4133-80a0-f70c69c22087	64c78705-988c-472e-8566-a3bb60a506da	condition	destination_number	^\\*5900$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5603932f-bc8b-4133-80a0-f70c69c22087	9b7e28a9-d2d0-493a-bb82-b5c00dc9be40	action	set	park_direction=in	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5603932f-bc8b-4133-80a0-f70c69c22087	0cb67d91-ab27-4554-bac3-8f9455b301e8	action	set	park_extension=\\*5901	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5603932f-bc8b-4133-80a0-f70c69c22087	96126b79-7b5a-4a51-872c-2aa72092bcbb	action	set	park_range=3	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5603932f-bc8b-4133-80a0-f70c69c22087	3ccb9218-fd17-4f1e-bf1f-a2c3892a5f30	action	set	park_announce=true	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5603932f-bc8b-4133-80a0-f70c69c22087	2a1555cb-14ba-49bf-816b-64f2bf20e61f	action	set	park_timeout_seconds=70	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5603932f-bc8b-4133-80a0-f70c69c22087	bc094660-7b4d-4112-ac92-df1a0fe4fc15	action	set	park_timeout_destination=1000	\N	\N	0	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5603932f-bc8b-4133-80a0-f70c69c22087	888715a8-0be3-4f23-adc7-1579d3f81667	action	set	park_music=$${hold_music}	\N	\N	0	40
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5603932f-bc8b-4133-80a0-f70c69c22087	45eb64c4-f597-4665-aa03-a902bbfac1bc	action	set	park_caller_id_prefix=	\N	\N	0	45
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5603932f-bc8b-4133-80a0-f70c69c22087	69c28715-3dc8-48d2-86c6-d71a9d61406f	action	lua	park.lua	\N	\N	0	50
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e62e709a-6de3-4b1c-82b3-e61f13472390	907a95ce-d304-4b27-96d0-fbe81ab7360f	condition	destination_number	(^\\*59[0-9][0-9]$)	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e62e709a-6de3-4b1c-82b3-e61f13472390	de1a8e32-411c-4a94-872b-46fb2c962b20	action	set	park_direction=out	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e62e709a-6de3-4b1c-82b3-e61f13472390	16d3b13d-8ce7-46f6-8724-4f2096730aef	action	set	park_extension=$1	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e62e709a-6de3-4b1c-82b3-e61f13472390	9fae6bbc-2d12-456c-a8b4-c93d8c693902	action	lua	park.lua	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7734ba60-dc3c-44d9-9cbc-d238f253ae0f	76c7e1c4-e141-478f-bd0f-e4829abd73f7	condition	destination_number	(^\\*59[0-9][0-9]$|^\\*5902$|^\\*5903$)	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7734ba60-dc3c-44d9-9cbc-d238f253ae0f	dbd8f23e-53da-4100-aaf5-0a0b9469a155	action	set	park_extension=$1	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7734ba60-dc3c-44d9-9cbc-d238f253ae0f	8cb8ff7d-d78b-43c7-9f18-b9a92c6e7eb3	action	set	park_direction=both	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7734ba60-dc3c-44d9-9cbc-d238f253ae0f	2b854e13-be17-44a5-b649-be7d34a99da8	action	set	park_announce=true	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7734ba60-dc3c-44d9-9cbc-d238f253ae0f	9f82bfb0-55af-4212-9759-978eff4dbced	action	set	park_timeout_seconds=250	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7734ba60-dc3c-44d9-9cbc-d238f253ae0f	982a9595-3737-4525-af05-547b6d4a3d40	action	set	park_timeout_destination=1000	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7734ba60-dc3c-44d9-9cbc-d238f253ae0f	12270f75-557e-4c65-adbd-d3ced7096ebe	action	set	park_music=$${hold_music}	\N	\N	0	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7734ba60-dc3c-44d9-9cbc-d238f253ae0f	686df1a2-c138-47bd-bbe5-9885fcf0bba1	action	set	park_caller_id_prefix=	\N	\N	0	40
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7734ba60-dc3c-44d9-9cbc-d238f253ae0f	88fb6b8e-40ad-4d83-8ca3-2524a273e2ef	action	lua	park.lua	\N	\N	0	45
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	e21d785b-45a0-4d09-8547-f988b42bb7bb	condition	destination_number	^(park\\+)?(\\*59[0-9][0-9])$	never	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	0c07b476-d6e7-4919-9d06-0ad138d0472f	condition	${sip_h_Referred-By}	sip:(.*)@.*	never	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	7c090e12-727a-43a0-a107-17edcfd976a5	action	set	referred_by_user=$1	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	fc4056c8-4e3e-4048-b397-d11f4657023d	condition	destination_number	^(park\\+)?(\\*59[0-9][0-9])$	never	\N	1	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	4b603c14-a7ae-4f1f-b074-34ff2275432f	action	set	park_in_use=false	\N	true	1	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	2cbc89e6-2c2c-4543-8d03-6a1346e19fd3	action	set	park_lot=$2	\N	true	1	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	4945f0e7-6f8c-42e5-9b47-9cf45cba2f99	condition	destination_number	^(park\\+)?(\\*59[0-9][0-9])$	\N	\N	2	45
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	5108c2fe-9d21-41ac-b631-c08775122414	condition	${cond ${sip_h_Referred-By} == 0 ? false : true}	true	never	\N	2	50
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	d405c88c-7599-4213-9965-fc2fbc6aa2f9	action	set	park_in_use=${regex ${valet_info park@${domain_name}}|${park_lot}}	\N	true	2	55
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	da1bf3b5-f4f1-4bfa-82cf-480a1c33a8f3	condition	${park_in_use}	true	never	\N	3	65
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	1ce85a0c-adc4-48ab-ab45-258dd7c7b537	action	transfer	${referred_by_user} XML ${context}	\N	\N	3	70
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	7d40d380-b857-4ca3-a55f-ba7a601bdf68	anti-action	set	valet_parking_timeout=90	\N	\N	3	75
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	0bb37895-c581-4c23-8c31-16c6fd786db7	anti-action	set	valet_hold_music=${hold_music}	\N	\N	3	80
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	0a6a4d61-6373-4b39-ac3c-4fe2025d9fd1	anti-action	set	valet_parking_orbit_exten=${referred_by_user}	\N	\N	3	85
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	2aeae2ef-1897-41f6-b403-716dff2b5bf4	anti-action	valet_park	park@${domain_name} ${park_lot}	\N	\N	3	90
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e54d0024-756d-4aa6-ba20-77a1646dbe54	5c20d8cd-2528-4174-87be-c12e9155156b	condition	destination_number	^(park\\+)?(\\*5900)$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e54d0024-756d-4aa6-ba20-77a1646dbe54	fc7b9266-b35d-40df-96d8-9760fc1d4bdd	action	valet_park	park@${domain_name} auto in 5901 5999	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	084d9145-a799-4e48-9250-76baa39556f5	dd0b2b76-e25b-4495-80c8-1f396f421835	condition	destination_number	^(park\\+)?\\*(59[0-9][0-9])$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	084d9145-a799-4e48-9250-76baa39556f5	597f5df1-06de-4333-82dd-ab8043aebf34	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	084d9145-a799-4e48-9250-76baa39556f5	355dcb5c-0a28-49e6-9724-e306b4cb4c5c	action	valet_park	park@${domain_name} $1	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4d4b32b7-ff68-4e0f-a714-16ddec23d4fe	d539f493-f656-4ec9-8d40-6c19ae3733d7	condition	destination_number	^0$|^operator$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4d4b32b7-ff68-4e0f-a714-16ddec23d4fe	aec59082-3f3d-4a74-9bb6-0dcc792df18a	action	export	transfer_context=panthosoutsourcering.c-call.eu	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4d4b32b7-ff68-4e0f-a714-16ddec23d4fe	9fc1da54-908c-44c2-8c71-78b08291c6f8	action	bind_meta_app	4 ab s execute_extension::att_xfer XML features	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4d4b32b7-ff68-4e0f-a714-16ddec23d4fe	67ba41c7-ba53-4279-a662-001abc1e9d3e	action	bind_meta_app	5 ab s execute_extension::xfer_vm XML features	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4d4b32b7-ff68-4e0f-a714-16ddec23d4fe	9e9fc16f-ec44-4e8e-a708-e34559632b45	action	set	domain_name=panthosoutsourcering.c-call.eu	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4d4b32b7-ff68-4e0f-a714-16ddec23d4fe	79c90a9b-b2da-4882-bf4d-505c71a0948b	action	transfer	1001 XML panthosoutsourcering.c-call.eu	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d8284ec9-b8a4-4068-8036-2a21b5323f9e	67311be2-6ccf-43ed-b391-1fdef5110054	condition	destination_number	^\\*000$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d8284ec9-b8a4-4068-8036-2a21b5323f9e	976fd755-1683-45cd-a317-a5e440bf7913	action	set	dial_string=loopback/operator/panthosoutsourcering.c-call.eu/XML	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d8284ec9-b8a4-4068-8036-2a21b5323f9e	cb90a9fb-494a-404f-be86-df76b833bbec	action	set	direction=both	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d8284ec9-b8a4-4068-8036-2a21b5323f9e	b738d5f3-f785-4db2-9bdf-ecf896000591	action	set	extension=true	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d8284ec9-b8a4-4068-8036-2a21b5323f9e	70da8d14-125f-4d2b-a1b7-60a071245753	action	lua	dial_string.lua	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7a95ee0f-ea92-41c7-9fc8-3deb436940f4	c375e3ed-eca6-4c48-a064-34fadc15ae64	condition	destination_number	^\\*78$|\\*363$	on-true	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7a95ee0f-ea92-41c7-9fc8-3deb436940f4	b740ab78-0bac-4985-b24c-4f3d69ec9282	action	set	enabled=true	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7a95ee0f-ea92-41c7-9fc8-3deb436940f4	11468897-0dd1-42a3-9c65-e0eb7f3118db	action	lua	do_not_disturb.lua	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7a95ee0f-ea92-41c7-9fc8-3deb436940f4	e503c49b-2d09-4333-9e83-fc82cf938d08	condition	destination_number	^\\*79$	\N	\N	1	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7a95ee0f-ea92-41c7-9fc8-3deb436940f4	6f5f5923-65dc-46a4-adaf-a5177fa6b706	action	set	enabled=false	\N	\N	1	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7a95ee0f-ea92-41c7-9fc8-3deb436940f4	b304bab9-f0d5-41b3-b828-11ee3828e3ad	action	lua	do_not_disturb.lua	\N	\N	1	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	893d6433-c331-4040-941d-e5d568151af0	c5de4768-ef01-4c55-8c69-9b1d24b30a74	condition	destination_number	^\\*72$	on-true	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	893d6433-c331-4040-941d-e5d568151af0	0465d2f3-a2f6-4205-b696-e7aac5c6c3c5	action	set	enabled=true	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	893d6433-c331-4040-941d-e5d568151af0	939f72cd-a2f5-421a-85f3-3b7ed2f9cfe4	action	lua	call_forward.lua	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	893d6433-c331-4040-941d-e5d568151af0	80a57208-c61b-4c29-872a-58fc1ae41e29	condition	destination_number	^\\*73$	on-true	\N	1	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	893d6433-c331-4040-941d-e5d568151af0	a7312f2c-e43a-409a-a6b8-0f381a719eda	action	set	enabled=false	\N	\N	1	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	893d6433-c331-4040-941d-e5d568151af0	dbdbe270-effb-47ca-9e6b-4d1134179e15	action	lua	call_forward.lua	\N	\N	1	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	893d6433-c331-4040-941d-e5d568151af0	f214d4ff-9dd8-4f17-aed0-a237f7825dfa	condition	destination_number	^\\*74$	on-true	\N	2	45
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	893d6433-c331-4040-941d-e5d568151af0	539be086-0f4d-4b95-b68d-7082630098cb	action	set	request_id=true	\N	\N	2	50
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	893d6433-c331-4040-941d-e5d568151af0	50e30001-4c39-4789-bbdb-574a7be4ad86	action	set	enabled=toggle	\N	\N	2	55
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	893d6433-c331-4040-941d-e5d568151af0	0ade350e-24c1-4225-bb78-39ee6dccd567	action	lua	call_forward.lua	\N	\N	2	60
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	c098c60a-6bc8-4c41-b8bb-69a7709f0fca	9ff4821e-8009-41b9-a83a-ef97dff9de04	condition	destination_number	^\\*21$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	c098c60a-6bc8-4c41-b8bb-69a7709f0fca	89a331f4-ea92-4cb1-addf-a4635fa4f24f	action	answer		\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	c098c60a-6bc8-4c41-b8bb-69a7709f0fca	5282062e-d070-426c-b9bb-62ec8f6374c9	action	lua	follow_me.lua	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	d3fad71c-33d8-4721-95fc-7e59fc5730d5	condition	destination_number	(^\\d{2,7}|\\D+$)	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	e747e06e-ae5d-4eda-8036-b37b4ffad3d5	action	set	dialed_extension=$1	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	8136881a-3e49-4475-9cae-6953aed00a25	action	export	dialed_extension=$1	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	e1eeb4b8-78fc-4f0f-a2c7-fb8cf6871dd9	action	limit	hash ${domain_name} $1 ${limit_max} ${limit_destination}	\N	\N	0	20
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	adba5879-8c0f-480b-990d-07a532f1a0e7	action	bind_meta_app	1 ab s execute_extension::dx XML ${context}	\N	\N	0	25
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	523b736d-d4f3-4b3a-9ce6-0196b17fe89f	action	bind_meta_app	2 ab s record_session::$${recordings_dir}/${domain_name}/archive/${strftime(%Y)}/${strftime(%b)}/${strftime(%d)}/${uuid}.wav	\N	\N	0	30
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	1599aa8f-c469-4be5-86f5-d4051b4aec7e	action	bind_meta_app	3 ab s execute_extension::cf XML ${context}	\N	\N	0	35
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	2266e602-0586-414f-b428-573c6534d2dd	action	bind_meta_app	4 ab s execute_extension::att_xfer XML ${context}	\N	\N	0	40
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	763711c3-ca2c-47a7-be09-0036b1149578	action	set	hangup_after_bridge=true	\N	\N	0	45
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	cf55d2b8-c7be-423d-bb4f-659e18169de0	action	set	continue_on_fail=true	\N	\N	0	50
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	099eb7df-09dc-4580-bdd8-1d849f8f6309	action	hash	insert/${domain_name}-call_return/${dialed_extension}/${caller_id_number}	\N	\N	0	55
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	d89f53e7-19a4-498b-9145-ff757421e404	action	hash	insert/${domain_name}-last_dial_ext/${dialed_extension}/${uuid}	\N	\N	0	60
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	d6017861-b2ef-41ea-b9e2-e969c61d638a	action	set	called_party_call_group=${user_data(${dialed_extension}@${domain_name} var call_group)}	\N	\N	0	65
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	2d79dfcd-adb4-4428-a11f-ead17f173a1e	action	hash	insert/${domain_name}-last_dial/${called_party_call_group}/${uuid}	\N	\N	0	70
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	43fb11b2-ba0e-4d41-a072-8289867e0a82	action	bridge	user/${destination_number}@${domain_name}	\N	\N	0	75
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	219b0da9-29d9-4226-991a-0174d0be37af	action	answer		\N	\N	0	80
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	f3081c04-74be-4a25-8985-a6eac271dcf5	action	sleep	1000	\N	\N	0	85
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	f80227a0-6b4c-4f68-ac0a-944bfd9febd3	action	set	voicemail_action=save	\N	\N	0	90
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	8bb6769f-e30c-4793-a564-9ead46a8f5f3	action	set	voicemail_id=$1	\N	\N	0	95
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	38b60835-1817-4d03-a0fd-f926500c6afe	action	set	voicemail_profile=default	\N	\N	0	100
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	9dd1dc1d-3ade-420e-858a-63c369077ca1	action	lua	app.lua voicemail	\N	\N	0	105
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ee6ea23e-ea23-4778-a6ff-6c5a55ee9362	c3c50017-c557-478a-9970-716456c6542c	condition	destination_number	^\\*072$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ee6ea23e-ea23-4778-a6ff-6c5a55ee9362	8e9f287c-1833-4d5c-b77a-da9604f00f87	action	set	direction=in	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ee6ea23e-ea23-4778-a6ff-6c5a55ee9362	fb2df906-48ec-4e82-8496-f3a6d56871f0	action	lua	dial_string.lua	\N	\N	0	15
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0f2067ad-4b9b-4fe7-830e-0d0c70ec87e4	5be399a0-0a15-4ddf-bd96-1d8e50188263	condition	destination_number	^\\*073$	\N	\N	0	5
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0f2067ad-4b9b-4fe7-830e-0d0c70ec87e4	c01adfbc-1db4-41cb-9c14-cf6bd3d79b30	action	set	direction=out	\N	\N	0	10
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0f2067ad-4b9b-4fe7-830e-0d0c70ec87e4	16e45c97-7a37-4509-9255-4ad5d123ab57	action	lua	dial_string.lua	\N	\N	0	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	3b5fe071-1b2a-412f-b359-89a7daff618f	abd3777a-7d84-4750-b5aa-57c6b261c631	condition	destination_number	^(\\d{10,20})$	\N	\N	\N	5
60d6731b-e548-4175-ab3e-3fbb261e60f5	3b5fe071-1b2a-412f-b359-89a7daff618f	6262149c-93a1-4696-9eaf-2ac4f2e065b1	action	set	sip_h_X-accountcode=${accountcode}	\N	\N	\N	10
60d6731b-e548-4175-ab3e-3fbb261e60f5	3b5fe071-1b2a-412f-b359-89a7daff618f	c747fdc5-3c2e-4ef2-903b-ca15a753c862	action	set	sip_h_X-Tag=	\N	\N	\N	12
60d6731b-e548-4175-ab3e-3fbb261e60f5	3b5fe071-1b2a-412f-b359-89a7daff618f	10832a59-4add-408c-a51f-92219d4f9cda	action	set	call_direction=outbound	\N	\N	\N	15
60d6731b-e548-4175-ab3e-3fbb261e60f5	3b5fe071-1b2a-412f-b359-89a7daff618f	88bf7e61-0ad7-4a5a-8ef5-aa49998b1313	action	set	hangup_after_bridge=true	\N	\N	\N	20
60d6731b-e548-4175-ab3e-3fbb261e60f5	3b5fe071-1b2a-412f-b359-89a7daff618f	2676d684-13db-43db-81f0-7a5d40f68381	action	set	effective_caller_id_name=${outbound_caller_id_name}	\N	\N	\N	25
60d6731b-e548-4175-ab3e-3fbb261e60f5	3b5fe071-1b2a-412f-b359-89a7daff618f	e2182b2a-e448-4c37-be11-5554ff8bddff	action	set	effective_caller_id_number=${outbound_caller_id_number}	\N	\N	\N	30
60d6731b-e548-4175-ab3e-3fbb261e60f5	3b5fe071-1b2a-412f-b359-89a7daff618f	4ae4c4e0-55e0-4885-a4c2-0abb0ab77ca5	action	set	inherit_codec=true	\N	\N	\N	35
60d6731b-e548-4175-ab3e-3fbb261e60f5	3b5fe071-1b2a-412f-b359-89a7daff618f	5d65c781-08ba-4f7d-bf3c-ff07a9002a34	action	set	continue_on_fail=true	\N	\N	\N	40
60d6731b-e548-4175-ab3e-3fbb261e60f5	3b5fe071-1b2a-412f-b359-89a7daff618f	11df7dd3-a1b2-482e-9361-aa553c46b560	action	bridge	sofia/gateway/b3c04082-9cad-41b7-a664-d15ecd92fd02/$1	\N	\N	\N	55
\.


--
-- Data for Name: v_dialplans; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_dialplans (domain_uuid, dialplan_uuid, app_uuid, dialplan_context, dialplan_name, dialplan_number, dialplan_continue, dialplan_order, dialplan_enabled, dialplan_description) FROM stdin;
60d6731b-e548-4175-ab3e-3fbb261e60f5	832a2be2-fe70-493c-a3aa-87a4c6554370	897845b0-1f13-444c-84fe-432fd47338ca	default	user_exists		true	10	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	3c2507d5-d6e9-4d95-93f1-11ec718fc350	9f356fe7-8cf8-4c14-8fe2-6daf89304458	default	variables		true	20	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	db204b4d-fd08-41bf-84d6-e34e3e038916	a1685b18-21aa-4d77-9f95-c0013b7286a3	default	is_local		true	30	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	33c104e2-9cb9-4d0d-b6c9-2579b8d97249	b1b31930-d0ee-4395-a891-04df94599f1f	default	call_block		true	40	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	bb81df82-fd70-4ce6-a8e3-d97f1937a94c	43539dd3-d555-4498-835a-3245a0b184ca	default	user_record		true	50	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	cf3f8ed6-740b-42f4-91e6-5794d59ed981	2eb032c5-c79d-4096-ac90-8a47fe40f411	default	user_status	*22	false	210	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	8afcc1af-0e57-45cf-a036-f33914a14f99	59aff1f2-719c-403e-9306-5eb3a0cbd16c	default	global-intercept	*886	false	220	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	b199391e-8909-4555-aad3-9722b0ee2c0f	15332e83-12f5-44d3-8472-633736eb4b9b	default	group-intercept	*8	false	230	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	9901adea-b483-4253-8d86-ae4d8b6209c1	459da8c1-073a-458e-ae7e-8194342f9583	default	redial	*870	false	240	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	cd5d4471-614a-4717-8d2b-382104eaa4ae	2011c518-696d-4878-a9b2-b217b6311311	default	page	*724	false	250	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	ef20d627-dc21-4a1f-82a5-1cbefc08846e	e944af7e-8fcc-429b-a32f-0dcdce1585d8	default	eavesdrop	*88[ext]	false	260	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	ce999264-89f0-4baf-9741-26037935276a	eb478e66-f637-4ae7-b1eb-9a7b87a1bd9e	default	call_privacy	*67[d+]	false	270	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	7c7232bb-f386-461c-96cf-6c89029939c8	fa516204-920f-4802-8bb1-04c6a010bfe1	default	call_return	*69	false	280	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	0b35b102-7acf-420a-8b45-729bcdad6490	2b7b2f82-edfe-4339-8cc5-7d0cf36e1e68	default	intercept-ext	**[ext]	false	290	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	04600444-9f69-427b-a108-3a3bca0751eb	ddcf7740-78ca-4035-8c19-e2df10cebf67	default	dx	dx	false	300	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	aed96a7a-7a7a-45e3-b5fc-885adcb8f101	1b224444-de8b-448d-b2d1-19feaac3effa	default	extension-intercom	*8[ext]	false	300	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	e4e6b6a6-65d3-463b-98ec-f738a7949477	7bd7a113-0afc-406a-b4a7-33077c22ac39	default	att_xfer	att_xfer	false	310	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	80917642-03da-4136-98bc-aea43b97b2d1	8a2e7b81-996c-4d6b-87df-b879b972a572	default	extension-to-voicemail	[ext]	false	310	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	2b7218c5-0498-4b48-91c9-e5878a8c5069	001d5dab-e0c6-4352-8f06-e9986ee7b0d8	default	send_to_voicemail	*99[ext]	false	310	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	59ab0756-6dff-4ca9-8df2-776676042feb	d085a1e3-c53a-4480-9ca6-6a362899a681	default	vmain	*98	false	320	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	3cdb7409-f0b6-4e09-b055-923a878595e4	44a4b26d-9e13-41dc-8405-7ff2e4a215e0	default	xfer_vm	xfer_vm	false	320	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	ac8513db-39fa-4c0a-a337-a29afcb0f568	da8e22c7-9e07-4ff5-a28b-faf35ba8d411	default	is_transfer	is_transfer	false	330	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	5aac9fad-64b5-4fc4-ac31-f6b83c7acfd8	5d47ab13-f25d-4f62-a68e-2a7d945d05b7	default	vmain_user	*97	false	330	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	f0e49bed-4dc7-4628-ab70-094b16585e16	f13df3df-bfb4-4c11-bee1-6548cd983729	default	cf	cf	false	340	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	4c7f90b6-b099-4f3b-8a46-4ea123a04c02	fe638409-b347-46d7-9aca-489561df8b35	default	delay_echo	*9195	false	340	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	b616b00e-e9ee-4f3a-8b59-2e0f8ea481d1	1f894dfb-0567-4e20-9026-d538bbaa5261	default	echo	*9196	false	350	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	7c753572-237c-422c-b05d-db7917dc9335	c3ea29d1-db41-421e-91b7-b0984e50bcae	default	please_hold	1001-1019	false	350	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	424f79ed-c9bc-4734-bf3e-407dd883fd7d	296acca3-d30f-42a0-ba90-5af2208ad7f8	default	milliwatt	*9197	false	360	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	22f0e3b2-9891-400a-a2fe-1922d6ca8d90	98ccdb0b-c074-4f74-b28a-9528372faa7d	default	tone_stream	*9198	false	370	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	fd4876cf-5db5-4b2c-8132-73bcd150434e	b824b88a-e6da-486e-9f17-7b93cbaa318e	default	hold_music	*9664	false	380	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	46ea5764-9958-437c-b004-2f8b40fe667c	430737df-5385-42d1-b933-22600d3fb79e	default	recordings	*732	false	400	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	a356af9a-10c3-4d3b-a939-b7ccfb87ad07	8e3ad78e-4b48-4d36-af7e-4920a9757043	default	freeswitch_conference	*9888	false	410	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	0f67fd8e-031d-42dc-bdb8-6c828dafeb59	3ade2d9a-f55d-4240-bb60-b4a3ab36951c	default	disa	*3472	false	420	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	ef158010-9457-4f6a-9a3c-8cbdf92160cc	a223dc70-28a1-4979-834e-8af813cd8ea6	default	directory	*411	false	430	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	657266f0-4ed5-4a2c-b31a-a6df573569ee	e27abe68-41c0-4188-bb0f-67d93de0c610	default	wake-up	*925	false	440	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	104cbd03-d8f5-4664-9a2f-70db92418771	ffb30da6-30e4-43b8-9847-2ed40e232f89	default	park_in	*5900	false	450	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	7f937674-97a3-45e2-a4c2-fa5bbfd44dd4	cf8234b6-a039-4ec2-87bd-3db0799a3f75	default	park_out	*5900-*5999	false	455	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	8890ec25-7ff2-4554-bdad-e07362930f43	f096f899-c78e-409c-8290-00f6d423b31c	default	park_slots	*5900-*5999	false	460	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	b155896e-6525-4166-a7df-9e6ad8a7cdad	3cc8363d-5ce3-48aa-8ac1-143cf297c4f7	default	valet_park	park+*5901-*5999	false	470	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	862a8a6b-24dd-4ea8-a283-70467d0c7b32	c192ee50-084d-40d8-8d9a-6959369382c8	default	valet_park_in	park+*5900	false	470	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	09ea7c5a-77b5-4b7a-8a03-1b07b77c10f3	242130d4-61d6-4daf-9dd1-b139a2b3b166	default	valet_park_out	park+*5901-*5999	false	475	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	2e7ac7bf-ea6d-4ada-b7be-9fafd97a249b	a90d3639-3b82-4905-a65d-85f58b6c4a19	default	operator-forward	*000	false	485	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	bf6b14a1-ed80-4122-9759-0dde8852c885	df32d982-e39e-4ae5-a46d-aed1893873f2	default	do-not-disturb	*78,*79	false	490	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	91cdaa61-862c-4ec8-b800-9cb12a2c32bd	b4b32fb4-0181-4876-9bec-b9dff1299d60	default	call-forward	*72,*73,*74	false	500	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	b538b96f-598f-44e1-ae53-086d11951fdb	b8c28c75-1a03-4dad-9a53-980ca5f487f0	default	follow-me	*21	false	510	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	d4f6bccc-4d14-4725-84ed-795a42df0737	71cf1310-b6e3-415b-8745-3cbdc8e15212	default	local_extension	[ext]	false	999	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	86a8e20c-325d-4eec-8b14-7642bbcc41b5	3780f814-5543-4350-b65d-563512d1fe71	default	call-direction	720	true	20	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	a7712974-9f5c-4ef6-a5c5-13cfe7136af3	89aec992-e3bb-43a1-a64b-ca70800e30fd	default	hot-desk-login	*072		470	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	00a48ebc-e877-44da-a25e-4169bf2e37be	97e920d9-dddc-458f-bae0-837a48c3f401	default	hot-desk-logout	*073		475	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fe423c2f-114d-40bf-8c7b-79c14f0dbaaa	2b7b2f82-edfe-4339-8cc5-7d0cf36e1e68	sigil.c-call.eu	intercept-ext	**[ext]	false	290	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	38f2ab98-d586-4d5f-8813-00e59b495dcc	ddcf7740-78ca-4035-8c19-e2df10cebf67	sigil.c-call.eu	dx	dx	false	300	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	620ec152-be51-4f59-863d-4bb94259e8c8	951808c2-b778-404d-bd17-50b5df4b88f4	default	is_zrtp_secure		false	360	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	1fe7dfc6-2c74-4e7a-ad49-339c974ff6be	b57306e0-36df-4048-b182-7ad0f69d8c03	default	is_secure	is_secure	true	370	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	54d55a11-9a04-45a5-86c2-dd3234487e0a	0e1cd2d7-9d84-4959-8b6c-0cb23de4de59	default	operator	0	false	480	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	cd410c7d-6f98-4117-88d6-1d96b05fcae1	897845b0-1f13-444c-84fe-432fd47338ca	sigil.c-call.eu	user_exists		true	10	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ca2d180c-599e-4e64-87fc-7ea36586fbc3	3780f814-5543-4350-b65d-563512d1fe71	sigil.c-call.eu	call-direction		true	20	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	66beee43-444e-4bb5-b351-a56483c823eb	9f356fe7-8cf8-4c14-8fe2-6daf89304458	sigil.c-call.eu	variables		true	20	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bab0214c-e8a5-43e8-9cd9-7f523574f346	a1685b18-21aa-4d77-9f95-c0013b7286a3	sigil.c-call.eu	is_local		true	30	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6f6c99c7-4289-4633-9d25-61825c963fa5	b1b31930-d0ee-4395-a891-04df94599f1f	sigil.c-call.eu	call_block		true	40	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b06591c-d0cd-4cb5-9cbc-a09fe26055b4	43539dd3-d555-4498-835a-3245a0b184ca	sigil.c-call.eu	user_record		true	50	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	27c9d813-de2f-4891-bd6d-01b75cd87551	2eb032c5-c79d-4096-ac90-8a47fe40f411	sigil.c-call.eu	user_status	*22	false	210	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	30dfceb7-a3e3-4464-ba6e-a6b559c219cc	59aff1f2-719c-403e-9306-5eb3a0cbd16c	sigil.c-call.eu	global-intercept	*886	false	220	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	51036f37-3403-4916-9476-4eec18b90393	15332e83-12f5-44d3-8472-633736eb4b9b	sigil.c-call.eu	group-intercept	*8	false	230	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f31459d6-c0b9-4f6a-a426-27e50a5939c3	459da8c1-073a-458e-ae7e-8194342f9583	sigil.c-call.eu	redial	*870	false	240	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	42f12517-9d06-4d11-92e4-0d1a890706cb	2011c518-696d-4878-a9b2-b217b6311311	sigil.c-call.eu	page	*724	false	250	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6871595a-f860-4fdb-86d5-92e818102b1f	e944af7e-8fcc-429b-a32f-0dcdce1585d8	sigil.c-call.eu	eavesdrop	*88[ext]	false	260	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	7579668a-b69a-40d4-9fc1-8b53556c831b	eb478e66-f637-4ae7-b1eb-9a7b87a1bd9e	sigil.c-call.eu	call_privacy	*67[d+]	false	270	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	eb7ef69a-5333-42e5-a181-021d40b7c6af	fa516204-920f-4802-8bb1-04c6a010bfe1	sigil.c-call.eu	call_return	*69	false	280	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d5e4f1df-5af2-4f50-91c5-e0cf7069cf76	1b224444-de8b-448d-b2d1-19feaac3effa	sigil.c-call.eu	extension-intercom	*8[ext]	false	300	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4e53e1c0-20d9-404d-8203-d3eda7ebce80	7bd7a113-0afc-406a-b4a7-33077c22ac39	sigil.c-call.eu	att_xfer	att_xfer	false	310	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	bb6a3c6c-74f7-4829-8acd-083acbbb273d	8a2e7b81-996c-4d6b-87df-b879b972a572	sigil.c-call.eu	extension-to-voicemail	[ext]	false	310	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f3baf3fc-2691-415c-9804-61afa7e4b0b9	001d5dab-e0c6-4352-8f06-e9986ee7b0d8	sigil.c-call.eu	send_to_voicemail	*99[ext]	false	310	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	a2f8ca40-15c6-438a-a116-fb21b48670ad	d085a1e3-c53a-4480-9ca6-6a362899a681	sigil.c-call.eu	vmain	*98	false	320	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	0f16bb3f-cb3b-4907-8580-ebed46649a85	44a4b26d-9e13-41dc-8405-7ff2e4a215e0	sigil.c-call.eu	xfer_vm	xfer_vm	false	320	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	14c6ab00-c40f-4000-8d07-73e9392a00b2	da8e22c7-9e07-4ff5-a28b-faf35ba8d411	sigil.c-call.eu	is_transfer	is_transfer	false	330	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ebb1fcaf-03ec-46e0-a1a2-3989acc6cd31	5d47ab13-f25d-4f62-a68e-2a7d945d05b7	sigil.c-call.eu	vmain_user	*97	false	330	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4b59d1a9-6052-4c41-ae75-62b729c8559f	f13df3df-bfb4-4c11-bee1-6548cd983729	sigil.c-call.eu	cf	cf	false	340	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	2d227581-5b0e-4f35-862a-a5093b2d2c3a	fe638409-b347-46d7-9aca-489561df8b35	sigil.c-call.eu	delay_echo	*9195	false	340	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e4c191df-b205-4621-b559-daba14750f84	1f894dfb-0567-4e20-9026-d538bbaa5261	sigil.c-call.eu	echo	*9196	false	350	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	4a3e487d-202c-48de-9d0e-e5c02c6e9199	c3ea29d1-db41-421e-91b7-b0984e50bcae	sigil.c-call.eu	please_hold	1001-1019	false	350	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	97640de5-c50f-42aa-bae3-e2ab77e15b2d	951808c2-b778-404d-bd17-50b5df4b88f4	sigil.c-call.eu	is_zrtp_secure		false	360	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	66761ab2-9158-4cd5-bf53-804a28d475ee	296acca3-d30f-42a0-ba90-5af2208ad7f8	sigil.c-call.eu	milliwatt	*9197	false	360	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6644feaf-af62-4862-8b4a-c697dd84c34d	b57306e0-36df-4048-b182-7ad0f69d8c03	sigil.c-call.eu	is_secure	is_secure	true	370	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	6b8ec018-a99d-4e31-81e2-3e58703afdd5	98ccdb0b-c074-4f74-b28a-9528372faa7d	sigil.c-call.eu	tone_stream	*9198	false	370	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	26c5e134-9773-4ab3-b52a-41d584a26daa	b824b88a-e6da-486e-9f17-7b93cbaa318e	sigil.c-call.eu	hold_music	*9664	false	380	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b01ee25e-973a-4487-a19e-f5880be8566c	430737df-5385-42d1-b933-22600d3fb79e	sigil.c-call.eu	recordings	*732	false	400	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	80eaf8fe-134c-4a8d-b3dc-f42ce47012d5	8e3ad78e-4b48-4d36-af7e-4920a9757043	sigil.c-call.eu	freeswitch_conference	*9888	false	410	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9cb26349-f2fa-4e91-910d-94015679b89f	3ade2d9a-f55d-4240-bb60-b4a3ab36951c	sigil.c-call.eu	disa	*3472	false	420	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	0105f5cd-d66a-4aeb-9a33-5e235ff68751	a223dc70-28a1-4979-834e-8af813cd8ea6	sigil.c-call.eu	directory	*411	false	430	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	8be784dd-139f-4106-b314-560bb32e0ee3	e27abe68-41c0-4188-bb0f-67d93de0c610	sigil.c-call.eu	wake-up	*925	false	440	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	d1a07b69-b4fa-44f8-9034-39f911baab21	ffb30da6-30e4-43b8-9847-2ed40e232f89	sigil.c-call.eu	park_in	*5900	false	450	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	7f3eeb36-c66d-4ab0-9974-c0604b0268c5	cf8234b6-a039-4ec2-87bd-3db0799a3f75	sigil.c-call.eu	park_out	*5900-*5999	false	455	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	e76e9134-4392-4af0-9ae9-749dc2084fd7	f096f899-c78e-409c-8290-00f6d423b31c	sigil.c-call.eu	park_slots	*5900-*5999	false	460	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	aa0a249e-d095-46cb-bb96-4fd8444c449b	3cc8363d-5ce3-48aa-8ac1-143cf297c4f7	sigil.c-call.eu	valet_park	park+*5901-*5999	false	470	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	57289bf2-49d6-4734-b786-d4218e4bc0e5	c192ee50-084d-40d8-8d9a-6959369382c8	sigil.c-call.eu	valet_park_in	park+*5900	false	470	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	2c6a4345-7759-47d7-bc63-67241d67b731	242130d4-61d6-4daf-9dd1-b139a2b3b166	sigil.c-call.eu	valet_park_out	park+*5901-*5999	false	475	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	3d574307-cd3e-4a51-a3d0-5f0fa9a616b9	0e1cd2d7-9d84-4959-8b6c-0cb23de4de59	sigil.c-call.eu	operator	0	false	480	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9225e08b-320d-42b9-aa0d-9a0f77b55570	a90d3639-3b82-4905-a65d-85f58b6c4a19	sigil.c-call.eu	operator-forward	*000	false	485	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b27e7d9d-5512-4685-ad78-48b850748a9e	df32d982-e39e-4ae5-a46d-aed1893873f2	sigil.c-call.eu	do-not-disturb	*78,*79	false	490	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	67171a49-bdb0-4adf-8574-f2a32a45d22a	b4b32fb4-0181-4876-9bec-b9dff1299d60	sigil.c-call.eu	call-forward	*72,*73,*74	false	500	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	f88b3ea8-4445-4a12-a6a0-105fd91f7a7d	b8c28c75-1a03-4dad-9a53-980ca5f487f0	sigil.c-call.eu	follow-me	*21	false	510	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	c3a6efce-ab9d-4c03-8da4-f26259a79695	71cf1310-b6e3-415b-8745-3cbdc8e15212	sigil.c-call.eu	local_extension	[ext]	false	999	true	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	b626cbb2-684a-4256-a978-8f57f7a47e04	89aec992-e3bb-43a1-a64b-ca70800e30fd	sigil.c-call.eu	hot-desk-login	*072	false	470	false	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	9b4ecefb-e961-433d-93f8-2343e1d52d14	97e920d9-dddc-458f-bae0-837a48c3f401	sigil.c-call.eu	hot-desk-logout	*073	false	475	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	709af092-f796-4de2-a779-d7bdd9acc279	c03b422e-13a8-bd1b-e42b-b6b9b4d27ce4	public	48598881023	48598881023	false	100	true	\N
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	66ed2e61-1ae0-4c0f-938e-13a5a13f5d3b	897845b0-1f13-444c-84fe-432fd47338ca	panthosoutsourcering.c-call.eu	user_exists		true	10	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1d5aaa24-14e8-43ea-9020-ee10bbcdeff4	3780f814-5543-4350-b65d-563512d1fe71	panthosoutsourcering.c-call.eu	call-direction		true	20	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	f2896ce9-6bee-46ed-8b0a-2262d6970535	9f356fe7-8cf8-4c14-8fe2-6daf89304458	panthosoutsourcering.c-call.eu	variables		true	20	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ea39f721-ca6c-40e5-b03e-df826c4f3dfd	a1685b18-21aa-4d77-9f95-c0013b7286a3	panthosoutsourcering.c-call.eu	is_local		true	30	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fde54f5d-e465-46cb-a3ea-40401c74df9e	b1b31930-d0ee-4395-a891-04df94599f1f	panthosoutsourcering.c-call.eu	call_block		true	40	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4c706e0c-b841-4943-a388-4f13df425928	43539dd3-d555-4498-835a-3245a0b184ca	panthosoutsourcering.c-call.eu	user_record		true	50	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	6215ebd1-81d3-4d61-b2a5-c38d0073cc03	2eb032c5-c79d-4096-ac90-8a47fe40f411	panthosoutsourcering.c-call.eu	user_status	*22	false	210	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	555aaa34-1eb7-4af5-a1d8-fa23cd391f00	59aff1f2-719c-403e-9306-5eb3a0cbd16c	panthosoutsourcering.c-call.eu	global-intercept	*886	false	220	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	cfd67640-914a-4b84-9c0b-ace4396da28f	15332e83-12f5-44d3-8472-633736eb4b9b	panthosoutsourcering.c-call.eu	group-intercept	*8	false	230	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1fb228a6-8c8c-4c1b-9417-e299dd39e0f7	459da8c1-073a-458e-ae7e-8194342f9583	panthosoutsourcering.c-call.eu	redial	*870	false	240	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d46cd68f-1cc5-4d4c-bbaa-2e0bc563f9e6	2011c518-696d-4878-a9b2-b217b6311311	panthosoutsourcering.c-call.eu	page	*724	false	250	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	6685b141-c688-435c-806c-af6f412a3933	e944af7e-8fcc-429b-a32f-0dcdce1585d8	panthosoutsourcering.c-call.eu	eavesdrop	*88[ext]	false	260	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1993439c-f903-4270-9498-1bd06374b6c8	eb478e66-f637-4ae7-b1eb-9a7b87a1bd9e	panthosoutsourcering.c-call.eu	call_privacy	*67[d+]	false	270	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	8f02f7b0-f08e-4868-a635-697a9c6319b1	fa516204-920f-4802-8bb1-04c6a010bfe1	panthosoutsourcering.c-call.eu	call_return	*69	false	280	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	88a6fa14-8aac-4be3-bab7-4b849b593881	2b7b2f82-edfe-4339-8cc5-7d0cf36e1e68	panthosoutsourcering.c-call.eu	intercept-ext	**[ext]	false	290	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	1389aa00-b02a-48be-8054-8306d96b85b9	ddcf7740-78ca-4035-8c19-e2df10cebf67	panthosoutsourcering.c-call.eu	dx	dx	false	300	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	90877902-82d5-4b8f-8a16-a7a5ba245246	1b224444-de8b-448d-b2d1-19feaac3effa	panthosoutsourcering.c-call.eu	extension-intercom	*8[ext]	false	300	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5662f5dd-3802-42c5-8ed2-2f7a84a36f93	7bd7a113-0afc-406a-b4a7-33077c22ac39	panthosoutsourcering.c-call.eu	att_xfer	att_xfer	false	310	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cb11b91-bca8-4cb3-8587-348e96cb5b5d	8a2e7b81-996c-4d6b-87df-b879b972a572	panthosoutsourcering.c-call.eu	extension-to-voicemail	[ext]	false	310	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	17104eb4-244b-41c6-ab4d-2f43850030f5	001d5dab-e0c6-4352-8f06-e9986ee7b0d8	panthosoutsourcering.c-call.eu	send_to_voicemail	*99[ext]	false	310	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	01c95137-2f78-47ba-b518-8db287f6789c	d085a1e3-c53a-4480-9ca6-6a362899a681	panthosoutsourcering.c-call.eu	vmain	*98	false	320	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	a989b32c-eefb-47cc-9aad-5d7789905aa3	44a4b26d-9e13-41dc-8405-7ff2e4a215e0	panthosoutsourcering.c-call.eu	xfer_vm	xfer_vm	false	320	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b3c0f873-b537-49b7-a426-bda6dd20ada5	da8e22c7-9e07-4ff5-a28b-faf35ba8d411	panthosoutsourcering.c-call.eu	is_transfer	is_transfer	false	330	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	da91410a-1901-41df-85f9-01495a917103	5d47ab13-f25d-4f62-a68e-2a7d945d05b7	panthosoutsourcering.c-call.eu	vmain_user	*97	false	330	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0cf1fc90-b48b-4b48-9764-271618d0bd17	f13df3df-bfb4-4c11-bee1-6548cd983729	panthosoutsourcering.c-call.eu	cf	cf	false	340	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	39139117-3c74-440d-90ff-b476a5d1cb63	fe638409-b347-46d7-9aca-489561df8b35	panthosoutsourcering.c-call.eu	delay_echo	*9195	false	340	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0695301f-3f37-4e8f-ab98-3b9f7a640a3d	1f894dfb-0567-4e20-9026-d538bbaa5261	panthosoutsourcering.c-call.eu	echo	*9196	false	350	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	33241635-82c7-4315-8f23-66d9a096710b	c3ea29d1-db41-421e-91b7-b0984e50bcae	panthosoutsourcering.c-call.eu	please_hold	1001-1019	false	350	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0c561674-2b8b-4f4e-b5e2-c87f689ab9d4	951808c2-b778-404d-bd17-50b5df4b88f4	panthosoutsourcering.c-call.eu	is_zrtp_secure		false	360	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	2a6d9c67-2704-4a74-b5af-c491c284d9c6	296acca3-d30f-42a0-ba90-5af2208ad7f8	panthosoutsourcering.c-call.eu	milliwatt	*9197	false	360	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d337fd44-f1d0-41c1-ae2b-6fbb8322a24d	b57306e0-36df-4048-b182-7ad0f69d8c03	panthosoutsourcering.c-call.eu	is_secure	is_secure	true	370	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	76dac144-5693-4e0c-9946-7bf18f0b8a3b	98ccdb0b-c074-4f74-b28a-9528372faa7d	panthosoutsourcering.c-call.eu	tone_stream	*9198	false	370	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b794a80d-60e4-4030-92d4-e50ea6f11fd4	b824b88a-e6da-486e-9f17-7b93cbaa318e	panthosoutsourcering.c-call.eu	hold_music	*9664	false	380	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	8e2a4f36-2b53-41ea-83f9-ec71ec4a5836	430737df-5385-42d1-b933-22600d3fb79e	panthosoutsourcering.c-call.eu	recordings	*732	false	400	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	db93047f-8d8f-4874-805a-0293036885ca	8e3ad78e-4b48-4d36-af7e-4920a9757043	panthosoutsourcering.c-call.eu	freeswitch_conference	*9888	false	410	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	b4fb961e-f718-4f88-a7de-f13d22df7632	3ade2d9a-f55d-4240-bb60-b4a3ab36951c	panthosoutsourcering.c-call.eu	disa	*3472	false	420	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	23803346-5926-4209-8428-c6089118e1dd	a223dc70-28a1-4979-834e-8af813cd8ea6	panthosoutsourcering.c-call.eu	directory	*411	false	430	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	04a22f2f-3c53-4932-8a31-7e01df88304f	e27abe68-41c0-4188-bb0f-67d93de0c610	panthosoutsourcering.c-call.eu	wake-up	*925	false	440	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	5603932f-bc8b-4133-80a0-f70c69c22087	ffb30da6-30e4-43b8-9847-2ed40e232f89	panthosoutsourcering.c-call.eu	park_in	*5900	false	450	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e62e709a-6de3-4b1c-82b3-e61f13472390	cf8234b6-a039-4ec2-87bd-3db0799a3f75	panthosoutsourcering.c-call.eu	park_out	*5900-*5999	false	455	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7734ba60-dc3c-44d9-9cbc-d238f253ae0f	f096f899-c78e-409c-8290-00f6d423b31c	panthosoutsourcering.c-call.eu	park_slots	*5900-*5999	false	460	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4803566d-1786-4294-9fe3-b2520ef0ef6a	3cc8363d-5ce3-48aa-8ac1-143cf297c4f7	panthosoutsourcering.c-call.eu	valet_park	park+*5901-*5999	false	470	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e54d0024-756d-4aa6-ba20-77a1646dbe54	c192ee50-084d-40d8-8d9a-6959369382c8	panthosoutsourcering.c-call.eu	valet_park_in	park+*5900	false	470	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	084d9145-a799-4e48-9250-76baa39556f5	242130d4-61d6-4daf-9dd1-b139a2b3b166	panthosoutsourcering.c-call.eu	valet_park_out	park+*5901-*5999	false	475	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	4d4b32b7-ff68-4e0f-a714-16ddec23d4fe	0e1cd2d7-9d84-4959-8b6c-0cb23de4de59	panthosoutsourcering.c-call.eu	operator	0	false	480	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	d8284ec9-b8a4-4068-8036-2a21b5323f9e	a90d3639-3b82-4905-a65d-85f58b6c4a19	panthosoutsourcering.c-call.eu	operator-forward	*000	false	485	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	7a95ee0f-ea92-41c7-9fc8-3deb436940f4	df32d982-e39e-4ae5-a46d-aed1893873f2	panthosoutsourcering.c-call.eu	do-not-disturb	*78,*79	false	490	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	893d6433-c331-4040-941d-e5d568151af0	b4b32fb4-0181-4876-9bec-b9dff1299d60	panthosoutsourcering.c-call.eu	call-forward	*72,*73,*74	false	500	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	c098c60a-6bc8-4c41-b8bb-69a7709f0fca	b8c28c75-1a03-4dad-9a53-980ca5f487f0	panthosoutsourcering.c-call.eu	follow-me	*21	false	510	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	e241d5d8-f5ac-4639-b2d8-f96d891be218	71cf1310-b6e3-415b-8745-3cbdc8e15212	panthosoutsourcering.c-call.eu	local_extension	[ext]	false	999	true	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ee6ea23e-ea23-4778-a6ff-6c5a55ee9362	89aec992-e3bb-43a1-a64b-ca70800e30fd	panthosoutsourcering.c-call.eu	hot-desk-login	*072	false	470	false	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	0f2067ad-4b9b-4fe7-830e-0d0c70ec87e4	97e920d9-dddc-458f-bae0-837a48c3f401	panthosoutsourcering.c-call.eu	hot-desk-logout	*073	false	475	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	3b5fe071-1b2a-412f-b359-89a7daff618f	8c914ec3-9fc0-8ab5-4cda-6c9288bdc9a3	sip.c-call.eu	hive.c-call.eu.d1020	\N	false	100	true	
\.


--
-- Data for Name: v_domain_settings; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_domain_settings (domain_uuid, domain_setting_uuid, domain_setting_category, domain_setting_subcategory, domain_setting_name, domain_setting_value, domain_setting_order, domain_setting_enabled, domain_setting_description) FROM stdin;
60d6731b-e548-4175-ab3e-3fbb261e60f5	adef57d5-7b7b-487c-a61d-91bc4768c192	domain	menu	uuid	b4750c3f-2a86-b00d-b7d0-345c14eca286	\N	true	\N
60d6731b-e548-4175-ab3e-3fbb261e60f5	e97326ce-2dae-4e38-bb1d-fea0e09efc55	domain	template	name	enhanced	\N	true	\N
\.


--
-- Data for Name: v_domains; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_domains (domain_uuid, domain_name, domain_description) FROM stdin;
60d6731b-e548-4175-ab3e-3fbb261e60f5	sip.c-call.eu	
0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sigil.c-call.eu	
a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	panthosoutsourcering.c-call.eu	Panthos Outsourcering
\.


--
-- Data for Name: v_emails; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_emails (email_uuid, call_uuid, domain_uuid, sent_date, type, status, email) FROM stdin;
\.


--
-- Data for Name: v_extension_users; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_extension_users (extension_user_uuid, domain_uuid, extension_uuid, user_uuid) FROM stdin;
02696f1c-866f-42ec-b4d7-776df820e78b	60d6731b-e548-4175-ab3e-3fbb261e60f5	5ec78211-d93c-4b59-848a-fe7a2beaedf4	3af1a190-2327-4c03-822e-5dfbb93f1235
a411c21c-52de-4a52-8008-b2f33c5cfab0	60d6731b-e548-4175-ab3e-3fbb261e60f5	add67c66-604c-491d-aa14-678eeb3c4674	5ee2ef7d-35b2-4d11-8917-894fdfb32a57
\.


--
-- Data for Name: v_extensions; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_extensions (extension_uuid, domain_uuid, extension, number_alias, password, accountcode, effective_caller_id_name, effective_caller_id_number, outbound_caller_id_name, outbound_caller_id_number, emergency_caller_id_name, emergency_caller_id_number, directory_full_name, directory_visible, directory_exten_visible, limit_max, limit_destination, user_context, toll_allow, call_timeout, call_group, user_record, hold_music, auth_acl, cidr, sip_force_contact, nibble_account, sip_force_expires, mwi_account, sip_bypass_media, unique_id, dial_string, dial_user, dial_domain, do_not_disturb, forward_all_destination, forward_all_enabled, forward_busy_destination, forward_busy_enabled, follow_me_uuid, enabled, description) FROM stdin;
5ec78211-d93c-4b59-848a-fe7a2beaedf4	60d6731b-e548-4175-ab3e-3fbb261e60f5	118	118	Yu5dIiJ$HV	118118	48598881023	48598881023	48598881023	48598881023	48598881023	48598881023		true	true	5		sip.c-call.eu		30							\N	\N			\N		\N	\N	\N	\N	\N	\N	\N	\N	true	
add67c66-604c-491d-aa14-678eeb3c4674	60d6731b-e548-4175-ab3e-3fbb261e60f5	106	106	tojesthaslo	106106	48223072353	48223072353	48223072353	48223072353	48223072353	48223072353		true	true	5		panthosoutsourcering.c-call.eu		30							\N	\N			\N		\N	\N	\N	\N	\N	\N	\N	\N	true	
e9be1664-0e1a-4e79-891a-60c4e4e41c54	60d6731b-e548-4175-ab3e-3fbb261e60f5	834	834	RZR!HPugk!		48558811007	48558811007	48558811007	48558811007	48558811007	48558811007		true	true	5		sip.c-call.eu		30							\N	\N			\N		\N	\N	\N	\N	\N	\N	\N	\N	true	
\.


--
-- Data for Name: v_fax; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_fax (fax_uuid, domain_uuid, dialplan_uuid, fax_extension, fax_destination_number, fax_name, fax_email, fax_pin_number, fax_caller_id_name, fax_caller_id_number, fax_forward_number, fax_description) FROM stdin;
\.


--
-- Data for Name: v_fax_logs; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_fax_logs (domain_uuid, domain_name, fax_success, fax_result_code, fax_result_text, fax_file, uuid, fax_ecm_used, fax_local_station_id, fax_document_transferred_pages, fax_document_total_pages, fax_image_resolution, fax_image_size, fax_bad_rows, fax_transfer_rate, fax_retry_attempts, fax_retry_limit, fax_retry_sleep, fax_uri) FROM stdin;
\.


--
-- Data for Name: v_fax_users; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_fax_users (fax_user_uuid, domain_uuid, fax_uuid, user_uuid) FROM stdin;
\.


--
-- Data for Name: v_follow_me; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_follow_me (domain_uuid, follow_me_uuid, cid_name_prefix, cid_number_prefix, call_prompt, dial_string, follow_me_enabled) FROM stdin;
\.


--
-- Data for Name: v_follow_me_destinations; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_follow_me_destinations (domain_uuid, follow_me_uuid, follow_me_destination_uuid, follow_me_destination, follow_me_delay, follow_me_timeout, follow_me_prompt, follow_me_order) FROM stdin;
\.


--
-- Data for Name: v_gateways; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_gateways (gateway_uuid, domain_uuid, gateway, username, password, distinct_to, auth_username, realm, from_user, from_domain, proxy, register_proxy, outbound_proxy, expire_seconds, register, register_transport, retry_seconds, extension, ping, caller_id_in_from, supress_cng, sip_cid_type, codec_prefs, channels, extension_in_contact, context, profile, enabled, description) FROM stdin;
9bf535bf-b669-42e1-8579-2ffd2c540936	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip.voippro.com	ccalleu123	Factol098			voippro.com	ccalleu123		sip.voippro.com	sip.voippro.com	sip.voippro.com	800	false		30			true				0		public	external	true	
b3c04082-9cad-41b7-a664-d15ecd92fd02	60d6731b-e548-4175-ab3e-3fbb261e60f5	hive.c-call.eu:5161	986563	70992668		986563				hive.c-call.eu:5161			800	true		30							0		public	external	true	
78fe4e95-b381-4e29-8de3-15b91756a68d	60d6731b-e548-4175-ab3e-3fbb261e60f5	LocalGW	445901	90057496		445901				127.0.0.1:5757			800	true		30							0		public	external	true	
48723f05-eaae-4e67-80e4-5f5ddb5091ac	60d6731b-e548-4175-ab3e-3fbb261e60f5	hive.c-call.eu:5858	659991	2613428		659991		659991		hive.c-call.eu:5858			800	true		30							0		public	external	true	
\.


--
-- Data for Name: v_group_permissions; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_group_permissions (group_permission_uuid, domain_uuid, permission_name, group_name) FROM stdin;
7781ae1b-46a7-41a2-ad0c-bee4a8088218	60d6731b-e548-4175-ab3e-3fbb261e60f5	adminer	superadmin
a04ca507-b196-4d5d-9fee-12ab6c5402e1	60d6731b-e548-4175-ab3e-3fbb261e60f5	backup_download	superadmin
79146e46-9a6d-4124-b599-36198bbea41b	60d6731b-e548-4175-ab3e-3fbb261e60f5	backup_upload	superadmin
a317f4d9-c0c2-4aec-9277-0969596bd128	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_view	superadmin
58ce664e-731f-4006-8d3f-c62e6f652f2e	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_view	admin
e137d616-d8eb-4dc0-aed8-1171913dea95	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_view	user
6bfce4b7-9b7d-441c-b3a6-26d29c4a324e	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_add	superadmin
6a32f307-20fe-4a1d-aa3e-d96df47abe74	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_add	admin
3d43e7f9-20f6-4a9f-ac31-e6989e8224d0	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_add	user
eef4b52d-5ddf-4ede-a2db-2efb883d480d	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_edit	superadmin
6b5a3393-25d2-419e-a54c-e05425a6a4b4	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_edit	admin
21fa0e66-9bc4-4998-8877-591e6f673a7a	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_edit	user
c4d876d5-9ce4-4c41-98e7-40dc9b4a081c	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_delete	superadmin
21eb6ed0-367a-47f3-8796-0955222f8b2d	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_delete	admin
ee8db29e-70dc-4e1c-a4f2-e83226e075df	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_block_delete	user
0f26487f-7215-452c-9a88-484871ceecc8	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_broadcast_view	admin
a62a738a-7870-4039-b31b-59689a7dd93d	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_broadcast_view	superadmin
b80be9d7-9388-47af-a69a-47f56b6dfd9f	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_broadcast_add	admin
27fa3a1c-c676-4e24-9d24-31771d8ea5d1	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_broadcast_add	superadmin
7b52a38d-ad55-4f99-b1b1-042c10d2c04a	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_broadcast_edit	admin
72dccc4c-47ff-48d1-a58c-beade5de154e	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_broadcast_edit	superadmin
97330cd8-a743-484b-902b-f8b45610db48	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_broadcast_delete	admin
3e1e348f-d8d4-4528-a3f1-0f7e09a05992	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_broadcast_delete	superadmin
cdc92aba-2235-4385-bcdb-49e41bd4c409	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_broadcast_send	admin
966f961d-983d-4410-bf27-359e9aba29f0	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_broadcast_send	superadmin
55bb6e4e-c39a-4ccb-9264-ff92cac39f15	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_queue_view	agent
159c65f0-b3ea-43ad-b949-ce1062e76811	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_queue_view	admin
c08c034b-eac1-464a-93b0-2827540cda24	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_queue_view	superadmin
b2319b2e-cbd5-442b-b994-762506c82866	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_queue_add	admin
6788d108-d97a-4a49-bf94-69f69c2f4562	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_queue_add	superadmin
2bb1e8ae-e4ec-444b-abbc-042a113ec211	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_queue_edit	admin
633d430d-18e7-43f2-b740-0d88f93613a7	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_queue_edit	superadmin
7e97dd1a-91a6-41dc-a1fb-6144e90ccd14	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_queue_delete	admin
d7434d6a-1175-48af-a8cd-abbf0c19408d	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_queue_delete	superadmin
f9bcca22-a12f-430a-aa6e-1dc467799f4c	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_agent_view	agent
ad9fa4a2-8829-4afc-9ad3-f1a5e7ea2777	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_agent_view	admin
a722b820-c895-4a9e-8d06-44a1275dd04c	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_agent_view	superadmin
38a69eb5-d0b2-4f1a-8b0e-4c4704205e14	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_agent_add	admin
ba9cf324-253b-49e1-8ae9-d101e94739b1	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_agent_add	superadmin
431ba142-6200-4a2f-9261-0a36a66acc8c	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_agent_edit	admin
543d0382-9a7d-4d48-9eda-8bbd68ee2f81	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_agent_edit	superadmin
fabfc644-1949-4b82-9947-edb3a744ecd4	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_agent_delete	admin
c9a7f6ae-ed12-4bc1-9a39-dadf84d7dc93	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_agent_delete	superadmin
ca653e06-4f65-4c97-b6f5-07722a90b274	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_tier_view	agent
34ea7adc-f7f6-4de3-b3db-6325611ede09	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_tier_view	admin
884ac243-061c-4898-91ae-c03dcd476179	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_tier_view	superadmin
c7d4c26b-5d8d-4d4a-9386-90dd70594727	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_tier_add	admin
a071f3c9-2684-4112-a891-c40dac61144a	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_tier_add	superadmin
736bfdeb-9ce8-4e9a-a4fa-b3048a95d4fe	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_tier_edit	admin
0337edc7-7542-453e-817d-474e61a7a732	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_tier_edit	superadmin
7ab5de44-d9b0-4e0b-8190-32b95883abe6	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_tier_delete	admin
cfc2412d-e008-461d-a82f-134456e9cfd4	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_tier_delete	superadmin
a620f4cd-3b0f-4b88-b2b2-46b64bca2db4	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_log_view	superadmin
2ad2ff9b-d886-4451-a7f6-462a24601bf7	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_log_view	admin
7a1325be-635b-40cc-8288-049cd2a96f43	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_log_add	superadmin
230676a6-d07d-4410-b9fa-957ad218c696	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_log_add	admin
902cece4-89b0-45fc-8238-3b7ce1243684	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_log_edit	superadmin
31ab3e15-c0fb-48ab-9c2f-d923903a7e68	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_log_edit	admin
44653fe3-09e1-4ca6-b0d3-f3965fab5a57	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_log_delete	superadmin
c0599328-2fca-4f40-87b5-644e085e1522	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_log_delete	admin
bf9cde83-d822-492b-b389-915ffdead138	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_active_view	admin
3700a543-5b66-4431-99dc-8d88a087901a	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_active_view	superadmin
17259773-9197-4255-8dff-8068cfeddd3d	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_active_options	admin
5c5ec1a9-067d-4115-8290-ca8b90df46dd	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_center_active_options	superadmin
bd6b7f30-7c98-4d0f-86b7-5541d2abfc8e	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_flow_view	superadmin
e5fb0c9c-bfbd-4d5f-aff4-0380700c6e44	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_flow_view	admin
b5cecbeb-3e43-4d90-9d20-51b3548515a0	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_flow_add	superadmin
5abede72-f345-4ae4-ac70-4ea5fbad6dd4	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_flow_add	admin
689ba771-76c5-4b05-a948-4711b4cb1115	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_flow_edit	superadmin
336b2c09-5521-42e9-a14b-b337ac578488	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_flow_edit	admin
52daef11-3266-492f-9fa7-0f7534a6d79f	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_flow_delete	superadmin
602271fa-ca76-421b-b3b3-94f0c13ec186	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_flow_delete	admin
aedcf92a-4366-4b9a-b9ea-78c82d0a1f69	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me	user
32956873-b766-4a65-99e1-b9076b8e7e7a	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me	admin
f02914d8-5df5-4c6a-915c-854acce81905	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me	superadmin
40503b98-8802-4f5b-903d-9ac2ee8158b1	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_forward	user
1795ca4c-538e-42e0-97da-a04f56d78e04	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_forward	admin
8c11d25f-54bf-4b71-b04a-bff2e73e673b	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_forward	superadmin
59ea9331-63b7-45bc-85be-e4fff9931d78	60d6731b-e548-4175-ab3e-3fbb261e60f5	do_not_disturb	user
9b7897d1-242d-40cf-bf8c-4f3ebfe92aaa	60d6731b-e548-4175-ab3e-3fbb261e60f5	do_not_disturb	admin
1a18f832-9391-41bd-8a9d-cbfe419d0fef	60d6731b-e548-4175-ab3e-3fbb261e60f5	do_not_disturb	superadmin
869d2f32-bbe9-4682-9861-366cb51ffd2b	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_active_view	superadmin
d33196b6-0c0c-43ec-872c-8aeba5f10697	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_active_transfer	superadmin
c9b12d99-bc3d-406a-8a82-cd3e426cd5dd	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_active_hangup	superadmin
c0fa6fb9-6225-4dca-b94e-9c64ab53bb4c	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_active_park	superadmin
99856ca1-0aef-47ee-847e-0ffa63387c50	60d6731b-e548-4175-ab3e-3fbb261e60f5	call_active_rec	superadmin
ce029e1b-bd79-42e2-8056-50bf98ab70bd	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_view	admin
b18d5736-0e63-4b2b-9304-026350fef2b6	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_view	superadmin
ea3b2a62-e921-479d-b608-4751218e6a74	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_transfer	admin
08f297e4-9311-491a-a4f9-e5b84f3ce342	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_transfer	superadmin
971b2085-3e7b-4dbf-a663-f93662cdc9f7	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_hangup	admin
3293b727-c96d-4cfb-96b7-1b770f277ce1	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_hangup	superadmin
af13bd92-cdb3-412e-ab17-7b9ba20b7886	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_park	admin
2aa407bf-e8bc-4b1c-814b-5a7c1f0ee1f2	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_park	superadmin
55108551-9e25-4975-8e93-85ec8d9c292d	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_rec	admin
f0565d18-eb98-48b2-8b79-4e8be51e1bde	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_rec	superadmin
c13c8d7e-d886-4b81-8498-2887946a1b80	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_list_view	admin
bf33a802-a3ad-4ab1-81e9-227c1e6ec2bf	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_list_view	superadmin
33a1e7de-5fbb-4cd2-bc38-3443dbf128a8	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_assigned_view	admin
588cb756-96a1-4089-b1a4-c92095228bf8	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_active_assigned_view	superadmin
b640ba07-cdcf-494d-a780-91c0ba0ec8bd	60d6731b-e548-4175-ab3e-3fbb261e60f5	click_to_call_view	user
93a717a9-e77d-4a31-bc38-7447ed2e2cd7	60d6731b-e548-4175-ab3e-3fbb261e60f5	click_to_call_view	admin
918222d8-2c7c-436d-84e2-35609feb5a55	60d6731b-e548-4175-ab3e-3fbb261e60f5	click_to_call_view	superadmin
cde8e65a-990d-46a5-8180-d1169d2c29e6	60d6731b-e548-4175-ab3e-3fbb261e60f5	click_to_call_call	user
aa03f2cb-7bb2-4529-999d-0ffa6ce94ac3	60d6731b-e548-4175-ab3e-3fbb261e60f5	click_to_call_call	admin
4577fe45-67e2-470b-8bf6-e984e19d24f4	60d6731b-e548-4175-ab3e-3fbb261e60f5	click_to_call_call	superadmin
bd2689c0-9118-455e-8d20-a4598ee7026e	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_center_view	superadmin
dae46a51-c69a-488e-adb9-97d082022394	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_center_view	admin
e695fa03-f484-4611-9f95-4a4dd52338da	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_center_add	superadmin
b1aa70e9-20ba-4715-850a-1436883c7f80	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_center_add	admin
3fee3b17-ab42-4c56-9290-f6ba51015262	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_center_edit	superadmin
aa03322a-817b-45ea-9515-54e3bd9cd906	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_center_edit	admin
e6481043-a31f-4902-9ada-932cfd9fbb13	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_center_delete	superadmin
fada2a57-566a-4504-a823-fe409a38efce	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_center_delete	admin
efc06b37-f86e-499e-875f-79327c8d4a1f	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_view	superadmin
54544da3-07b2-4a11-9840-42449e010b0a	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_view	admin
9581be2b-6839-4fd6-b70e-bd02c4e7152c	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_view	user
219ec8b7-b79d-4ce2-a066-01e59bacc3dc	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_add	superadmin
c4af5e54-f2ec-48d7-a92b-e0b805e90db1	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_add	admin
04e70e0c-c2ae-4e6f-8ed5-434b82a51132	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_add	user
bc792735-6ee0-4b18-b225-d164c9988792	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_edit	superadmin
f55596e2-dfb8-4ad4-98cc-52ba0c3535f3	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_edit	admin
dfc5c53e-79e9-42a2-92f0-6c13d3038b03	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_edit	user
8ec5a522-14f2-4077-9e14-5c5d3d310df8	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_delete	superadmin
bdb1af98-7e3a-4e3d-a2f8-cf1a11208f7f	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_delete	admin
468d5c62-5503-4ba1-9386-b7afff8960e6	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_delete	user
21d55bff-c34c-4e76-886f-105429e77c27	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_view	superadmin
2f529cbd-b731-4fde-8c44-2f748c7f3e3b	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_view	admin
fb9fd287-6462-49f7-81fe-0a10e9da2d1f	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_view	user
694735f8-dcfe-4134-8d56-86def55eb14c	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_play	superadmin
398219bd-87ff-4e88-81d6-d90f02bc56d1	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_play	admin
b58c05b0-2042-4c1b-84a1-cf88bd710bd5	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_play	user
c096228a-152c-42bb-a902-16f5a395c951	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_details	superadmin
9f89cb0d-8fc7-42e3-b8fb-f01a5800c79b	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_details	admin
eb9332a2-398e-4362-8142-c5810bf6e511	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_profile	superadmin
5ba982e4-7dba-4144-b18b-df671a744ab0	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_profile	admin
83d3a569-de6f-4564-962f-edab2701d62b	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_profile	user
433fef4c-cb55-4b64-99ae-1885e2751ff2	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_record	superadmin
bf2f6515-aca1-4134-a9e9-e834f5c1e12c	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_record	admin
88269bbd-ee5a-442c-a1c9-a6bf025d53a2	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_record	user
cf7da61f-7dfb-40f6-b56a-6adcc2b2ea2d	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_max_members	superadmin
b28f6514-b79e-4ea0-949d-6c1897ca58f3	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_max_members	admin
6720cb52-98a7-4b36-a859-25ceef088143	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_max_members	user
b3cd67e6-7169-4ea3-b74a-d125571d8678	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_wait_mod	superadmin
3a966233-21db-47bb-833c-3969c9835b1e	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_wait_mod	admin
88222244-3714-4245-b074-39ce82543b72	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_wait_mod	user
1c9a0b3e-07cf-46aa-93b5-f8fa26e16f81	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_announce	superadmin
7dc4dd2d-7682-4d67-bc1f-abaa885643d1	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_announce	admin
53c6ccda-272a-4c8e-957c-e781e5771631	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_announce	user
5e44bbe9-98e0-4506-bab5-3d41cae5a219	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_mute	superadmin
b113e5fe-b8a2-4078-bef4-abfca86520fc	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_mute	admin
e8f7b312-2aa3-4d97-8d24-f300049533da	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_mute	user
009cfaba-1e91-4ded-893a-5185471ef877	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_sounds	superadmin
6fe8b5ff-1546-4366-9f88-d41f80e11d14	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_sounds	admin
96cf6301-7314-443c-831e-7b42a3ae6fab	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_sounds	user
d16c173b-e7c6-435d-841e-bc0976428e73	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_enabled	superadmin
ecbbb978-9ff3-4c99-add7-9ebb13571102	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_enabled	admin
693366da-5bbc-4fdf-a520-96a51493b795	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_room_enabled	user
40bfdceb-391e-4c23-9a0a-c7598c698ebd	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_add	superadmin
204c1f31-fef7-4850-9d07-3b903ec63e39	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_add	admin
f3e7a4a3-b92a-4169-9ab6-04cfa30dcc30	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_edit	superadmin
7ab7fe06-9b99-4640-bf50-05aad50981e6	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_edit	admin
266ae7b3-064c-46d7-a927-c3c2f2ab71e2	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_delete	superadmin
b3ede1d1-7413-424f-aca2-cd8ecc5faa63	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_delete	admin
1a7ecfdd-2035-4ff7-b70f-2e3ee9332421	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_detail_view	superadmin
75ab71e5-ce7d-4d20-9838-b0e0f8e3617d	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_detail_view	admin
c46906d7-5c3e-4c28-a146-a78e283255cd	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_detail_add	superadmin
388404e8-82a4-4382-a9fd-83f2c88d61de	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_detail_add	admin
284ee356-1997-47eb-a5a9-739bd37ed9a7	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_detail_edit	superadmin
bf3f66fa-c09f-4f68-b88f-44e54c065dcb	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_detail_edit	admin
fe0c6416-4838-455e-aed2-e48b98258fdc	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_detail_delete	superadmin
156dac4f-dd2a-4e04-80e8-8f45a603a0ed	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_session_detail_delete	admin
22634a25-438b-41c1-a536-2c49a9e3ad3c	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_view	superadmin
58912efc-285e-4459-8806-284d30801574	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_view	user
5acf918f-287c-41f6-95f7-53e8a6b6f53f	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_view	admin
8155903a-93ec-4abc-ad89-146d55f779a7	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_add	superadmin
a2889942-09b5-4fdc-9aca-a86596841152	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_add	admin
599ea93f-addc-429c-92ee-ef445e9176da	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_edit	superadmin
d5cca6bb-0c86-4a80-9453-c075ee0e0ffe	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_edit	admin
7b3fdf1b-9b31-446c-9149-3d893e6c7cfb	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_edit	user
0e4006bb-6314-4c03-8fff-809022848d2f	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_delete	superadmin
59dc5843-0a6c-4eae-8306-0764ca1c7740	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_delete	admin
c8eea5d7-7398-4ae7-ae0d-96ba8a4a7cb0	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_delete	superadmin
1f4b9a61-045f-4114-9c6d-d05a9c87b46c	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_delete	admin
8479598d-5cd8-4729-b754-8c6bba44f587	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_user_view	superadmin
d383abe5-b188-4eba-b325-b7a5bed08c16	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_user_view	admin
c4f40e59-a0e5-47dc-a0e6-4bd6b9d27b58	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_user_add	superadmin
a6dca4a7-f3d5-4f75-9c4b-f5b3e1fcd5de	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_user_add	admin
68344d23-2971-4326-b44c-83806d46e8dd	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_user_edit	superadmin
130b2a1b-5054-410c-a027-3ddabff3380a	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_user_edit	admin
edf2aa8c-4d29-4673-acaa-8ea3c55cae2a	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_user_delete	superadmin
a737d7dd-517b-486f-8791-f387b7cc284c	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_user_delete	admin
72cd069c-4b69-4c31-ae90-f3cff061f22c	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_view	user
1ceb2ce8-6ded-4639-b550-82e9c6c5a9b1	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_view	admin
facbf420-3a54-43c6-b73f-d099a1b7acb6	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_view	superadmin
8069fd89-46c1-4b12-9391-4b38f82ffeb9	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_lock	user
c3e28c58-4632-4f57-8963-3f61a90a1bc4	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_lock	admin
d6e3e4a3-c25a-49b2-ab3c-f025e3668425	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_lock	superadmin
8cd365ee-f548-4dad-bee4-8e547a10e75c	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_kick	user
e798ba34-1cdc-47ff-9f05-38299cc82237	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_kick	admin
44bbcc50-f352-4196-b972-d9ab00a6207b	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_kick	superadmin
ee613720-d522-494d-9368-94f9f88fd763	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_mute	user
067b3dc9-9a8f-407d-b933-80871174ec6f	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_mute	admin
a07770b7-1cc5-42cd-a556-4944062d3f91	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_mute	superadmin
96e78ff1-5dcf-4fd7-a943-db60602a6b67	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_video	user
494ceee1-3e06-418f-bf9e-7877a8970d25	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_video	admin
fbe4bdc3-dd34-46ff-89c6-f585177357d5	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_video	superadmin
ee17a133-0199-46f9-8baa-5ae39aa782e8	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_video	user
212f6559-cec4-456e-ac4c-d892caf2c157	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_video	admin
985e1afd-6b73-43e1-be74-7fb69e38e182	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_video	superadmin
95be65e6-fba1-4682-b78c-5aa6453a445c	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_advanced_view	admin
47354997-48ad-4679-80a0-e6133ed775e7	60d6731b-e548-4175-ab3e-3fbb261e60f5	conference_active_advanced_view	superadmin
b6e8e6b9-28cd-48fd-973b-34915fa0c0f1	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_view	superadmin
9c126eb4-2065-4c10-b867-ef6aff96e3c2	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_view	admin
43018104-6a73-4fdc-a4ec-b944e653fda2	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_add	superadmin
441054e8-cf01-499f-a720-82a8daab2a73	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_add	admin
70278af7-094c-442e-ba65-fef22d81167d	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_edit	superadmin
e65c1354-c805-46d9-840d-da3072cf507f	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_edit	admin
5b102096-9f94-4a0a-af6e-42294f984ea6	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_delete	superadmin
70a215ce-85db-4bc9-a244-ad1c063cc83a	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_delete	admin
428fb9a8-4624-4be0-8a7f-8b14cdc0f8bf	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_address_view	superadmin
20205fb4-1455-420c-8475-c0fb337dda3d	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_address_view	admin
5374e813-984f-400e-ac56-0e2092e189c3	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_address_add	superadmin
aa062f2b-a30f-4ff3-bfe3-6a051f9d4ec7	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_address_add	admin
ad8d2ade-5ef9-426f-be47-09c28cb440e5	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_address_edit	superadmin
434a87d8-a40a-4214-a75f-a3f9a781ec7a	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_address_edit	admin
d7c6fe3b-2a12-4b22-b5db-afa541a9114e	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_address_delete	superadmin
9c720796-015f-4489-a36a-ca368513a956	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_address_delete	admin
1fa81943-fcce-49df-bce7-a9d2859911aa	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_phone_view	superadmin
c0373aa2-6c34-4de3-bfa7-70983b88b087	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_phone_view	admin
3d7fc6fa-1a45-47c4-8c5b-f8eb639e7452	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_phone_add	superadmin
67881ada-6b4d-4bf3-a7a7-2f27c7183535	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_phone_add	admin
d02b9744-0f98-4e69-98dd-8bad58736054	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_phone_edit	superadmin
8c321743-49ef-41ad-a3b9-e0884abd4a15	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_phone_edit	admin
a053f4b4-32a2-441c-aea1-2b8d78dffd8e	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_phone_delete	superadmin
82a061f5-12d4-4e4b-a021-ee8b4b3d91ff	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_phone_delete	admin
d60a7e1f-2657-4eb2-a781-4785415b610a	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_note_view	superadmin
df1f052d-8c20-4ffb-889f-658d60e2e01a	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_note_view	admin
4476d500-7fb1-4046-89e6-4c577674dd38	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_note_add	superadmin
956d11c3-c9ca-4233-9e88-47e4d2bbb896	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_note_add	admin
a428d896-5e7d-4180-b9d1-5ee9701a6385	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_note_edit	superadmin
1a36db3d-bb95-4392-8706-a6feb1c5e19d	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_note_edit	admin
4fa8e242-9edd-4c12-8b6c-7311d4041522	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_note_delete	superadmin
36c8d36c-07d4-41c3-94ca-ad032a56bfae	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_note_delete	admin
5d397abd-9a19-4494-bc70-989012a8f2d7	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_group_view	superadmin
8167ad30-c96d-4e77-8b61-023d67cc0a44	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_group_view	admin
781305ed-4c82-4e3d-b3b2-6b656672412c	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_group_add	superadmin
2a4cd176-63ea-4188-b035-d9f2d7867122	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_group_add	admin
3b242a0a-3cb7-4425-8af2-0e32045b382e	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_group_edit	superadmin
4f3a9b33-9927-4054-9cc1-00b4482bd43b	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_group_edit	admin
64dc731f-2692-4ee3-bda8-4d4de3c5f4e7	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_group_delete	superadmin
72773437-d171-4092-9367-6c120a5db530	60d6731b-e548-4175-ab3e-3fbb261e60f5	contact_group_delete	admin
d6014aa3-3650-4e57-b0dc-00c11e231f21	60d6731b-e548-4175-ab3e-3fbb261e60f5	content_view	admin
b78dd3c3-47a8-486c-b2b8-32b9927b9072	60d6731b-e548-4175-ab3e-3fbb261e60f5	content_view	superadmin
efa072f1-e678-4073-8092-2a47bd9121d0	60d6731b-e548-4175-ab3e-3fbb261e60f5	content_add	admin
78573329-01e3-42ad-9b1d-23bbb18c6175	60d6731b-e548-4175-ab3e-3fbb261e60f5	content_add	superadmin
1f1e4687-e322-4767-af2e-947ccfcd1cdf	60d6731b-e548-4175-ab3e-3fbb261e60f5	content_edit	admin
683f06e8-45d1-4ff5-8814-be1c52dd7a27	60d6731b-e548-4175-ab3e-3fbb261e60f5	content_edit	superadmin
70be4cfb-f796-432e-a203-1f924717a528	60d6731b-e548-4175-ab3e-3fbb261e60f5	content_delete	admin
cea6e9b4-2383-48d9-92e9-4e7b5602a349	60d6731b-e548-4175-ab3e-3fbb261e60f5	content_delete	superadmin
1dbd6063-22ac-4f2f-b2e9-f5a9d70f949a	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_view	superadmin
c367cc4a-2a45-48cd-b3bb-3cbcb5a53633	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_view	admin
8829754e-d053-4907-a9c3-59cb41fa07c8	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_view	user
b50002ca-b42e-4834-9c66-8a7db8ad5dfe	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_add	superadmin
1287217d-d970-4b53-b752-78a028409d91	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_add	admin
e2c3bfab-b6fc-4802-839c-1db7e0921595	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_add	user
4cc8efd9-53ad-4975-9012-938fd64cb174	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_edit	superadmin
235f909d-e522-4e1f-886a-c6f649189701	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_edit	admin
622638c0-a4ff-44d4-88df-e1fa80b8c97b	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_edit	user
47557521-19d5-4c4c-9a69-7269c4d45419	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_delete	superadmin
539f04ed-bbc7-482a-b8ce-13cdaadacb1d	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_delete	admin
c9508e16-6f93-4680-9cbe-77ba055e6618	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_delete	user
ead08e6b-cbc5-4a17-8994-c188fe2f27a0	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_view	superadmin
21c983cd-f7e6-441e-a6db-0fee5969727f	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_view	admin
a44dd6e8-dab9-46be-b76e-13b16a9a6238	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_view	user
3df925d8-7733-4a52-b5c9-4ee1ca25dda7	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_add	superadmin
f2681d2b-860a-4b4a-933b-73a304d4465a	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_add	admin
f60078f8-beed-430c-9e21-bf8006aa58d8	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_add	user
5141bf82-5e43-4acd-88ce-a08988ce3553	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_edit	superadmin
d7b7e1a5-b766-4e39-b89e-5e0ec5a1fef7	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_edit	admin
3fe8039f-6e6b-45c0-b0c9-d7baf79b208b	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_edit	user
7d7a4c75-b14a-4a7c-81b5-bb4b480ffca1	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_delete	superadmin
e8f4369c-5a45-40c0-a283-c86e9fc5c853	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_delete	admin
680cd4db-e4f7-48fb-9573-ea66cacf8538	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_delete	user
4ed4e348-bd4b-4b0a-8b0f-051765afd386	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_view	superadmin
751e297d-2a79-41bd-a9fb-a2700d9e84f1	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_view	admin
c93f5f64-19ba-4733-8cc7-6d78dd232e87	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_view	user
98b3ab0f-d322-4445-a011-cae0412d277d	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_add	superadmin
ef20f738-a9cf-48dc-b4b8-705bd4424fdf	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_add	admin
d50976c3-58b6-456c-9dd9-4d46521cabcc	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_add	user
d32d1411-8539-4b3e-a50b-2ca7bf676e4e	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_edit	superadmin
547a6646-e6b7-4a69-9e14-5c4f19d24cbf	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_edit	admin
c7ffa1de-fdda-4b06-a200-0e24d56137fc	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_edit	user
c8a1ff51-fad4-4ac7-85a8-44c1eaec97fe	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_delete	superadmin
85ce99da-aa96-49a0-81d6-de320baceb73	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_delete	admin
95b2cd74-7c2f-4d96-a917-1bd0672549c1	60d6731b-e548-4175-ab3e-3fbb261e60f5	rss_sub_category_delete	user
a467d6d1-c3df-42b6-a5ac-fea67ac7eb8f	60d6731b-e548-4175-ab3e-3fbb261e60f5	destination_view	superadmin
28835975-c9b5-4bb9-a9c4-b76d36da4972	60d6731b-e548-4175-ab3e-3fbb261e60f5	destination_add	superadmin
108e96c4-8792-422e-9fb8-1476fdf07cc8	60d6731b-e548-4175-ab3e-3fbb261e60f5	destination_edit	superadmin
58edcee9-ea1b-4e2f-957a-12e055b09a2f	60d6731b-e548-4175-ab3e-3fbb261e60f5	destination_delete	superadmin
19376862-dbfd-49ef-8ee6-db2c9e3915d0	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_view	admin
32d3542c-5f91-4a6b-ad62-31e693cebb9d	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_view	superadmin
23f6fedf-1e88-4040-a196-830df775af5a	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_add	admin
43a83753-3d97-4972-8026-683e145b3f88	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_add	superadmin
f4f394d3-779f-4767-8f63-a4c19fe7e8d8	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_edit	admin
ebb75fa5-9871-4eca-8ab8-d973fbc42718	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_edit	superadmin
a346f541-041e-496e-9a84-2c59f4894a55	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_delete	admin
1b33252f-7b2b-4162-9838-5fed66b1800f	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_delete	superadmin
e7977f8a-07ff-4736-a347-58d93552ff19	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_extension_view	superadmin
b61067c7-d87a-4eef-b184-2dcbd0a8c6de	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_extension_view	admin
0b86aff8-ae41-4ff7-a646-22a8d88b9091	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_extension_add	superadmin
7d5fdaf1-b248-4f5c-9ced-15786d0499ef	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_extension_add	admin
fef3914e-27f6-4de6-87e5-c96a001637fd	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_extension_edit	superadmin
5d5233c1-a89f-4761-b798-9ca784a6420d	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_extension_edit	admin
8223bb3b-c1e8-4e16-920b-eb2ecf351443	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_extension_delete	superadmin
4d7d0c75-a892-495d-8dc8-54d49ed3bc05	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_extension_delete	admin
9848bdf7-16ad-4e3e-a996-c7217311d31b	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_line_view	superadmin
67a9195b-ceb5-4ae7-96e4-d84f30d847eb	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_line_view	admin
84b789ab-0488-455e-8fc9-af919f5d0847	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_line_add	superadmin
1a156858-e59c-442b-a7e9-46d6224f6601	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_line_add	admin
2b86077b-e590-43d8-9198-08a458a5a209	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_line_edit	superadmin
468911b6-c323-4b6c-9ab4-54b1856d0aec	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_line_edit	admin
8f674db1-0760-4447-bd43-896268f09a15	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_line_delete	superadmin
5bdd75c1-c01d-4ddd-b2fc-0bb2ec18ba16	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_line_delete	admin
2fd23cb7-70ad-4f2a-9e24-92e133744935	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_setting_view	superadmin
9cc2e8f2-abde-44a8-9ff2-2fb269d29c87	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_setting_add	superadmin
3fd2c242-a3a5-4725-86d9-7a0af33c2f4a	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_setting_edit	superadmin
3d664e02-cba9-4089-9aeb-83f02aa3f1eb	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_setting_delete	superadmin
34e1bcc7-9292-4db5-9ec7-02a09d3f3bbc	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_key_view	superadmin
6f771e8a-08f2-4a59-a851-11cefbf6d93e	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_key_view	admin
77fb49d7-32d8-4075-8930-604c2b52168c	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_key_add	superadmin
768e0bf1-2a05-4bd4-9eec-a2ac839aa15b	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_key_add	admin
817cb86a-fb8b-4c6e-8094-9962a850ec5d	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_key_edit	superadmin
72529539-6318-4e2b-9e90-1c71cdfd1ce0	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_key_edit	admin
12d35071-0e45-4f17-adac-42e2347ffe01	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_key_delete	superadmin
fee49c38-1dcc-40e4-9d10-187dac616cb4	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_key_delete	admin
3179d55b-6b7a-4377-ab93-cdf685fec273	60d6731b-e548-4175-ab3e-3fbb261e60f5	device_domain	superadmin
7f6bf568-c08f-46fa-9a41-ba0ba34bd390	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_view	admin
fce2a61a-fff7-4857-a9de-bd97197a82bb	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_view	superadmin
bea9569c-63c6-420d-9d2d-9b25a984c3d3	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_add	superadmin
bc830b88-b5c8-4801-ae0f-8e9d6f949843	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_edit	superadmin
614cbdc7-9f7b-4438-8790-c18a2e8946ed	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_delete	superadmin
d8c01fc1-0f7a-4419-84af-6e5d1e06e5c1	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_advanced_view	superadmin
e3f14067-fd99-4551-b96b-35d280b999ac	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_advanced_edit	superadmin
f98b9fbe-5c73-4ca4-9258-924bd3525830	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_detail_view	superadmin
bb80447b-7b58-46a9-8e83-b3ffd27d2b41	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_detail_view	admin
f2038777-3807-4067-8146-2b1007f393f7	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_detail_add	superadmin
b4b76746-1367-4522-ac02-0b7df107f203	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_detail_edit	superadmin
8bcde478-95f2-453e-a252-cb6330dbd786	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_detail_edit	admin
eb27e1d0-0eeb-4588-af7f-88f7436c5e21	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_detail_delete	superadmin
6ba9cf8e-f062-46af-a9bb-5db9d6afe758	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_detail_delete	admin
0a787e9e-1c4f-49a3-b6bf-3f2ee1bd6fdf	60d6731b-e548-4175-ab3e-3fbb261e60f5	dialplan_domain	superadmin
d2b0ff56-c54b-421f-afaa-9137b7e3eefd	60d6731b-e548-4175-ab3e-3fbb261e60f5	inbound_route_view	superadmin
74fd65d1-98f9-46e3-b717-2a62c5199905	60d6731b-e548-4175-ab3e-3fbb261e60f5	inbound_route_view	admin
688dbe3f-f0ae-49d5-a179-b80006f8fcea	60d6731b-e548-4175-ab3e-3fbb261e60f5	inbound_route_add	superadmin
a1d28772-def7-456e-8e2e-c2ae4774b21b	60d6731b-e548-4175-ab3e-3fbb261e60f5	inbound_route_add	admin
50944a28-711e-448d-a352-eb7a307d8db7	60d6731b-e548-4175-ab3e-3fbb261e60f5	inbound_route_advanced	superadmin
8812542a-04c2-492b-b817-1c42dcc20671	60d6731b-e548-4175-ab3e-3fbb261e60f5	inbound_route_edit	superadmin
2e12ba2c-4652-464e-b2cd-c151b169fe01	60d6731b-e548-4175-ab3e-3fbb261e60f5	inbound_route_delete	superadmin
2c08f5db-c4b7-48c7-b7e8-71506a148f43	60d6731b-e548-4175-ab3e-3fbb261e60f5	inbound_route_delete	admin
3e184c28-ed1d-4736-8635-6858619b3722	60d6731b-e548-4175-ab3e-3fbb261e60f5	inbound_route_copy	superadmin
60ac01b3-fe5d-4913-8420-48508a05dd9e	60d6731b-e548-4175-ab3e-3fbb261e60f5	outbound_route_view	superadmin
3c190efe-872a-4986-8177-4ca81fe37987	60d6731b-e548-4175-ab3e-3fbb261e60f5	outbound_route_add	superadmin
3378a631-3ae6-41f1-803b-ba9b3a26e190	60d6731b-e548-4175-ab3e-3fbb261e60f5	outbound_route_edit	superadmin
d0f003eb-fca6-4901-a96e-c2298a2d8254	60d6731b-e548-4175-ab3e-3fbb261e60f5	outbound_route_delete	superadmin
6105efec-7ec9-475b-8b48-7d8b3ac00867	60d6731b-e548-4175-ab3e-3fbb261e60f5	outbound_route_copy	superadmin
52370d6f-0574-4713-9365-131052978936	60d6731b-e548-4175-ab3e-3fbb261e60f5	outbound_route_any_gateway	superadmin
e4d5a905-0768-4c4d-a6dc-bbf3c599982c	60d6731b-e548-4175-ab3e-3fbb261e60f5	script_editor_view	superadmin
c0f4adb1-83b8-4481-af7a-25b3f5a22c8d	60d6731b-e548-4175-ab3e-3fbb261e60f5	script_editor_save	superadmin
7e70bdcc-cc84-4dff-894a-e76b546f350f	60d6731b-e548-4175-ab3e-3fbb261e60f5	provision_editor_view	superadmin
78c29546-509d-4b99-8e2a-72f82af1e1ca	60d6731b-e548-4175-ab3e-3fbb261e60f5	provision_editor_save	superadmin
d0eebb51-e69b-4d03-95fa-d15000344429	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_editor_view	superadmin
89fda1bb-7d0d-43f3-ae91-e2fbd240daf1	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_editor_save	superadmin
fc3607f9-ba05-4c3a-9403-480589fef192	60d6731b-e548-4175-ab3e-3fbb261e60f5	php_editor_view	superadmin
bf15f15d-ea69-4073-b233-05cf96e8490c	60d6731b-e548-4175-ab3e-3fbb261e60f5	php_editor_save	superadmin
db1e4fcc-b593-4df0-bd5b-5297e68c2ec6	60d6731b-e548-4175-ab3e-3fbb261e60f5	grammar_view	admin
58a1c768-db54-439a-bdd9-7a0bfdd76aaa	60d6731b-e548-4175-ab3e-3fbb261e60f5	grammar_view	superadmin
1cc93484-31e4-4501-a7ee-6ab82b1b6cd4	60d6731b-e548-4175-ab3e-3fbb261e60f5	grammar_save	admin
c9b39601-abed-4590-baef-f6d43306afad	60d6731b-e548-4175-ab3e-3fbb261e60f5	grammar_save	superadmin
c2f79140-4d8d-4960-ab8c-2d7dfb241652	60d6731b-e548-4175-ab3e-3fbb261e60f5	clip_view	superadmin
479b9989-bf64-4058-872e-22639c9b1e99	60d6731b-e548-4175-ab3e-3fbb261e60f5	clip_add	superadmin
86f132bd-404a-4511-87ff-4fa078a88056	60d6731b-e548-4175-ab3e-3fbb261e60f5	clip_edit	superadmin
ee41091f-1a88-4535-95b3-30748bf9f6ae	60d6731b-e548-4175-ab3e-3fbb261e60f5	clip_delete	superadmin
ec16279b-38e4-470f-a358-df958cc98374	60d6731b-e548-4175-ab3e-3fbb261e60f5	exec_command_line	superadmin
71c472e2-66e0-4938-bf12-83c8438de6c1	60d6731b-e548-4175-ab3e-3fbb261e60f5	exec_php_command	superadmin
eb58b24d-96d8-4863-ad0f-44bfc4b406f7	60d6731b-e548-4175-ab3e-3fbb261e60f5	exec_switch	superadmin
b4a6bf1f-1ae9-4923-9200-eda3600204cb	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_view	superadmin
f20bc8da-d241-4463-925c-f494a1cc5d5d	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_view	admin
4715fcfe-95f2-45c9-a619-3afef9683f6f	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_add	superadmin
dd668325-f59c-481c-81a2-43e0783bbb7a	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_add	admin
016910f0-d769-4680-968b-6f1b2ea47b54	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_edit	superadmin
2b69d527-739c-441b-a987-a3d9dee6c8ce	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_edit	admin
138579d1-1141-43b9-9282-4d2ce0590840	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_delete	superadmin
6bfa24e9-193a-4529-8a9e-910518a01c10	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_delete	admin
37d3cb03-556c-4a39-a8d7-c61010590e4c	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_toll	superadmin
e87ee0ab-b680-4eaf-8982-bb3cb18db4d6	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_enabled	superadmin
ba9f4e43-edf7-434f-9100-8401661c30fc	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_enabled	admin
3258ce58-a838-4d65-9e48-462b3d970109	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_user_view	superadmin
f32bf6a2-a934-4ce2-8ca7-b9118e1ea96b	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_user_view	admin
5dd9f7dc-02d0-4d3b-bf7e-93132a6e8454	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_user_add	superadmin
b3c0c3fb-e015-4bc8-a0af-a363382663b5	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_user_add	admin
f4cf4f9f-6adf-43c3-b1bb-b913b9546a0b	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_user_edit	superadmin
8c55a2f0-1945-43b9-a616-992308085c21	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_user_edit	admin
8e9bbb84-68c1-4d2e-a9d7-c08a5309ff00	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_user_delete	superadmin
7cca3dfd-415e-4d87-8041-245e7776aca8	60d6731b-e548-4175-ab3e-3fbb261e60f5	extension_user_delete	admin
453f98f3-21e7-408e-854c-9a9e45cd7874	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_extension_view	superadmin
862b75c8-47b9-4f1e-ad14-c4037e72d85d	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_extension_view	admin
bc71072a-dfdd-49e6-b294-3b150445c5ec	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_extension_view	user
f185f886-3489-4367-93e9-0e95d3abf5c0	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_extension_add	superadmin
04284d80-b771-4887-b284-062ea7c3efd9	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_extension_add	admin
0821f300-6a98-41a9-9fee-58f18480d390	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_extension_edit	superadmin
93e2027b-60bd-44e7-a540-0a9a4b6c7c41	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_extension_edit	admin
41ffc133-8798-49a7-91a6-d53835c9bdcf	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_extension_edit	user
bc83facb-d4c9-4b23-953e-25da82d76fb9	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_extension_delete	superadmin
4d246df6-a73a-4667-a653-854e785b04c8	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_extension_delete	admin
ac10e0ae-0998-47f0-b10b-39bedc83c380	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_inbox_view	superadmin
f7a287bc-3c64-4916-b7d4-a6dd7edbc257	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_inbox_view	admin
46f29b33-6420-4bdc-9aec-8c441eb88322	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_inbox_view	user
897342da-68cf-4e74-afeb-924bcc88126e	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_inbox_delete	superadmin
23d95235-bf9b-4b1f-b4e0-1d8806b52e1f	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_inbox_delete	admin
3fc4353e-8832-41be-90d3-823bedbc7075	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_inbox_delete	user
6405efae-ac4e-4cfd-a68d-91dace788c70	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_sent_view	superadmin
2cefedea-d176-477b-bfdb-c2039bf7ac8d	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_sent_view	admin
0ab27425-2210-4392-a4f6-6a3f3c507894	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_sent_view	user
78056238-c86e-49c2-bc27-c4fef5d0a5f9	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_sent_delete	superadmin
8f01fa3b-79fa-40ab-a4b0-e5f6faba4b08	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_sent_delete	admin
e1848eb0-6452-468b-aac8-dbd9b3fcbeb5	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_sent_delete	user
728c4596-4cf9-4de1-a949-5b017ac11f57	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_send	superadmin
22b73440-4238-481b-ba3c-e450f96a0677	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_send	admin
4b5fe85d-0b6c-437b-a65e-7e4295bccd64	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_send	user
aa2b9906-c6cb-4f15-bf31-066e9acc3204	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_view	superadmin
406d73c8-9292-4834-bb5f-ef2c2f881a6a	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_view	admin
9ec4176b-3b5c-44b3-82bd-0f4af0a4f079	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_view	user
d274f067-a17e-49fd-b18a-23b037133e64	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_add	superadmin
4207c6ed-1612-4149-b121-9a416eff42f3	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_add	admin
e363d342-6b89-4198-8c9a-2bb26b31c7af	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_edit	superadmin
3f9f622b-e56f-4e56-b96a-3ab925add98b	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_edit	admin
c6636f85-7f5a-4ed8-ad32-396cd6089300	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_delete	superadmin
be5197ba-16a9-4753-9d4f-b733df5fcc9b	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_delete	admin
3b544e22-6262-4c8d-b7bf-6c9c24e742c7	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_user_view	superadmin
c85ae4c9-c870-4a3e-aa7f-6d492cea77bf	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_user_view	admin
08cc0cb6-49b1-4dcc-9fa4-d7a89c17146e	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_user_add	superadmin
411174cf-ec5b-4caf-a5e7-6184c2c04210	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_user_add	admin
d63e4e29-4fc5-4813-9f3f-e8ef7518379c	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_user_edit	superadmin
882f0ac7-45a1-4ee3-bf46-52eb803b15d1	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_user_edit	admin
ee8451c8-7e9a-4844-b2c1-e5ea0a44f4c5	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_user_delete	superadmin
2c2c2051-68db-454d-b7e2-e9a869d4e4a7	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_user_delete	admin
86d2f2c8-191b-4189-8dbc-fd3e74f142f6	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_log_view	superadmin
96a8b89d-a9ae-41aa-b4e6-8484d81672f3	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_log_view	admin
c43512ba-2925-4355-90ca-14b598bf4ef6	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_log_add	superadmin
213f0a44-1b78-42a5-80fe-6d09686b4712	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_log_add	admin
ebaa5092-bd17-417f-a13b-c986869c158e	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_log_edit	superadmin
bbdd8ba4-21a4-42c6-b662-d8b115d24921	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_log_edit	admin
86c5dcbf-673c-4ca2-a22d-98255e62887e	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_log_delete	superadmin
86a149b6-5425-48a7-bfa8-196eacf12087	60d6731b-e548-4175-ab3e-3fbb261e60f5	fax_log_delete	admin
4b6bef30-d3b6-4378-a0c8-bbad2f37f060	60d6731b-e548-4175-ab3e-3fbb261e60f5	fifo_view	admin
50f25095-d988-4c6d-baf3-21bc8155784d	60d6731b-e548-4175-ab3e-3fbb261e60f5	fifo_view	superadmin
e2b62951-9c1d-4be4-84ff-bcd315fc9053	60d6731b-e548-4175-ab3e-3fbb261e60f5	fifo_add	admin
fdd88a78-ca19-499d-a6ab-e2a177aa408d	60d6731b-e548-4175-ab3e-3fbb261e60f5	fifo_add	superadmin
7d1e0c95-3a92-43fb-a7b8-fddbe7b3105b	60d6731b-e548-4175-ab3e-3fbb261e60f5	fifo_edit	superadmin
6dbdd699-8062-44b2-9492-7edf6c5c1b13	60d6731b-e548-4175-ab3e-3fbb261e60f5	fifo_delete	admin
9b8c6acf-5450-4e21-934c-a7a4eed75d8b	60d6731b-e548-4175-ab3e-3fbb261e60f5	fifo_delete	superadmin
104b87bf-5c9f-4f09-9816-f9ba9a74a92a	60d6731b-e548-4175-ab3e-3fbb261e60f5	active_queue_view	admin
2661acb5-7061-4538-9504-10ae533dd450	60d6731b-e548-4175-ab3e-3fbb261e60f5	active_queue_view	superadmin
2efb0db2-0120-4299-97f8-78e73bc431ca	60d6731b-e548-4175-ab3e-3fbb261e60f5	active_queue_add	admin
3747024b-adc1-4e6f-be96-872275498e17	60d6731b-e548-4175-ab3e-3fbb261e60f5	active_queue_add	superadmin
864a5acb-d54b-4a25-b129-c28cf005a600	60d6731b-e548-4175-ab3e-3fbb261e60f5	active_queue_edit	admin
34bd31f9-8532-464e-8b83-4c6db0bd0d69	60d6731b-e548-4175-ab3e-3fbb261e60f5	active_queue_edit	superadmin
02857894-4366-4f9b-b145-10679cee699d	60d6731b-e548-4175-ab3e-3fbb261e60f5	active_queue_delete	admin
6509161b-53d4-4853-8f6e-e5d61ea0028b	60d6731b-e548-4175-ab3e-3fbb261e60f5	active_queue_delete	superadmin
30c67204-4135-42e7-b3e9-293dd597a8bc	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_view	superadmin
34a3f11e-804c-461c-b79c-c45c6d320775	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_view	admin
3b709c98-0099-4900-a5d2-bcf8b5593feb	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_view	user
28695fc5-feb2-44a4-b390-980a888f3dfa	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_add	superadmin
5c14fe4c-1c8c-4f1a-bb86-18352a5cc93b	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_add	admin
5025e783-01d5-4124-a51a-c61b1d9c96d9	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_add	user
f5bfee72-51a3-4931-82e8-219723bb5d2e	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_edit	superadmin
7dbab2c0-30fb-4ed8-b9fb-2aaae6ecd479	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_edit	admin
6c46d8fc-4f4d-4603-a85c-1046731cd18b	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_edit	user
673a74f0-fbca-411a-bd14-c5ea3a63c53a	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_delete	superadmin
6e1775c0-4b3b-4f1c-a48e-be15c27f2d31	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_delete	admin
2cb2055d-b32b-4a12-915a-e7ba3b3c3f7d	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_delete	user
a7fed92a-5641-4f18-aa7b-33833d83f83d	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_view	superadmin
e0142adf-ef14-4485-a774-d49674bb8cff	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_view	admin
9e1e0a9b-7e0f-4415-aa43-71d38782f1aa	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_view	user
21b2eff1-32dc-489a-a8e1-bc860c463c34	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_add	superadmin
cc53579b-a035-4c4a-8be6-b15d02082da4	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_add	admin
0e204783-e74e-4707-b82c-1f04508bdc0f	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_add	user
73e643e0-a78c-4bc0-9b55-a26abdb902a9	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_edit	superadmin
143cd37c-ec05-48e0-bbfc-7f2d121e4462	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_edit	admin
64def9ad-e770-4611-acc0-6105ca4b2c1b	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_edit	user
b9405e82-26b2-4d21-9bdb-c02b95902540	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_delete	superadmin
0ed995ad-6051-4148-bea7-32ce9f4a1c8f	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_delete	admin
454fab45-7dc2-4dec-996e-8650cbac6080	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_destination_delete	user
5888fca7-9e6a-46fd-938e-befd16579099	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_prompt	superadmin
421e134f-3526-4c4b-b467-b729a8c3e716	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_prompt	admin
d8e59e3c-4be3-4a9f-b0cc-691f8d6b8f06	60d6731b-e548-4175-ab3e-3fbb261e60f5	follow_me_prompt	user
c872ba78-9566-4871-a755-e2973ec88833	60d6731b-e548-4175-ab3e-3fbb261e60f5	gateway_view	superadmin
0d541fa4-576f-4409-a163-56504e939ebc	60d6731b-e548-4175-ab3e-3fbb261e60f5	gateway_add	superadmin
5d663bbb-6684-4c71-b3d4-da6365e814f5	60d6731b-e548-4175-ab3e-3fbb261e60f5	gateway_edit	superadmin
ec64f731-fdd4-49a2-882d-ab04a81a3c49	60d6731b-e548-4175-ab3e-3fbb261e60f5	gateway_delete	superadmin
c5f5af0b-ef66-4d51-a91d-af7e59d0dcf3	60d6731b-e548-4175-ab3e-3fbb261e60f5	hot_desk_view	superadmin
2579bf7b-1015-4b84-9b7b-8f5dcd14eb32	60d6731b-e548-4175-ab3e-3fbb261e60f5	hot_desk_add	superadmin
e7e59e63-d3e4-44a8-9424-ef8925307198	60d6731b-e548-4175-ab3e-3fbb261e60f5	hot_desk_edit	superadmin
c99df1ac-c6dd-4efb-8c02-d7152c2da199	60d6731b-e548-4175-ab3e-3fbb261e60f5	hot_desk_delete	superadmin
45b53d83-0625-4bcd-a86b-1119c66ab159	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_view	admin
e48286cc-db80-4f82-a972-cc487234a1e9	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_view	superadmin
81838b38-2299-4608-b493-27854b2a156e	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_add	admin
73622db5-361e-4c67-bde1-b9802c4121c6	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_add	superadmin
e812660f-ce27-4fae-9790-65b6c05c0394	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_edit	admin
82a91678-ca5d-4a48-9d56-5dc3db6328dc	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_edit	superadmin
cf5bb4e8-9207-4eae-a932-6cca109846cb	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_delete	admin
56ce1f37-5fd4-463e-ae22-a6e66a07fc0d	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_delete	superadmin
da85996e-fd7b-43a8-9e4a-141cbef4c84a	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_option_view	superadmin
d6852e94-25d7-467c-9727-50093c87eb0c	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_option_view	admin
0c9ed160-dcea-490f-a710-306f115c2dd8	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_option_add	superadmin
8432d353-81b6-4992-bf85-6a5bb2ddf7b0	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_option_add	admin
b679e7f5-c386-40b7-9f8c-17e69e701aea	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_option_edit	superadmin
677a4c46-a109-4eed-8fdd-7a102eee6d1c	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_option_edit	admin
31595be0-5c88-4f6f-958c-83819b770105	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_option_delete	superadmin
375009bf-2e54-4374-9d65-a3fc7335ef67	60d6731b-e548-4175-ab3e-3fbb261e60f5	ivr_menu_option_delete	admin
43b98986-b85b-4ba9-a980-3db6f7222646	60d6731b-e548-4175-ab3e-3fbb261e60f5	log_view	superadmin
6394e1e7-3ed9-43a5-9b07-578eb17f2661	60d6731b-e548-4175-ab3e-3fbb261e60f5	log_download	superadmin
abf9eb83-eed4-4b1d-a73d-de39717d2086	60d6731b-e548-4175-ab3e-3fbb261e60f5	log_path_view	superadmin
f4b6eebb-f878-491a-972d-662c11b6c435	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_view	superadmin
9ed1e6ac-ab5e-4994-92ca-586ebe4d3d41	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_view	admin
e0d2ab10-dd25-4d1a-b7cd-9942bef08961	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_add	superadmin
3e38b769-373e-4fc3-977b-783d39fb5db0	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_add	admin
8980fe8d-c49b-4d99-8b9f-da0e6377f0ad	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_edit	superadmin
db7b7bf1-68de-45e9-81e6-52c23445008c	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_edit	admin
441d5a29-f297-4dae-9dff-0c9094858e43	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_delete	superadmin
5834b78b-7881-4236-9aca-3553e5cd3815	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_delete	admin
c01a39c4-b983-473e-b954-e40564b4494a	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_user_view	superadmin
ca446453-5619-4799-8e7c-b859ee942932	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_user_view	admin
100ef610-a255-4e22-84c5-a16df538712e	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_user_add	superadmin
5f6d0c8b-ab39-49e0-bea4-f6abe19443b7	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_user_add	admin
b7816831-e45c-4859-b611-d6316df034f9	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_user_edit	superadmin
e4cf68bf-ae47-4bdf-acc3-8e91e017ec6a	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_user_edit	admin
ea74d8d8-444b-401e-928c-93d481c6d6e8	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_user_delete	superadmin
3e243bca-1a4a-481c-8e63-c1cb08df6ea5	60d6731b-e548-4175-ab3e-3fbb261e60f5	meeting_user_delete	admin
fb2ef8d2-60d4-47a0-8ffb-2a882c455909	60d6731b-e548-4175-ab3e-3fbb261e60f5	module_view	superadmin
9239b177-7406-4efa-8479-b42014ab21d8	60d6731b-e548-4175-ab3e-3fbb261e60f5	module_add	superadmin
5926bb69-bd7d-49bd-b93b-422725971396	60d6731b-e548-4175-ab3e-3fbb261e60f5	module_edit	superadmin
f391c997-8366-4a34-9bd1-082a7c054eaf	60d6731b-e548-4175-ab3e-3fbb261e60f5	module_delete	superadmin
826e75b8-a213-4ab3-99ae-dab97f585e9e	60d6731b-e548-4175-ab3e-3fbb261e60f5	music_on_hold_default_view	superadmin
007c2211-ed71-4c74-93f2-d35810d31536	60d6731b-e548-4175-ab3e-3fbb261e60f5	music_on_hold_default_add	superadmin
288f4e55-d789-44ad-ba9f-f031b569c27c	60d6731b-e548-4175-ab3e-3fbb261e60f5	music_on_hold_default_delete	superadmin
46c03293-7330-407b-bb2a-a4870decd14f	60d6731b-e548-4175-ab3e-3fbb261e60f5	music_on_hold_view	superadmin
dcaa5202-9bed-4f0e-8246-8d10cbe30671	60d6731b-e548-4175-ab3e-3fbb261e60f5	music_on_hold_view	admin
c03bee93-527c-4def-9fca-73f61a6ee6dd	60d6731b-e548-4175-ab3e-3fbb261e60f5	music_on_hold_add	superadmin
579242e7-9121-472b-86b8-f780b9f293aa	60d6731b-e548-4175-ab3e-3fbb261e60f5	music_on_hold_add	admin
fcdcfe84-8b24-4333-9f6d-4e7bdc7a1169	60d6731b-e548-4175-ab3e-3fbb261e60f5	music_on_hold_delete	superadmin
7aefc957-021f-40e5-b375-3bd631a87970	60d6731b-e548-4175-ab3e-3fbb261e60f5	music_on_hold_delete	admin
4725ed7d-200a-44df-852a-47b63fc46733	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_view	admin
41f89b62-d4f3-49b9-9700-5535b66710c7	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_view	superadmin
1cf11ac5-ac8b-4390-9793-1d3c958dbc3b	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_add	admin
8ecf314a-660d-4abf-8a18-fe1034496f8c	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_add	superadmin
46b270a8-9792-4c50-ae48-bdbaf1d7eb5f	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_edit	admin
acac493b-343b-43c5-8727-a8940b0961ff	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_edit	superadmin
c16b6ccb-88d0-419d-8cd1-f965ed8cb0cd	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_delete	admin
f4037deb-ae40-41d7-ae7c-185b5c5bb188	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_delete	superadmin
970fb9d0-49f7-466d-b793-072da459e5a0	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_upload	admin
cb78d849-bae1-49ee-9c4d-7554063d8303	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_upload	superadmin
52d1cde2-1103-4a72-bb59-ec9cf39f3908	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_play	user
6f59ce29-0874-4802-955f-b27c838f8c14	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_play	admin
3387431e-954a-4543-a2ae-0a88956229a2	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_play	superadmin
a149e533-5a1c-4b43-bef0-1fa48812f738	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_download	user
0f417222-fece-48fa-8ad9-f535c26122a3	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_download	admin
c2e1a5b3-b0e2-48b0-a73f-5c2ee115ec8a	60d6731b-e548-4175-ab3e-3fbb261e60f5	recording_download	superadmin
f267807e-f1fa-4ca8-998f-e93de51791ee	60d6731b-e548-4175-ab3e-3fbb261e60f5	registration_domain	admin
81b088dc-d455-4137-82b1-3f4c701fb33d	60d6731b-e548-4175-ab3e-3fbb261e60f5	registration_domain	superadmin
c8e34984-d835-49f0-b077-26b9a11e8f10	60d6731b-e548-4175-ab3e-3fbb261e60f5	registration_all	superadmin
f2e0c27a-50f0-48e6-9d10-487b4ce37d20	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_view	superadmin
58b3e0ce-8926-44a3-b484-403ab057f0f0	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_view	admin
ea47c534-7a0e-46fb-8aa4-d454ca7f2d80	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_add	superadmin
18edd074-3322-423a-8b63-9e0f7c18f50b	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_add	admin
0d084f8a-8711-412c-91be-98a71a9b5b04	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_edit	superadmin
3175ee3b-b23e-4df2-a9b6-02bdc662c97b	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_edit	admin
9872e4b0-19cd-48e5-a101-aeefb35b7d00	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_delete	superadmin
b05a4862-bc7a-444f-9bde-347c2132f8d7	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_delete	admin
8ad8282e-4e81-4402-bc1b-067873d54d8e	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_forward	superadmin
3cc48afc-1efe-4f79-8c62-0cade47b9812	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_forward	admin
36d232c3-254e-48f0-8a8e-65291d314261	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_prompt	superadmin
239e4b5c-105d-47e0-9b3a-3a2f296b2e69	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_prompt	admin
30370f9c-402c-4886-95d5-8c9b44dc38d2	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_destination_view	superadmin
4c6c02d4-942d-4ecd-b996-4cda0536058a	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_destination_view	admin
01e69fcc-0877-49bd-a2e6-8055ccef2830	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_destination_add	superadmin
d2189fa2-f428-4157-a2eb-5e6d90cb9b99	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_destination_add	admin
ec703528-5a68-4f1f-84d4-473276fa5f7b	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_destination_edit	superadmin
181d899b-ce45-4c9a-a9b3-6e1cbacf1476	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_destination_edit	admin
78ad17c1-2d9d-44cd-81bf-7cbe5a8d81d1	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_destination_delete	superadmin
72a5302c-510a-4b11-a356-1526fc5fe3ee	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_destination_delete	admin
1d4f54a5-51d7-488a-9a3e-567c130afda4	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_user_view	superadmin
e0ac0ef7-5ab4-45f3-8f44-82dfbb2653c7	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_user_view	admin
6c4df9ce-58a2-4545-b342-1cc2af6771a5	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_user_add	superadmin
996d958f-0600-4da3-878a-bb18a13f9e30	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_user_add	admin
86700f45-d16e-4cfc-8c9c-69142bc3e4e9	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_user_edit	superadmin
9e666a62-40a0-414d-b944-c1df6456393f	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_user_edit	admin
235e75b7-5d5b-4dcc-8f5c-88c04bc4a294	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_user_delete	superadmin
6d9f7307-9655-42e8-ab1f-ac1c7d2a0cb0	60d6731b-e548-4175-ab3e-3fbb261e60f5	ring_group_user_delete	admin
2c7cfc8c-a788-42f0-830a-cdeb35ddc968	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_view	superadmin
d7a905c8-33db-4186-abac-f84f24487973	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_add	superadmin
905adef8-8a89-4348-9879-34a9cea2e8e9	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_edit	superadmin
ad1412e2-2114-4ca3-83b1-d1d5c387ac52	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_delete	superadmin
1e5b8212-1097-4fb3-9381-3c0e443a3102	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_data_view	superadmin
71acd29f-31e7-4efb-bc0e-684bd7c44cdc	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_data_add	superadmin
f6ffd8e1-9c86-4d06-9e56-29cf54cfe785	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_data_edit	superadmin
a01d8798-835b-49d3-909a-adf6a33a3c8f	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_data_delete	superadmin
dd14b81b-91c3-4f30-9090-e20d9e0ed9b7	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_name_value_view	superadmin
199e86ac-202e-4cbd-93dc-468d95c427c8	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_name_value_add	superadmin
a28a702b-f2f7-4213-b0b8-4f79d41f204a	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_name_value_edit	superadmin
6da5c8e6-9beb-4b62-8729-7fee97e949a4	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_name_value_delete	superadmin
8b6a9413-dd98-4221-a502-73fa1b911e6b	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_field_view	superadmin
ebd0dce0-18ec-423f-a1a8-c7370e791626	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_field_add	superadmin
80773bfd-bc03-4af2-8db8-5bd90911dbff	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_field_edit	superadmin
430d6dc3-6b3e-4f07-ab7d-8c0629668ae2	60d6731b-e548-4175-ab3e-3fbb261e60f5	schema_field_delete	superadmin
4dd0306b-ca7c-48fd-acf8-9dd545829fbd	60d6731b-e548-4175-ab3e-3fbb261e60f5	service_view	superadmin
23903846-2cbf-4de6-9f99-b81b2d7f0892	60d6731b-e548-4175-ab3e-3fbb261e60f5	service_add	superadmin
a1684c14-554b-4409-83db-11f9227b7cba	60d6731b-e548-4175-ab3e-3fbb261e60f5	service_edit	superadmin
e262bcce-ec8b-4c6b-b348-bd5ffc1df099	60d6731b-e548-4175-ab3e-3fbb261e60f5	service_delete	superadmin
920d89ab-d2d7-4b4f-9df0-e6e91aede3bf	60d6731b-e548-4175-ab3e-3fbb261e60f5	setting_view	superadmin
af86dbae-3089-4525-8df6-d6805ad69058	60d6731b-e548-4175-ab3e-3fbb261e60f5	setting_edit	superadmin
1ab619db-5344-4537-b8af-82441be6cb94	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_view	superadmin
6edcd2e3-0584-4e09-a6a3-fc89b0b46c46	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_add	superadmin
0be34380-8984-4c53-8531-cd8bf617353b	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_edit	superadmin
b9718fca-6a49-4d78-b752-e2fab19efa93	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_delete	superadmin
16767423-2253-40d0-8751-e4b31f162059	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_setting_view	superadmin
d6da58f8-b3fd-4aa8-8f8e-08ae83adc494	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_setting_add	superadmin
32c1f1f1-1d06-4edf-b0ea-edc148a2f3b0	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_setting_edit	superadmin
8327f8a9-5bf6-42cb-9887-4dfe8819d1d3	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_setting_delete	superadmin
853f9711-b14b-47de-8111-61d4cb6bd0ae	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_setting_view	superadmin
da2d1e74-2ef3-40fe-9828-8473ce3b06c9	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_setting_add	superadmin
20a8534d-6809-474a-91d5-76c33508d3e7	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_setting_edit	superadmin
8d74f8a2-272e-436e-b48f-347e6b7e0d17	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_profile_setting_delete	superadmin
3825b4e2-40c7-4353-bed9-bb3c6e81d830	60d6731b-e548-4175-ab3e-3fbb261e60f5	system_status_sofia_status	superadmin
f5411747-f9c9-46a6-993a-8a41ccb90600	60d6731b-e548-4175-ab3e-3fbb261e60f5	system_status_sofia_status_profile	superadmin
781e3d57-4a31-4404-9a19-a434bd261e05	60d6731b-e548-4175-ab3e-3fbb261e60f5	sip_status_switch_status	superadmin
764c7739-daf7-42ff-a5c0-7467bfbde919	60d6731b-e548-4175-ab3e-3fbb261e60f5	sql_query_execute	superadmin
926d7427-aa44-41ff-a042-c3f3cabc73f8	60d6731b-e548-4175-ab3e-3fbb261e60f5	sql_query_backup	superadmin
75b18b42-e7c6-4ef5-8805-7061171c6ab7	60d6731b-e548-4175-ab3e-3fbb261e60f5	system_view_info	superadmin
af30011e-f9f9-42d1-8f92-7e5151c8cdc7	60d6731b-e548-4175-ab3e-3fbb261e60f5	system_view_cpu	superadmin
9d2f3a41-0361-4cec-8282-0b2f51b5f634	60d6731b-e548-4175-ab3e-3fbb261e60f5	system_view_hdd	superadmin
4d6c2321-4b9e-4a29-8bab-95b4e0a29f8f	60d6731b-e548-4175-ab3e-3fbb261e60f5	system_view_ram	superadmin
55bf39a2-28cb-41aa-9c0d-22025181a5d3	60d6731b-e548-4175-ab3e-3fbb261e60f5	system_view_memcache	superadmin
49cf73ef-8d76-40d9-8076-5244d8545301	60d6731b-e548-4175-ab3e-3fbb261e60f5	system_view_backup	superadmin
5cdc0275-c870-4bab-9683-ae5ee19e72f9	60d6731b-e548-4175-ab3e-3fbb261e60f5	software_view	superadmin
348619b1-a219-4b1b-aef8-96819763578d	60d6731b-e548-4175-ab3e-3fbb261e60f5	time_condition_view	admin
7a475304-7a37-431e-a4cc-be1a3af64a54	60d6731b-e548-4175-ab3e-3fbb261e60f5	time_condition_view	superadmin
0ecea272-bfc6-4713-8296-399f8d0f03da	60d6731b-e548-4175-ab3e-3fbb261e60f5	time_condition_add	admin
44e6b151-cd43-431f-9549-01e2644ba17b	60d6731b-e548-4175-ab3e-3fbb261e60f5	time_condition_add	superadmin
77cb0117-3749-4c64-920b-846dd1f07829	60d6731b-e548-4175-ab3e-3fbb261e60f5	time_condition_edit	admin
226d7450-184d-4924-a442-cb29a9213a76	60d6731b-e548-4175-ab3e-3fbb261e60f5	time_condition_edit	superadmin
8fbe6966-8d2e-4757-aba3-b93a11d49f7d	60d6731b-e548-4175-ab3e-3fbb261e60f5	time_condition_delete	admin
834a8e03-bb4b-4bce-9b87-1af52b216eef	60d6731b-e548-4175-ab3e-3fbb261e60f5	time_condition_delete	superadmin
97eac9ec-b1da-4803-b6fa-0cfadf21ad9e	60d6731b-e548-4175-ab3e-3fbb261e60f5	traffic_graph_view	superadmin
027f5b67-1f9b-4da0-b2f4-9b48ec08b37c	60d6731b-e548-4175-ab3e-3fbb261e60f5	var_view	superadmin
64260036-e333-4161-b042-67d6bdba806e	60d6731b-e548-4175-ab3e-3fbb261e60f5	var_add	superadmin
8ac82272-a546-4fcb-a1ed-54193f88957a	60d6731b-e548-4175-ab3e-3fbb261e60f5	var_edit	superadmin
be445547-1110-4888-a038-88a63fbf574e	60d6731b-e548-4175-ab3e-3fbb261e60f5	var_delete	superadmin
5b6eb127-8707-4a39-ae07-41cb9064dc8d	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_view	user
2e6ec593-a482-42c6-ad65-bc8f1526a9bf	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_view	admin
f7527e85-b8dc-4ea4-bbe2-fba77d4356d1	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_view	superadmin
4bf16944-1b7d-4855-a7b2-7c6a80f1b093	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_add	user
a6ecb610-eebc-41c5-ab6c-904c1c55e3eb	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_add	admin
15a38522-4a0c-47f8-9bcf-f48aaecfc7bd	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_add	superadmin
9c9f0846-ce94-4baa-b61b-64cdd7e943e9	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_edit	user
1c3e5aa1-39ab-49b9-9bec-f8c922a68989	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_edit	admin
acc09042-ec82-420b-b3d0-c6a325b957b0	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_edit	superadmin
38805215-0e7e-4f74-aaaa-0fba4a6daf85	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_delete	user
c5bc497b-90f8-4d33-945d-5d0970eb1f38	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_delete	admin
b912a5e2-c878-4980-9e58-a419e63e5910	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_delete	superadmin
480e26b8-8eda-431d-9246-fe2ba10b086a	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_upload	user
d6e8abab-cf2a-451e-96fe-fa704ce6f352	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_upload	admin
aff97720-e868-4de9-9b67-02a9e48e9805	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_upload	superadmin
dc432005-0b33-41aa-a036-ab83c92e3242	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_play	user
0c953c97-1a21-4884-9e7a-29388173994c	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_play	admin
c58d3026-c68b-4c11-8cc5-e647c70ed197	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_play	superadmin
2f7d2bd7-0f02-4b57-9b5f-a58c900bac1a	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_download	user
5882f1eb-c0cc-4c68-847e-11c529c0b537	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_download	admin
bc8d09da-7be9-4178-89b3-7c8c61b84842	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_greeting_download	superadmin
e7d464b1-555c-4c81-a0c4-bb616ce1a9a9	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_view	superadmin
d584dcc1-750b-41ca-a471-09f7a4ff6c1d	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_view	admin
ada80743-8f4a-4d73-b9d2-e8796e9b6030	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_add	superadmin
750078d9-b0fe-4f76-b3c2-bbc4231ee8e0	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_add	admin
411dada8-ec20-481d-8155-ed3f97e2df91	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_edit	superadmin
166173ad-4728-4939-a502-5f6a24efc19e	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_edit	admin
7b8028b0-22e3-4fd3-9eaf-75fc073810cf	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_edit	user
2f861a93-1c21-43f7-9af4-cea32b89d37c	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_delete	superadmin
b23ca950-f5b1-4cdd-9d95-47ad764b1fcc	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_delete	admin
61d928c1-e1ee-4fb2-a719-ae03730b9cfb	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_message_view	superadmin
573d733d-98c1-4660-b50c-18e9f9a54409	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_message_view	admin
94b3890c-c4a6-4e29-8737-86e21fec9a06	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_message_view	user
f3002f6a-ee5c-4ec2-b03d-f0d95ba674f5	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_message_add	superadmin
ca2d47b6-6363-4a6d-b604-0a9141fffd68	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_message_add	admin
c04abcfa-c682-4fe5-b286-e16e64336578	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_message_add	user
3acc2f11-7bc5-49d6-b624-c83a4b36f443	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_message_edit	superadmin
7929c0bd-5ed7-42e8-a0e1-5445c56bbe71	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_message_edit	admin
aea784d5-bfa0-446f-bf8c-3abb5f6b53e5	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_message_delete	superadmin
8c21935d-d4c9-4a0d-9357-21938408e369	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_message_delete	admin
504dd8b9-be18-4a39-a8ae-0676480283a1	60d6731b-e548-4175-ab3e-3fbb261e60f5	voicemail_message_delete	user
6507db1d-8386-4a12-85bc-e3d499432c60	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_view	user
4b0ec89e-345b-49b3-988a-2db89277979b	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_view	admin
5dfd78b7-a5a6-492d-b82a-0ee7599a62f5	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_view	superadmin
3409f6d7-640f-4bbe-86cc-1af4365d7975	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_search	user
2c3fc577-e792-407e-b7ae-06a62d0ead59	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_search	admin
13f75b42-c259-42c0-96b1-5918d9d79822	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_search	superadmin
0debcd79-6642-4eb8-81be-813431346f1f	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_search_advanced	admin
960bb9aa-254b-4e91-862b-9f0b8be90ca9	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_search_advanced	superadmin
0519a1e3-0bba-48a5-ac69-abe1d4c21402	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_domain	admin
db39c1b6-c510-4c0c-8655-55ba9f5429a3	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_domain	superadmin
ca629eef-0260-434e-9ecc-81b3beb887d4	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_add	superadmin
3ffc9665-adc1-41cf-bc4d-dea73b0b5d38	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_edit	superadmin
96bbd107-51b4-4130-95a0-53c2c54eccb3	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_delete	superadmin
18cacb6f-4514-45f9-80f7-5f2c5e3907be	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_pdd	admin
2a271518-8e27-483a-b867-6187609f65b3	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_pdd	superadmin
46da3c47-f454-489b-9c6f-09a500c8b1d3	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_mos	admin
faefb0d3-a59d-4ea9-add3-54869c6b51c1	60d6731b-e548-4175-ab3e-3fbb261e60f5	xml_cdr_mos	superadmin
37ae2bfd-7ae0-45ec-b668-e69a7417529b	60d6731b-e548-4175-ab3e-3fbb261e60f5	xmpp_view	superadmin
41992ba1-6162-4283-8844-735e748cb9e6	60d6731b-e548-4175-ab3e-3fbb261e60f5	xmpp_add	superadmin
e7a5450b-1788-426a-abf7-24efe317eccd	60d6731b-e548-4175-ab3e-3fbb261e60f5	xmpp_edit	superadmin
c2059dfa-ba97-43f0-a6bd-2bf792dc5146	60d6731b-e548-4175-ab3e-3fbb261e60f5	xmpp_delete	superadmin
1aafb518-e52d-444a-b682-edf5f4f66d53	60d6731b-e548-4175-ab3e-3fbb261e60f5	app_view	superadmin
0569602f-19d3-451c-b3ff-311941eee0cb	60d6731b-e548-4175-ab3e-3fbb261e60f5	app_add	superadmin
70600e98-6dcc-4cbb-b118-76ecd1bc3e45	60d6731b-e548-4175-ab3e-3fbb261e60f5	app_edit	superadmin
b9122d89-dacb-4fbf-84fa-9ab63eda482a	60d6731b-e548-4175-ab3e-3fbb261e60f5	app_delete	superadmin
b2d01021-2a1e-4bd4-b4fb-ed02acc1ba32	60d6731b-e548-4175-ab3e-3fbb261e60f5	database_view	superadmin
8634f2b3-f095-4250-ae73-8828af1bb249	60d6731b-e548-4175-ab3e-3fbb261e60f5	database_add	superadmin
b9de477a-d1d4-4720-bd41-c0e0560b1ded	60d6731b-e548-4175-ab3e-3fbb261e60f5	database_edit	superadmin
528f4596-3e29-48c6-831c-7dd9c1812449	60d6731b-e548-4175-ab3e-3fbb261e60f5	database_delete	superadmin
04583687-e3ab-431c-9b44-ad1a67137bbf	60d6731b-e548-4175-ab3e-3fbb261e60f5	default_setting_view	superadmin
d9d7ef62-fce6-4926-9909-6c70a9caccf5	60d6731b-e548-4175-ab3e-3fbb261e60f5	default_setting_add	superadmin
acc5aff8-666d-4da8-ba0f-6ead316ba665	60d6731b-e548-4175-ab3e-3fbb261e60f5	default_setting_edit	superadmin
f3704739-7ac5-4c9d-ada5-033ac31d311b	60d6731b-e548-4175-ab3e-3fbb261e60f5	default_setting_delete	superadmin
31d775f7-fa31-47bd-9775-fd48a06a68e4	60d6731b-e548-4175-ab3e-3fbb261e60f5	domain_view	superadmin
7a8a3faa-04c7-4dd2-b877-55b24c1a55a2	60d6731b-e548-4175-ab3e-3fbb261e60f5	domain_add	superadmin
467bf831-1f75-4923-97b6-6fda0c4782f5	60d6731b-e548-4175-ab3e-3fbb261e60f5	domain_edit	superadmin
6dcffb7c-239c-4041-a928-4296fcf5e13b	60d6731b-e548-4175-ab3e-3fbb261e60f5	domain_delete	superadmin
a4582909-4189-4355-b296-4c221f46a328	60d6731b-e548-4175-ab3e-3fbb261e60f5	domain_select	superadmin
c354923b-d4bb-4971-97a1-d4206266fc7c	60d6731b-e548-4175-ab3e-3fbb261e60f5	domain_setting_view	superadmin
9b2bd95d-2590-4ff4-82cb-efd13077a5c9	60d6731b-e548-4175-ab3e-3fbb261e60f5	domain_setting_add	superadmin
8154d9d4-1c8b-4033-9fb8-c52958669196	60d6731b-e548-4175-ab3e-3fbb261e60f5	domain_setting_edit	superadmin
3ac03342-89b0-4a2c-9b56-899e20498b5a	60d6731b-e548-4175-ab3e-3fbb261e60f5	domain_setting_delete	superadmin
65c282f9-56c8-4155-938d-346166182b32	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_view	superadmin
4a640a39-1a9b-45e4-a8a6-e4a742a56c23	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_add	superadmin
b432c61c-430f-4fe0-8433-3fc27bcda46f	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_edit	superadmin
4b25c40c-d44d-4d1d-a72b-d21f2dd4adce	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_delete	superadmin
096c11f1-92b7-4cb3-9b96-7b597ed81148	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_restore	superadmin
bd68bba8-6a80-41c0-926c-ddaabc8783d3	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_item_view	superadmin
ddb1f595-a1bf-4bc9-9ff2-0ed339a96e68	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_item_add	superadmin
385085b4-210f-419a-82df-a8aeafd00ad0	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_item_edit	superadmin
df577b0d-f8b0-41c5-bf4f-21442b1cf958	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_item_delete	superadmin
7cb9cc34-744a-4121-9e67-8d6d27966d11	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_item_group_view	superadmin
72f190b2-57d7-4fa8-a95f-2844a8ec9d10	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_item_group_add	superadmin
41e29bac-b82d-4364-a761-2be0b26b844a	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_item_group_edit	superadmin
157ec515-aa61-4a58-a47e-ae6187062500	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_item_group_delete	superadmin
f5464efd-ac48-4420-9ce4-c068e379b4bd	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_language_view	superadmin
ec6d0a3d-6735-4511-8ede-8d7e918cd1c0	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_language_add	superadmin
f2d49fbc-3f58-4130-a2ec-ebd182558f7b	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_language_edit	superadmin
11556f28-1c2b-490d-8187-99fb95b14fb5	60d6731b-e548-4175-ab3e-3fbb261e60f5	menu_language_delete	superadmin
5a260ceb-848f-4390-9033-462a24e5d53c	60d6731b-e548-4175-ab3e-3fbb261e60f5	upgrade_svn	superadmin
26a2ba5c-f748-4159-b90c-a15259c84a63	60d6731b-e548-4175-ab3e-3fbb261e60f5	upgrade_schema	superadmin
adff9cac-aeef-476f-b7c9-37e5ff7d4c20	60d6731b-e548-4175-ab3e-3fbb261e60f5	upgrade_apps	superadmin
ddeed63c-12e5-46e4-8e9c-318c744f9cf0	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_account_setting_view	user
5a76e3f8-e77e-404e-86d6-475b6bf6def2	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_account_setting_view	admin
05885a01-3330-49e3-ba30-39433be6d3ec	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_account_setting_view	superadmin
f595ba5c-cdd5-4a61-bcab-dca468219742	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_account_setting_edit	user
0c1ef096-610b-4821-bb0c-3abb0ff0277c	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_account_setting_edit	admin
46c24617-85eb-44d5-b56b-b8887e5de760	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_account_setting_edit	superadmin
84148d44-46d4-4648-8db6-1b73506f60eb	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_view	admin
7b440820-5f39-4f56-a433-d8bbe1d22d13	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_view	superadmin
43485889-ab71-42dc-b56a-b373d5f21d1e	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_add	admin
7d30fb81-acb0-4cf1-ab47-acab76748483	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_add	superadmin
811a3e18-a14a-492f-8f96-fc23c0886b94	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_edit	admin
0cdef1a4-d45e-4e7b-97ac-748ad8474af1	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_edit	superadmin
ab0fb0b3-a037-417d-87ff-e02dedfc4c73	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_delete	admin
cdc925cf-91f6-42dd-851e-01013404d85d	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_delete	superadmin
1c9a605c-40d6-4733-9f18-6e7801308787	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_view	admin
e6740b25-4f00-49ef-8bf3-0f361c0b7358	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_view	superadmin
64efb666-ad76-4654-bdcf-36cd85d8193a	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_add	admin
ccb87b0a-c991-451a-95d4-41f69508f5c8	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_add	superadmin
e7f55db2-6bf8-419d-976c-c4ec649d0d63	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_edit	admin
d9304fd0-fe9d-4fc8-ba3a-5060b650d07f	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_edit	superadmin
93b59515-f74d-4078-ae90-1ca6e5744980	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_delete	admin
bad1ff2c-1b91-439e-ab17-f221b4b6ce74	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_delete	superadmin
269b7671-fd78-4c77-ad22-db57a146abb4	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_member_view	admin
fee82c72-9d6f-47a5-bd01-c721f9831a92	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_member_view	superadmin
7498e5bd-b461-4e25-9a49-cfffc08ac65d	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_member_add	admin
5a1aa06a-e8ab-4528-b46c-a267164209f9	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_member_add	superadmin
9bcc4169-3e3a-4232-8c5d-297b19c54261	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_member_delete	admin
25ac782e-8d0f-4e62-9624-d82d2ff95b3e	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_member_delete	superadmin
ca7a6ba3-8fa1-4b87-bddd-a396c685573d	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_permissions	superadmin
02fb8a5b-8f52-4ada-9148-f2e6dcd3c231	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_user_view	superadmin
4a1cb6de-bd6f-477d-85a3-d806a0e7c011	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_user_view	admin
c45c4f32-1163-4006-b7e8-cc58bf24e7a1	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_user_add	superadmin
6900d075-8a10-4219-ac6e-f7f731c93a0d	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_user_add	admin
926fad5c-99bd-49a0-a6cd-245bfbfa5a3b	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_user_edit	superadmin
5064e066-744d-44fc-bd15-26287841ba4e	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_user_edit	admin
85565de8-32be-49c7-853c-e8f6c4136860	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_user_delete	superadmin
85678a02-3de5-4d18-a4a5-b76d17a07d3b	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_user_delete	admin
707ecd4e-27f6-4d3f-825b-46f135715155	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_permission_view	superadmin
b9fa307d-21fb-489b-80b8-f2dada84dbab	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_permission_add	superadmin
cd574dbb-c5ce-4997-ba14-50501dfe9e77	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_permission_edit	superadmin
8c04550d-e2ed-43e4-b03a-547c12228271	60d6731b-e548-4175-ab3e-3fbb261e60f5	group_permission_delete	superadmin
b6aed297-65cd-435d-849f-93086159feda	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_view	superadmin
ed570950-99d5-4e40-a6aa-f8282991902b	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_view	admin
56f2f3bc-ec08-4491-9b65-67e7575b4791	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_view	user
b6760130-86ec-4ffc-911a-93f21ec5b181	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_add	superadmin
dcb93d30-1d3f-417e-85cc-1e2fc50a47b0	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_add	admin
07e11f11-c71b-42c3-9b3a-ad98245ffda5	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_add	user
fe9fb621-c321-418c-9386-e53ebebf8151	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_edit	superadmin
c06ffa5e-ad2b-4a37-8c41-87faecab83bf	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_edit	admin
e6775224-265d-456b-9670-b3dcb4573cde	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_edit	user
7b77f2e2-f494-48ec-8ceb-d88a015ccd66	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_delete	superadmin
2d396cbf-55c0-477c-a44f-47ab8346e54e	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_delete	admin
ac7478da-0d78-4916-ad40-1bbb5f21e78a	60d6731b-e548-4175-ab3e-3fbb261e60f5	user_setting_delete	user
c19b56f3-7f95-414e-bde7-00d183085368	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	adminer	superadmin
f6f4472c-8bfe-4213-ae05-38f7f247b478	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	backup_download	superadmin
9fd64c51-e3fc-44d9-996b-7d659f88beed	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	backup_upload	superadmin
f27dc2fc-a19e-4d67-bab6-03008ee3e277	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_view	superadmin
6f359890-2f9d-4747-8afd-81dbe7ce0ea8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_view	admin
ce31f07c-bd6b-40a6-a954-0b0196fd645b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_view	user
9ae0d3b5-7bd1-49de-94ac-21236f87bdee	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_add	superadmin
4e22e402-8328-4594-bcba-ca0a1469d10c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_add	admin
7c5e8517-952f-4263-af51-1e71d544dd8d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_add	user
d0ce3f88-e180-455f-bee4-a32f5a4160ea	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_edit	superadmin
562ca182-62f2-498d-ae95-0a8ce04e4037	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_edit	admin
d1f9fff7-db06-4a57-8bb4-636e51777e70	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_edit	user
12469e94-ff3f-40b9-b293-c1a216c0a00e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_delete	superadmin
9d87374e-b30f-466d-ab9d-734b0b5cea69	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_delete	admin
84ffc9ab-fe92-4960-870e-3f7dd2dbe5cf	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_block_delete	user
8e2356d7-0440-4502-9dfa-be14f1dd9064	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_broadcast_view	admin
8fbc109d-8e58-4573-9bf6-b71232586bcb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_broadcast_view	superadmin
0848ed5e-0d41-4bbb-81f1-d553582695c1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_broadcast_add	admin
b604c03d-c458-4f1a-a348-622bc64b7986	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_broadcast_add	superadmin
2362f685-5369-4f55-9213-fbda9458640c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_broadcast_edit	admin
fe490a57-f622-4585-aa34-257f1177b6f2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_broadcast_edit	superadmin
439fd6d0-dc77-4a9a-a018-ed4569b6a2cb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_broadcast_delete	admin
07554425-4272-4e58-908e-2f5892fb2fcc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_broadcast_delete	superadmin
853dd2d8-b719-4ae1-81aa-22a81d212d5d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_broadcast_send	admin
10624bc7-9157-4ec8-bb40-1e16c03af13b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_broadcast_send	superadmin
3725860a-36b1-4980-aae4-da8d1e2d0d80	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_queue_view	agent
b2ba7a9d-7336-41b4-820e-1abfb04af386	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_queue_view	admin
9dc0c759-56b8-4ee9-8a02-55376be94dc7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_queue_view	superadmin
0a0c4d43-d7e2-4f46-a17c-938b6eec90d6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_queue_add	admin
cc5c449f-1ce7-4981-a910-d984629ab0d3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_queue_add	superadmin
f837973d-85c7-435d-960f-639342162522	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_queue_edit	admin
08722e24-6b13-4922-8634-e4d58f23250d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_queue_edit	superadmin
a1f77058-da60-46cf-8942-60cc0b98f2ab	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_queue_delete	admin
8c7e6af8-cc06-452c-ac18-db223dac132a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_queue_delete	superadmin
a5f5e1ac-59c6-4cd2-ba0d-cea21c2b7ba4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_agent_view	agent
c5c8738c-adf6-4ac2-a8b6-0004256539c6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_agent_view	admin
c9452a7b-866b-4c94-92b3-b2181e90e119	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_agent_view	superadmin
80e88143-3d42-4729-aa20-84f2a3408297	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_agent_add	admin
115db657-1133-44f2-99da-66d369a96231	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_agent_add	superadmin
4bb8d872-1f96-4499-b875-73e79fd0e7ea	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_agent_edit	admin
2678d5a5-e4ad-4fee-8357-e95cabd483de	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_agent_edit	superadmin
bd87ea90-2570-40e1-9805-fd96aa8f310b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_agent_delete	admin
55895f2d-a191-4827-b798-4b84774581a7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_agent_delete	superadmin
da28ceb5-df3a-4c0a-b62d-ffea9fc79698	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_tier_view	agent
90d961d8-c829-467c-abe8-5d6dcde68766	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_tier_view	admin
3285d324-c442-4e75-b2c8-a7419ff2fcf5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_tier_view	superadmin
f3319efa-54f1-4767-a230-d66416e5d82c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_tier_add	admin
422fbea7-7474-41d4-a3db-a605bf5e90b5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_tier_add	superadmin
817151c9-2e15-40ab-bb59-a612903ee9e5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_tier_edit	admin
5b9bd73c-4d87-4ba7-a3f0-ffec6a6f9cdc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_tier_edit	superadmin
c9777159-70a9-41a0-8087-8e3ffeac15cd	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_tier_delete	admin
8f44e6d9-114f-4771-8cf2-c3edfaae7093	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_tier_delete	superadmin
165b1cf7-171a-4ccb-ba77-3d12112d827e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_log_view	superadmin
9aa6f917-197a-4b4f-b38c-a9ba9f9953a5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_log_view	admin
8dc1fc33-2db2-48e4-b730-ce07c1911312	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_log_add	superadmin
74dcc1f9-e3ee-4fc5-8557-264a51fb8bb3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_log_add	admin
aae17ce9-398e-4fad-8871-165fa51d18d4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_log_edit	superadmin
a190fed7-e8e4-4bc7-9b69-ca14310b8d1e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_log_edit	admin
dc4c7653-5dae-4da3-9cfb-2953ee076dc6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_log_delete	superadmin
87b97029-8a60-4a98-8cd3-b5bf273bd139	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_log_delete	admin
5372b27d-11d8-4552-8846-06d6a5d55529	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_active_view	admin
9c0a61e6-6c20-4885-8182-8e2626768dc9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_active_view	superadmin
916f4ef1-6b97-44c8-aeb4-02d6af3547e2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_active_options	admin
49503ad1-9811-463e-81dc-fc6c433a476c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_center_active_options	superadmin
8c5ce8f2-80ba-43b4-8bb7-4eb1c6c6ad71	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_flow_view	superadmin
b654b1f6-55f3-4c77-8421-cd7e50bcdb7b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_flow_view	admin
e38f209b-658a-4076-a1fc-52e21fbd3d07	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_flow_add	superadmin
b2619eaa-e7fa-42be-9275-07fa9b13b0ca	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_flow_add	admin
16a5f35d-b9fb-4c80-839c-4fbf7227e882	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_flow_edit	superadmin
56f0ce1b-0134-4fbc-aaed-455445cddaff	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_flow_edit	admin
e205e6e0-cf90-4b0b-95d0-a47a9d9d51fb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_flow_delete	superadmin
65c849ec-b1d6-4a98-8e1d-897bfd54e299	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_flow_delete	admin
4c2b3a69-73c2-485d-9013-d2a716750e2b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me	user
ac61d9bf-d8e3-45c7-9720-d6127297905e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me	admin
dac6d91b-fb22-4b2f-b4a4-9c954a7124aa	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me	superadmin
552d22d9-bacf-4817-8d45-5334628c0e60	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_forward	user
cbf7b250-018f-4f53-a20a-9c7da9d7cd0b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_forward	admin
45896506-8026-4a06-b051-a8b170655d7a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_forward	superadmin
af272267-368a-4c2d-bd77-17a036bb19d7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	do_not_disturb	user
1c4a7779-265f-4a00-8d77-279aec8adddf	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	do_not_disturb	admin
aa21da5f-f996-4f9a-9e33-90de12b9b594	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	do_not_disturb	superadmin
5d15e7e5-efc6-4a29-95b3-79f612dbeb3d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_active_view	superadmin
5ad4034a-3ead-42bb-914d-3d9dc6e5c9f8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_active_transfer	superadmin
54483f41-e848-4ad5-aaaa-501c7a88f910	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_active_hangup	superadmin
a9ac8556-898a-46e3-964c-b380f82baca9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_active_park	superadmin
e50b6bfe-f6c3-4681-90bc-22c1fcc4f433	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	call_active_rec	superadmin
ac083fc3-3509-46d6-bad7-f435148366b2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_view	admin
e78ba5eb-6553-4a55-ab99-b23c1648f0bb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_view	superadmin
47f1cd93-9711-429e-90b0-37551314e67c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_transfer	admin
bd480f8d-ba4b-4c5e-9333-d362ebbadb6b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_transfer	superadmin
e733568f-5006-4950-aa7f-429e606fb5b8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_hangup	admin
9ff6d58e-fbc9-4246-aa81-67931fa97b04	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_hangup	superadmin
530433a7-8527-4c01-a2a0-feec7a63d6ba	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_park	admin
28c10cb6-dfbe-46f4-bb19-89a8c5bb1b5d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_park	superadmin
612273fc-d6ee-4b81-a496-ba0d6fa768fb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_rec	admin
c7a37d2d-4249-4eea-81e8-6a1ac496b001	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_rec	superadmin
3f6f78ed-965f-463b-9ff5-a1c32e9155a7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_list_view	admin
ca287787-245d-4e9e-bc5b-5d4d7dc6eaf9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_list_view	superadmin
2880d3ba-1dbc-4a45-997e-729805353e9a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_assigned_view	admin
92be3b74-f14f-4cf6-9dd9-8ebfb99cc6ff	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_active_assigned_view	superadmin
1a5541ad-512f-4fa3-a012-5eeedae67e7b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	click_to_call_view	user
c4bd9d4b-6885-474e-9cc3-9ad5a7fd1041	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	click_to_call_view	admin
c7b2c659-1da3-4757-82af-0c8aa7331177	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	click_to_call_view	superadmin
e8588507-ef0e-4f08-a4df-13df1efc04de	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	click_to_call_call	user
bd46f8e7-892b-4058-8c34-de83fff43743	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	click_to_call_call	admin
a3a5213c-e4f3-4dc4-8cc2-85da0f4ac573	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	click_to_call_call	superadmin
c448d96b-0987-4cb9-8c78-fc967d347c3e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_center_view	superadmin
0b3d9bb0-b970-4e98-b8e2-9aa9456d6e36	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_center_view	admin
f0ec401a-20d8-4984-9742-e5e75c05900e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_center_add	superadmin
bbd545af-eeea-485a-b93f-a72dc4301a3b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_center_add	admin
5683c93d-f85f-4c6a-8b3b-5de1aefde2b3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_center_edit	superadmin
17d960cf-0f31-432e-981a-b84bfc657ba1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_center_edit	admin
1d42d6a2-3d3c-40f7-bba0-6f2706d581ea	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_center_delete	superadmin
0b192d5b-0eee-444e-b7e3-f9683cb93b19	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_center_delete	admin
2158ef3b-3f62-4311-80b4-8e7df3e932a7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_view	superadmin
31c66898-5752-4462-811b-37b6da399ff3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_view	admin
28e59019-dc42-4f14-8df6-6bf9af9c1f5c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_view	user
15feb1b5-4c08-4213-9d9e-579c52317ef0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_add	superadmin
ff37987f-a7d7-4f31-bc31-db801e25cd09	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_add	admin
27fbd3e3-210b-4c2a-8eed-ffaaa025bb29	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_add	user
8b783cf5-1d75-4927-b36d-5619f1c17b4c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_edit	superadmin
e6ed9137-a773-4861-9f85-856fb3f0027e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_edit	admin
b43be9a2-03c3-426e-8819-7433c84bb085	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_edit	user
d1616c6f-e555-49a7-95d0-0e39d085a053	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_delete	superadmin
cc9e52d2-28f9-4c37-958e-113fcd9b312a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_delete	admin
2d1be545-248c-417b-9b37-015343bbf8d6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_delete	user
6e4e23b2-69f8-4333-87a6-edd4cef572c9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_view	superadmin
16ad3d4a-cc18-4f99-a55c-4bfad09b8e83	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_view	admin
11e9cb9a-00e4-4c17-b11f-9ca1beb6d9f1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_view	user
c573f397-1f1a-4cdc-81b9-934d0e96a41f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_play	superadmin
be0f63f0-f3f3-4f78-8677-6603e3269e40	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_play	admin
23b98335-8714-474c-9f0a-1a8573110307	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_play	user
f3485c04-ea90-417e-8738-d3d78509a643	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_details	superadmin
5f7e7e80-2dc5-4675-bbf2-f60abc2c6436	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_details	admin
d83ffb75-c080-4fb2-84b1-8cc3e3590054	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_profile	superadmin
34fd2318-6c98-4e91-a6f0-23e95152daa3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_profile	admin
716d4e47-f5b6-4a04-aaae-3b195ddb904d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_profile	user
2efead08-b47b-4d62-a48f-5720724ed44d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_record	superadmin
3a4eef53-0ab3-484b-87aa-4f229e6c5728	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_record	admin
d257a5b1-2fbb-4d9f-b71c-2f9fe0d55c6c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_record	user
4cab39de-3c48-4ac5-9af7-45d9ad5b6bf7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_max_members	superadmin
94ea9096-9e26-4c4b-b01b-420d4a2f0199	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_max_members	admin
804bb8b9-a4ca-4eca-9660-2d11fe332a8a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_max_members	user
559c460a-e8e3-4d8c-899d-54ef49721376	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_wait_mod	superadmin
a5bf238a-838d-4ae3-bb8b-a3800419da1d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_wait_mod	admin
c2a20bd1-6702-4a14-930f-5cfe65d2d140	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_wait_mod	user
412f6679-1acf-4edc-9fcc-816ef9464188	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_announce	superadmin
69ac8ac4-a348-4c12-a46f-46266c74a0c7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_announce	admin
752a90a1-025a-4d29-ab23-0c4eadcf30f2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_announce	user
a90797b4-13dd-4ad7-b685-5c8328b102e8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_mute	superadmin
6ee19376-c759-4ba0-8367-f21d27ada5c5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_mute	admin
ebfb175c-7a73-4c85-aae0-48ae2e2cca19	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_mute	user
77efb489-91b9-48f5-9185-980e8b8de411	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_sounds	superadmin
77d8491e-2746-4182-b35e-8e6b81245ecc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_sounds	admin
8997e12a-2433-472e-9116-ab616c193f35	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_sounds	user
9572a58d-08c2-48f7-af16-de2401113487	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_enabled	superadmin
3aae8ec4-fa42-451e-97ad-5f6a2dcce489	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_enabled	admin
17092472-f41a-4cdf-b0e2-7e24e0b4b9e9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_room_enabled	user
1523c6ec-a570-4ff6-a2a0-f718aa381ca4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_add	superadmin
6467445e-8903-4e03-91e7-358e1e68f40c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_add	admin
af99ca29-00e4-40de-bcde-09236c442a1d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_edit	superadmin
bf767f9e-29bf-4ba8-96d4-c05be6afe0d1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_edit	admin
b5c21b4b-69d3-4295-b568-bbdae46831e3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_delete	superadmin
1c009d1d-99cb-4960-8db1-ee197acacf77	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_delete	admin
2d1900e7-2313-4001-a51a-730530a840e0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_detail_view	superadmin
5388e657-62e7-4931-a369-e0f1fc7b4ae6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_detail_view	admin
e8d14726-768e-47db-be1f-b8b4fcb5b178	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_detail_add	superadmin
e89340d6-7141-4e8f-a625-a66ed354a286	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_detail_add	admin
319cafd5-df37-465b-a96c-0b55e49408e7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_detail_edit	superadmin
22392099-2951-453a-ab4b-82d3ea35c68c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_detail_edit	admin
66898d38-31db-4dd7-b589-d5b1abc20833	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_detail_delete	superadmin
30f99b6c-356c-431f-a1b4-66a441a2342b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_session_detail_delete	admin
b9653082-62d4-4c64-b81d-1def1f8e7cba	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_view	superadmin
89811392-93f2-48a7-a948-2728c38613d0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_view	user
7983bc1a-4d49-4028-ac2d-7ccfdce7f297	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_view	admin
a8845b3c-73c3-4bdb-8ab9-58b994e9c267	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_add	superadmin
af60885a-b198-4a95-a4ba-2bf4febe2c5a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_add	admin
3815e28e-a487-497a-8c5f-378c9367dfa9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_edit	superadmin
453a60e5-2e1f-4455-83f0-e483bf2a8239	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_edit	admin
30b618e9-8501-4a12-83ab-4f0f11a91809	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_edit	user
c41b6e08-d9a5-4d8b-a463-254c482abba5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_delete	superadmin
9ee159cf-fa12-4313-9767-5e9ffac77cc6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_delete	admin
5e27bf23-2c5b-42e4-a66f-d1d871d6755a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_delete	superadmin
41a5a101-48a7-417c-b806-220b5ebfa8ea	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_delete	admin
8c7a89c4-e636-48b7-8de3-259435cba663	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_user_view	superadmin
0d1974b8-2dd2-460b-af01-f3d1f934b27d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_user_view	admin
d665d6e7-ec10-42e2-9c39-787d14b8bc98	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_user_add	superadmin
98360916-0ff4-4192-9182-538c78b48dfa	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_user_add	admin
dea830f9-14e6-4ea3-a1f7-96f2e229742f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_user_edit	superadmin
bf923a99-fb5b-47f2-93e9-c95428e04853	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_user_edit	admin
a1bac41e-99b8-44c7-98dd-1b9be8fdc8c3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_user_delete	superadmin
d17fa9f4-ef08-470b-ae9d-dff1b497b465	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_user_delete	admin
df730ab0-16b5-4a61-a6d1-8ff6f85194f8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_view	user
b08b8ecd-8733-45d9-9e93-f56dc0a77a7d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_view	admin
90d2802c-0bba-42a6-92e2-ed85ee468f9d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_view	superadmin
3524b376-a72d-478f-a699-8335dd32cdae	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_lock	user
50fca3fc-7cb1-4a0d-ae6e-1f4a9c5bbc22	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_lock	admin
86a5e480-729b-47eb-b4a6-659cfdd848a6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_lock	superadmin
39d05ac0-2140-4ae3-bf3d-68e9fc9d91ce	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_kick	user
3102b17d-1798-4a33-9e4f-62f750b1953b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_kick	admin
3b8b5815-670d-4746-9676-1fc53c1dee59	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_kick	superadmin
24a7884e-ee87-4439-81ff-bf41ecc82edc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_mute	user
752ed1f0-e507-4bc7-8610-4d9911e04162	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_mute	admin
18738075-d502-4ea8-87ac-c56521d2ffc2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_mute	superadmin
39e2a5c6-e08a-4eb2-b070-4215f60be7af	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_video	user
acc4a1ac-b237-4cf3-b94b-5d82e47369fe	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_video	admin
7f5b77a7-1a35-4786-86ea-70e57be0ba14	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_video	superadmin
7ac4e9bb-3d6b-42e2-92a0-8b469d526eea	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_video	user
7ff3487d-dafb-454a-a55f-7c132a19f8ae	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_video	admin
dbc2cca6-88e5-43ed-baeb-ad2f06984de8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_video	superadmin
9a2f3111-01cf-4d6d-be97-cc8807dea50c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_advanced_view	admin
566888ce-a716-40cf-88c8-f503deb86273	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	conference_active_advanced_view	superadmin
164ed234-89e3-4b59-b118-3b9d23644b32	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_view	superadmin
f7c32ec7-e558-4ebc-923e-5e715745e045	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_view	admin
525727c3-0eec-49c7-b2df-cffb14e389a8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_add	superadmin
769c3540-f91d-4a18-bf1a-5d8476abe32a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_add	admin
14926ec8-a5e5-427a-aa96-564dde3b91e0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_edit	superadmin
08c6e3d5-32d7-475d-a71e-828f8ff2e280	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_edit	admin
119273d0-c69b-4035-a40f-48a8f621fc33	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_delete	superadmin
4d3928a4-f2ea-42c8-8ef4-3042631313f6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_delete	admin
7eeebc04-2e64-44fd-9dc5-3a07d6b61cce	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_address_view	superadmin
3c014759-624d-44c7-973f-9e71923e8d14	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_address_view	admin
084e245b-a119-41d8-a606-30e11d088bf2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_address_add	superadmin
273a24fa-3641-4326-8f56-258039fb65c6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_address_add	admin
dc2db325-26ed-45f3-88cb-b7d2b37a4066	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_address_edit	superadmin
93a46009-6041-459b-a8e7-eaa324ecd306	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_address_edit	admin
b3ad19e3-0192-4ec1-be8e-230de5743dd9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_address_delete	superadmin
96c5bf49-3d67-4dcc-be25-c136519c6ff4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_address_delete	admin
1217ed49-a770-4fbf-964c-874c28d493e3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_phone_view	superadmin
9f25d462-b173-40f0-a1d2-0408f1df77fa	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_phone_view	admin
c8b3d6fa-4bf7-4612-a23d-5af693f936f0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_phone_add	superadmin
73044365-98fc-46d4-8da3-1ac4b654baab	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_phone_add	admin
fefc3baf-1d68-4acf-a13c-f62d00f4109d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_phone_edit	superadmin
453e6e2e-962d-457b-96e9-d6024495ab0f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_phone_edit	admin
cbe8a8df-a06a-4997-b51c-401b42504639	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_phone_delete	superadmin
7fa9910a-0d90-409c-9aa7-aba695ebc6f8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_phone_delete	admin
edc96e08-cb3d-4080-af9e-0df587ded4eb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_note_view	superadmin
60ca715e-1d51-4e31-bc45-6f9a5fa4713b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_note_view	admin
dcacf91f-c242-417b-a845-b4b173b60c38	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_note_add	superadmin
46986a9a-cf67-4545-945a-76a0d49cb04e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_note_add	admin
182bf332-8981-44fc-9564-8f3ba59b3adb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_note_edit	superadmin
1a3a2710-f140-410c-87a0-ee2580d92ff4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_note_edit	admin
4eea6244-7f3d-4930-ae3d-84664634f45b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_note_delete	superadmin
22f81f7f-49aa-4180-b609-d84c89d69205	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_note_delete	admin
86a86c1b-5dfe-4992-8863-29ee5a1ae65d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_group_view	superadmin
e9c6f994-63d8-48be-9308-912a61f68261	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_group_view	admin
f5665a2f-491d-4db1-90a1-4ee170b63a6d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_group_add	superadmin
00536dc2-4624-4165-a25f-91876518ca0b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_group_add	admin
533d3595-eeba-4845-b418-b8f7e53f5303	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_group_edit	superadmin
4f8feedc-4f94-4a64-935f-ca8e9da939b5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_group_edit	admin
b79dfdae-e2d2-4dfc-9a70-7a644ea9321d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_group_delete	superadmin
82e5093c-e71a-4a60-b670-a356c41291a7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	contact_group_delete	admin
b99b22f9-7081-4ca3-95a6-7b26d6d5cab3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	content_view	admin
0e297369-6983-4292-b55c-0e148e083ef4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	content_view	superadmin
4f409e4b-170f-4c2c-8840-66e74bf5926c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	content_add	admin
2ccb9289-4dc0-4e84-a663-820eaa38ca4b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	content_add	superadmin
6d47185a-2c11-419e-8b03-fd7474bfe6a8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	content_edit	admin
288617dd-df81-473d-8a5a-32e1d1053314	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	content_edit	superadmin
f6524c7c-071f-428c-8c13-216bf67dcd49	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	content_delete	admin
13fa4ca9-8a61-4285-8948-51e9221531c7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	content_delete	superadmin
21036da6-2c24-419f-9f3f-dab245cb2133	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_view	superadmin
5730b803-d030-47b6-9b0d-90ed4aacc76e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_view	admin
d532e600-7c2b-4622-8d01-6e26cfde599a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_view	user
73f16668-4f9d-4cab-aefb-c2db8cfb76e2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_add	superadmin
48320f60-273a-4fc1-9ee4-7c8e7b013d38	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_add	admin
335147e4-8d31-4e13-ae3f-009fd1316253	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_add	user
158fccf4-fd90-4919-81c1-f93505fab04b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_edit	superadmin
2361d2e6-e657-4c91-beae-03ba8f76e130	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_edit	admin
4f3a0ff6-d636-4013-9767-0a4f3520adba	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_edit	user
46514551-fefe-47e8-b3f9-92357415f83a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_delete	superadmin
cab72729-f81e-4a0c-b3d0-192dd839f3dd	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_delete	admin
4a62e83d-3edd-46a0-98da-786d08baf40f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_delete	user
7d5933fe-2c19-42d3-80a6-5626f29e0c19	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_view	superadmin
ef91059d-de0e-4f28-a1e0-131c1787ce4b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_view	admin
19b5e349-de82-4ad5-8060-4720399ad9a2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_view	user
92e378cf-4f1d-480c-add6-5aa1004d9f5e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_add	superadmin
4877af26-bed2-46fb-b87b-63e98203ae81	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_add	admin
c247a622-e782-4880-ad22-e1f0feb46ad7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_add	user
334872d1-deca-4f55-ac75-782ad1d1146e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_edit	superadmin
4ed0f0d0-502b-4010-a1d7-9ca8007cddef	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_edit	admin
a4540243-9000-416c-a766-22d2ed7be35f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_edit	user
b65e8b1c-3a63-440f-acce-64bbd88f088a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_delete	superadmin
086db13c-2339-4af7-ba0d-f53ff6c3bb36	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_delete	admin
3da3c2b9-8813-4328-95ef-c5a2dd1adbc9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_delete	user
17f1831b-0bdf-461d-a8a9-e1e96eee9065	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_view	superadmin
777489b8-bbd5-464e-ab0f-b43ce83b8f03	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_view	admin
268ab548-9664-460b-8c22-06913905ffae	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_view	user
0b376da9-2518-4d4c-8fbf-7decf96b7edd	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_add	superadmin
55f370d2-69d0-4657-8f11-19faee321eb5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_add	admin
80016ead-142a-4005-a870-85ace23e2189	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_add	user
0af4828f-0f88-4cc9-87e5-22f860c3f4d6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_edit	superadmin
0374f96b-fba5-4e5a-bc16-b6f8da704320	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_edit	admin
531dda3d-b47f-4fa9-b47b-98a088de4c14	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_edit	user
3b25967e-c3c6-41bf-8b1c-8b9481b47b95	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_delete	superadmin
b44b2e66-e4b2-46af-88fc-357df9a55dc9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_delete	admin
5d063c4b-cae3-4989-ad0b-562b219e0277	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	rss_sub_category_delete	user
15ea6163-b298-4bc9-a61d-0652c5531198	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	destination_view	superadmin
3c48c881-1c85-4bdb-9d05-3f9775ac91ea	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	destination_add	superadmin
a9a20d06-c3d5-47e7-9df9-4b66b8ebfd93	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	destination_edit	superadmin
5da3fefb-0acf-4144-b835-f690564a7a6f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	destination_delete	superadmin
87586701-8adc-4e76-9e47-d763d2604990	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_view	admin
ed8ad9f0-9e57-422b-9805-d67166b76d87	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_view	superadmin
cfad4f59-dff4-460a-a052-24656558519b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_add	admin
b8e15e9d-e407-4669-a460-b6eeb9f53313	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_add	superadmin
4b57ab6c-aace-4adf-b022-61a54e53cd4e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_edit	admin
806017e8-50c4-4dd9-a989-ca51dc7fa4ac	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_edit	superadmin
916ee19e-4f7f-4b0c-9060-523c38b173f9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_delete	admin
3b3a0fc1-db12-4603-a1f3-23f2737a0118	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_delete	superadmin
38ed1a1f-df66-4371-a24a-194005f55eee	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_extension_view	superadmin
749dd280-5805-49f6-9ce7-4f76f2554470	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_extension_view	admin
b975c08f-95bc-495d-a1f1-7aa073dc80cc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_extension_add	superadmin
9c727506-68c5-42b7-af3d-1a25ed722bb7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_extension_add	admin
9466d09c-8c6d-439d-9d74-b580d3f25c41	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_extension_edit	superadmin
da89eeab-fa80-403d-a1f8-294bfb6f6231	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_extension_edit	admin
d262e9fe-7968-4fb2-90e5-15b99e19d089	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_extension_delete	superadmin
82bfac18-5f17-40f2-bc46-83490ebfed43	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_extension_delete	admin
233739bd-67f6-4b28-bda0-c6d4adee9364	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_line_view	superadmin
fa5e9094-0ad0-4d4a-9338-3144e7c53bc7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_line_view	admin
2ef70602-6ed2-4d2d-8aa6-7beaa2d79d98	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_line_add	superadmin
bcecf68f-503b-4cf5-bbe2-0dfa7c7656ac	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_line_add	admin
351d27a4-f733-4d7c-aff3-dd772c111c1b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_line_edit	superadmin
1c1f1d7f-f468-4610-85fb-ef9c0c5c118b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_line_edit	admin
853413ec-4095-4616-9492-e8be243ece9b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_line_delete	superadmin
56309589-cc34-4341-863b-8fac4acd0dc9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_line_delete	admin
aec85aa7-92f0-4c4b-879b-080c48a1aaa0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_setting_view	superadmin
908e61b8-76d5-428a-b732-96ee8489ec61	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_setting_add	superadmin
6aa6bf4e-1092-4482-890b-662b7e4c2cd5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_setting_edit	superadmin
be62901a-9058-4576-8d20-4469822995b0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_setting_delete	superadmin
faed68a1-28cc-484b-b560-cc0754251232	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_key_view	superadmin
cdf7dfb2-0bb8-4909-97e0-720f62db3952	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_key_view	admin
72d5bc58-7969-4964-be3f-5abca6315a59	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_key_add	superadmin
58b09271-5ecf-4946-8e9c-13bf79ecf9a6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_key_add	admin
0d8f2b65-a250-4be3-946c-33b391b3d1a7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_key_edit	superadmin
c637cc3d-2ac7-4eee-8e06-7acb67490672	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_key_edit	admin
1037290f-4526-4138-a378-d22efbc479eb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_key_delete	superadmin
1dc55f35-c112-46c8-8986-754bcf3454ce	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_key_delete	admin
78b5497e-9a2b-4291-a298-2e7488444cba	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	device_domain	superadmin
2ca34e4f-ef5e-4135-b0a5-c9937f08390d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_view	admin
80cd4890-9537-4c13-9214-3c52aa633e3f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_view	superadmin
8ced564f-6b87-4344-9919-55debba6745f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_add	superadmin
50d150e2-ebb7-4841-9116-ff7081d1b127	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_edit	superadmin
b0164f55-d570-4da6-bc11-0d42bb2b3124	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_delete	superadmin
00b3cd45-b33a-4b36-823f-1d9908d74023	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_advanced_view	superadmin
8505ed62-e7c3-4bbc-bba7-30bdfcfb7d50	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_advanced_edit	superadmin
7e23af83-1e5c-430f-83e6-64173589a3c6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_detail_view	superadmin
8c2cd16a-221c-4e58-aff0-836b91d264ba	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_detail_view	admin
e7c34843-04b3-41d1-9c23-f787ed583ee7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_detail_add	superadmin
33c413fc-7a21-452b-b328-ff56949395fb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_detail_edit	superadmin
66973ba3-e4df-4ce2-a834-42ca504cf7a7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_detail_edit	admin
1de59dd0-4f26-4f34-ac62-60181d6c441c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_detail_delete	superadmin
4a9d11f3-c97e-43e0-a385-37a1d2370b7e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_detail_delete	admin
cb196096-8baf-48ae-bed8-1c4cc2180061	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	dialplan_domain	superadmin
f0898a45-a631-4b3e-989d-3e5475a20276	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	inbound_route_view	superadmin
cdf87534-970e-4fe1-91b0-519424e37c8f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	inbound_route_view	admin
f54e10d5-f283-488e-8a72-c38bd34624c3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	inbound_route_add	superadmin
007082f0-507c-4385-bbfb-583b908ba3c3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	inbound_route_add	admin
d62642bd-cc04-4f9c-a89c-8c311db403a6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	inbound_route_advanced	superadmin
459cde12-e608-4409-90b7-d4dd36e38129	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	inbound_route_edit	superadmin
ab4bdffb-3b65-46ee-ae21-d800e4779b77	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	inbound_route_delete	superadmin
ecd1443c-ea1c-4ef3-9549-7ff8fe91b536	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	inbound_route_delete	admin
0a225c35-3b7f-4095-b0d5-7a5079e3dd15	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	inbound_route_copy	superadmin
0ff83c53-d610-4032-89d6-e7acd1750d44	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	outbound_route_view	superadmin
5480a490-1d93-43b4-930b-3c8eef0f0663	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	outbound_route_add	superadmin
3470b8a2-38dd-4e21-9ba6-60a95860209a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	outbound_route_edit	superadmin
0f5abd80-085a-4134-8376-258a94790ab9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	outbound_route_delete	superadmin
9ce061c9-0082-4220-8c2d-f17c5c28aee6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	outbound_route_copy	superadmin
f88efe2d-19f5-423a-96e3-d85d693841ea	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	outbound_route_any_gateway	superadmin
454bbfa4-6697-4a16-bd36-b84acd7c9369	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	script_editor_view	superadmin
b65db88b-1f19-4928-a17f-33c99b8f7520	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	script_editor_save	superadmin
ddf29dc3-f07e-49e9-9975-46cb69e4e6ec	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	provision_editor_view	superadmin
5ba33391-8524-4d0f-8197-df2e8983aa60	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	provision_editor_save	superadmin
bd58baf8-50ae-4eb8-8c7d-211595538da1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_editor_view	superadmin
25b0f581-7385-40a3-8958-559a0a8e2dae	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_editor_save	superadmin
d91edb4f-3a31-49fe-9ec8-375455430049	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	php_editor_view	superadmin
db1bf4ef-14b5-4300-aef0-b3c83bf4cb43	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	php_editor_save	superadmin
4da915a6-7558-4ead-96b7-9f724df75ce7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	grammar_view	admin
574d2649-018c-4384-9ef7-e604d853587f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	grammar_view	superadmin
a4020af2-582c-4a08-8695-1c347287e434	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	grammar_save	admin
df678bbd-6f50-4931-978d-b50dcdfb0c65	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	grammar_save	superadmin
ec951b4d-9861-47f6-b528-5fcd611c1337	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	clip_view	superadmin
59aeefa5-1f3c-4beb-95b9-a1bca8abeb59	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	clip_add	superadmin
978f341d-3a59-495c-9139-ed2f27954c6f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	clip_edit	superadmin
584ab835-bfbd-4e27-a40c-ca9d3193b9d6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	clip_delete	superadmin
252fff47-f0b2-4bb9-875a-37dcbfc99163	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	exec_command_line	superadmin
bf112b8f-25ea-4e68-90e5-7de1becac792	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	exec_php_command	superadmin
202dc8c7-5444-4a7b-8ae9-ec5392cb50c0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	exec_switch	superadmin
c99c272b-b35d-47a9-90e6-4c217bb615ce	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_view	superadmin
d82b9b87-ce64-4b42-9f96-7a21d25b7ebf	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_view	admin
ac8ca6f6-a5ce-48d5-af63-0babfd0b98b2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_add	superadmin
7532181c-637f-4296-b2c2-5a404d8794b0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_add	admin
06ce24b8-2bb3-48d6-8423-475c1129a8e8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_edit	superadmin
5f92ce31-6cf8-4745-b986-e0798efc9ca8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_edit	admin
1d8ee4db-0c39-44bb-9d81-81ceffc70b86	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_delete	superadmin
d29d3318-004a-4d4b-87d9-e8eb23bb77c4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_delete	admin
cc70abcf-3e57-42cc-829c-298248b2f436	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_toll	superadmin
1bcfa5f6-1db2-44ae-8e51-39cbba46505d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_enabled	superadmin
d0f22b54-534a-4180-97f4-2a7e7887b2ba	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_enabled	admin
745b0072-8ca7-4482-a9ce-c98dc13ca2b6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_user_view	superadmin
3082ec40-e2b9-42ee-bc8d-9c671e1a70f3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_user_view	admin
a4be87c9-cc18-48a7-a2a7-00c49b1557e9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_user_add	superadmin
8d1c801f-4e82-48cf-9236-5e88de5d0cb2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_user_add	admin
2aa37dc1-b1e8-4147-85a7-c06e32a1300c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_user_edit	superadmin
892987ad-2ae4-4636-b71f-d62708114602	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_user_edit	admin
73f2a7cb-82ac-4870-b75a-f4da1783caf1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_user_delete	superadmin
d423611c-c90c-4d1f-ac59-9aa5d8674c92	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	extension_user_delete	admin
adecba7d-3737-42f9-bd42-0ceb900d32d6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_extension_view	superadmin
8dd5a1c8-3de7-4089-b846-3b168124f73e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_extension_view	admin
d3199dbd-b9d0-4b3f-b44c-37ead51bd2aa	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_extension_view	user
3240c75e-eb25-4614-856d-f34dcc726e78	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_extension_add	superadmin
e0a67790-8a65-4b66-87c6-ad5581ad67fd	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_extension_add	admin
df72fa78-7eee-4218-9553-aa6dac4fc5fa	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_extension_edit	superadmin
244af8be-2802-4dab-ba4b-48f398d55ce4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_extension_edit	admin
5b747c9e-e6af-4c0b-b93e-a550a2b0f825	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_extension_edit	user
3453e0e6-d8d8-4949-8f67-d1d9be3539f0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_extension_delete	superadmin
b3caa2c5-15a4-40e7-a332-a50aca6d65a9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_extension_delete	admin
65565dd3-b708-420f-8fd6-1c8876f59dbf	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_inbox_view	superadmin
9aeaacbe-f9c0-432a-8151-14e72be71776	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_inbox_view	admin
212b3e4e-dab5-4999-9907-0cbbac5bec6b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_inbox_view	user
4a34c4b9-4893-4b51-a534-f237db69c32c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_inbox_delete	superadmin
7ebb8519-6022-41aa-bac9-fe8cffed79cf	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_inbox_delete	admin
cca34cdc-ab0f-4e41-8a36-b7a02eeb3dcb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_inbox_delete	user
3fd15137-a9c1-46d9-8484-abcc2a65d0e6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_sent_view	superadmin
3070a527-52d4-494c-baa2-addf3b381e2e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_sent_view	admin
79451b7e-f8d4-49c1-8d08-35ddd1c87b08	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_sent_view	user
f2105839-a059-4949-93d9-507e78c69a48	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_sent_delete	superadmin
4dedfae3-375d-4900-8afe-601be72f3c52	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_sent_delete	admin
a6064f61-0d48-402e-a0a5-e795d98bf604	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_sent_delete	user
435c38bf-65ef-47d1-982d-28d89945e964	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_send	superadmin
e75beebe-8d23-4323-975c-9dec21c3383f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_send	admin
e1a20f84-04ed-40ae-b456-f3de30cefd7b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_send	user
ed3a332a-8ba2-43ec-9e40-fb3cfdd0ba42	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_view	superadmin
2394991c-e563-4474-938d-0c87ce475653	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_view	admin
dbf5c1b8-bfe7-4bb4-9fe4-49f8d81b1532	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_view	user
71f21339-51f0-4b92-bac3-2f4e4111111d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_add	superadmin
d73b48cc-eae9-44d0-90c0-c9aadfaec1f9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_add	admin
95049174-9665-472e-b968-b9ee3b44d2f4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_edit	superadmin
60dcf800-8242-43ab-8d68-dc94e14dc110	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_edit	admin
a86acf58-4177-42ed-bc1c-2300f1e0b813	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_delete	superadmin
275661df-07fd-4991-9a0c-6d7c00ce07e4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_delete	admin
e1af8e1a-57bd-4406-b769-a5c73744c8cc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_user_view	superadmin
eba6ac03-ab31-45c5-b15a-ee795071f9c7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_user_view	admin
44b5d437-e9ff-4c68-bc4a-290004d084e2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_user_add	superadmin
e6e3ec90-0b7c-47ad-8115-b37edbb336f8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_user_add	admin
8a79bd31-3a55-44cd-81bf-80fa11f6e467	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_user_edit	superadmin
356f6772-5559-4369-9fdd-e08bf6bbb424	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_user_edit	admin
0202d9d7-946a-4ee2-abfd-701694b09bfd	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_user_delete	superadmin
a8ce93fa-f75f-4dc3-aaad-91aeee34cf5b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_user_delete	admin
07d05341-c90b-4c30-baf7-e0be5686af7f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_log_view	superadmin
50d9b6e6-b37d-4361-b61e-54b146c9d037	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_log_view	admin
230d1bb1-4dc9-4b26-85af-c5c2f54a11cb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_log_add	superadmin
7b4c3a54-f085-4768-9694-94c1162c3f9a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_log_add	admin
79d4a213-8230-4773-9576-a5b811d03b35	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_log_edit	superadmin
7b62aa1b-bbe0-4ce2-a0fd-390825c13e59	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_log_edit	admin
d1edb819-ff4a-4013-be8e-de49f1606bc4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_log_delete	superadmin
8e609cde-238a-4a07-bf1c-4f9216493c55	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fax_log_delete	admin
06b9dea3-8705-48eb-b488-39344ba85fe8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fifo_view	admin
f566f391-bbe9-40a5-9755-bd99ff0835d1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fifo_view	superadmin
cd2e1398-163a-4de2-9e79-7c2dc175f31c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fifo_add	admin
03c15d9c-2212-42d0-b1e6-236f3873ad7d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fifo_add	superadmin
32abdf00-4c9e-47a6-a9af-3ceb6eec131c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fifo_edit	superadmin
3c09967e-e84c-4656-9468-ad3eebfc8ec2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fifo_delete	admin
0d688dc3-ff69-494f-ae30-047a6d0fa391	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	fifo_delete	superadmin
c45c3477-8e91-4f98-86d9-1ab2eee7efb6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	active_queue_view	admin
6f482eaa-d69d-4f49-a2a0-87fe9748c85c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	active_queue_view	superadmin
94a41726-1d83-4cd3-965b-926e6218839f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	active_queue_add	admin
bf00724b-6c3d-4970-83c8-4d733c2ddb22	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	active_queue_add	superadmin
a1d636ab-0f5a-47b6-b4f4-de6973ebc319	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	active_queue_edit	admin
0dbe6057-77dc-4fce-90e9-42671d3bd38f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	active_queue_edit	superadmin
955b4f02-6281-48aa-9e75-c21ced70831f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	active_queue_delete	admin
5b0460bc-11d0-4c87-8b9d-06013e468254	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	active_queue_delete	superadmin
71d7ea14-7a56-4841-9345-b52d37cfe53b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_view	superadmin
5404b365-5047-4678-bc24-2d2401e7613b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_view	admin
bd2919b9-7ee5-4643-bcfb-44046d33681f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_view	user
e6fb4b92-6cb7-4b63-a9c0-faffcd4041fa	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_add	superadmin
eb85036f-17a8-4c5d-9eca-c2319fe2020e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_add	admin
fcdf4439-3f96-48a6-9d8b-f725cac0c28a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_add	user
aae54030-fd98-4780-8d00-0dab6b646810	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_edit	superadmin
a1287d57-256a-4bac-8739-c8eea3a7e433	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_edit	admin
bcebfb26-fa0a-4b2e-b8c1-624bc920399b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_edit	user
18069e61-a797-4e16-9f7e-ebcf366986a3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_delete	superadmin
51e5e0e2-d876-48c6-937e-b50db5165fe4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_delete	admin
70212144-4ce5-4655-8995-fb3caab1746b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_delete	user
271adb77-83f0-4514-9e71-22370a686279	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_view	superadmin
0fea4891-31e4-4777-8dfe-2856da13825b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_view	admin
5b80fcfe-b9cb-42a7-b7d2-17befc8f8c54	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_view	user
575c99e9-5fb7-48b3-aca7-369ea512e266	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_add	superadmin
2d897a4e-60e3-4b4b-8144-07cae68c138d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_add	admin
c5d714f6-bb54-43d5-a362-d7457f660885	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_add	user
76ba7331-ae91-4342-9b23-08627c7bdac2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_edit	superadmin
9c3aab51-3eac-4aa8-8dad-8ec7ee03eaff	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_edit	admin
0f42ff4e-bca2-4953-a5f2-9c36fa0da311	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_edit	user
fd8f1dba-d84a-46bd-a1c4-36f90e69d8ce	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_delete	superadmin
c8f1a73e-32a1-4fe5-8127-dc09d3e129ec	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_delete	admin
f1d8b951-4e1a-4099-ae88-75a50a8f67a1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_destination_delete	user
2de5da78-ad07-428a-9b6b-ce4103b835f2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_prompt	superadmin
dc5bf0fa-179c-4f18-9661-88ae49b72307	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_prompt	admin
444aa28c-34fe-444d-9923-3a350f32aea2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	follow_me_prompt	user
4edd82ea-292b-4bad-ad23-1a9b73ba3d46	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	gateway_view	superadmin
6ea0451f-7ebf-46b8-91af-418efe5e0a0c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	gateway_add	superadmin
c4bba3f8-4469-4f85-ab0a-552b7fe22c20	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	gateway_edit	superadmin
29de4ce5-d5d6-4bab-bd4a-7d84e83e6d48	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	gateway_delete	superadmin
ed55b5e6-07df-4883-be48-d03b04f8e507	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	hot_desk_view	superadmin
88145cce-d553-4938-90f0-080a3669e8e4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	hot_desk_add	superadmin
68503e5c-ae58-4be2-8002-73e1beb27176	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	hot_desk_edit	superadmin
de764280-49eb-4c2d-ae2c-cee78191fd5d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	hot_desk_delete	superadmin
a5790c7c-fde2-4c2e-ac88-1c055c1d2c93	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_view	admin
865e45d1-7499-4651-9d32-956fdb2d1072	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_view	superadmin
09938ad4-b5b7-40a3-b624-e914371d0f08	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_add	admin
a3c8d5cc-1544-4254-b100-37a560d8df8f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_add	superadmin
80f583cb-2880-44c6-9f02-e8e737d391ed	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_edit	admin
d93b6a07-2451-41ee-bba6-4148dbcf8503	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_edit	superadmin
19b19c76-c15d-4ca9-a9c1-023c623f3900	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_delete	admin
f2edfba1-641a-45ab-97eb-e3de81074e01	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_delete	superadmin
f3c6068d-b188-49a1-98e1-19e1914a886f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_option_view	superadmin
5837fa99-30a8-4337-ad01-93312776946e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_option_view	admin
1b30402c-49e8-4c92-b92c-cda62e9f66d0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_option_add	superadmin
87d0df20-0b45-430c-8784-2f26827b274b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_option_add	admin
fc8a3cf5-dd26-4315-a2bf-17f2d2dd3f1a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_option_edit	superadmin
09091f5e-b3b7-463a-bab6-3ee2a68373aa	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_option_edit	admin
b0f1f836-d74f-4e80-bcbc-c21cb071bdfa	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_option_delete	superadmin
bc405de8-a826-4416-abd7-7189c16780b3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ivr_menu_option_delete	admin
ec0db9ac-6c5e-4209-b3a2-464f21ce8bab	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	log_view	superadmin
a7afd553-c86b-442e-99e3-8ae27ba64496	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	log_download	superadmin
845a9e96-8c5d-41d3-8bdd-fa649773562e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	log_path_view	superadmin
b5d302fa-60eb-4fc8-9149-4530501b0bab	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_view	superadmin
29a0e0d8-6da9-4bf0-bb8b-52c06fae9b9e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_view	admin
80d2544d-d106-4a1e-aeea-cfb578fd167c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_add	superadmin
cdf12568-5791-41b8-87b6-0b21c92e4880	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_add	admin
107719d0-3839-446d-b1e1-638e02d3a0af	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_edit	superadmin
5c1af3f3-d597-4245-a6dd-5dbb582240f9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_edit	admin
27cb5f17-5b0c-4dcc-b0db-9455545b9a59	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_delete	superadmin
7e85c0e3-1ea6-4573-98cc-200003469663	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_delete	admin
4e157663-de01-4cea-bc95-f1c14dd6f9ad	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_user_view	superadmin
aa3f2df4-77a7-4cd8-bce2-0acd0ddf410f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_user_view	admin
67b854f0-8924-45af-a0da-75174bfb4258	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_user_add	superadmin
4a438a6a-b8be-49cd-933f-f1e700731a7c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_user_add	admin
43f0bb0c-1ebb-4acd-a1a5-d9638c72e117	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_user_edit	superadmin
0d6c06b9-178c-4d7b-9db3-e29c8b510a42	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_user_edit	admin
f0e7bd11-54b9-4f68-a08d-cc469f9d02b6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_user_delete	superadmin
6565a8cc-0152-4002-840b-3192658e67f3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	meeting_user_delete	admin
30bb4fd5-594f-47da-b095-27b1dd923264	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	module_view	superadmin
094c08a1-c56d-4e97-9bd2-156001f2a1a7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	module_add	superadmin
86f48673-d98d-4f2b-b3b7-b3c5c7beb5b1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	module_edit	superadmin
e040dc51-f285-4451-9422-82fc9145ae5b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	module_delete	superadmin
5f65266d-da57-411e-8f8c-c7502127d694	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	music_on_hold_default_view	superadmin
1da5f259-d0d4-4755-a0c1-d3f270cec846	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	music_on_hold_default_add	superadmin
11ce1210-5de9-4537-8ea3-6daa6984e137	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	music_on_hold_default_delete	superadmin
274e6d5f-ed8e-4019-90c5-52d1165218cb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	music_on_hold_view	superadmin
5d476522-ab11-45e6-af5c-8d2958323c5d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	music_on_hold_view	admin
389e7e1e-e13b-4556-97bb-556fefcb067c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	music_on_hold_add	superadmin
52f01cab-9f40-47a4-b477-02d3b470ed04	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	music_on_hold_add	admin
9e0fd776-7540-4b2e-8634-ad02f52364e0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	music_on_hold_delete	superadmin
a13c57ed-7844-4ef1-97a8-fc2516b8b2e3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	music_on_hold_delete	admin
a69b5f88-8fee-49c3-b978-09d6b3996b81	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_view	admin
b38556b7-8e84-496e-b987-f9f984271f26	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_view	superadmin
fbbf84df-fc4a-4ccf-bf7e-41bd8d8b7038	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_add	admin
50e63a1d-b19c-4012-ab38-fae6a370ccd0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_add	superadmin
85ef88f7-bf91-4caa-91f4-db56aa7bbcd0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_edit	admin
7ddf3aaa-c1d3-4515-aec3-174d94d272cb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_edit	superadmin
4bac0545-b580-4a50-8d09-bb68f9112944	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_delete	admin
ce54e17d-27da-4ad1-a2ff-009330139fa5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_delete	superadmin
140401c6-2acd-4649-b596-75bb2c2ec826	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_upload	admin
b108e89f-a577-4483-bf6e-4110ad479a66	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_upload	superadmin
4a1c3560-e369-4627-85b7-f15553df8fbc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_play	user
c917cc82-978a-479c-a9a4-cef5525d92b1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_play	admin
ca1c9410-41af-41e8-a474-b0a423334ea0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_play	superadmin
09dd05cb-143e-493a-abb7-e0ed8f28d020	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_download	user
799e4ce5-71d1-4c48-981c-46a2fa142679	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_download	admin
2bbef1c9-bc0e-4300-8e75-1ad22aa5d643	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	recording_download	superadmin
44f1a7ad-d4e7-40c8-8547-f7cfb7f5b94c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	registration_domain	admin
1547a76e-bf30-4e93-9e97-2f180de37c52	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	registration_domain	superadmin
f6b24629-deac-44f4-aa37-8c7c3e329731	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	registration_all	superadmin
e8588ad0-a6f4-42fe-b486-61ba908571a0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_view	superadmin
1ba7f5e9-f75c-44bc-973f-f4f92e498385	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_view	admin
9471e7c0-28f8-4289-9124-dbb2302463e7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_add	superadmin
0fbcb701-43d3-4a8b-bd2f-31f041961eda	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_add	admin
8a424959-fa1b-4c5c-a8b3-d3de49d12698	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_edit	superadmin
6d38efac-48ba-4dde-9f11-30baba1cf44f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_edit	admin
22428e65-2048-4582-8313-782d78e16278	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_delete	superadmin
b93ac1c1-7fca-4b24-a154-b9d26f3989fa	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_delete	admin
ea65ddd1-b74e-4345-ae5c-185d1c219623	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_forward	superadmin
0ba522b8-3730-4f06-82c6-8f9d08319d68	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_forward	admin
83f28ae6-cdc0-4259-91cf-c88eaca87b6a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_prompt	superadmin
ddcf0cad-2f1f-43fc-ba5e-f36ddc45f3f2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_prompt	admin
2fcaeb99-fe9c-4d4d-8422-895d310cd897	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_destination_view	superadmin
b1a5b9cc-8f74-402d-b86b-bea0849721e1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_destination_view	admin
6c8f4c9d-5c62-4527-9be5-ec9b02fe606d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_destination_add	superadmin
0b358c23-c388-4ac4-9b58-3e53f2db68db	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_destination_add	admin
6464bddb-6eba-4ccf-8c90-797aad600021	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_destination_edit	superadmin
87597ef1-b601-478e-81cb-57d9487b262b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_destination_edit	admin
24b273b3-d163-477f-b1b1-260156369e85	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_destination_delete	superadmin
e0b58dc7-c653-44d1-bd0f-a6189f771025	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_destination_delete	admin
7a77dfc3-4ae5-4fcb-a644-bd9fe97877f3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_user_view	superadmin
d2435b29-506b-450c-8a80-a9f04ed8f488	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_user_view	admin
c06ea27b-127a-4b8d-bb6e-855d1c01c52a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_user_add	superadmin
21d52081-7d0b-4c06-a358-245a5aa8faf1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_user_add	admin
c8df8e32-630a-44a5-9843-77e6a1be852b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_user_edit	superadmin
d662cd2a-edb4-46f5-b56a-720d6a561763	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_user_edit	admin
826c83f7-2715-4ea2-90e7-5fefb41af0af	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_user_delete	superadmin
0245d7b5-f447-4c8d-9c84-795a2ec12fa0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	ring_group_user_delete	admin
33c30d94-1885-4206-92a2-9ec6b1575444	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_view	superadmin
c3499b8c-6338-4c8b-980b-e5f0f4857a46	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_add	superadmin
41c7377f-05b2-4b63-a136-5f0fcf93339a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_edit	superadmin
b018f370-8e5b-4630-a93d-5a9a9a9e5aa0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_delete	superadmin
a10b2ab1-11f5-414d-a333-bd82b02e5229	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_data_view	superadmin
787d7ab8-6812-43ac-8391-f0ec351c579e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_data_add	superadmin
45ade696-6293-4f66-805d-63efeb3107af	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_data_edit	superadmin
f49b0360-545d-4f64-9761-f6e392c95c7f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_data_delete	superadmin
64f92e92-ea36-4d04-aef8-5c0936daf9ad	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_name_value_view	superadmin
f00b01c2-456f-4f7f-a3b5-83198dcea88c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_name_value_add	superadmin
c6ca61fc-7c26-427a-a464-5b301edccda8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_name_value_edit	superadmin
5e86ca55-e175-4b45-be87-88a773be19da	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_name_value_delete	superadmin
0a570c95-412b-4b72-88bb-344bb0a8d0e2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_field_view	superadmin
3acf18bf-b187-462a-b2eb-ebee5af76300	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_field_add	superadmin
59c1f58f-6f63-40ca-b9e4-1a05453e2da3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_field_edit	superadmin
db21a50d-4603-4682-a550-94ccb124974b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	schema_field_delete	superadmin
2cac1670-b04f-4891-806f-bdec3ff4adc4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	service_view	superadmin
1764a64b-b5d9-4aa8-87ee-bbd90bff0312	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	service_add	superadmin
93fe1cbb-4dcb-4fa6-8170-23692989fe06	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	service_edit	superadmin
68c7511e-e1a4-4480-b34a-84174f78d5e8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	service_delete	superadmin
466bfb24-7f48-477b-85e8-04c8b75d8d71	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	setting_view	superadmin
a2fb63f3-753b-44ab-90da-a61f4cebb4c1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	setting_edit	superadmin
1ffe810c-4496-47c5-8963-a0b00e759b25	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_view	superadmin
c04fde6b-cd11-45ed-8e90-ec1927e3ba8c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_add	superadmin
18b534ee-c360-4416-8f29-2d42da5ff351	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_edit	superadmin
6be5e2a8-c65d-4854-9088-19b48267b15c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_delete	superadmin
6e019b6f-3faa-4711-bfb5-54847422a0d0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_setting_view	superadmin
f549b0d2-7fe5-4a0f-8955-c9cef38f5c32	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_setting_add	superadmin
7ef1e320-c35a-4589-8a79-281c14722547	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_setting_edit	superadmin
956590cf-0fbc-4374-84a2-ac974081b284	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_setting_delete	superadmin
faa7f829-e5d1-4882-aa29-88bcbd3cb5a8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_setting_view	superadmin
a5d59bd0-7852-4d0b-aff7-76e6b2f65a57	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_setting_add	superadmin
1f751827-968d-4698-8482-b25e48f14caf	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_setting_edit	superadmin
2866ee4a-3ff8-4625-a986-7cd5af888897	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_profile_setting_delete	superadmin
efe556e6-84e1-4b14-8e6f-20ff2d9c77b7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	system_status_sofia_status	superadmin
2a0d9adb-5227-49d3-af26-a9af102d48ec	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	system_status_sofia_status_profile	superadmin
0bcc6ffc-82b6-497d-bf08-2bb42912a7d9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sip_status_switch_status	superadmin
85611566-8d03-45a0-8f46-ac0134a80bc3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sql_query_execute	superadmin
2f2959fe-5cc0-46cc-89ea-9d59a1f141b9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	sql_query_backup	superadmin
19b5d24a-acdf-463e-b1ce-5e65d07add50	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	system_view_info	superadmin
24fa8afb-22ee-4372-a079-851978a52628	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	system_view_cpu	superadmin
d3087941-6967-49c2-9683-a030a3b9b446	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	system_view_hdd	superadmin
198f09f0-e40d-4dd4-9647-3d166cae4462	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	system_view_ram	superadmin
88baec91-4c1d-475c-9c4b-6ad80690f817	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	system_view_memcache	superadmin
2e004d58-f762-4bbd-8059-b8b491291f80	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	system_view_backup	superadmin
c0167821-0906-419f-82bd-6ceb91f00713	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	software_view	superadmin
dcea24f0-42c2-46ae-a40c-df039f9829ed	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	time_condition_view	admin
83a47747-bf5b-461d-8f28-cf4ccd8f94cb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	time_condition_view	superadmin
63691775-6fe0-463b-909a-f2bb0c8d2af3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	time_condition_add	admin
d79fd78a-9921-495d-ae86-44ed79c66b23	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	time_condition_add	superadmin
76d304bc-57ef-4978-90b0-bba7bc3b9620	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	time_condition_edit	admin
431ed697-b46e-4f31-a5e7-f61b81f775f6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	time_condition_edit	superadmin
e8b86164-446e-47ef-886e-037b8c75c42f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	time_condition_delete	admin
3faea77c-e553-434a-927c-11befc6d5fa5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	time_condition_delete	superadmin
c5d566bc-b367-45e1-bce7-ef7778dddcfb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	traffic_graph_view	superadmin
18a6985e-89d4-405b-b1e1-e2da8d45dd60	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	var_view	superadmin
2cb2adc1-e91b-4751-bd82-4a1f2736029b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	var_add	superadmin
263a41e0-8ff2-4e01-8a59-eb0d1107f4d8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	var_edit	superadmin
725080c0-20c6-42f8-9d96-bcbda3711d55	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	var_delete	superadmin
a70ad06b-9197-41fb-953e-c5ee3fc0a8e0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_view	user
26c59060-e046-46bf-a913-ed5a2aa9b180	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_view	admin
44ad0da0-b18e-46b4-96fc-fe2ff2e9fc35	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_view	superadmin
9a8a1d91-16c3-4adb-a98e-27b0d6c6c863	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_add	user
f8dedc70-09c3-4777-88c6-5eb4870623cd	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_add	admin
963e8531-7985-48c2-b85d-685d1ca90bd3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_add	superadmin
56d03483-5d7a-4114-b7d4-ae2d54c21cce	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_edit	user
4053b4ff-66b6-4253-800d-81b217459a66	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_edit	admin
8d67718c-a6a1-47e7-b41e-ddd3470b7e6f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_edit	superadmin
300bd49f-32ab-4838-b6b3-248c3172cd72	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_delete	user
52f02dd1-2430-457c-a64d-70ad52d96b0a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_delete	admin
95bbcba6-00ae-4698-8733-334ee3db57a9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_delete	superadmin
99a30cca-f7e8-4673-a932-8afcd2b5285d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_upload	user
f0e6ff1d-224d-4a3b-8237-9c25edd23fff	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_upload	admin
9954b311-b73e-4e59-a823-b28eb6c195c9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_upload	superadmin
aa304bb5-ce21-4a7f-973a-98983ad8261c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_play	user
6e6d0689-d4b1-4ed7-85c1-2484f20bbf25	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_play	admin
0d8e86b8-e6c3-4bb9-9315-fbbc9476c1a0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_play	superadmin
60660a51-7321-42e7-8764-3ed449f829c3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_download	user
698fd624-0c2f-49ee-9a26-ae51b0efa8d5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_download	admin
0073400d-1071-47bd-8ba5-3c238fa6faed	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_greeting_download	superadmin
95315d05-ba40-4d38-a667-9f21a6ff67d0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_view	superadmin
76136926-c0af-4e61-b037-d845c476bcf0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_view	admin
99c1cd46-6dee-40b5-8343-677f58c015c1	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_add	superadmin
3eacf573-9db8-46a5-91bb-c2b15ddd9b45	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_add	admin
024043d8-eb6f-4f28-8337-5c0cf44b64dc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_edit	superadmin
74d646f6-e596-4612-b2a2-14c15c2bf9dd	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_edit	admin
fc9c6e7d-6a10-4734-9e5d-662e6cd991b8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_edit	user
c6b3b2f3-58d8-47fc-87e6-cbb523374510	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_delete	superadmin
e2a9e82f-2b0a-4ad4-b1ef-e55157d2295e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_delete	admin
a69dda07-e79b-4d53-8306-4674f08a1fe2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_message_view	superadmin
5b3375b0-6e4c-4c21-9ccc-568eb53599d0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_message_view	admin
f9a8c762-e00e-4d83-aec4-382d09c29fef	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_message_view	user
7813c4bf-1e9b-4441-bc39-dff7f0554f5e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_message_add	superadmin
4c2dad0a-091f-4067-8f3d-3bc777b56b49	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_message_add	admin
cbcc69ba-414b-43e8-8adc-1d005bfccb6c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_message_add	user
54548bcf-6f52-4ad4-824c-70eac37c0463	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_message_edit	superadmin
0105990b-3f09-40f1-ac64-655b339a2d70	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_message_edit	admin
84ad5131-e86d-4ed8-a456-e9655b506a87	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_message_delete	superadmin
3869d549-8052-446a-b78a-271fa3688565	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_message_delete	admin
6a682e0d-7141-4942-88df-28480dfcd51c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	voicemail_message_delete	user
d58f6d12-0ce4-4b8b-9236-ddbc50f44258	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_view	user
af7332f3-80e8-4387-87a8-f358e617ebcd	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_view	admin
9c441e7b-6178-4d38-8e4e-73840bbcf062	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_view	superadmin
5d8375b3-a883-4b13-862f-92914b1865b6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_search	user
255d013f-44c6-47f0-b856-06b81f2f7af8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_search	admin
162ea6f8-8500-4209-9224-db90c7f34b50	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_search	superadmin
eaa71745-0f76-482c-92ad-19789dcb2512	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_search_advanced	admin
70b34887-1368-429b-bc0c-e2b28243e92d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_search_advanced	superadmin
21d47b3b-3534-44d9-92b1-e56db8c4e10a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_domain	admin
c852ee5f-9bff-44a4-bc37-f6db63096512	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_domain	superadmin
0a27acda-62f7-4f2e-a84c-ca2dae3b453f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_add	superadmin
fec10956-93c8-432c-9f77-b09d2eebb33f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_edit	superadmin
e804af28-aaad-45ad-9685-2a2f7c576dc4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_delete	superadmin
8aee9119-fe72-4028-928c-7edf685e4d2e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_pdd	admin
987bf599-d0cf-4ff2-868d-b7caa7573bb9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_pdd	superadmin
d80f3b70-2cd6-41b4-8684-a25f39371591	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_mos	admin
c3e8e862-407c-498b-9182-2da331488bd4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xml_cdr_mos	superadmin
0ace333f-fc78-47aa-8f04-3812cac091e9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xmpp_view	superadmin
371976fb-0ace-4c65-ac45-dcf80c6d5236	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xmpp_add	superadmin
9757cb65-da0e-421a-a73c-b494d6a77f1d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xmpp_edit	superadmin
fa687a56-46e1-455e-bbf6-64a023bd1bec	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	xmpp_delete	superadmin
3b069193-8058-464b-9589-b023c2f81b7d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	app_view	superadmin
74ceb06f-019a-450c-9933-b1f63041f176	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	app_add	superadmin
c109998a-3e9c-40d4-8ddc-ef7d324c7886	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	app_edit	superadmin
4f4e3ec5-6b96-42df-b445-1fc1a201a5a4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	app_delete	superadmin
06079bd9-96d0-42fc-b385-3f677151f8a5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	database_view	superadmin
9eb93ea4-9a8a-483d-9177-ea56b64cd6f6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	database_add	superadmin
2dd7e17b-ba72-4754-a7ff-c0607cbbef6d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	database_edit	superadmin
d51b0bd8-12a1-426f-9896-098489d6f8cb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	database_delete	superadmin
e23db233-ed7b-47fd-8bf2-8c7f8320d899	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	default_setting_view	superadmin
fbd1891b-21a2-4c1c-8f23-ada5acbcf7d4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	default_setting_add	superadmin
2431be77-88f7-4c65-b395-a5102b26210f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	default_setting_edit	superadmin
69585f70-392c-41f7-a76c-8ed52a5826e9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	default_setting_delete	superadmin
82e27142-cb09-445f-a40b-4ec4d9c24cd3	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	domain_view	superadmin
43b7ba1a-ffbc-4c71-b321-158bd9bc47c7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	domain_add	superadmin
a0e71bbe-59d0-49c0-9f92-770c840facb9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	domain_edit	superadmin
3ef1f059-a599-42b8-a147-2177e351770a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	domain_delete	superadmin
4ef0ce4c-7e94-455f-badb-a8a0b736bbce	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	domain_select	superadmin
3a57eabd-1c84-4d96-a869-2415a946efa7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	domain_setting_view	superadmin
59504cc7-3061-4ee1-8ba0-6a8575c5f762	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	domain_setting_add	superadmin
4b46d858-b16f-4b63-baba-f909357ade7a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	domain_setting_edit	superadmin
b79d4cb5-fc8c-4058-8f45-7a5465ed94b9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	domain_setting_delete	superadmin
8b242041-e509-414f-945c-fe011ad5be70	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_view	superadmin
e78cf731-7803-46a8-bf10-31c2f2a91c9a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_add	superadmin
f791cf00-ad4e-4688-921d-1986f5234080	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_edit	superadmin
89fecd52-50f8-4557-9378-629e3c9781c4	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_delete	superadmin
e415b5af-4fbb-4585-b4c8-6c6b525dde28	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_restore	superadmin
76541e6a-ce7e-481b-af13-64e7728e8efe	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_item_view	superadmin
214c06b0-5a31-4f56-933f-c6e8e95cc411	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_item_add	superadmin
cc311d04-31f5-4694-a7a3-7337f3d65fbc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_item_edit	superadmin
07efa2e5-59cb-41e2-8779-74f2c5aac51a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_item_delete	superadmin
1733921f-1bb6-4d89-9410-a8b9b8a2e88d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_item_group_view	superadmin
e5f2917a-569b-4871-8880-9409934a20db	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_item_group_add	superadmin
b00063d1-97d1-446a-af1f-5dd930ea33f7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_item_group_edit	superadmin
fe544d37-98eb-4cf4-a7b1-9853ee49c76c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_item_group_delete	superadmin
f6c5b429-400a-42c7-bc67-59c082a7eb47	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_language_view	superadmin
cdf81b05-d334-4349-ad0d-a985e9fd2fdc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_language_add	superadmin
2a3cc744-8a8f-4be8-a483-910d81e1c228	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_language_edit	superadmin
e9947270-52dc-4bba-9d8e-0a3f1fd224b2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	menu_language_delete	superadmin
87b0557d-801d-4a0e-b04a-fe96fadd7ef9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	upgrade_svn	superadmin
4eab5476-92ae-425c-bc26-f5747d7bb2e5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	upgrade_schema	superadmin
b9eb3959-107b-4ffb-a32d-5df31a54f7cc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	upgrade_apps	superadmin
a4f03ef3-512d-4375-9413-fd119543b9fd	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_account_setting_view	user
6b57821d-96e2-4784-8025-ce2e1204f16f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_account_setting_view	admin
9373ed96-558c-40b0-9c1e-f8906ab8daf9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_account_setting_view	superadmin
e7f9da20-b909-48ed-a08c-be4b52cf6ee2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_account_setting_edit	user
ff090c90-4517-437f-839f-263d3178b995	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_account_setting_edit	admin
b503f7b7-84c9-4127-a154-9d41e8eacdf9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_account_setting_edit	superadmin
8cd0115a-3256-4244-a305-9f27f565d0b0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_view	admin
89f2241c-39bf-42b5-b453-1dd37fad408c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_view	superadmin
af64b72d-792b-4e38-b503-2c57f4067341	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_add	admin
8b322ebd-72a2-4b66-a23e-50ee2cde689b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_add	superadmin
0fcd1bed-82a8-445e-b9ff-a3c5cc76efdf	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_edit	admin
a029c4b5-c741-4d38-be35-6064192382b5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_edit	superadmin
348b870e-8f32-4ca0-8043-3a6fea5135fb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_delete	admin
4a73169a-849c-477a-9e70-c3b0f5fc781c	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_delete	superadmin
366d51d1-f78c-4696-9f0b-ac7673a7513a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_view	admin
61bf38da-1bd7-4133-a944-e0cef6d50c89	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_view	superadmin
3235634d-447d-413f-b2cb-a56a4ed375c5	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_add	admin
24128f32-e889-4b30-8f81-c586158ea912	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_add	superadmin
06455c6c-7162-47bc-a4a8-f125bd9b6789	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_edit	admin
db55704e-af2c-488e-9f8c-81dc653b9068	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_edit	superadmin
8f9774c2-c3da-4a2d-8580-cf78d351ca36	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_delete	admin
f2c05b77-5244-40d1-b174-3ae100fff5cb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_delete	superadmin
b8581b9e-1ca7-4d92-b2be-add3eaa7e8d9	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_member_view	admin
aaffe1eb-e232-443a-91e2-84c292ccdc7f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_member_view	superadmin
9de6b834-0142-495c-82a8-239ed7bc1bbf	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_member_add	admin
619a79bf-5a33-4c2e-9f49-7e8d8c2718dc	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_member_add	superadmin
c9d99a77-2141-463a-9779-da1d398eb84e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_member_delete	admin
7ed54c23-8397-4367-bfd1-41a6b7ff393f	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_member_delete	superadmin
71f7c7a1-e4df-4320-9d48-9ba3406f610a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_permissions	superadmin
ff914174-43ad-4655-b813-c04e82053371	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_user_view	superadmin
4fd8fdcf-c0c6-40f0-bf10-247058d28961	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_user_view	admin
00baf8a7-8402-4646-8801-84408ac37a48	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_user_add	superadmin
56c619d7-a7c2-4e37-874c-1ee4f4413c74	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_user_add	admin
f9f6c933-06ef-4343-ab0b-6352ad6c143a	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_user_edit	superadmin
eb843ec4-3b82-4f92-b7b2-76b373979056	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_user_edit	admin
9711a11d-8150-4254-830f-4fea6599f30d	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_user_delete	superadmin
cbb32d79-3bdc-42ae-b260-55b826be090e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_user_delete	admin
3c33ab3f-442e-4c66-a214-a9275439a51e	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_permission_view	superadmin
c7e55620-2371-4986-aaad-6a3b78a603c7	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_permission_add	superadmin
641b33d1-c081-4be1-a8a2-3c3bb8965bca	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_permission_edit	superadmin
4414228d-0da2-4c05-b794-247f6b66b3d0	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	group_permission_delete	superadmin
b73ca92d-0271-4971-a709-913de46f94b6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_view	superadmin
92139d3e-0f4e-4f8c-9a6e-73139cfde2f8	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_view	admin
748685c9-5c87-4026-b3f9-0bbd055c0753	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_view	user
4a0fbccc-9513-4936-b8da-a68e8afde121	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_add	superadmin
9c33b68a-5511-418b-998a-1128daf96623	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_add	admin
7cb9a2f6-bd64-486a-a330-fa2a1e96b6ea	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_add	user
e72e3a71-5501-4b24-aa43-237b61087bbb	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_edit	superadmin
293305ad-6a6d-4794-bde5-ea095cb70225	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_edit	admin
fb6764d6-69a3-40ee-828a-3976dde18ea6	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_edit	user
59a6349a-a3e0-4e61-8b8a-5d8c6578a230	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_delete	superadmin
ed8e9b3c-51a1-44ff-8d8a-1f2db213a93b	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_delete	admin
c89bdcfd-7d84-4d63-bed3-c97704b36697	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user_setting_delete	user
7eaeda9f-7a0c-4228-a601-ebc3fe055e60	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	adminer	superadmin
c2b44310-4b5c-4efd-90a6-b3a5c8b233eb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	backup_download	superadmin
e3735ad8-f56c-4265-aed0-e4795ebea074	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	backup_upload	superadmin
87a3d33b-cdaa-4ac7-b40e-87f999751124	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_view	superadmin
465c0a4a-8d04-40c4-9fde-c6d8dfaedcf9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_view	admin
c4f437d9-969f-472b-8bbd-6798a6a9e771	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_view	user
2aee1387-e9ed-475c-b3fe-9833c698b2ad	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_add	superadmin
bc0d426f-aacc-40bc-8158-261798f08674	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_add	admin
3c22db0f-0b0d-49ee-8d81-f7808d2a2125	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_add	user
d41c27d9-b024-4694-9ea5-d041e7588704	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_edit	superadmin
e3664c82-f5cb-4c1a-ac1b-7d9bc1e76ebf	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_edit	admin
d52ce41a-915f-4063-8119-b9e6a50d838d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_edit	user
6b570770-1d78-44b4-9723-72e444dcc4d9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_delete	superadmin
72f026c1-d6b1-427d-8ff2-c01a5a3ed5f5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_delete	admin
ee622dea-38c2-44d7-9a34-16f177a00e03	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_block_delete	user
3494c190-50ed-4f57-938d-a2e293061e61	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_broadcast_view	admin
98c09e87-673b-4def-ba74-7523795a90a0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_broadcast_view	superadmin
c9d15a1f-1a6b-4f93-98c8-30194399db71	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_broadcast_add	admin
0eabc014-12a8-496b-8f7b-d278368a45ed	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_broadcast_add	superadmin
29d47e15-b31c-49ea-b216-cb54d11c8181	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_broadcast_edit	admin
67eb734f-1693-4533-8717-94ab6fff6aa9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_broadcast_edit	superadmin
635df4fb-32b6-430d-8548-d52ce4966557	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_broadcast_delete	admin
d49a0ab3-e15b-4c5a-8594-7f1806b7ab83	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_broadcast_delete	superadmin
e61e1893-bfde-4600-8945-805fc34b2435	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_broadcast_send	admin
967a7218-11c2-4556-8dd3-cc82ed1df796	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_broadcast_send	superadmin
6e55bdce-f516-4ef6-9b60-0c963fc71f2a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_queue_view	agent
9ae74713-83ed-462f-96ce-d6ac35dbbeef	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_queue_view	admin
a30d1ccb-9aff-4898-ac21-744b23c03470	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_queue_view	superadmin
6da2cb2f-89c9-43b0-9484-bf41c5cb1e58	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_queue_add	admin
6b8fa763-5f07-4c4b-b4ec-6e2bbf7dcf97	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_queue_add	superadmin
d6b00dfc-cb4c-4d49-8f4f-03bc1cc23be7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_queue_edit	admin
b3a598bc-581b-4897-8ee3-14024fac715c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_queue_edit	superadmin
7afbe63c-1670-4fcc-acf6-8eed47ff7696	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_queue_delete	admin
657835e7-fb0b-4e09-8dc0-a81356f575ca	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_queue_delete	superadmin
c521b80f-5a05-4e14-ad6e-4190292af733	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_agent_view	agent
951b1f4e-1421-4ea1-9cc4-9e1302b4bf4f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_agent_view	admin
86838ea4-ff4d-4862-921e-8716daa41d4d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_agent_view	superadmin
90adffa4-5060-403f-9dc2-69131fb223bb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_agent_add	admin
acbd65f2-50e9-4d89-8f14-7b517b732402	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_agent_add	superadmin
a3dc1aa3-5cbb-45f2-8f11-bb4f5453e778	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_agent_edit	admin
50e83529-8b80-4740-987b-f64611ee6990	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_agent_edit	superadmin
2cee1ec0-9d47-4800-9f51-2a94b1986cdb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_agent_delete	admin
f2196e16-4a3c-428d-9013-9c94927f5911	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_agent_delete	superadmin
62465b52-6afc-4961-88a3-6ab5da3c1f69	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_tier_view	agent
b0c9a362-cae6-4746-b6c5-ede4612bf809	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_tier_view	admin
825b37e2-7c2d-4f04-8fee-ab12162b9350	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_tier_view	superadmin
3e0c6575-feff-46ec-957b-77ff6005a8c4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_tier_add	admin
e8d9a058-86d5-4764-8032-2b02dc3a2a2e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_tier_add	superadmin
e4bbda2f-f529-4ab7-b481-a2bfae03b539	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_tier_edit	admin
78b855fb-fb22-4bfd-b16d-cd2f4ba5ca16	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_tier_edit	superadmin
256e6ea9-01df-4b77-8c22-88ea62c576f9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_tier_delete	admin
30cebfd3-9265-4e30-a423-6bf9a41ba704	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_tier_delete	superadmin
64c195cf-404e-4c01-a6d0-49bfa6fe6830	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_log_view	superadmin
f7f6f19c-a3a8-4008-ae51-187126f0a374	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_log_view	admin
f76f14cc-205b-44ee-8a93-84606f9e52d2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_log_add	superadmin
5f495aac-15ad-49a4-9234-7efb9fde6007	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_log_add	admin
540b908f-6b26-4bf2-a265-f79de100ce98	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_log_edit	superadmin
bd1b2f16-2c8b-4e06-8c03-d0f5adc98ff7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_log_edit	admin
4d509ca2-a982-4110-8dc4-b258b8abcb5a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_log_delete	superadmin
efcbd7f0-96e0-4e9a-ac0e-8eda2d348fcd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_log_delete	admin
141c0df0-6c98-4f5b-b151-fceb3480dd34	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_active_view	admin
57d488fc-421b-4b55-a9b0-5691e3531b96	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_active_view	superadmin
ea0e0faf-021c-4926-a368-58f4c3b4d877	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_active_options	admin
20326611-83b9-4c23-b927-607fd64c8a40	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_center_active_options	superadmin
14724bc3-0a5d-4ba7-865a-26ec72f66103	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_flow_view	superadmin
9b26aa20-e731-47e9-b4d9-c50f17eae31a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_flow_view	admin
7f6a2b1d-7830-4a14-981e-e01d577b9eed	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_flow_add	superadmin
3ff6e02e-a2c1-4e4c-b953-334c3d3ec300	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_flow_add	admin
a1ec33e9-8dd2-4946-9136-78182d271a6e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_flow_edit	superadmin
c25e8410-13e1-4f15-bd35-e2331a4baa4e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_flow_edit	admin
83626f71-edf7-474d-90c9-c084dc61ddc6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_flow_delete	superadmin
84fff814-a047-4caa-a648-94e9090648e5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_flow_delete	admin
4d817953-8e0b-41ae-883d-bf503ce57aff	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me	user
a5ab2aa9-195e-4fc5-97ec-f131642c4df6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me	admin
54a27f3c-1214-4fe5-8544-94140a3b8822	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me	superadmin
e53a8038-b90b-47c7-9f13-a169860625dd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_forward	user
4432cc6a-5c03-422e-8f9c-0e0a9e70e3e1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_forward	admin
3cd8fbdf-47b7-4c3a-bb05-cc8b7445ffff	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_forward	superadmin
049a0ce0-d9f5-4080-ae1e-00979306b46f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	do_not_disturb	user
e30fe92b-e523-456d-ac33-1a10adc7a0c4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	do_not_disturb	admin
8a276327-8e6d-4bf8-a8b1-dde62e3d315d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	do_not_disturb	superadmin
6b447205-014a-4ebc-ae33-2211a3c2731e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_active_view	superadmin
00c12367-77f2-4675-9aa5-95dfc09fa3c3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_active_transfer	superadmin
73de3aa3-44cb-4c8b-bfe5-8ca607e7ab67	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_active_hangup	superadmin
35eb4a3b-d249-4bcc-a2ef-7ef5264c0bc2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_active_park	superadmin
b8cfc6b0-e554-4b62-8ac1-4b3c37dc0f2f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	call_active_rec	superadmin
554d0449-86be-4975-9d5e-9a1e1715b11f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_view	admin
010e7427-933e-4acd-98f3-e8f3f39272ac	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_view	superadmin
0d6d780e-4e92-4ec9-a0ea-9d36fdf021b8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_transfer	admin
e139a152-9266-4310-9af9-cb04cad98abb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_transfer	superadmin
aed81704-26c9-451e-aa9e-dd34076d2c2b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_hangup	admin
ad574b4b-4757-4b02-852d-71453cf7dbc5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_hangup	superadmin
2caa12a1-06c0-49cd-8dc0-2454a9731f47	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_park	admin
fe137b1a-372c-4230-97cf-bd57c2fb2fef	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_park	superadmin
8c0b3b97-6a34-4fc9-bf72-a23430af5946	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_rec	admin
85aeb95a-f3aa-42ba-ae3d-0c8c8dcc72af	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_rec	superadmin
9c5dabc5-50ca-400b-b9f8-b447c967e856	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_list_view	admin
67058767-18e1-4f9e-a64c-746fa3a3a4f3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_list_view	superadmin
34b62cfd-75fd-48b8-85ba-3c390cbae8ef	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_assigned_view	admin
e84aa72f-5a24-44dc-8c6e-506eb70ceae4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_active_assigned_view	superadmin
0ee133fe-ab75-4775-ab9e-42e750fca05c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	click_to_call_view	user
d51e8d35-828f-406d-a3d2-0e46e09c9ddd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	click_to_call_view	admin
16372e8c-9b08-4ae6-8f08-845d48f939a8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	click_to_call_view	superadmin
bbab4d14-f735-4cf1-afc7-184391342335	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	click_to_call_call	user
2f560e4b-257f-4e10-8f30-6f70f1c95819	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	click_to_call_call	admin
f534408a-83fe-446d-8bed-b34b8ad3ded3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	click_to_call_call	superadmin
2ba74efa-ed69-415c-963c-cddbaaba2554	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_center_view	superadmin
57842c4c-c47d-49f0-a32b-8bedd62a500e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_center_view	admin
87d3f4fa-2028-4a05-bb59-395c940603f3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_center_add	superadmin
22d97b32-b8c6-4698-9557-49d32058120f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_center_add	admin
f95d018d-a694-4095-8a01-2511951d40b7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_center_edit	superadmin
9485ca01-02fe-424a-b5a0-c1db9ba7fa75	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_center_edit	admin
12dee4ef-211a-416c-99c5-416d425798ba	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_center_delete	superadmin
1a4d63ed-32f6-4940-a7cb-514c0fd82636	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_center_delete	admin
5d6cc8c9-0f51-4712-a35d-4b3db952217d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_view	superadmin
72bcfb48-ed98-4fe6-b94b-b9832531f408	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_view	admin
d779ff7d-550c-4afc-95fe-09e3aac42287	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_view	user
58c00693-8bcf-4195-9545-c44e1bc9f2c1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_add	superadmin
123793d4-fc00-410c-9e21-3a3dcbe8a140	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_add	admin
29959897-9a31-436d-b0c7-59adc2fefee1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_add	user
3b2aabdd-ff1a-4d08-be87-93e1c00306fe	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_edit	superadmin
aaa741db-62f0-4a84-88ee-dc46f5ba8a07	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_edit	admin
d3ee0bc9-916f-4198-8ec9-82ab4661a808	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_edit	user
14181b4d-f731-4dcd-bff3-54fe3b11087d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_delete	superadmin
e5d9500b-aa07-47d4-8359-e81e0a2ebf23	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_delete	admin
1ac62556-b57a-434b-b6c4-71f5e5c1b6fd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_delete	user
730e0e4a-7c50-4d51-a9fb-86e2ef9462d5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_view	superadmin
9ff6b34b-2594-47f5-935e-4898f337413b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_view	admin
b5876bb3-85d6-46b1-bea8-ab32fd7433f4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_view	user
dc4b98ea-e470-40eb-9a34-2e913aa3bdc7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_play	superadmin
cb7c3613-5fee-4755-94cc-ded3a837530d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_play	admin
0a8930b2-6fdf-4dc5-80d0-44120d89cc16	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_play	user
30115681-55cb-4cb7-ae67-52c7e1df81d7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_details	superadmin
2596eefd-4a5f-4918-9195-b295114e85ff	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_details	admin
795b89b2-6de5-40b1-bfdd-a3f8b6a74c87	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_profile	superadmin
69e1be47-8b39-4d5e-8690-90160f3dd9a3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_profile	admin
e5d2a497-6b38-4d8b-9d9c-3d5b2c1d9898	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_profile	user
d3234959-9333-44e0-b958-db357c574a18	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_record	superadmin
2e4353ee-c346-40c9-8e4d-986fd154a05b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_record	admin
8a5e6b16-2f4f-448a-9190-c86bdacaaddf	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_record	user
b9a688e3-9ff5-48fc-924c-783662b938b4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_max_members	superadmin
0cb489c1-e0a1-468e-9ab5-77aeb5f5a86f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_max_members	admin
9d355e26-b9e8-458c-8851-726f579aeb4d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_max_members	user
c18b7187-d885-4665-b383-54926769b4cf	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_wait_mod	superadmin
25913b88-66f5-4a25-a14c-11831ac72cd3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_wait_mod	admin
3df7aa94-74ee-4348-974f-39c23706d16a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_wait_mod	user
5144b921-0ee4-443e-b727-55b365284dcd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_announce	superadmin
14278f2c-402a-47c9-b1f6-eac0bb71f802	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_announce	admin
61b26dd1-8972-4b9d-87f4-46104b4f9cbc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_announce	user
59d2fdfd-20b5-456d-bd98-4ec80f125b9e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_mute	superadmin
f0dc2bbf-c226-49c7-b343-a910dad81ed9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_mute	admin
a7bc04f1-8e17-4600-858c-ab84c156e393	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_mute	user
4b16cbf4-3894-49f6-aaa3-11a47c146c28	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_sounds	superadmin
b417eafa-dd83-4c79-8dec-9022c124eaa8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_sounds	admin
716ed81a-1f3f-44cf-9ff2-1978df56ef6f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_sounds	user
6155fedd-73f1-4c6e-87ce-077810e5f950	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_enabled	superadmin
0cd695ad-7f01-4665-a085-367c1f2ecae7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_enabled	admin
d994f72a-c81c-437f-b06f-d7c9cfbfff97	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_room_enabled	user
4f6b093c-7fbb-490e-be01-32f370d94136	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_add	superadmin
a98b28d5-8515-460d-9012-dc8659026601	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_add	admin
b536799f-4bef-426b-b613-9bc17108afb0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_edit	superadmin
ceac5270-ac31-4451-8dc6-e4944d79aadf	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_edit	admin
0bd40c98-de99-4752-9583-4dc0628467e1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_delete	superadmin
fc717a11-a003-488a-9c5a-015236738906	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_delete	admin
55368b49-f0cb-4208-b374-6dad3c2bd38f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_detail_view	superadmin
bbaa8cb5-962f-49a3-8601-faabc72fa3e8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_detail_view	admin
10a58987-db5d-4d17-8e41-fc6a660c692d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_detail_add	superadmin
6b63b5ea-4298-4cdd-940b-3dc97824cfac	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_detail_add	admin
59ce2eb7-9c71-462a-b14f-2a55ccb74d80	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_detail_edit	superadmin
b7fc5c06-d064-49b7-b096-85a15747ae49	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_detail_edit	admin
5f199957-4e31-442e-ba1b-95dbf30051de	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_detail_delete	superadmin
dd7a382d-54e2-40c2-9422-f9e1fc294754	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_session_detail_delete	admin
7aabbcc3-a7f8-4074-85f2-da77b4f28086	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_view	superadmin
86469fd4-f248-4104-9196-9d75e4d332d7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_view	user
00b2fb5d-c5c0-4f93-ab26-51e865284700	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_view	admin
ef766080-df61-4404-90cc-a406faeda212	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_add	superadmin
120b48c5-aa87-4839-bde1-8460004c1847	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_add	admin
3e861ba1-ae97-4fe5-b278-3e97894a61d5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_edit	superadmin
e30c44be-8845-4e8b-bb4b-4ad38b4aa67e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_edit	admin
eda4330f-3e11-499f-91e9-7367f39c7957	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_edit	user
6356ce98-bd9b-412b-a6c3-43919cf78194	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_delete	superadmin
102eb66a-352f-46a5-b775-0b8ab69a0fcb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_delete	admin
d5764705-1979-4d39-815f-29f44c472be5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_delete	superadmin
bdd72aab-f61b-4d07-9a12-5f99794bf5a9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_delete	admin
73368b21-5bf9-4533-bb12-b298437f269b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_user_view	superadmin
32b61b3e-1690-4d49-b82e-bc454d6aa8c9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_user_view	admin
1ec1548f-fc89-449b-895e-dcf7401a0654	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_user_add	superadmin
d539d94a-458f-4dd7-8089-fdea45013f1a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_user_add	admin
dfb5b8e0-a0b3-4e69-8669-0f2eb4c3b7d2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_user_edit	superadmin
ef0d9211-3fa8-4961-b23b-c5c6b61a8359	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_user_edit	admin
38f19e23-c4c3-413b-a546-f6c73ab6d953	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_user_delete	superadmin
ec07b92e-d253-4231-ab2f-c917e4ba8be5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_user_delete	admin
3a0b43c5-745d-4fec-9c58-84b9f112c5ef	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_view	user
78f32e38-8832-48f8-8ca0-1e89c673aa29	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_view	admin
23d2c112-faa5-4ecc-9096-341ec48e14ea	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_view	superadmin
bb1ed890-3f6b-446a-ad34-d8d90c94bb2c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_lock	user
4a65b3e1-b2b4-495a-95ac-1c4499110bad	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_lock	admin
e08d8dcd-43bd-4659-bb56-10b4d0109fb2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_lock	superadmin
4b10a523-9976-457e-9524-0963b2f2a294	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_kick	user
924b469e-be84-4eec-bd06-40b0d65d0da9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_kick	admin
9694c9fa-fe07-4010-a3f1-7537a67d4546	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_kick	superadmin
dcd94409-e322-4301-b8d5-200977112436	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_mute	user
b31f1a87-5161-4c95-978b-bf411eaaae5e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_mute	admin
259bc27f-d7ea-4902-b029-3bd9d96574f7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_mute	superadmin
aa0e882e-e612-4bfd-b3ea-32450582785a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_video	user
2be510f3-d1ed-491b-9208-236c0bae773e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_video	admin
3638df3a-50b6-452b-84be-0ea008bd6968	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_video	superadmin
6104fb82-955b-495d-9b52-d9604162ba29	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_video	user
b066554c-dca4-4498-a169-0a7d496a2f25	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_video	admin
fd0cbea6-2229-42ed-9837-1f37cc3b6e78	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_video	superadmin
5df37d2e-3ddc-4148-bad2-40f5d6099288	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_advanced_view	admin
f6ef6cf9-ecac-4fec-b570-e12f6570fd51	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	conference_active_advanced_view	superadmin
b7c41a07-f2e0-4083-bf31-2cb409a0c6d9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_view	superadmin
2c53ed9d-85c4-451b-966f-cbe017e1976c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_view	admin
e3138800-a97f-4905-aa9b-dcfddc3cc91f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_add	superadmin
4bd94435-fff2-4f69-afde-f2f4284d2918	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_add	admin
eeb2a4ed-278d-442c-b295-4a328def61cc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_edit	superadmin
2809a3e1-30e7-4c82-9c26-f938fe7cbfc4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_edit	admin
96a78619-4bad-4c9e-89ab-a62a175495dc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_delete	superadmin
4e5806e7-d98a-4389-be2e-6e93d9d2c428	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_delete	admin
8422fcb4-2930-4660-8ef4-d7ae4dc69cd7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_address_view	superadmin
e2d8b902-d99f-4e0d-b0bc-3dafadd137da	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_address_view	admin
1e4c9630-2833-4c2c-9b8d-e70dd5bdff19	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_address_add	superadmin
fdc7f2ad-f87e-41e3-81cc-edeb1782e4bb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_address_add	admin
9bc161ae-7082-4f62-9e6f-a98880d8a788	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_address_edit	superadmin
57ab4dc0-e4ca-4559-bdfa-92c442d2762b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_address_edit	admin
48658394-53df-4c4c-a0ba-125a4fbe9f26	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_address_delete	superadmin
6734668a-dfa2-44f2-baff-31d6dbbdd4c0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_address_delete	admin
438ed4d5-e7b3-475e-89b0-d6ef910e0eb7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_phone_view	superadmin
12bffd2a-a954-4553-bec4-7b93ded57f20	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_phone_view	admin
1d38fea5-186d-49bd-9165-609f01e2fe92	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_phone_add	superadmin
d795cf83-bb2b-4094-8883-4b68cc79fcf9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_phone_add	admin
b416f177-2e7c-44ef-b8d5-772862b41a5d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_phone_edit	superadmin
0135f8ca-3ae2-4c6a-b81c-ec92d8897513	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_phone_edit	admin
f0ca926a-6db1-48f3-884e-25c29f656645	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_phone_delete	superadmin
4485b9b4-5bfb-4950-903a-1dd02418a9ed	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_phone_delete	admin
7bd3f6bf-3ae1-4a16-8395-75f5426750b1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_note_view	superadmin
985a1a73-c127-4210-8d1e-f467100b83b6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_note_view	admin
bfc090dd-8927-4488-ba49-8f746562ff4e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_note_add	superadmin
56a33433-0441-4758-b0de-d915e3738b4a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_note_add	admin
f885f4cd-ed6f-441f-9d08-e4abcd7eaa6f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_note_edit	superadmin
439415aa-c5c8-40ce-81d0-3b49833a9502	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_note_edit	admin
c339c5d8-754d-458a-a370-789b8eed8b4c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_note_delete	superadmin
77f526d4-7b1c-4e94-b71d-96849791eefd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_note_delete	admin
783d2cae-f2c6-48a8-a57c-761b1caca188	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_group_view	superadmin
479da628-902a-4df2-a3be-4da4722e5c0c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_group_view	admin
fadb0e4a-8b3e-4a3e-a3b1-0c75f59a97fb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_group_add	superadmin
88fc0911-ce42-498c-86c2-1289b76d40d6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_group_add	admin
b3ddea8b-7b05-448e-8b0d-6aa363a041d4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_group_edit	superadmin
a565b7ee-bb19-484e-8d47-76fed2c19213	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_group_edit	admin
001d06cc-81d2-41cb-9af7-1319b0b7d998	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_group_delete	superadmin
3d36f4c4-b9d0-4c75-b7a5-f80611050e69	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	contact_group_delete	admin
1aa6a102-29bd-4bd2-b573-a4edfcf2d3ed	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	content_view	admin
a6b77ec9-8869-43bd-81bd-7464759997a1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	content_view	superadmin
66ef6d27-85a6-47f1-9d0f-cae4bd67d67e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	content_add	admin
385507ca-e368-429e-a82a-14c3a08a5adc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	content_add	superadmin
85e128f6-78fd-4c8c-8e01-2831f6563b17	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	content_edit	admin
a6774487-47b6-47b2-9774-f3444a54ec24	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	content_edit	superadmin
9978c583-fdf9-43e3-9bf1-7b191a3687f0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	content_delete	admin
e3bd1afd-5c54-42ec-b38b-821c2af3175f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	content_delete	superadmin
8f4d78af-8b0f-4af6-9a3e-75769e7ec651	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_view	superadmin
e2188ef0-7389-4286-b96b-ac522b5c5945	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_view	admin
78af0b2a-f610-4b8f-9a37-310c905059a9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_view	user
0ea41431-97fe-425b-a498-bde6cff6f508	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_add	superadmin
50c84d79-0a3e-4f5c-b8d3-eaae055ea790	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_add	admin
56e94c31-d18f-48a7-bce1-3d2aaef913b8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_add	user
b7b139b4-04ef-4a6b-b285-38174f582212	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_edit	superadmin
8a1ffc8d-f947-4fc8-adea-f1f91dbff19a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_edit	admin
9ff7d7d2-ab2c-4e46-a114-adf6ad729488	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_edit	user
44de5a9b-e6f2-45a9-b0bd-ea7c3d14c8dd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_delete	superadmin
a1864a1c-2763-4445-8e62-21156fdd0a71	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_delete	admin
c7714d34-d3c7-4675-8114-dc2960a5b225	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_delete	user
004cfc38-5fde-4717-9e24-275b9305ea70	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_view	superadmin
d5e7508c-2748-4f28-8dbd-e03434d1542f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_view	admin
28805a2d-0466-4dd2-8b1d-562b94b26c8c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_view	user
db815003-e2a5-4acb-b814-63a0a396d57f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_add	superadmin
1648e0e5-3591-4834-9b75-49b970765f52	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_add	admin
5211cbab-63d0-4b78-ba64-fdf6659b8136	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_add	user
dc9beec2-1d15-4eb4-9fe4-baf4cb4e225c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_edit	superadmin
7f8c4b5b-7635-4ff3-9bc7-968a74c53b16	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_edit	admin
b770c457-d7b0-4166-b79c-72e6547af49b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_edit	user
513c1719-ecb7-4fcd-86c4-ef01653f7d4b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_delete	superadmin
f1136943-a6d4-422e-afc6-d65b1058330f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_delete	admin
1bf6c593-17a7-4751-966b-71d2db1921bd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_delete	user
98cad9bf-d730-4e0c-b944-8c8f76864e82	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_view	superadmin
f0ac4108-bfc3-476f-99fc-9a6b46fbe048	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_view	admin
fb30b5a6-5421-4516-8af0-e6f688de4b7c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_view	user
b2efb583-929c-46ee-82e4-748c140f0869	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_add	superadmin
b6dfb9e5-a3dc-4bc2-a1a5-6365f4adf107	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_add	admin
544ed8f4-94b2-4eb0-8b38-2422d420ff45	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_add	user
debd720c-8db0-4b6d-8f1c-d92c82543f08	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_edit	superadmin
aaf99db6-c3b2-4bb1-9e42-5cf260b64fa8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_edit	admin
2e8ac428-065a-4994-bb45-f59de42172f9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_edit	user
77d7c0ee-163a-4235-ae63-92fa5848cfb5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_delete	superadmin
366589d3-78f1-474e-a923-9ef30ceba5c3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_delete	admin
b7431e61-1aeb-4f85-8a3c-e2a86cec5aed	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	rss_sub_category_delete	user
91e32bdf-d967-444d-b5ab-56ef382609f2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	destination_view	superadmin
8523fd18-e134-4c9a-b026-d7e635ddb1c6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	destination_add	superadmin
edaac510-e4de-4adf-b7e1-9deedb252341	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	destination_edit	superadmin
c3691c14-2c21-47db-b5d0-96697ebc84e9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	destination_delete	superadmin
80f3256f-100c-4fdf-bbe1-3f0b379d06c8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_view	admin
00b4f68b-d3d1-48e7-87dc-f46d653ff8ce	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_view	superadmin
01af635c-787a-423d-a155-d7364f99855a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_add	admin
4e564b82-88ee-4911-a849-47a6c23f92b0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_add	superadmin
b90b24c6-13b4-4891-8fcd-cbabb8ccb58d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_edit	admin
ad150171-ed4c-45ec-99b8-1633a413b92f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_edit	superadmin
1b328d52-5c4f-44e5-acfb-64df38b55168	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_delete	admin
5338e518-c0e5-4d50-84a6-7d4550cbd6ee	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_delete	superadmin
7c681b68-03fe-4a47-a2e2-4309f66ff7bc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_extension_view	superadmin
d20e7fac-1ee3-48e9-a704-3277e62f86ba	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_extension_view	admin
80b6e22d-93ae-43b5-b89d-b17f83392b34	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_extension_add	superadmin
45fcd5ac-d2da-4c69-b406-dc5d4388628f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_extension_add	admin
ab296b1e-9361-4ce2-bf7c-74e010058f4c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_extension_edit	superadmin
893c064e-e9ca-46cf-8d1d-fac8c5b583a6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_extension_edit	admin
a62bb636-60b3-4815-aec2-2949c33576d2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_extension_delete	superadmin
0a5d51b4-7431-4eb5-96d4-3244928b7678	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_extension_delete	admin
b6a4f70e-df04-4c09-98ee-4d16a4569d87	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_line_view	superadmin
ba9726fc-43f2-4d68-aeb6-ec16eca8c445	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_line_view	admin
a4e12f15-4add-4ec7-bc45-9629532d3917	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_line_add	superadmin
a836020f-93fb-40a3-8581-eeeeb017f19f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_line_add	admin
281a21ec-6f50-47f1-a3b3-a2698f083643	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_line_edit	superadmin
f4a43407-b8d6-41f5-9e0b-e54e33e9b4ad	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_line_edit	admin
c4069838-2c6e-4ceb-b3f0-066c02c1fa05	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_line_delete	superadmin
32842d6b-e1e8-444b-b546-d2f918328f5a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_line_delete	admin
275e9686-fbbb-4308-bb07-4e69a4f4bd4a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_setting_view	superadmin
7256f509-8bad-411e-a5b9-453e6d60e165	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_setting_add	superadmin
371a7df1-a870-4362-b2ea-5a9d1482c350	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_setting_edit	superadmin
18d11258-cb8b-40ef-b74b-32622e975217	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_setting_delete	superadmin
2c4a861b-9fab-43d4-b4ee-4cf9a6e3a99c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_key_view	superadmin
d616e296-bf38-4088-b50f-d2fc8d909aa4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_key_view	admin
04a3cfec-1b3c-45b4-9efc-7086734acdfd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_key_add	superadmin
a5cdc4da-46e8-424f-ac21-e7605a4d66cb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_key_add	admin
0553ef1c-8f93-4740-9517-577e2aa8b74f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_key_edit	superadmin
2f18105a-f1a0-48ae-befb-5d017583cc4c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_key_edit	admin
498b1840-a4b5-4ff3-8c16-a0a59cc36782	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_key_delete	superadmin
e2792179-170d-4661-91a0-874778faf12c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_key_delete	admin
51eb3447-cd7b-4fe3-831c-1ff809acead5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	device_domain	superadmin
e681e45d-154f-4604-8069-14b524b8f71b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_view	admin
fef09023-4a7f-472e-a42e-dc191bf3085d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_view	superadmin
69fe8bfb-69cd-4bba-90c1-af0c103e7c2c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_add	superadmin
4c16b73d-134c-4fb1-87e7-a264cadfbf56	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_edit	superadmin
c59087ab-b8d4-49e1-86c5-33860f976725	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_delete	superadmin
850c07f4-3001-4d9c-94a7-a0a432c85df6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_advanced_view	superadmin
c5c5968c-8452-46d1-b39d-9ea18b7b403a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_advanced_edit	superadmin
eb7e8026-08b6-47ad-81cc-2740df513799	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_detail_view	superadmin
8a7515f4-4432-482a-b024-9f9318edd1c6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_detail_view	admin
1f67ba4f-0de0-4b23-ac16-bb64159746b3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_detail_add	superadmin
1cf5c9d4-7215-4c2f-a87b-ea639a8c4c15	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_detail_edit	superadmin
58b19fee-8f70-4e4c-aa14-a2315e714411	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_detail_edit	admin
269d56de-3f20-4a2f-bbb4-c34f834f940e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_detail_delete	superadmin
e26ec7ac-917e-423e-9d65-45c91d414c79	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_detail_delete	admin
c4159f7c-5f4c-49a9-8dbd-147a028ef384	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	dialplan_domain	superadmin
b42a820f-23ee-4228-8653-ca8d78ed8c1a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	inbound_route_view	superadmin
86fbea96-8dcb-4b2e-8b88-d7e51e12c03b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	inbound_route_view	admin
35f21f2c-4492-4b4c-9c10-e9c5ffa6b7bb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	inbound_route_add	superadmin
f18a93fe-a3cf-4dea-a93c-466028f2f02a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	inbound_route_add	admin
21447a5a-dd12-4169-84cd-3c99da0e1c10	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	inbound_route_advanced	superadmin
71dfa5b9-43ea-4bab-973c-a2fef3a56303	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	inbound_route_edit	superadmin
7902aef7-bcfc-44fa-b0d6-f3e6369034b0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	inbound_route_delete	superadmin
42c09454-aa8d-41ad-be5d-8bbea6734b4d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	inbound_route_delete	admin
fb518205-57c4-4c61-85a7-02ea610b9851	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	inbound_route_copy	superadmin
ffe0deb1-ff9e-41fb-aa6a-4c8efa87655e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	outbound_route_view	superadmin
32b4dbe1-1bde-43e0-abc8-4b30a47fc252	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	outbound_route_add	superadmin
8b029fb5-0a6d-4ed0-b5e1-182e338d82fc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	outbound_route_edit	superadmin
87c59085-0113-4ce8-931c-a734d2aa4fd5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	outbound_route_delete	superadmin
01a6336d-7a3f-4936-a030-9f709c3d57a4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	outbound_route_copy	superadmin
4f9fc8de-3350-4a0f-ae07-dec9b387c6b2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	outbound_route_any_gateway	superadmin
7fa4d61f-92d3-4f68-a1a1-f8c05041f4ec	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	script_editor_view	superadmin
f6422ac8-2fff-4ad4-b011-a340d95382bf	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	script_editor_save	superadmin
7581da23-f52a-4b08-9430-1d0c3572f5c5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	provision_editor_view	superadmin
0b37e875-4835-4098-b2db-db7e2040d9e9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	provision_editor_save	superadmin
9bffc249-8502-41ab-aed7-848643fbb262	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_editor_view	superadmin
7c501523-a725-474e-87d7-b48cba299ec9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_editor_save	superadmin
214742dc-4b78-4433-bce9-783a4eb0c863	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	php_editor_view	superadmin
60bc79ba-33f1-41f0-8641-85a5da998b76	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	php_editor_save	superadmin
4d19edc9-e529-45d8-bd10-5309e3892c88	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	grammar_view	admin
b8199bed-adce-48a2-878b-7b841fc0a801	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	grammar_view	superadmin
1ad6982e-a929-4b77-88d2-68600ebea106	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	grammar_save	admin
f0f7bcbc-732d-4ba8-ba3c-97bf11d5b2f4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	grammar_save	superadmin
12ec48f1-ad4e-407d-b93b-4efb2285eed8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	clip_view	superadmin
5c237a52-8364-42d1-b5e3-3f2bae005c95	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	clip_add	superadmin
1b2724e0-fd46-43fe-9ba0-b83bc6baebb6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	clip_edit	superadmin
e6dc3687-6e72-4847-bf63-a74c2c3c625d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	clip_delete	superadmin
ce5eb5e4-858a-42e0-be67-e113748de451	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	exec_command_line	superadmin
508d167a-8266-4a18-ad5a-0a03a7f5f746	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	exec_php_command	superadmin
30c32f60-be73-4584-bfd6-47944a6d3389	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	exec_switch	superadmin
f369a651-6bca-42e1-bf6e-3a5afaf9963d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_view	superadmin
4d6ba538-ec25-4252-8fd6-7498fc05fd73	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_view	admin
16f8f88c-675a-48a4-be07-ba52dce1bfc0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_add	superadmin
faa82cd9-0dd6-44c3-8f62-e7711821df0e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_add	admin
b13f7ba7-a304-40bd-9e97-7c7aef80f46d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_edit	superadmin
a9e95d5b-705e-4c22-8ba7-df2b77e09f77	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_edit	admin
2c6da932-34e8-48ba-b511-7c9d406f0c92	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_delete	superadmin
9fb12ea6-a793-47ac-978b-418ce714bdcc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_delete	admin
25430eb9-b315-458e-9d5b-b3a33d5a0987	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_toll	superadmin
930fe84b-2bc7-4efb-9825-4c5166b3c593	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_enabled	superadmin
20eff3fb-7756-45bb-bf82-ad7ba30795fd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_enabled	admin
4fdf3f5c-b4e2-487d-81a8-1dd025de0dd9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_user_view	superadmin
98863eb9-d2ad-4c1b-9f5f-ad33e92a6e9d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_user_view	admin
7e667b59-2c81-4493-abad-231340146333	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_user_add	superadmin
9856df57-8ed3-440e-98ab-cdb145c7175e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_user_add	admin
210bf7e8-eaee-4760-a0bc-931097f34071	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_user_edit	superadmin
3f4ad572-ac01-46e7-8148-3afd617eba47	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_user_edit	admin
b5144a66-9113-4b90-9654-849fc0129556	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_user_delete	superadmin
d9518de4-51f8-47a9-9889-32edce443447	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	extension_user_delete	admin
dbc52441-9845-4462-bba7-7861bea39e0f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_extension_view	superadmin
a63277d8-8512-42d9-b200-7008cd2d135f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_extension_view	admin
e14af91c-ac60-48a8-b93c-70cb91f29699	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_extension_view	user
ac79fe61-96fe-4e93-b2a3-63699b21365c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_extension_add	superadmin
e20e2ee9-7576-432c-854b-cf502dd8e21c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_extension_add	admin
fa4f56f7-889c-4014-917d-a3bfb3812139	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_extension_edit	superadmin
2447b862-7eac-4a9e-a7fc-69c61b713b51	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_extension_edit	admin
8c803616-5cd6-4782-9679-b098c925f871	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_extension_edit	user
dfd995ae-0bc1-4a09-a72a-7b8689ac7fe9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_extension_delete	superadmin
12bccb65-a436-41df-ae8d-7f52878fff56	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_extension_delete	admin
97c747ef-441a-4204-8fc7-c9aab8515023	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_inbox_view	superadmin
04da2353-de63-439a-ba68-3f68701abdfc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_inbox_view	admin
6cae344a-c2eb-481b-9e5b-4d102ffaa3a7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_inbox_view	user
475f86d1-9cd1-4b36-94fd-be625cc8e2f5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_inbox_delete	superadmin
4a343b54-e0f1-48c0-aa08-d9d1fe0d54cb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_inbox_delete	admin
4333c813-80f7-447c-9402-74ec9ed94cb6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_inbox_delete	user
b4bfc43d-9de1-447e-bc06-8e567ca0098a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_sent_view	superadmin
7f7fff45-f740-478e-9959-baed083862b7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_sent_view	admin
b1248307-ba6f-4748-9d41-0930d90f3daf	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_sent_view	user
2edf48ee-9ef4-4c02-ac7f-c9b6e12bdc68	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_sent_delete	superadmin
7ca2a47a-d2ec-4da4-a72b-d65e2def6c63	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_sent_delete	admin
f2349b73-62fd-4a2a-add0-6fd249ba068c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_sent_delete	user
ed50c810-d29d-4522-9185-4edbd8d8a073	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_send	superadmin
3ab69237-3570-43f8-891d-429d93e34afe	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_send	admin
2430b80e-f774-429e-80e1-66d8c49b3fad	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_send	user
eb3967d9-2f6a-47ab-b52b-60eab57f67a5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_view	superadmin
e5c909d7-52c4-481b-a03d-8ff9bc918cd8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_view	admin
bb449610-8ecd-4887-b0bc-a1ffd2a0f9e3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_view	user
aa1d2fb3-f587-42ab-a67f-a07437c47a33	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_add	superadmin
ad03bb73-b968-419d-bdda-85f31a73de5b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_add	admin
26b28745-457c-4363-a0db-628e4c4196cc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_edit	superadmin
5377d3f2-3083-4037-8c03-e5dfd11e470d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_edit	admin
9de7863c-2911-4918-8853-be00974a3ae8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_delete	superadmin
38f66b9d-caf4-448d-92fa-04d117f5e2c3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_delete	admin
57456364-b90b-46be-89b1-0afb8f37b3ed	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_user_view	superadmin
524a81aa-9652-49ba-b81d-c097277834fa	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_user_view	admin
2c2262f0-b5d6-4725-9263-303d63f08404	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_user_add	superadmin
6006f37a-57b6-4c9b-b7a8-df5c25700681	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_user_add	admin
009c2330-a74b-427b-97c4-8a2e216cdf7a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_user_edit	superadmin
fc7ef846-1102-42f7-98c8-bfe902593fed	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_user_edit	admin
3de9df1d-ac20-4b2d-81a4-8ff3a2f5ba66	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_user_delete	superadmin
7f9d65dc-be3a-4cae-b2c2-adba24eb371f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_user_delete	admin
e850990d-4fa4-4d52-af4d-6c57a167ad14	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_log_view	superadmin
50f1f9fe-2aa7-4fb5-89e6-c43afa959adf	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_log_view	admin
028b88f5-97f5-4df5-9397-df90c13d2c02	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_log_add	superadmin
a526fc33-6505-4b20-91cd-b8d3ac6c1448	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_log_add	admin
275eca32-4712-49c0-8ad9-bd6d7f9292e2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_log_edit	superadmin
ba6d1d7d-eb4e-41c5-a7dc-e01624ee36ea	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_log_edit	admin
a0873cdf-d3fe-4395-b03f-d6e742d300a9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_log_delete	superadmin
1238e144-2f7e-461a-b6aa-c0da0f418e56	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fax_log_delete	admin
7c4de048-5efc-46a7-b2cf-32678feebd02	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fifo_view	admin
6778a299-ac28-4240-b820-e96120ae3ea2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fifo_view	superadmin
c83f4f1d-504b-4198-bab1-cb308748ce4b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fifo_add	admin
8afe745e-4053-4d86-89b6-2ae64037bda0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fifo_add	superadmin
f4f35324-27f5-4b0e-a611-3a090bf0b93f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fifo_edit	superadmin
12165c23-a774-4a51-9923-a1487d20f964	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fifo_delete	admin
6f1dfdf7-cfe7-49f7-a91b-fb4967991858	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	fifo_delete	superadmin
7679d628-f753-41cd-be3c-6d444ba1f3a4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	active_queue_view	admin
1294d100-d0d0-4cfc-9663-b4fbd510c583	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	active_queue_view	superadmin
f80a89d8-9e8a-4189-b21d-712652878bd7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	active_queue_add	admin
f7f50162-eea9-4cea-be3d-309dfd37987e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	active_queue_add	superadmin
e35fc5df-4eaf-4565-a8e1-7a5735714493	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	active_queue_edit	admin
df905b12-f07d-447d-bcc0-db1c084b1b70	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	active_queue_edit	superadmin
3e494759-fb24-4076-92be-7fdc869de0fc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	active_queue_delete	admin
da273381-ad5f-42b7-b3d3-358719295e44	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	active_queue_delete	superadmin
42a0cd42-842c-4487-b914-8fff155f0ac2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_view	superadmin
f89f1d2f-0343-49de-92bf-9499d94a4de9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_view	admin
5a6f4728-7d6d-464b-9743-0221f066caff	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_view	user
7f278450-8b7a-4d8f-b9a0-d225e833867f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_add	superadmin
a3e0370a-71a5-4dff-b3ee-8986bb166a00	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_add	admin
7a7aacb4-bee8-4c0a-9d92-e404cd635a25	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_add	user
89d115ee-8198-4f6b-9f5a-a793924dbd8d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_edit	superadmin
340d6d9b-b4ff-4557-bf5a-94b34800153c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_edit	admin
bc743b91-14b4-4736-ac67-e4e11d49806f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_edit	user
cec13b09-12af-4fe0-8b0d-95be780a2c39	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_delete	superadmin
b18b4ba9-9ce2-48dd-bf4e-8adf625fd8d4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_delete	admin
82eeb522-cf0f-42d7-ba7a-51a11b14cb94	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_delete	user
e79aeaf9-4dc0-41b0-9185-4f4ba0dd3685	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_view	superadmin
117ea125-fcfc-443f-b80f-3954a071a6ac	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_view	admin
df6cc073-36b3-4099-b806-9e3f42a533c7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_view	user
97ccf318-da8f-4673-bed5-8891225a807c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_add	superadmin
df2d1230-5d4e-4b76-8822-5ce781156a88	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_add	admin
c1bfab68-0355-4ba6-9e52-9bc58d71e4bb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_add	user
9858e201-a63c-4f5a-833b-4985bc98e152	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_edit	superadmin
199bf081-7b7e-4a1d-be1b-0173d0ae11cb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_edit	admin
80d5923a-733e-495e-b75e-d116fc54f69d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_edit	user
a77f131e-9345-458a-adac-0435e9cc0c6c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_delete	superadmin
c7385a52-03a9-4b3e-b668-f6996b9f3e9f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_delete	admin
e1cc8d3a-02cd-401d-9d31-0694385be436	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_destination_delete	user
e5024033-0183-4379-9c40-ee2e19ae23cb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_prompt	superadmin
97fb02df-1cc1-4078-845e-d05f29bfa0b6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_prompt	admin
12d71bd5-2f33-4aa4-a9c1-18a2f5880047	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	follow_me_prompt	user
65e45a84-c035-4240-bdd7-f74f731cc715	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	gateway_view	superadmin
d30bca2e-9397-410e-8950-03085fb3315e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	gateway_add	superadmin
26b24594-5097-4cd9-b7d8-a325d6ac7d45	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	gateway_edit	superadmin
99b1102c-1f10-4724-bb8f-5fea5445f2f2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	gateway_delete	superadmin
b1ccd6c4-164c-42b6-818c-7a02f21f071d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	hot_desk_view	superadmin
18aafe1c-ab9f-4d4b-b662-19a736e1355d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	hot_desk_add	superadmin
79e8bc22-d7c1-4e59-aebe-8d206208fac8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	hot_desk_edit	superadmin
1112ddaa-d678-444b-91e1-08d5fa50f324	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	hot_desk_delete	superadmin
33472c67-49b9-4b25-93d4-685bcae4756b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_view	admin
02f84571-355e-41d1-a2f0-3794cad4b48d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_view	superadmin
33873786-5204-4b3e-9f30-1ea05067436a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_add	admin
f8509ff7-a579-43fc-88bc-c36a875ba4fe	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_add	superadmin
afe618a4-8f45-4137-92fe-32a47739f46c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_edit	admin
f47f7393-25e0-41dc-a3bc-e6167ebe0e30	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_edit	superadmin
c1d064ad-7e9f-48c8-89c0-c010195d13fe	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_delete	admin
ab123f98-26e1-42ea-b184-74768006ead2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_delete	superadmin
0d639757-63c8-4fe7-8bf3-3b74df25cfa5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_option_view	superadmin
f077937d-7dcf-4847-9e23-520608bcd470	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_option_view	admin
e28f5a03-7169-4c29-a284-56dc2dd1a0a4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_option_add	superadmin
f96a8548-ba28-420f-b65a-db9b3ef4e732	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_option_add	admin
826e50d4-d4db-4be5-8ebd-93ade61ffe3d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_option_edit	superadmin
36551031-8b69-443f-a6df-7667631277e3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_option_edit	admin
4b0f8fe8-570f-44c4-8dd3-15ced4d05182	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_option_delete	superadmin
ed88f417-af07-43ef-a3aa-b958c557519e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ivr_menu_option_delete	admin
4e733a3e-8a71-4f9b-ae4f-3ddb403de344	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	log_view	superadmin
0d3dbff3-1031-44ca-87f1-acd9b5431905	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	log_download	superadmin
7b4b5044-3304-460b-bc7e-8ab9dce29bcf	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	log_path_view	superadmin
5f3feede-0761-4711-a2bb-d6ed4508e5d3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_view	superadmin
668222b7-23e4-47f5-84b3-0836f5d0cd8f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_view	admin
3382138c-f15a-410c-9fbc-30149af68f2a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_add	superadmin
72efc38b-d101-49c9-9c3e-44ce2d6918e6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_add	admin
d2f8b688-af7e-46e5-b62c-71f089f1ac7d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_edit	superadmin
f61cfa20-efb2-41be-91f8-3ef2b5854e70	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_edit	admin
73318064-928a-4222-84c9-26dbae781d84	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_delete	superadmin
6ee1fcbd-13b2-4fd2-ab08-5abcb095e0eb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_delete	admin
1c539cd8-3184-4a05-a3f9-98bdc04c2fb9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_user_view	superadmin
0ad5cdf1-1958-4446-9811-5a5ab35b41d9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_user_view	admin
85c9f2ae-cb91-4779-b487-874e62498db5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_user_add	superadmin
7f7f1ffc-d3ee-44e9-a51b-aa8c7096828d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_user_add	admin
450da4cc-1ffb-4230-8102-fdb47edb2ad7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_user_edit	superadmin
99f1e010-9291-41be-a749-2698861a1968	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_user_edit	admin
0407f560-c30a-4501-bcbf-d1f80ecaa596	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_user_delete	superadmin
4a425788-e22d-42c5-9b5c-2b8d855ab665	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	meeting_user_delete	admin
7fdb148f-5a11-4597-b12a-8695583b326e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	module_view	superadmin
23c2ddf3-2d77-48bb-8637-5c6f135f552c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	module_add	superadmin
af91c933-d4fe-4bac-90a8-079bc75a1e72	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	module_edit	superadmin
03480b8a-8b61-4da5-af46-ccfc73797529	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	module_delete	superadmin
baf628a2-6f1e-45f9-84a9-24228d81c053	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	music_on_hold_default_view	superadmin
8c0c428e-624c-4e54-8bd7-b510e7db3a45	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	music_on_hold_default_add	superadmin
16b215f0-4b3b-47f1-a6b1-b4d4854e927b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	music_on_hold_default_delete	superadmin
92c55103-1c40-41e2-9a51-e94aa1c7e4aa	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	music_on_hold_view	superadmin
bd434849-4399-40cd-9e80-ac0694f5d116	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	music_on_hold_view	admin
f11e80fd-4275-4ea2-b232-2798ca6a5ffe	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	music_on_hold_add	superadmin
119f8216-5d09-492e-9817-36b53bd44838	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	music_on_hold_add	admin
3903848b-eb04-40e0-aeff-f2e5e76fe952	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	music_on_hold_delete	superadmin
caf6b285-d9d9-4195-9cbf-2842ec825592	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	music_on_hold_delete	admin
994f56df-39df-4903-9f71-5adf936ebbb7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_view	admin
93f6ef52-1d5d-4f1b-a57b-334313b4f686	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_view	superadmin
0b19d721-d279-4121-ae27-46c002b48d69	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_add	admin
9cae5405-3c92-41d1-a833-777dd49228c0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_add	superadmin
01d7ef8b-7e72-4dec-9f25-9b05a0dbf42f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_edit	admin
81968f7f-2c66-4b6a-bfc4-09d706279a94	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_edit	superadmin
b87b470c-cb8a-4163-a747-5e21017ad823	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_delete	admin
ac30446e-969c-4a20-9fb6-566eb05d6f93	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_delete	superadmin
2d627411-bb2a-4f39-a8df-0203f2324a50	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_upload	admin
01a26563-0db0-46d6-9c54-583236432d73	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_upload	superadmin
3b52e1e4-4543-4285-b3ac-c46717d2e756	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_play	user
8628167e-cc6a-4306-a862-1beb6170b113	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_play	admin
6364f28e-6f85-4297-9bc5-f363795bebe8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_play	superadmin
d01a3f42-a66a-424f-92c4-2096c20f6ffc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_download	user
473b7202-9420-4085-9f56-03d6c5116096	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_download	admin
b3e8d408-cbb5-4d93-bf9d-a4ccb879c1af	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	recording_download	superadmin
c160164b-4cad-4820-b72d-4c8e5ac2dbe2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	registration_domain	admin
64d0581c-1ef5-4a01-9f41-b3a59390e79b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	registration_domain	superadmin
b4b10959-95a9-4a8b-91dc-f4da68bd3d32	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	registration_all	superadmin
5549f811-ec42-42d7-baa1-f19d8eb568e3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_view	superadmin
48a5833a-ac78-444a-b98c-3b19e3acd39b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_view	admin
b4d7d15b-8040-4fbc-844c-0e684f6722ec	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_add	superadmin
778a573e-7ec5-4c10-a3e5-2147cf49a2da	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_add	admin
8851d179-bcd3-4a37-adf4-ffd844c1e7f4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_edit	superadmin
821ad3c0-2e5e-40f9-a8f9-c5be2c0dede5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_edit	admin
e61ad7f7-0b85-4d33-8731-9a8684567664	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_delete	superadmin
c3a2a16f-13a3-46cd-94aa-e8fbd4e27c1d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_delete	admin
297e3793-38d8-4c0c-98e0-305445177575	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_forward	superadmin
43e3c17c-9f82-4799-b15f-cbb78639a960	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_forward	admin
cc885e0f-aa0f-4c3f-8057-27bef77b0426	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_prompt	superadmin
e456f346-8661-40ac-9a1c-9c5dbe03f569	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_prompt	admin
5a87f673-bc1f-4030-90c4-ed8a85a75e09	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_destination_view	superadmin
ef682ccb-783f-481a-8583-009ec62bfbfd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_destination_view	admin
f298b8d7-be3c-4dd8-8c7d-17956811f4c5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_destination_add	superadmin
34ef3c9e-b281-4b8a-b0c6-107c85e5240d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_destination_add	admin
bc48d2b3-1ee7-4915-b8cf-09ec82f9b3d7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_destination_edit	superadmin
c0fafb59-c6db-4dd6-9e03-33b2e1d59c02	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_destination_edit	admin
00542149-90b2-445c-a229-de8880b3a4cc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_destination_delete	superadmin
a1e6eef2-4f2d-4c15-bbef-683acbd0068f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_destination_delete	admin
629261ae-5315-4f9d-b47f-49fa5af2187e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_user_view	superadmin
3a893b97-3c69-4569-9533-927394948083	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_user_view	admin
d6dc2da3-4e89-489a-940c-0f187228355a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_user_add	superadmin
07d9e812-e2a4-4433-9b72-a7433de3614c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_user_add	admin
fe791b93-ede0-448e-b756-f0710033b057	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_user_edit	superadmin
50d59b4d-1e1a-4bdd-a8ac-86ec7dc73de1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_user_edit	admin
f4e36409-de32-48a0-89b9-b4cc972a215d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_user_delete	superadmin
3c593ab0-a785-43fa-a5cb-d9528f290bfa	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	ring_group_user_delete	admin
b7b31842-93b7-4e7b-9e1a-ca64f9c953ba	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_view	superadmin
380c496d-90ca-40a9-86e7-aa9cf9f1405a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_add	superadmin
4a8dadb4-2707-4636-8d18-240557c5e2bb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_edit	superadmin
452e5704-5b34-4699-9adb-7b7800cae112	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_delete	superadmin
8cf20529-e8d1-4ba0-afc0-5ab3786dabaa	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_data_view	superadmin
d25c73e1-3059-4882-905c-c26c37415a9d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_data_add	superadmin
a0815afe-29cc-4dc9-a3b4-73869955619d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_data_edit	superadmin
831ca14a-be3c-4010-b5cf-099f30e93e32	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_data_delete	superadmin
a6085e42-522f-4b27-a4b7-a390c7f3313e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_name_value_view	superadmin
f10dede1-9112-4b1b-90df-421482d44468	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_name_value_add	superadmin
55bfdcdc-ee06-43a1-8b55-f6e4c004fa64	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_name_value_edit	superadmin
ddb09489-7e59-43d6-8064-635ea3333293	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_name_value_delete	superadmin
7248e40d-69ec-4a90-b0e9-ee7c04795c9a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_field_view	superadmin
e38165a0-2dc6-4ada-b639-13d157167062	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_field_add	superadmin
84822497-3da1-474e-8f40-c492a365554f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_field_edit	superadmin
d74b1355-ec14-4967-a7ec-457afcffde73	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	schema_field_delete	superadmin
34861adf-b75e-46a3-928f-1c96a98d303c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	service_view	superadmin
c255732a-50b5-458e-982a-7fdf2ab0a848	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	service_add	superadmin
86e2b571-2937-4e5d-aa59-c3f716233207	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	service_edit	superadmin
a4e3b077-7e83-4dce-9d11-4627765799d0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	service_delete	superadmin
e6e35eba-1a7c-4f90-af13-609f3ce59333	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	setting_view	superadmin
c342db9e-1276-4cc9-a277-70b20846bedb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	setting_edit	superadmin
477931ef-ad0c-414f-97a9-ace6a5d1aa6d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_view	superadmin
ab4dc13d-6f41-4b35-bbf7-32e4efde1f9e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_add	superadmin
7da57f93-db30-4485-901c-c3c560e988e1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_edit	superadmin
d136f791-f120-46ea-869f-c360f0a3ac54	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_delete	superadmin
7ceac87d-171b-4fd8-bfb9-c9e034ef8561	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_setting_view	superadmin
7c17271d-c65b-4ae5-96a3-86193f0eb003	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_setting_add	superadmin
61bf405d-84d6-473b-a36b-65e93d4f7188	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_setting_edit	superadmin
11b72020-144d-4c73-b338-129b86cbd2ad	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_setting_delete	superadmin
3f64e7ce-1657-49d3-8be2-b16e62238c7b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_setting_view	superadmin
97350bec-b523-4d7c-8c17-da524bfa2bce	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_setting_add	superadmin
77bcbd00-aa2b-4725-942e-628df7bc426b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_setting_edit	superadmin
990b22f7-9f5a-4adb-928f-0672adbf3798	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_profile_setting_delete	superadmin
1a5ea6f2-6553-41f5-a90e-7ae4171eb167	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	system_status_sofia_status	superadmin
ee231668-a811-4757-b402-65be63d05fb8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	system_status_sofia_status_profile	superadmin
9eb06127-2fec-472c-ba8a-c5e172c5bede	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sip_status_switch_status	superadmin
fb32af8a-835b-4245-8044-0151579b1ff1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sql_query_execute	superadmin
0828a706-d2fa-4834-8d43-0b784ee0d995	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	sql_query_backup	superadmin
9df26eca-6063-4e4c-b1e1-15bbe1dd6742	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	system_view_info	superadmin
7beeb3de-e882-4cfb-929a-9ca4bbcec2a8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	system_view_cpu	superadmin
4462d652-53ab-4188-b49c-5b431b8247ff	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	system_view_hdd	superadmin
92625bac-e35a-4ba6-8a15-b1f63515339c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	system_view_ram	superadmin
750822a0-1c18-4cc4-8a55-501eb4c8f430	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	system_view_memcache	superadmin
32b0fe76-d4fc-4b2b-b91e-32b3898da640	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	system_view_backup	superadmin
0d3409b2-4837-4333-9343-4ca5cbee650d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	software_view	superadmin
35aff1b6-8318-445b-9267-d288b5e947ce	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	time_condition_view	admin
ae4f422c-fafd-4c0b-a861-3004a28f732f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	time_condition_view	superadmin
48a55cfa-0c36-4cd2-a6b1-8338976a1ddf	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	time_condition_add	admin
6455c975-c8fb-4fc1-9aeb-4aa2a36b544e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	time_condition_add	superadmin
38ae3d94-a178-4030-aac9-2acc537235d1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	time_condition_edit	admin
1d488012-061c-40bf-b208-2760d8a8c385	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	time_condition_edit	superadmin
d3fc32c8-5f32-449b-85d9-1602e98d8e1a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	time_condition_delete	admin
dbeabefc-dd4c-4c9f-8b8e-8d0f669dc29b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	time_condition_delete	superadmin
65e21cb5-e563-42b9-af33-56e1fec928fb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	traffic_graph_view	superadmin
334bb864-6267-4269-aadf-b340fd396bb4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	var_view	superadmin
1f1b6b58-e338-4e58-8fd8-7c6d48d346bb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	var_add	superadmin
4b67fd97-2809-4228-91e0-584f6373091c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	var_edit	superadmin
121171af-3501-4a3c-b1e7-fcb211cee259	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	var_delete	superadmin
f5c6869f-535c-457c-bfb1-85681ebfbe17	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_view	user
6b5939be-82b2-41e9-8ea2-a875e8c7f94e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_view	admin
43863e53-69af-4cf4-81df-30039ca54162	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_view	superadmin
1bf6cd66-b3d4-48a4-8052-209d2378e6d1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_add	user
61994ea1-4492-4bd5-afbd-ba621f770f76	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_add	admin
1fc88faf-54cb-42e1-9019-f59696322cc3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_add	superadmin
73a98acd-e218-458b-898a-d701ff6b356d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_edit	user
c8bb7963-7ec0-437c-a635-b54a2c24ade9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_edit	admin
9bbf7dbc-7f22-44bd-b65c-ed0b9012592d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_edit	superadmin
6f0d7d5f-082d-41b5-bf48-d97824e29ad2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_delete	user
5cf5422d-886e-46e2-a4f6-3eda525fc7a3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_delete	admin
2e925b4a-5184-4737-bbab-3263425992b6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_delete	superadmin
c7a0f2cb-a130-486f-b165-988383da127e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_upload	user
398bfe11-18b7-44ea-bf78-1f8c6db06723	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_upload	admin
8698fc24-6b84-4178-bdfa-d24c6a7d8062	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_upload	superadmin
3352a01d-fab1-4dc4-a8ff-52e92b76df6f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_play	user
68dfe9ad-ec74-4b9e-9efe-a2617b094c9b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_play	admin
ab9c887f-6282-40f4-b5fc-b7a36bfdfcaf	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_play	superadmin
d8779261-bdd9-40b1-b0cf-172d5dfbec85	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_download	user
9f2c814e-c060-4c0a-a625-a83ba95dbf98	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_download	admin
2741738e-41ed-444f-8f50-771eef60b20c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_greeting_download	superadmin
423de126-1e1d-484e-9a16-ce033686509c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_view	superadmin
bd07da00-cc28-4832-9399-f11090ec9d8e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_view	admin
ca42e5df-b9a5-4cb4-aa8f-09937b11bcb4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_add	superadmin
85720ec4-bf94-4c92-b52c-c7e5cbbb04cc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_add	admin
e064173c-03f1-4410-a4ee-0a24616ade74	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_edit	superadmin
96a749ca-4e09-4fa2-b744-27ec5149c0eb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_edit	admin
d5663005-4d2d-4a9b-b597-034e19946339	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_edit	user
4f085114-33c1-4ba3-944c-cb2e235f5fc4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_delete	superadmin
58013e64-c17f-45d7-9b27-7f1ee5dd35dd	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_delete	admin
29c36d09-ecbc-448d-b0cb-90ee598b240e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_message_view	superadmin
feb0601d-1347-4211-aa19-5d43661be0d5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_message_view	admin
e1f6989a-b134-4806-aa0e-e3e6bb4ad356	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_message_view	user
9169b12d-3743-461a-b794-9c0cca9ca411	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_message_add	superadmin
92d588fb-9a8e-4223-a528-89133c5b7a58	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_message_add	admin
2e29c336-31cf-4ab8-a97e-ff6d82d7838a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_message_add	user
f91411df-8d60-43e2-b145-b39b66dae67c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_message_edit	superadmin
e7ecd16b-3462-42b6-a241-da8457ac9a4e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_message_edit	admin
264a4b38-74f2-4b3b-8f2a-22a4ac0eb8c2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_message_delete	superadmin
e72a44e8-9c00-495d-b077-877cb26d0e49	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_message_delete	admin
fa2cd652-247b-4eb0-8f06-07c07412ae07	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	voicemail_message_delete	user
b4128c48-01ec-43e7-829c-609eff4f4c95	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_view	user
c8364b1e-7604-438b-adfb-82179f7435c1	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_view	admin
e16c55e0-7e44-4118-8211-c30501e2449b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_view	superadmin
143c3050-f88d-4d73-bfcb-a053dc322896	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_search	user
c9bd5826-6a2c-454d-a960-7af318bcfb3b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_search	admin
f140acbf-8471-4522-87de-6a92acedc893	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_search	superadmin
35acfe3e-8ddc-4506-a90d-50a9b206eb5e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_search_advanced	admin
a54b6549-2ab1-4a7e-90d9-0021af868208	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_search_advanced	superadmin
f0306c52-da9c-4767-9ffd-6d9edca876ca	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_domain	admin
723aea34-8f40-43d1-af72-d7350d42de82	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_domain	superadmin
890758c1-0742-491e-817f-70860eb0f565	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_add	superadmin
087accb3-ad5a-4c44-8ca6-3bb0bb14ade6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_edit	superadmin
2f04c94a-e94e-494f-bcaa-9bc97ab68e64	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_delete	superadmin
095ab22b-0813-4a4f-84f6-c0c00a05ae34	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_pdd	admin
f8954410-6da3-408e-9ed3-60ecdd57e3c3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_pdd	superadmin
5a300704-1ccd-4318-8b96-eb218945c3ba	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_mos	admin
994355f9-fddd-4f7a-a52e-2902a4bebd04	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xml_cdr_mos	superadmin
eadb2c20-ea18-4fbd-a0b0-213eb1828d83	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xmpp_view	superadmin
d59df7e3-1684-4595-8cea-7a80c86d120e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xmpp_add	superadmin
865cbc17-8af8-4917-85ff-140c3b06a4fb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xmpp_edit	superadmin
90bf0dc7-bd8d-427f-b094-bf8d902cc408	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	xmpp_delete	superadmin
367dcb2a-79ce-4abb-9451-0f3cfa79672b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	app_view	superadmin
4e592f47-7184-4161-b34b-4b020b9023c4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	app_add	superadmin
e0b98cbc-8e0a-4e24-b87d-6767ef4fb70b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	app_edit	superadmin
7a49cc9e-4411-4604-8510-7e4e31d4234a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	app_delete	superadmin
63d7e461-fea7-4df7-ab6d-37ead5b7778e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	database_view	superadmin
b41e9c3a-b167-4a79-ac6d-7dae4faf1aa2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	database_add	superadmin
97e8f2c4-2516-49e0-923e-e915bf7f5fcb	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	database_edit	superadmin
ba334bdb-d7e8-423e-9034-0b5a2847aaff	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	database_delete	superadmin
a8c12476-4f5e-4fbb-8c8d-1abf45a7555a	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	default_setting_view	superadmin
ca81fb49-dc34-4012-864e-d9af834f77b2	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	default_setting_add	superadmin
6653acee-d75b-4784-8833-c5832f313e4b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	default_setting_edit	superadmin
3650e548-8baf-4e25-a22b-c45de7c120c4	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	default_setting_delete	superadmin
f4d2ecae-8b7f-4f7e-bbca-62c320bb1b94	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	domain_view	superadmin
a2099d12-559b-4f4a-a625-1c5992f2ef02	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	domain_add	superadmin
e2a71fe8-84a9-4a96-bd91-c23211de0450	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	domain_edit	superadmin
e3f1e170-e8bf-409f-8e51-109e9a049242	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	domain_delete	superadmin
fedcc75c-88de-460d-b813-21470fcb0473	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	domain_select	superadmin
e4a5c4cd-a43a-4e72-91cf-02519c115f58	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	domain_setting_view	superadmin
f9caf654-8290-413c-bd36-ac02e01ee446	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	domain_setting_add	superadmin
215533da-e7d3-42d0-aaff-61205a0970e8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	domain_setting_edit	superadmin
a3a0b999-9035-4f43-8168-9b265a6a5879	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	domain_setting_delete	superadmin
c3ea0906-b7d3-42e9-b3d0-6e2b63755697	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_view	superadmin
9d1461b6-b194-4bad-9911-a304fcbef33c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_add	superadmin
bcf8d582-d8d6-4cce-be98-dc7efa8db462	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_edit	superadmin
ac69faf2-e058-4f7d-a7cd-bba39750e744	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_delete	superadmin
bc93b9fc-d407-487e-bc4e-b59f24def14f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_restore	superadmin
6ff2a7f8-b40c-4f26-b10e-ef4f47e24132	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_item_view	superadmin
2deef8ce-500f-4800-b3e1-00b0bcc4a655	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_item_add	superadmin
886f03a0-8b10-4d7d-8cf8-1619fee2ca41	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_item_edit	superadmin
c2718095-e369-4368-b1dd-f6dab07c9d7c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_item_delete	superadmin
813ad05d-85c9-4c42-bd52-407de4d6d0f8	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_item_group_view	superadmin
36414e08-ce4a-43f9-9dad-5b3d2d1f3566	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_item_group_add	superadmin
e5688c9a-d12b-4c98-8670-83a87cc793b0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_item_group_edit	superadmin
eb643fcb-4680-4e29-9268-bb5c71fda596	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_item_group_delete	superadmin
13842c91-26f9-4c1d-9ce1-939011ec6d47	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_language_view	superadmin
b1d7b34c-043c-4d34-aecc-943000446c4e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_language_add	superadmin
11e8d0cb-0604-4cb9-b9c1-4749deb59394	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_language_edit	superadmin
825bb7dd-2885-463a-82b7-9f02050cd169	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	menu_language_delete	superadmin
29419d60-868c-4984-8b7d-adb1bca545fc	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	upgrade_svn	superadmin
18b2be9b-c62c-4fe0-8067-e4b5dc17240c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	upgrade_schema	superadmin
087c9ea0-84ae-440a-b779-b36bffa5d83c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	upgrade_apps	superadmin
78f939f9-2857-4823-a4ed-433ff30917d5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_account_setting_view	user
43993607-13b4-4620-a3b0-d479bdcfe02d	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_account_setting_view	admin
f6a229a5-618c-4007-85e7-38e27c543198	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_account_setting_view	superadmin
5049d202-5d3a-4b22-b025-e896d2fecc70	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_account_setting_edit	user
92009989-57c2-4c62-a134-03434bc8d758	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_account_setting_edit	admin
de496aad-5ae9-47cc-930b-73f621fcbc62	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_account_setting_edit	superadmin
28a91b4b-33d7-43d2-8993-1d8d260876c3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_view	admin
58918244-e835-4073-b119-738f86c6d15b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_view	superadmin
89650db0-15e1-4c44-99ea-8bd7bd7e4094	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_add	admin
b64d929f-c01b-46d1-b6e2-7eb0c31742ad	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_add	superadmin
d9712413-b5ef-4b88-8755-5fd03ac56104	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_edit	admin
c506af5e-09b6-4399-b192-3ff9fe569684	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_edit	superadmin
b2b15251-a65e-4e5f-8f9e-883641217372	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_delete	admin
459cadf7-5b6d-4ea6-8d52-e1ee63c95891	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_delete	superadmin
34a18662-f9f3-4367-b8a3-bc0c5d2fa55e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_view	admin
adc68a84-fb4c-4830-9522-7bea66929603	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_view	superadmin
4910989b-ee5d-4b82-8df4-04be90903a87	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_add	admin
47712eb3-a76f-49f3-a3eb-d5eddf9c8be3	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_add	superadmin
e79066cf-f2da-495a-b852-fb5ce15ad438	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_edit	admin
8bfc78e7-8a3d-4c2b-940a-8fa0a0c7e640	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_edit	superadmin
27380cea-5d45-4200-a57b-e32bf5ec469b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_delete	admin
e5e5e30d-3210-47d8-becf-11e2cdc2972c	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_delete	superadmin
fd4c8a3c-d8f1-43b4-a751-272108dd71d9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_member_view	admin
ac59456a-86f6-4beb-8974-c5e63fbcd52e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_member_view	superadmin
f46baeff-c124-4433-9174-faa2eb630451	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_member_add	admin
ef44b349-0dec-4cfe-94f1-87c2ab9fc649	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_member_add	superadmin
9fbcd1e2-0a72-4fe0-bb4b-979b69631794	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_member_delete	admin
e15974a2-9776-404b-8aa1-969492b8ad7e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_member_delete	superadmin
baed27a4-2566-4792-8318-3c096acc8c1e	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_permissions	superadmin
397c8a2a-5323-4269-839d-543b2ab7e767	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_user_view	superadmin
309169f1-2a03-43f4-ab44-e208a8bfc899	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_user_view	admin
9d5e2de5-4459-4915-bc1f-f4e00b1c19d6	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_user_add	superadmin
266524d6-a491-40f3-996d-e635a7cc1d80	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_user_add	admin
d9f7c057-b45f-44e2-b8de-58cd5f94a7ef	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_user_edit	superadmin
ea34e722-025f-4203-8625-aeb130d84734	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_user_edit	admin
8fa0b0fd-2fda-44eb-9d36-6167a72d90a5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_user_delete	superadmin
822fdbda-3e0b-45a7-bb27-cd6e61583141	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_user_delete	admin
505609ff-be71-43e8-b7d5-e7227d88b897	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_permission_view	superadmin
7e053c43-54ba-4eb0-8000-2a5fdc71d6e7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_permission_add	superadmin
9a92c893-c4fe-4afb-9fa9-a4341f9a6caa	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_permission_edit	superadmin
8632d1db-47ee-4247-ba27-699bc948d22b	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	group_permission_delete	superadmin
565f4b39-630e-43ab-8df1-137b6c5dd9e7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_view	superadmin
7eb0b240-d983-493b-9305-263d4b3f3f01	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_view	admin
89778fef-5913-4dc6-862f-a3f6c7ec1c24	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_view	user
8a28f170-2758-4f93-a740-ce1bd31c11d7	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_add	superadmin
51a1f78b-d435-48b7-aae2-8cf195dae0ed	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_add	admin
634918bf-20ac-4df2-81f1-10b9aef9e189	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_add	user
d598ca79-348d-472c-9e81-feb6b1bb5d52	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_edit	superadmin
d8403402-7287-4208-b8a2-681d5f5c7d95	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_edit	admin
10bf8d92-c44d-4a88-b744-3069eab25bab	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_edit	user
12626833-0de1-455a-8101-82be061d5d0f	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_delete	superadmin
c0af21b3-9a99-41aa-bd1d-65f1ba92f111	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_delete	admin
fee83b03-dfde-4ab0-9377-c326f669f5d0	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user_setting_delete	user
\.


--
-- Data for Name: v_group_users; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_group_users (group_user_uuid, domain_uuid, group_name, user_uuid) FROM stdin;
421b525d-1f89-4d39-a5fd-aad63a7c5ee0	60d6731b-e548-4175-ab3e-3fbb261e60f5	superadmin	037f1bf1-d664-4be5-920f-a3ff35a2d336
2dd21163-e30e-4688-909f-376f49a33bed	60d6731b-e548-4175-ab3e-3fbb261e60f5	admin	3af1a190-2327-4c03-822e-5dfbb93f1235
bb46b866-2cde-45bc-a7dc-64af81f10463	60d6731b-e548-4175-ab3e-3fbb261e60f5	agent	39c49bfa-f8ec-4831-9bfc-1a0489456839
\.


--
-- Data for Name: v_groups; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_groups (group_uuid, domain_uuid, group_name, group_protected, group_description) FROM stdin;
2909ba10-fb7c-49f3-b6ce-b2e94808a603	60d6731b-e548-4175-ab3e-3fbb261e60f5	superadmin	\N	Super Administrator Group
2916f78a-7eae-4338-9f7b-de13026c05ed	60d6731b-e548-4175-ab3e-3fbb261e60f5	admin	\N	Administrator Group
d2f64077-b4b0-4a40-bdd1-daa577f42886	60d6731b-e548-4175-ab3e-3fbb261e60f5	user	\N	User Group
db3680ee-7453-41e1-b6ef-d248ed912ccf	60d6731b-e548-4175-ab3e-3fbb261e60f5	public	\N	Public Group
d862b38a-ab44-424b-9d20-f07132c2cc67	60d6731b-e548-4175-ab3e-3fbb261e60f5	agent	\N	Call Center Agent Group
93c0e423-8b7a-4f2a-a866-fd43c4ef70aa	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	superadmin	false	Super Administrator Group
3c2e73b4-4f44-42f0-9a61-1168006a37c2	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	admin	false	Administrator Group
8012164d-0292-4655-9792-a5a68f402711	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	user	false	User Group
eb23a7e6-b3d1-47f2-98f3-ed161c01fd70	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	public	false	Public Group
e75467dd-e72a-4b03-b24c-30597931a519	0dce1a8d-9327-4fa4-a7bc-7615a0b70c8c	agent	false	Call Center Agent Group
35795a61-686b-47af-9875-bb62f82f5577	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	superadmin	false	Super Administrator Group
6eb2359c-8da7-4279-b692-cedcd00ac734	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	admin	false	Administrator Group
42e80284-e495-470d-9b5d-b7c909ee1ea9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	user	false	User Group
5541695e-2078-4bca-843e-5ac936e9e9e5	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	public	false	Public Group
346ae113-c2ab-4f31-8d21-16e0c13e1fb9	a7d46f8e-31e2-4578-a7cd-b2410fcf3f86	agent	false	Call Center Agent Group
\.


--
-- Data for Name: v_ivr_menu_options; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_ivr_menu_options (ivr_menu_option_uuid, ivr_menu_uuid, domain_uuid, ivr_menu_option_digits, ivr_menu_option_action, ivr_menu_option_param, ivr_menu_option_order, ivr_menu_option_description) FROM stdin;
\.


--
-- Data for Name: v_ivr_menus; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_ivr_menus (ivr_menu_uuid, domain_uuid, dialplan_uuid, ivr_menu_name, ivr_menu_extension, ivr_menu_greet_long, ivr_menu_greet_short, ivr_menu_invalid_sound, ivr_menu_exit_sound, ivr_menu_confirm_macro, ivr_menu_confirm_key, ivr_menu_tts_engine, ivr_menu_tts_voice, ivr_menu_confirm_attempts, ivr_menu_timeout, ivr_menu_exit_app, ivr_menu_exit_data, ivr_menu_inter_digit_timeout, ivr_menu_max_failures, ivr_menu_max_timeouts, ivr_menu_digit_len, ivr_menu_direct_dial, ivr_menu_ringback, ivr_menu_cid_prefix, ivr_menu_enabled, ivr_menu_description) FROM stdin;
\.


--
-- Data for Name: v_meeting_users; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_meeting_users (domain_uuid, meeting_user_uuid, meeting_uuid, user_uuid) FROM stdin;
\.


--
-- Data for Name: v_meetings; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_meetings (domain_uuid, meeting_uuid, moderator_pin, participant_pin, enabled, description) FROM stdin;
\.


--
-- Data for Name: v_menu_item_groups; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_menu_item_groups (menu_uuid, menu_item_uuid, group_name) FROM stdin;
b4750c3f-2a86-b00d-b7d0-345c14eca286	1f59d07b-b4f7-4f9e-bde9-312cf491d66e	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	user
b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	agent
b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	agent_admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	eba3d07f-dd5c-6b7b-6880-493b44113ade	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	user
b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	user
b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	user
b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	public
b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	user
b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	0347f82a-62a0-49d0-bacd-511d080c46d5	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	0347f82a-62a0-49d0-bacd-511d080c46d5	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	user
b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	user
b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	user
b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	ef00f229-7890-00c2-bf23-fed5b8fa9fe7	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	834b2739-9e99-4345-9b0b-7ec3ca332b67	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	4fa7e90b-6d6c-12d4-712f-62857402b801	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	user
b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	user
b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	8c826e92-be3c-0944-669a-24e5b915d562	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	user
b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	92c8ffdb-3c82-4f08-aec0-82421ec41bb5	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	admin
b4750c3f-2a86-b00d-b7d0-345c14eca286	0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	superadmin
b4750c3f-2a86-b00d-b7d0-345c14eca286	3b4acc6d-827b-f537-bf21-0093d94ffec7	superadmin
\.


--
-- Data for Name: v_menu_items; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_menu_items (menu_item_uuid, menu_uuid, menu_item_parent_uuid, menu_item_title, menu_item_link, menu_item_category, menu_item_protected, menu_item_order, menu_item_description, menu_item_add_user, menu_item_add_date, menu_item_mod_user, menu_item_mod_date) FROM stdin;
b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	b4750c3f-2a86-b00d-b7d0-345c14eca286	\N	Dialplan	/app/dialplan/dialplans.php	internal	\N	15		\N	\N	\N	\N
c85bf816-b88d-40fa-8634-11b456928afa	b4750c3f-2a86-b00d-b7d0-345c14eca286	\N	Login	/login.php	internal	\N	99		\N	\N	\N	\N
0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	Logout	/logout.php	internal	\N	3		\N	\N	\N	\N
49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	Modules	/app/modules/modules.php	internal	\N	5		\N	\N	\N	\N
148ea42a-3711-3d64-181b-07a6a3c3ed60	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	Settings	/app/settings/setting_edit.php	internal	\N	7		\N	\N	\N	\N
7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	Variables	/app/vars/vars.php	internal	\N	9		\N	\N	\N	\N
f9dce498-b7f9-740f-e592-9e8ff3dac2a0	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	Devices	/app/devices/devices.php	internal	\N	11		\N	\N	\N	\N
d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	Extensions	/app/extensions/extensions.php	internal	\N	12		\N	\N	\N	\N
237a512a-f8fe-1ce4-b5d7-e71c401d7159	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	Gateways	/app/gateways/gateways.php	internal	\N	13		\N	\N	\N	\N
fd2a708a-ff03-c707-c19d-5a4194375eba	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	Destinations	/app/destinations/destinations.php	internal	\N	17		\N	\N	\N	\N
52929fee-81d3-4d94-50b7-64842d9393c2	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	Dialplan Manager	/app/dialplan/dialplans.php	internal	\N	18		\N	\N	\N	\N
17e14094-1d57-1106-db2a-a787d34015e9	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	Outbound Routes	/app/dialplan/dialplans.php?app_uuid=8c914ec3-9fc0-8ab5-4cda-6c9288bdc9a3	internal	\N	20		\N	\N	\N	\N
29295c90-b1b9-440b-9c7e-c8363c6e8975	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Call Block	/app/call_block/call_block.php	internal	\N	22		\N	\N	\N	\N
50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Call Broadcast	/app/call_broadcast/call_broadcast.php	internal	\N	23		\N	\N	\N	\N
6c072b29-5b6c-49fc-008e-95e24c77de99	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Call Center	/app/call_center/call_center_queues.php	internal	\N	24		\N	\N	\N	\N
8f80e71a-31a5-6432-47a0-7f5a7b271f05	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Call Detail Records	/app/xml_cdr/xml_cdr.php	internal	\N	25		\N	\N	\N	\N
b0939384-7055-44e8-8b4c-9f72293e1878	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Call Flows	/app/call_flows/call_flows.php	internal	\N	26		\N	\N	\N	\N
f862556f-9ddd-2697-fdf4-bed08ec63aa5	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Click to Call	/app/click_to_call/click_to_call.php	internal	\N	27		\N	\N	\N	\N
95f88726-4706-43f0-b52b-9504a0b8046f	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Conference Center	/app/conference_centers/conference_centers.php	internal	\N	28		\N	\N	\N	\N
9f2a8c08-3e65-c41c-a716-3b53d42bc4d4	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Conferences	/app/conferences/conferences.php	internal	\N	29		\N	\N	\N	\N
f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Contacts	/app/contacts/contacts.php	internal	\N	30		\N	\N	\N	\N
9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Fax Server	/app/fax/fax.php	internal	\N	31		\N	\N	\N	\N
a1144e12-873e-4722-9818-02da1adb6ba3	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Follow Me	/app/calls/calls.php	internal	\N	32		\N	\N	\N	\N
baa57691-37d4-4c7d-b227-f2929202b480	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Hot Desking	/app/hot_desking/index.php	internal	\N	33		\N	\N	\N	\N
72259497-a67b-e5aa-cac2-0f2dcef16308	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	IVR Menu	/app/ivr_menu/ivr_menus.php	internal	\N	34		\N	\N	\N	\N
1cd1d6cb-912d-db32-56c3-e0d5699feb9d	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Music on Hold	/app/music_on_hold/music_on_hold.php	internal	\N	35		\N	\N	\N	\N
c535ac0b-1da1-0f9c-4653-7934c6f4732c	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Queues	/app/dialplan/dialplans.php?app_uuid=16589224-c876-aeb3-f59f-523a1c0801f7	internal	\N	36		\N	\N	\N	\N
e4290fd2-3ccc-a758-1714-660d38453104	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Recordings	/app/recordings/recordings.php	internal	\N	37		\N	\N	\N	\N
b30f085f-3ec6-2819-7e62-53dfba5cb8d5	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Ring Groups	/app/ring_groups/ring_groups.php	internal	\N	38		\N	\N	\N	\N
6be94b46-2126-947f-2365-0bea23651a6b	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Schemas	/app/schemas/schemas.php	internal	\N	39		\N	\N	\N	\N
0347f82a-62a0-49d0-bacd-511d080c46d5	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Voicemail	/app/voicemails/voicemails.php	internal	\N	41		\N	\N	\N	\N
7fb0dd87-e984-9980-c512-2c76b887aeb2	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	Active Call Center	/app/call_center_active/call_center_queue.php	internal	\N	43		\N	\N	\N	\N
eba3d07f-dd5c-6b7b-6880-493b44113ade	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	Active Calls	/app/calls_active/calls_active.php	internal	\N	44		\N	\N	\N	\N
2d857bbb-43b9-b8f7-a138-642868e0453a	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	Active Conferences	/app/conferences_active/conferences_active.php	internal	\N	45		\N	\N	\N	\N
6dd85c19-cb6b-5cca-bf32-499bbe936f79	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	Active Extensions	/app/calls_active/calls_active_extensions.php	internal	\N	46		\N	\N	\N	\N
450f1225-9187-49ac-a119-87bc26025f7d	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	Active Queues	/app/fifo_list/fifo_list.php	internal	\N	47		\N	\N	\N	\N
597c483a-51a9-f95a-8d54-ea7d87ada2b8	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	Agent Status	/app/call_center/call_center_agent_status.php	internal	\N	48		\N	\N	\N	\N
781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	Log Viewer	/app/log_viewer/log_viewer.php	internal	\N	51		\N	\N	\N	\N
17dbfd56-291d-8c1c-bc43-713283a9dd5a	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	Registrations	/app/registrations/status_registrations.php	internal	\N	52		\N	\N	\N	\N
c28f14e9-e5ad-e992-0931-d5f5f0db6a79	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	Services	/app/services/services.php	internal	\N	53		\N	\N	\N	\N
b7aea9f7-d3cf-711f-828e-46e56e2e5328	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	SIP Status	/app/sip_status/sip_status.php	internal	\N	54		\N	\N	\N	\N
5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	System Status	/app/system/system.php	internal	\N	55		\N	\N	\N	\N
05ac3828-dc2b-c0e2-282c-79920f5349e0	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	Traffic Graph	/app/traffic_graph/status_graph.php?width=660&height=330	internal	\N	56		\N	\N	\N	\N
1f59d07b-b4f7-4f9e-bde9-312cf491d66e	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	Adminer	<!--{project_path}-->/app/adminer/index.php	external	\N	58		\N	\N	\N	\N
7e174c3c-e494-4bb0-a52a-4ea55209ffeb	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	Backup	/app/backup/index.php	internal	\N	60		\N	\N	\N	\N
06493580-9131-ce57-23cd-d42d69dd8526	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	Command	/app/exec/exec.php	internal	\N	61		\N	\N	\N	\N
c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	Grammar Editor	/app/edit/index.php?dir=grammar	external	\N	65		\N	\N	\N	\N
eae1f2d6-789b-807c-cc26-44501e848693	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	PHP Editor	/app/edit/index.php?dir=php	external	\N	67		\N	\N	\N	\N
57773542-a565-1a29-605d-6535da1a0870	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	Provision Editor	/app/edit/index.php?dir=provision	external	\N	68		\N	\N	\N	\N
f1905fec-0577-daef-6045-59d09b7d3f94	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	Script Editor	/app/edit/index.php?dir=scripts	external	\N	69		\N	\N	\N	\N
47014b1d-13ad-921c-313d-ca42c0424b37	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	SIP Profiles	/app/sip_profiles/sip_profiles.php	internal	\N	70		\N	\N	\N	\N
a894fed7-5a17-f695-c3de-e32ce58b3794	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	SQL Query	/app/sql_query/sql_query.php	internal	\N	71		\N	\N	\N	\N
16013877-606a-2a05-7d6a-c1b215839131	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	XML Editor	/app/edit/index.php?dir=xml	external	\N	73		\N	\N	\N	\N
fd29e39c-c936-f5fc-8e2b-611681b266b5	b4750c3f-2a86-b00d-b7d0-345c14eca286	\N	Apps	/app/xml_cdr/xml_cdr.php	internal	\N	20		\N	\N	\N	\N
02194288-6d56-6d3e-0b1a-d53a2bc10788	b4750c3f-2a86-b00d-b7d0-345c14eca286	\N	System	/core/user_settings/user_dashboard.php	internal	\N	5		\N	\N	\N	\N
bc96d773-ee57-0cdd-c3ac-2d91aba61b55	b4750c3f-2a86-b00d-b7d0-345c14eca286	\N	Accounts	/core/users/index.php	internal	\N	10		\N	\N	\N	\N
0438b504-8613-7887-c420-c837ffb20cb1	b4750c3f-2a86-b00d-b7d0-345c14eca286	\N	Status	/app/calls_active/calls_active_extensions.php	internal	\N	25		\N	\N	\N	\N
594d99c5-6128-9c88-ca35-4b33392cec0f	b4750c3f-2a86-b00d-b7d0-345c14eca286	\N	Advanced	/core/domain_settings/domains.php	internal	\N	30		\N	\N	\N	\N
4d532f0b-c206-c39d-ff33-fc67d668fb69	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	Account Settings	/core/user_settings/user_edit.php	internal	\N	1		\N	\N	\N	\N
90397352-395c-40f6-2087-887144abc06d	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	Content Manager	/app/content/rsslist.php	internal	\N	2		\N	\N	\N	\N
da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	Menu Manager	/core/menu/menu.php	internal	\N	4		\N	\N	\N	\N
f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	Notifications	/core/notifications/notification_edit.php	internal	\N	6		\N	\N	\N	\N
92c8ffdb-3c82-4f08-aec0-82421ec41bb5	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	User Dashboard	/core/user_settings/user_dashboard.php	internal	\N	8		\N	\N	\N	\N
0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	User Manager	/core/users/index.php	internal	\N	14		\N	\N	\N	\N
1808365b-0f7c-7555-89d0-31b3d9a75abb	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	XMPP Manager	/app/xmpp/xmpp.php	internal	\N	15		\N	\N	\N	\N
b64b2bbf-f99b-b568-13dc-32170515a687	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	Inbound Routes	/app/dialplan/dialplans.php?app_uuid=c03b422e-13a8-bd1b-e42b-b6b9b4d27ce4	internal	\N	19		\N	\N	\N	\N
67aede56-8623-df2d-6338-ecfbde5825f7	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	Time Conditions	/app/dialplan/dialplans.php?app_uuid=4b821450-926b-175a-af93-a03c441818b1	internal	\N	40		\N	\N	\N	\N
032887d2-2315-4e10-b3a2-8989f719c80c	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	CDR Statistics	/app/xml_cdr/xml_cdr_statistics.php	internal	\N	49		\N	\N	\N	\N
4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	Extension Summary	/app/xml_cdr/xml_cdr_extension_summary.php	internal	\N	50		\N	\N	\N	\N
ef00f229-7890-00c2-bf23-fed5b8fa9fe7	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	App Manager	/core/apps/apps.php	internal	\N	59		\N	\N	\N	\N
ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	Databases	/core/databases/databases.php	internal	\N	62		\N	\N	\N	\N
834b2739-9e99-4345-9b0b-7ec3ca332b67	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	Default Settings	/core/default_settings/default_settings.php	internal	\N	63		\N	\N	\N	\N
4fa7e90b-6d6c-12d4-712f-62857402b801	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	Domains	/core/domain_settings/domains.php	internal	\N	64		\N	\N	\N	\N
3b4acc6d-827b-f537-bf21-0093d94ffec7	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	Group Manager	/core/users/groups.php	internal	\N	66		\N	\N	\N	\N
8c826e92-be3c-0944-669a-24e5b915d562	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	Upgrade	/core/upgrade/index.php	internal	\N	72		\N	\N	\N	\N
\.


--
-- Data for Name: v_menu_languages; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_menu_languages (menu_language_uuid, menu_uuid, menu_item_uuid, menu_language, menu_item_title) FROM stdin;
ffa7e462-5622-455c-906d-f1817b912095	b4750c3f-2a86-b00d-b7d0-345c14eca286	1f59d07b-b4f7-4f9e-bde9-312cf491d66e	en-us	Adminer
de62b910-c39e-4407-9968-fab0ea8dcfef	b4750c3f-2a86-b00d-b7d0-345c14eca286	1f59d07b-b4f7-4f9e-bde9-312cf491d66e	es-cl	Administrador
811572a7-8c76-43fb-b5a3-7a22ec9e3c1c	b4750c3f-2a86-b00d-b7d0-345c14eca286	1f59d07b-b4f7-4f9e-bde9-312cf491d66e	de-de	
18e71ceb-86b7-41c2-8e0d-34e9f81a9389	b4750c3f-2a86-b00d-b7d0-345c14eca286	1f59d07b-b4f7-4f9e-bde9-312cf491d66e	de-ch	
87dcf98c-2826-4e97-a1da-bb20e6652f0d	b4750c3f-2a86-b00d-b7d0-345c14eca286	1f59d07b-b4f7-4f9e-bde9-312cf491d66e	de-at	
cec5c3b3-ae6f-4860-8d17-73238008c6f1	b4750c3f-2a86-b00d-b7d0-345c14eca286	1f59d07b-b4f7-4f9e-bde9-312cf491d66e	fr-fr	Admin BDD
4d031318-1f47-4aaf-9174-6cd454d2c915	b4750c3f-2a86-b00d-b7d0-345c14eca286	1f59d07b-b4f7-4f9e-bde9-312cf491d66e	fr-ca	
e1150de0-2679-42e7-97d6-f74de33324d1	b4750c3f-2a86-b00d-b7d0-345c14eca286	1f59d07b-b4f7-4f9e-bde9-312cf491d66e	fr-ch	
dd4f6899-b91f-4ed7-9cb5-6845590eaba3	b4750c3f-2a86-b00d-b7d0-345c14eca286	1f59d07b-b4f7-4f9e-bde9-312cf491d66e	pt-pt	Administrador
3e5a490b-e9c3-4c8e-b266-d4b114bcf982	b4750c3f-2a86-b00d-b7d0-345c14eca286	1f59d07b-b4f7-4f9e-bde9-312cf491d66e	pt-br	
9aecf2e9-7176-471e-bb70-606ffe7cbc7f	b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	en-us	Backup
ea96374e-309c-4747-8b16-43aa2d9cf973	b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	es-cl	
014f4d75-a166-4f20-9337-f2ace7a54ef0	b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	es-mx	
16814700-3d19-400c-9c87-17d526ec1be6	b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	de-de	
22761296-afc1-4c45-909e-ab93c16fc4d8	b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	de-ch	
2ba18312-5647-4d75-9eb0-733e42923abc	b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	de-at	
fb271aa3-9d01-404b-a501-73e9aee31ce6	b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	fr-fr	
0eef21e2-4302-4ce2-82ff-58906fe607af	b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	fr-ca	
9036512f-0083-4f0f-9dc4-f49242fea9bb	b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	fr-ch	
d51b96e4-8a08-420c-96c7-78fea275785a	b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	pt-pt	
eb489e5f-57e6-4ebe-a081-762563dc934f	b4750c3f-2a86-b00d-b7d0-345c14eca286	7e174c3c-e494-4bb0-a52a-4ea55209ffeb	pt-br	
edb10006-9172-47eb-9157-d7132017fb32	b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	en-us	Call Block
035bd4e3-7d39-4738-b179-b89fa7bacf7f	b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	es-cl	Bloqueo de llamadas
a99d609c-fa11-4f39-8d68-fa53e5608fd1	b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	de-de	
1e2d68ec-c6ff-4306-ae4d-6a516c9ca661	b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	de-ch	
5f8e52d0-16c0-474b-bdcc-74e4b80a881c	b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	de-at	
5d3427bc-fd5b-4e38-9197-e4ccfe338273	b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	fr-fr	Blocage d'appel
5262b050-bac4-44ae-8f82-ae3506884f49	b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	fr-ca	
2b103ab4-fc3b-4499-af82-de2b33d9292f	b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	fr-ch	
7dfdf8b5-25c8-456e-b09f-f3862089bd6d	b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	pt-pt	Bloqueio de Chamadas
1b0e8398-1c0c-4941-ad00-5cbcc409840f	b4750c3f-2a86-b00d-b7d0-345c14eca286	29295c90-b1b9-440b-9c7e-c8363c6e8975	pt-br	
39edf470-a971-4bec-9fe1-a5a829335d8c	b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	en-us	Call Broadcast
77328aa8-b1f1-467d-8f2e-e2b11e534d8b	b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	es-cl	Llamada Broadcast
67580b57-b0e7-4111-911a-b992c5ca4550	b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	de-de	
1f755261-1b3b-4737-b24f-21cd9d432862	b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	de-ch	
bb976497-a980-4ae8-8162-0736c6861e37	b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	de-at	
103a6533-a19c-4df0-a2b5-6f12cce056cd	b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	fr-fr	Appels de masse
69da25c5-c26f-4bc4-98c1-b3bb83506dd8	b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	fr-ca	
00fbeac0-3efd-4b51-b91e-531a7d23fc82	b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	fr-ch	
1651cd29-0849-46be-9557-d850cfd06acb	b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	pt-pt	Chamada em Broadcast
aa5e0657-7995-4000-a97a-ff7884c8045a	b4750c3f-2a86-b00d-b7d0-345c14eca286	50153bbf-78c5-b49e-7bd9-4b3e4b1134e6	pt-br	
f69f4837-bd4a-4f87-b037-35c86f33c65f	b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	en-us	Call Center
be58bd9c-327a-4ed3-a045-94bb526f01bd	b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	es-cl	Centro de Llamadas
8311462f-d57f-4f19-b79d-00588d12b90d	b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	de-de	
c1d24e7a-2b35-493b-9359-23a8b6dd98a9	b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	de-ch	
213a60d2-de6f-4438-8b1d-dda7e95955a7	b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	de-at	
a4eb8b64-cb67-461b-a3cd-cc920dcc4f08	b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	fr-fr	Centre d'appel
2f384e80-c1cb-4bed-adc8-632a8bc559fd	b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	fr-ca	Centre d' appels
21356244-2726-4fd2-b8dd-47202484c730	b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	fr-ch	
04e42d7c-3f10-4f52-8c61-b1b4d062b587	b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	pt-pt	Centro de Chamadas
36978913-39eb-4e14-9eea-d2ca3d76f523	b4750c3f-2a86-b00d-b7d0-345c14eca286	6c072b29-5b6c-49fc-008e-95e24c77de99	pt-br	
9c1fe0ee-629b-4f9a-8a30-5b67a678bafc	b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	en-us	Agent Status
95c382d0-c39f-4b31-9979-c88a3ea2b58b	b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	es-cl	Estado de Agente
331ad490-de86-48d6-85b5-19d0edf39647	b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	de-de	
b4528030-ba55-4505-93d1-4ac11b04bd32	b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	de-ch	
54e0a1c3-0852-4d61-b353-7bcb2a9b369c	b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	de-at	
b9e0e344-b3f9-4885-9955-8f8489da9fcb	b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	fr-fr	État d'agent
d15508c5-fc72-4434-9488-1c460f5b6bea	b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	fr-ca	
b627df28-914a-480f-a74d-02ff3531b359	b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	fr-ch	
125ff270-9863-445e-966a-317e1c500f6c	b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	pt-pt	Estado do Agente
4f855c5d-84f4-4ce6-abca-d3d156b8fee9	b4750c3f-2a86-b00d-b7d0-345c14eca286	597c483a-51a9-f95a-8d54-ea7d87ada2b8	pt-br	
820f0c2a-d467-4dc7-b411-b4186bab46a0	b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	en-us	Active Call Center
89befdb5-8d43-4af6-b0dd-954e6b8f504f	b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	es-cl	Centro de Llamada Activo
ab4dc89a-9fc6-43f4-9a42-2c28f9559eed	b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	de-de	
446f6286-124f-46ac-a17c-3f1c640e8373	b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	de-ch	
44a92d2d-5da6-482d-be75-861189efbf6f	b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	de-at	
2a3f7502-6ad8-4d36-b489-ccd36fc3cbee	b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	fr-fr	Centre Appels Actifs
a2ef6c27-2dda-48e5-a9ed-89853d0dad94	b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	fr-ca	Centre d'Appels Active
0d65bc1b-fda6-4391-b3bd-59fbf3cc2a9a	b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	fr-ch	
fb081b19-9e18-4f13-971f-7b602d58edaf	b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	pt-pt	Centro de Chamadas Activo
8eca4354-385f-4197-a22b-9f526a8b3c37	b4750c3f-2a86-b00d-b7d0-345c14eca286	7fb0dd87-e984-9980-c512-2c76b887aeb2	pt-br	
d8988310-883f-4b0d-b586-4c425d9f310d	b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	en-us	Call Flows
bb4329c0-d547-4f95-ad48-d9ca516f6929	b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	es-cl	Flujo de Llamada
242bb454-623c-424c-a5b5-acf6db1cd860	b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	de-de	
7d23b784-8506-4c98-8ee7-02b8eb3b0cc2	b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	de-ch	
7a1133bd-feb5-414d-a1bc-10e31032c5dd	b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	de-at	
a5351bf0-924e-4d39-b9b5-3a362fb81457	b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	fr-fr	cinématiques d'Appel
a2dce273-9d6d-44ff-98b0-47fdba6031d8	b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	fr-ca	Circulation d'Appel
5c2eef8b-2e80-4e79-bc75-28cc92158830	b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	fr-ch	
3329af97-e22d-4670-9ac7-a0d28c5c2861	b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	pt-pt	
2d497d35-234d-4239-a6ab-211a4d0d5828	b4750c3f-2a86-b00d-b7d0-345c14eca286	b0939384-7055-44e8-8b4c-9f72293e1878	pt-br	
f0dfe959-8bae-42dd-b580-a149cc1ed4aa	b4750c3f-2a86-b00d-b7d0-345c14eca286	eba3d07f-dd5c-6b7b-6880-493b44113ade	en-us	Active Calls
5e5adcf6-aeb2-40b9-a562-d99b45180ac2	b4750c3f-2a86-b00d-b7d0-345c14eca286	eba3d07f-dd5c-6b7b-6880-493b44113ade	es-cl	Llamadas Activas
1f58f973-529c-4e77-a547-548fbf140a2f	b4750c3f-2a86-b00d-b7d0-345c14eca286	eba3d07f-dd5c-6b7b-6880-493b44113ade	de-de	
2821e8c7-23ee-4a71-b8f1-160c715c37e1	b4750c3f-2a86-b00d-b7d0-345c14eca286	eba3d07f-dd5c-6b7b-6880-493b44113ade	de-ch	
a6ce5193-bc27-4560-84f2-ad5fe72034a8	b4750c3f-2a86-b00d-b7d0-345c14eca286	eba3d07f-dd5c-6b7b-6880-493b44113ade	de-at	
dd532b1a-917a-4544-8056-c40b8f254593	b4750c3f-2a86-b00d-b7d0-345c14eca286	eba3d07f-dd5c-6b7b-6880-493b44113ade	fr-fr	Appels en cours
6ce84257-18ca-4705-9d4e-9f526abea0aa	b4750c3f-2a86-b00d-b7d0-345c14eca286	eba3d07f-dd5c-6b7b-6880-493b44113ade	fr-ca	
49e0f15f-af29-417c-a238-bd680fac89dd	b4750c3f-2a86-b00d-b7d0-345c14eca286	eba3d07f-dd5c-6b7b-6880-493b44113ade	fr-ch	
07466bd8-092f-4b7d-8e38-af1c2469c896	b4750c3f-2a86-b00d-b7d0-345c14eca286	eba3d07f-dd5c-6b7b-6880-493b44113ade	pt-pt	Chamadas Activas
6e4b4496-e996-43a2-932a-f88d0d73a139	b4750c3f-2a86-b00d-b7d0-345c14eca286	eba3d07f-dd5c-6b7b-6880-493b44113ade	pt-br	
9b49f254-69a8-48f0-b86a-da7bfce3a23f	b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	en-us	Active Extensions
0bbcdd0f-c1f5-4855-9c84-3927ee3a57f4	b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	es-cl	Extensiones activas
496fd2ab-7b28-438e-b8e0-3874a870ac5d	b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	de-de	
3b8dcdd2-524d-40f5-957a-b5408aeef8cf	b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	de-ch	
4801cdd3-480b-401e-a6ca-fb149cf1b235	b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	de-at	
b7eec154-7d5b-4826-8f00-36402f4f70de	b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	fr-fr	Extentions actives
df83a40f-9676-408f-94e2-3dc7b0ee60e8	b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	fr-ca	
2d9df268-853a-4605-a64f-fe00bd39c6dc	b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	fr-ch	
4353b682-f23e-460b-aaec-cf8c7f646644	b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	pt-pt	Extensões Activas
58493c5b-d4a0-4a9c-864a-efcde7e69d21	b4750c3f-2a86-b00d-b7d0-345c14eca286	6dd85c19-cb6b-5cca-bf32-499bbe936f79	pt-br	
239d9be2-4724-4d03-a0e5-68683c1b0133	b4750c3f-2a86-b00d-b7d0-345c14eca286	f862556f-9ddd-2697-fdf4-bed08ec63aa5	en-us	Click to Call
8b3ac798-0df9-49e5-9ca6-30602683e5f3	b4750c3f-2a86-b00d-b7d0-345c14eca286	f862556f-9ddd-2697-fdf4-bed08ec63aa5	es-cl	Pulse para Llamar
069de088-35d8-4753-bed6-609e13ee1e5b	b4750c3f-2a86-b00d-b7d0-345c14eca286	f862556f-9ddd-2697-fdf4-bed08ec63aa5	de-de	
c397f507-a890-40f9-8828-03e078814899	b4750c3f-2a86-b00d-b7d0-345c14eca286	f862556f-9ddd-2697-fdf4-bed08ec63aa5	de-ch	
f455d7bd-748c-4fd4-aac1-3aaebd62ce03	b4750c3f-2a86-b00d-b7d0-345c14eca286	f862556f-9ddd-2697-fdf4-bed08ec63aa5	de-at	
410c3fc0-fda1-48ee-a78a-7e7538e29616	b4750c3f-2a86-b00d-b7d0-345c14eca286	f862556f-9ddd-2697-fdf4-bed08ec63aa5	fr-fr	Cliquez pour Appeller
83a77d37-02c6-4bae-b408-82548c1c5ad8	b4750c3f-2a86-b00d-b7d0-345c14eca286	f862556f-9ddd-2697-fdf4-bed08ec63aa5	fr-ca	
bf244f00-009a-441c-bfbd-757f460200d9	b4750c3f-2a86-b00d-b7d0-345c14eca286	f862556f-9ddd-2697-fdf4-bed08ec63aa5	fr-ch	
f5192ffd-df8f-4b35-91b6-b5df1f5502de	b4750c3f-2a86-b00d-b7d0-345c14eca286	f862556f-9ddd-2697-fdf4-bed08ec63aa5	pt-pt	Clicar para Chamadas
ba632423-e8e4-435e-979b-c0593925c197	b4750c3f-2a86-b00d-b7d0-345c14eca286	f862556f-9ddd-2697-fdf4-bed08ec63aa5	pt-br	
1a0c5083-c412-4b62-a361-4f3d66357a0f	b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	en-us	Conference Center
bd22b1f7-afbe-4244-a1e6-42b1aa04ad8f	b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	es-cl	Cent. de Conferencias
354e0489-385b-4260-9acd-1f3ac6b44364	b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	de-de	
f0c82a60-3286-4fb3-a901-a5bc557adaf6	b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	de-ch	
09010235-c12a-45ed-ad5b-9f08188816ae	b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	de-at	
0b2d81d2-cf49-4698-9cbc-fd6a4d288ac7	b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	fr-fr	Centre de Conférences
cf065a32-6300-44b9-b074-259f87b88d69	b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	fr-ca	
79f4ae55-c098-4d4f-b8a5-fe68f217cee8	b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	fr-ch	
26a4b899-7c39-4c0d-b86d-ddb5117cbe0b	b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	pt-pt	Conferencias
0dafbd02-c8af-4327-ad4b-e724a29b0dfa	b4750c3f-2a86-b00d-b7d0-345c14eca286	95f88726-4706-43f0-b52b-9504a0b8046f	pt-br	
b90bbc49-4b40-47e9-a9ff-380f4381855c	b4750c3f-2a86-b00d-b7d0-345c14eca286	9f2a8c08-3e65-c41c-a716-3b53d42bc4d4	en-us	Conferences
c5807783-3354-449a-a3a5-ee8f55ea7421	b4750c3f-2a86-b00d-b7d0-345c14eca286	9f2a8c08-3e65-c41c-a716-3b53d42bc4d4	es-cl	Conferencias
fc4bacde-301d-4dcb-a57a-5198c2f02ebd	b4750c3f-2a86-b00d-b7d0-345c14eca286	9f2a8c08-3e65-c41c-a716-3b53d42bc4d4	de-de	
6d377a82-248a-49fd-b29c-57c593c3ef0a	b4750c3f-2a86-b00d-b7d0-345c14eca286	9f2a8c08-3e65-c41c-a716-3b53d42bc4d4	de-ch	
f6c6d740-663d-4044-81ed-4c4201080a4e	b4750c3f-2a86-b00d-b7d0-345c14eca286	9f2a8c08-3e65-c41c-a716-3b53d42bc4d4	de-at	
e8368a3f-1c8c-4f45-a80d-9fc725014fdf	b4750c3f-2a86-b00d-b7d0-345c14eca286	9f2a8c08-3e65-c41c-a716-3b53d42bc4d4	fr-fr	Conférences
73f116b8-42ed-40d5-942d-fb577141c12a	b4750c3f-2a86-b00d-b7d0-345c14eca286	9f2a8c08-3e65-c41c-a716-3b53d42bc4d4	fr-ca	
0aa75e27-4211-47ad-a69d-11c3e7155a2f	b4750c3f-2a86-b00d-b7d0-345c14eca286	9f2a8c08-3e65-c41c-a716-3b53d42bc4d4	fr-ch	
74da3c88-4900-489f-9482-cb2f0d0fd7c7	b4750c3f-2a86-b00d-b7d0-345c14eca286	9f2a8c08-3e65-c41c-a716-3b53d42bc4d4	pt-pt	Conferencias
8a6bbee1-c6d8-4abf-9f20-3416f15671bc	b4750c3f-2a86-b00d-b7d0-345c14eca286	9f2a8c08-3e65-c41c-a716-3b53d42bc4d4	pt-br	
8b7886c0-2e04-4210-8607-a15a3cb08597	b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	en-us	Active Conferences
1dd54bd6-e617-454d-9cec-fae10738ea65	b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	es-cl	Conferencias Activas
3a299f1d-0967-43f3-bf1a-a8e86bec1451	b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	de-de	
263ee565-eb32-4fd4-ab14-c4e260609a03	b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	de-ch	
3b265ab5-13e8-410c-863f-9f81e3a94f00	b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	de-at	
691515e7-5f99-4836-8bd0-3ec33379435c	b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	fr-fr	Conferences en cours
15ba6b4f-a164-4136-a615-468fe57bb0eb	b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	fr-ca	
3b389ed3-4c19-4d1b-99cb-82bf7d7199d4	b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	fr-ch	
f1bba5cd-88b6-41b2-b16f-4cbcb43eb8b6	b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	pt-pt	Conferencias Activas
3407a9e7-7554-4ba3-85ef-5d67bf1ba97b	b4750c3f-2a86-b00d-b7d0-345c14eca286	2d857bbb-43b9-b8f7-a138-642868e0453a	pt-br	
d2fd0969-c89b-4f45-b6f3-b301aa577fad	b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	en-us	Contacts
a05589d6-1153-4970-be75-08f97b998f37	b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	es-cl	Contactos
49b75e21-b35b-4e66-8dae-ae028cda0def	b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	de-de	
381b7402-5215-481e-adb6-5d27e7c70a61	b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	de-ch	
d07e80da-9c0e-4152-939c-e127a22aa941	b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	de-at	
1f77c9ec-6818-4cc2-90e0-f585c305d840	b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	fr-fr	Contacts
abf09660-dc68-470b-b519-e4a9d19d432c	b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	fr-ca	
b6b9065d-79fa-4b4a-a70a-fae4ce93dca5	b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	fr-ch	
ac689855-e686-47e5-b670-6877a7a10968	b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	pt-pt	Contactos
b156c9e2-576b-4807-82c3-d5284137d8a8	b4750c3f-2a86-b00d-b7d0-345c14eca286	f14e6ab6-6565-d4e6-cbad-a51d2e3e8ec6	pt-br	
34700d48-b63b-476c-b72d-3bc75af30cd7	b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	en-us	Content Manager
31e03280-d4f7-4a49-b2a1-b0c2a6b551a3	b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	es-cl	Gestor de Contenido
f8a8172b-9bb8-4078-bd24-239ff040ab61	b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	es-mx	Administrador de Contenido
3e695355-daf8-4f92-930c-a5599c6ca84a	b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	de-de	
e8a21570-43b2-4ec4-afc7-58df21e8979d	b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	de-ch	
f5716312-4877-45e9-8191-49ab8b816c6e	b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	de-at	
bbb69054-da75-4770-8783-ddedf1f50e78	b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	fr-fr	Contenu
77e47089-7d6e-43d5-b2f8-fb1072845674	b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	fr-ca	Administrateur
92a982a6-0ad7-4dce-afb3-f2f54b263fc7	b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	fr-ch	
de30e93a-1e99-4644-80f4-5ca03da83525	b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	pt-pt	Gestor de Conteúdo
75d3a81e-8697-4b45-9437-b38be5d744ff	b4750c3f-2a86-b00d-b7d0-345c14eca286	90397352-395c-40f6-2087-887144abc06d	pt-br	
4af9504b-1f84-4697-ade6-15f48015d21d	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	en-us	Destinations
31451649-7410-4a33-aedc-91207bd5c1de	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	es-cl	Destinos
5fc2be91-0fe7-4c24-a24f-4c833e7f1da2	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	es-mx	Destinos
1fdd640c-fdd7-4588-873f-0e3a1255f1f2	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	de-de	
1e1d30cd-5043-4318-a3b3-ca7da7401f26	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	de-ch	
37699ae4-a263-4973-9aef-7cb3496a2381	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	de-at	
f5a165bc-e961-4ff8-833e-99a501133fc0	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	fr-fr	Destinations
07954c3b-5b3a-4219-8afc-3facf88efb17	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	fr-ca	Cibler
c13ea70a-022b-4707-9671-f163798d90df	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	fr-ch	
b064f32c-4a65-4b10-9474-28ef51071b06	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	pt-pt	Destinos
0309a9de-05ea-4068-9161-7cd5a5942d3f	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd2a708a-ff03-c707-c19d-5a4194375eba	pt-br	
5981ddcf-d4de-4e21-b0f8-e55093d00388	b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	en-us	Devices
1765c571-897b-4aa1-996e-75670587176c	b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	es-cl	Dispositivos
f15c08d6-5920-4a32-96f7-9f51bce5b76f	b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	es-mx	Dispositivos
45e1504d-4274-4a67-97ff-92121201355f	b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	de-de	
eaaf460e-41a5-42e1-9f9c-2252f5959b21	b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	de-ch	
92a3e0b8-90d1-4432-9753-5e4b386d03fe	b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	de-at	
28958915-22b5-488b-9565-2229a1f56e6c	b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	fr-fr	Equipements
9dadcbe1-0494-4c00-97f4-d4b2ca76e354	b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	fr-ca	Dispositifs
55ebf1fa-aea9-42f8-8e03-a156db3ce8ad	b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	fr-ch	
cb96d9eb-e4bc-43ef-b7e0-57d233675276	b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	pt-pt	Telefones
d7cdb691-11e1-421a-8bf4-e1e23344d6c6	b4750c3f-2a86-b00d-b7d0-345c14eca286	f9dce498-b7f9-740f-e592-9e8ff3dac2a0	pt-br	
3a6ad46a-4205-4c32-9975-8291066ae0ef	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	en-us	Dialplan
4e5541e0-e24b-4827-909b-9023ac60bc65	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	es-cl	Plan de Marcado
34164f34-5ccb-495f-b864-a5c350e4d384	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	es-mx	Plan de Marcado
4a961b08-b9f4-4260-b9f3-28cf739141e6	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	de-de	
23ae4aea-e292-4ed7-a15b-5f0686ffbc7a	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	de-ch	
4642caa4-965f-449f-b349-5c4f1facd06f	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	de-at	
f256298c-6dcb-4f0c-be78-d68d5ef1fe65	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	fr-fr	Dialplan
023c30c5-bcb2-4124-ba26-15b7337d463c	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	fr-ca	
26979488-b931-477e-b245-4da94dc7263a	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	fr-ch	
c89bfb8b-bcf1-4d9f-844e-522e95d6620d	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	pt-pt	Dialplan
425b27f0-bb6d-446e-8d1b-a37b71c96fb9	b4750c3f-2a86-b00d-b7d0-345c14eca286	b94e8bd9-9eb5-e427-9c26-ff7a6c21552a	pt-br	
3a351101-7367-4943-83a5-8907ec57e87b	b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	en-us	Dialplan Manager
ef429902-6f11-4bea-9845-5ebde0f895b4	b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	es-cl	Gest. de Plan de Marcado
b72aa92f-4a2b-4c9d-896d-6a3ffc73d480	b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	es-mx	Administrador de Planes de Marcado
0f4796a3-6dac-4a9b-a6cc-fd46ba074a5b	b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	de-de	
d9441d42-c44a-4389-bc42-1236fc5be530	b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	de-ch	
a9f685e2-5a0b-4fc3-8c3c-d82bad68425a	b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	de-at	
e6b6c81d-3260-4da4-85b5-c59b7f9618c7	b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	fr-fr	Gestion du Dialplan
603ed6bb-a044-46bb-9078-5fe3e77a0f66	b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	fr-ca	Administrateur de Dialplan
2e13fb5b-d1bd-468c-9cb2-c17184996295	b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	fr-ch	
754883c1-cc4f-4b04-a4d9-0d5b63185859	b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	pt-pt	Gestor de Dialplan
805e0738-1932-4cd3-a438-bd7b1f152af3	b4750c3f-2a86-b00d-b7d0-345c14eca286	52929fee-81d3-4d94-50b7-64842d9393c2	pt-br	
31bbb756-ee53-44e1-8ea8-6d71c2392323	b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	en-us	Inbound Routes
d7d72a7a-2a7b-433a-b2fc-6bd3e5f2d79f	b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	es-cl	Rutas de entrada
39cd3a61-c091-4f23-a689-64f64da52931	b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	es-mx	Rutas de entrada
031583b1-b13e-4cb1-82d6-e9d92648a7c1	b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	de-de	
9b8d0992-bb22-42c6-a432-a662582b316e	b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	de-ch	
2f57b590-4ad6-4db1-a6bd-b3a631277d19	b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	de-at	
efb9be2f-8753-4adb-b83a-9ced6f6c13dc	b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	fr-fr	Routes Entrantes
7e290bf8-c56d-43e0-aa09-b4214888aec1	b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	fr-ca	
a5ee8b5d-286f-4169-90b8-20731267fd42	b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	fr-ch	
5eaa4366-7473-45c2-abc6-2baf6fb63b7f	b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	pt-pt	Rotas de Entrada
b2627215-634b-40b8-add5-972a0f9a8996	b4750c3f-2a86-b00d-b7d0-345c14eca286	b64b2bbf-f99b-b568-13dc-32170515a687	pt-br	
71045625-7732-41db-88d1-b8f49a095bc5	b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	en-us	Outbound Routes
71fb45ec-7c44-43bb-a7b5-492523f58add	b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	es-cl	Rutas de salida
4f8b2116-1362-47e7-ae15-0659550f7b2e	b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	es-mx	Rutas de salida
d7b884e6-17a5-492f-9125-deeeb5146c0e	b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	de-de	
3ce22c95-1dcc-42a8-aac7-560ba093479e	b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	de-ch	
64775414-9322-467a-9f2e-fceee92364f4	b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	de-at	
ac2905c3-3fbc-490e-b59e-99fe1ef496e4	b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	fr-fr	Routes Sortantes
1c2bb910-8245-468b-bc70-f1784d211acb	b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	fr-ca	
90925cfe-8918-496e-a254-4228d9c4116f	b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	fr-ch	
4ac06b82-689e-47c4-a1d2-cf45f49accd5	b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	pt-pt	Rotas de Saída
302e5adf-8658-4e02-b587-fa3c0139a717	b4750c3f-2a86-b00d-b7d0-345c14eca286	17e14094-1d57-1106-db2a-a787d34015e9	pt-br	
057ae192-e624-406d-b3fd-130952f07d80	b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	en-us	Script Editor
6a4a05b6-7e13-4b1d-809e-d4827f5e0e1e	b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	es-cl	Editor de Scripts
f3379e4f-ffa5-4fdc-aea7-010a93be0951	b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	es-mx	
f0592225-7e04-46f9-b443-1cbb970eec58	b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	de-de	
e96e2a5b-7494-4a61-aa5d-206c17d82ba2	b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	de-ch	
8ae444d7-9f2d-4c55-ab23-691cb4537374	b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	de-at	
be442a2c-14f7-4736-a249-4dc3bb6de481	b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	fr-fr	Editeur de script
b3acf2fb-3e19-44f5-80ef-c6a942328ca7	b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	fr-ca	
3ef55ba8-8a04-4398-ba61-bd055f7f41c4	b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	fr-ch	
bf4f87b6-a227-4026-bf86-c13ee5a72e41	b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	pt-pt	Editor de Scripts
a91cc07b-91de-46be-af2f-153b1008a323	b4750c3f-2a86-b00d-b7d0-345c14eca286	f1905fec-0577-daef-6045-59d09b7d3f94	pt-br	
6e8a803a-b15d-4c5b-89c5-035a78c0a8da	b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	en-us	XML Editor
7b61d495-1f20-4fe7-b362-707e8722adf5	b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	es-cl	Editor XML
bbc3b564-ec02-4538-9ac2-49e29bd68c65	b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	es-mx	
17015f80-b582-496e-b867-a2fec2eb568a	b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	de-de	
aca644a0-1c17-4d6a-9290-2967dbf4624e	b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	de-ch	
98019b5e-d12c-4d14-ae38-35884a9dc28e	b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	de-at	
201c31dc-c636-4310-9f4f-3b3abdeabecd	b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	fr-fr	Editeur XML
6607a110-561d-4ea1-a208-71babc8ab9b5	b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	fr-ca	
40aa1145-e8f1-49f3-bd8e-f64323f5cb75	b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	fr-ch	
a58d1783-6927-4034-82dc-bf0ecd2a68d1	b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	pt-pt	Editor XML
859f71d0-b670-423a-a911-5327682cbea8	b4750c3f-2a86-b00d-b7d0-345c14eca286	16013877-606a-2a05-7d6a-c1b215839131	pt-br	
d6c2fa59-89e3-4169-9a12-129fdd81571f	b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	en-us	Provision Editor
3ec529d7-cf1b-4c99-a47a-4493e015cbda	b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	es-cl	Editor de Provisionamiento
575ee71d-c359-4cf3-acdc-21f9bb278978	b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	es-mx	
885d6da0-cacc-438a-b647-b7a7d5dd8e38	b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	de-de	
a6760a46-5943-4855-8229-2bf0f2f8fb45	b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	de-ch	
43cd2f03-acac-43c2-abbc-d37efe0b3add	b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	de-at	
6f102ca9-f3e1-4e18-acc5-9faf982c98fc	b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	fr-fr	Provisioning
a7fde661-f63a-406c-8dd0-4f2ac7584c72	b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	fr-ca	
08b12435-2463-4f9f-8af1-68d5961535c1	b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	fr-ch	
5770b1dd-6d81-4930-a1ef-dbcdd7613e69	b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	pt-pt	Editor de Provisionamento
7b7abac5-16b4-4cc0-aaae-35b005dfc20b	b4750c3f-2a86-b00d-b7d0-345c14eca286	57773542-a565-1a29-605d-6535da1a0870	pt-br	
47adb377-a33c-4ea8-b818-4adc71933bbe	b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	en-us	PHP Editor
0e69a813-cd93-463b-8289-daf3d7d66706	b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	es-cl	Editor PHP
98befc45-c67e-425a-a04e-a6216afcad74	b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	es-mx	
712909b0-72d8-4398-a465-537c4a964b17	b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	de-de	
95d184e1-a458-420c-8a98-2c3dbb91a2c3	b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	de-ch	
7ebe9c20-7e99-4072-9a59-66811476552c	b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	de-at	
e335bef2-928e-4ecd-9e6f-83103e4be087	b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	fr-fr	Editeur PHP
76717aa0-5af8-42fb-a9fe-063da273a430	b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	fr-ca	
b8dc4d4c-68ea-4be0-bc47-3fe80a10f493	b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	fr-ch	
a2318a91-f9d6-4e33-9cdc-d84c3a3c17ef	b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	pt-pt	Editor de PHP
b4935058-4fa9-471d-95cb-bba6857dc3c5	b4750c3f-2a86-b00d-b7d0-345c14eca286	eae1f2d6-789b-807c-cc26-44501e848693	pt-br	
c94607a4-4335-4f99-a859-fdf7ceff0c8a	b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	en-us	Grammar Editor
95d207b9-dfe9-4853-a411-c58554931fc0	b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	es-cl	Editor Gramático
76b9b3ae-3c66-4f4d-9969-c39e7545fb8c	b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	es-mx	Editor Gramático
f7e54df2-17f9-4b5f-9f53-91374731c2a1	b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	de-de	
d3a25ca2-7e8e-49b9-8f59-02da118a3738	b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	de-ch	
3ab3d78c-65ae-4b68-ab6b-225015d87533	b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	de-at	
daddf015-0a72-42b6-91a5-9cf9952dee72	b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	fr-fr	Editeur de Grammaire
c27d8041-66ef-4a1f-96b8-8af8eebf1ac6	b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	fr-ca	
45514910-f8a0-48ad-8fac-9719281a81e1	b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	fr-ch	
b3f6d4eb-1c1a-48d6-8eab-63b6008e832b	b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	pt-pt	Editor Gramático
7e95bc6d-26fc-4c56-a27d-d6c285eaa63c	b4750c3f-2a86-b00d-b7d0-345c14eca286	c3db739e-89f9-0fa2-44ce-0f4c2ff43b1a	pt-br	
11627fec-8cf2-4028-a458-f9183e332917	b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	en-us	Command
6c8aea89-8dcc-4506-9c12-3659b4442e3c	b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	es-cl	Comando
499fedc5-0b7d-4778-a167-3ac5fd64a5a4	b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	es-mx	Comando
a18a1df0-1424-4405-a3c3-2314345f88a9	b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	de-de	
a36debbe-5e12-4a0f-be86-ed943e435687	b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	de-ch	
848d99de-0e27-4680-b295-82b253a82744	b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	de-at	
eda8a4e3-213a-4586-abfc-7b4a40657fdd	b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	fr-fr	Commande
23099ff2-0e11-4f4e-b1ba-eb6ba0e31230	b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	fr-ca	Command
41974f50-22df-4b33-afa4-0cbb7c4fdcc2	b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	fr-ch	
9f90f500-e028-4baa-a2ff-2d06d8dad002	b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	pt-pt	Comandos
be5b2e2c-249f-4f1d-8ada-75dd78917fe3	b4750c3f-2a86-b00d-b7d0-345c14eca286	06493580-9131-ce57-23cd-d42d69dd8526	pt-br	
d1224bd5-17b3-4cb0-9d17-39c0d6b1a086	b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	en-us	Extensions
3be92695-f3cc-4331-8fbb-d09f893dcd51	b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	es-cl	Extensiones
f2244871-b73a-4b0f-a8e1-14f9d57ffeed	b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	es-mx	Extensiones
55f87ba9-9f8a-4208-b3f8-81db2cb9a4ca	b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	de-de	
2a04e60c-ecf0-4813-8b50-12de6fd094fa	b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	de-ch	
96d2b7df-0a3b-4b0e-9b9f-58f19647b5f5	b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	de-at	
90894fb5-499a-48ea-85b4-e74adb255eff	b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	fr-fr	Extensions
0f97f958-3c85-4319-9c5d-51788fa43f04	b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	fr-ca	Post téléphonique
f24b5426-f62e-4aa2-9e7b-03507395edcd	b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	fr-ch	
3b04613e-901a-44ab-a96f-8bd63a8c5c1c	b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	pt-pt	Extensões
5173a92f-bb84-4941-84ae-934928c6432a	b4750c3f-2a86-b00d-b7d0-345c14eca286	d3036a99-9a9f-2ad6-a82a-1fe7bebbe2d3	pt-br	
9a97fa8d-904b-4bed-a072-5527e85ec61f	b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	en-us	Fax Server
5aebabb5-63af-4de5-a39a-ef80c9859068	b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	es-cl	Servidor de Fax
d2647ada-78c6-4bf5-a4e7-3b6980226252	b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	es-mx	Servidor de Fax
324b76be-966e-4938-8a8d-4f122897d5df	b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	de-de	Faxserver
99eba66d-ee55-42f1-8a85-0ba2f0f66a0c	b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	de-ch	Faxserver
7de922ac-506e-4fc6-85cc-cccb8b9d405b	b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	de-at	Faxserver
6ceb9d72-4267-4d97-9889-85ba63bb0ab2	b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	fr-fr	Serveur Fax
22d3441c-dcd4-444f-963a-609367b72579	b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	fr-ca	Serveur du fax
e9d64378-f055-47ca-9d19-3a1097ec48cb	b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	fr-ch	
c8f2bd63-53c3-42ed-b0ba-802743293cc3	b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	pt-pt	Servidor de Fax
a626514c-f836-4788-b915-3a2518a12b73	b4750c3f-2a86-b00d-b7d0-345c14eca286	9c9642e4-2b9b-2785-18d0-6c0a4ede2b2f	pt-br	
a3fb0c6d-ef4a-46de-a45f-7df972b1014a	b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	en-us	Queues
93713c70-cad0-41ff-bacd-478d2bf273b3	b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	es-cl	Colas
72f9ba9e-8435-41c5-8290-df9feb95138c	b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	es-mx	Colas
42b71f51-2f5d-4424-ac98-6c72d9165df1	b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	de-de	
2f25e1ef-d756-4076-b905-938e9f788ccb	b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	de-ch	
7a715cce-d05e-48e6-8c4b-bace082b7254	b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	de-at	
3ee713ff-b734-4d42-8d87-628a46f44dad	b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	fr-fr	Queues
91945cf8-d060-4594-bac1-325f9811ec6a	b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	fr-ca	
f82455bf-73c3-4b25-a64a-e23acf0b47f3	b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	fr-ch	
3525167d-3870-4874-bcc3-d9fd438f262a	b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	pt-pt	Filas
ca4ef140-b39b-4fe7-9dc6-a7541f9d85bd	b4750c3f-2a86-b00d-b7d0-345c14eca286	c535ac0b-1da1-0f9c-4653-7934c6f4732c	pt-br	
c855f444-4f89-459d-880a-4f76fe28fbfe	b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	en-us	Active Queues
69cfaa05-d2ef-4a9b-a356-165982555910	b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	es-cl	Colas activas
02b40fc6-d78d-48ac-9a7c-4210faaf5276	b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	es-mx	Colas activas
6ce615e9-b015-45a0-9b19-e7108529566e	b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	de-de	
0e621141-1229-4f67-87eb-432c9a59014a	b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	de-ch	
bdc25fd4-3c8c-4de8-9b22-d213f5f65a0e	b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	de-at	
8a2153d4-3f3a-4bf7-9af9-97dd84817d05	b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	fr-fr	Queues actives
eb1a18f0-309d-4c7d-97a5-6056a2d62fc6	b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	fr-ca	Queues actives
aa8db4d3-9252-4586-a6e3-fe61dcec96e5	b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	fr-ch	
a3012332-6050-4530-8521-e74bb23f184d	b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	pt-pt	Filas Activas
58de349c-c25a-4c1a-b7c6-ecefbc8bddf2	b4750c3f-2a86-b00d-b7d0-345c14eca286	450f1225-9187-49ac-a119-87bc26025f7d	pt-br	
0678456a-ba63-4b6b-a9fe-ee3699d30384	b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	en-us	Follow Me
135c7591-9218-441f-bff9-2058fc6eb4bf	b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	es-mx	Sígueme
d0ef7201-301b-4ddb-b36e-6e40296bbb01	b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	de-de	
22c4cfb2-9403-49c0-9d76-eaa18c60c01d	b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	de-ch	
25487286-75a3-4944-89de-29d327a26916	b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	de-at	
53dc4df1-6027-471a-960e-b0194c4e4e9f	b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	fr-fr	Follow Me
efe5c32a-889d-4e14-bd4f-b0754b98c0e6	b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	fr-ca	
b4b2a988-e686-4b31-a128-78b09b5af7f7	b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	fr-ch	
add5f1c4-de47-44cc-ac22-32d594241dec	b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	pt-pt	
cea4c0a7-9494-4167-a1e7-615bfc9c3b14	b4750c3f-2a86-b00d-b7d0-345c14eca286	a1144e12-873e-4722-9818-02da1adb6ba3	pt-br	
a7efaf3b-3910-4587-9382-61e2c2c7e73b	b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	en-us	Gateways
99e13798-7eda-44fa-b777-ef0e4600fd36	b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	es-cl	Pasarelas
dbff3617-068a-47c1-8444-ad9cc77cf304	b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	es-mx	Pasarelas
7263094c-7b61-4fbc-835f-463cfbba0c89	b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	de-de	
2f845b58-3d1f-4457-b13e-89f22bc1e2d7	b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	de-ch	
da04b212-72fd-4674-8d53-4ca848e94e4b	b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	de-at	
3e350d87-0f03-45f9-a7d9-599d96122205	b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	fr-fr	Passerelles
b958d97e-b4e8-44b4-a7f2-53af0120a6b1	b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	fr-ca	
35678fe8-b096-4748-8d06-04dc0b856cbf	b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	fr-ch	
111bbf80-ba6b-4720-9929-e7c21fa275e9	b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	pt-pt	Gateways
1ff73cf4-89a1-4a1d-86d5-9e2f29c96236	b4750c3f-2a86-b00d-b7d0-345c14eca286	237a512a-f8fe-1ce4-b5d7-e71c401d7159	pt-br	
b2664bb7-4cb4-4b6d-9ecb-e81f0cb29177	b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	en-us	Hot Desking
56b5182d-af6d-48fb-8448-dc9d06d99fc6	b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	es-cl	Escritorio remoto
78ce87b5-051e-4df4-8609-f7b01ec6faf3	b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	es-mx	Escritorio remoto
3f47c309-9619-4503-ad51-2429fb0206bb	b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	de-de	
14f806d4-01f2-4f85-8d60-f625a03a8253	b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	de-ch	
b2f9c5f7-7ecf-4acd-8b85-bd743f5fa868	b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	de-at	
17d5fd38-2e50-471f-b875-2d7c16f6d282	b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	fr-fr	Itinérance
b9cb6d88-8a11-4085-9e9b-1fdacf95ab44	b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	fr-ca	Bureau Lointain
cb542676-7daa-4c52-a23b-306f7fd442bc	b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	fr-ch	
3d72247f-7d6d-4a11-827f-2e120f530e60	b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	pt-pt	Escritório Remoto
43855dbc-f78b-442a-b17e-7f76d3b342f4	b4750c3f-2a86-b00d-b7d0-345c14eca286	baa57691-37d4-4c7d-b227-f2929202b480	pt-br	
94698ca1-4a3f-4453-8a17-0c1fd93ba6fc	b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	en-us	IVR Menu
3eeadfde-663a-4479-94ee-f7e13079963c	b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	es-cl	Menú IVR
9ec0427a-13aa-49b4-9b00-5004d951b9e3	b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	es-mx	
f645fe21-7dfc-4845-9cd4-8dd923eb7d92	b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	de-de	
1d4cda66-92ad-4f81-a07c-04de5b00039f	b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	de-ch	
b6f3bbe3-12ad-4266-aa6f-bba197b2a7ee	b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	de-at	
5db02b10-63ac-42e5-ac1c-7c4c9c382bed	b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	fr-fr	Menu SVI
0beab1f7-a5ef-498f-8ddc-4236c5957c0c	b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	fr-ca	
1e8316ee-f635-4478-8ee7-2529a8333800	b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	fr-ch	
562c08f7-d556-4c85-b0e4-888dabc00597	b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	pt-pt	Menu de IVR
3104e6cc-a727-4132-8438-c862d41a539a	b4750c3f-2a86-b00d-b7d0-345c14eca286	72259497-a67b-e5aa-cac2-0f2dcef16308	pt-br	
f0c1a5d0-e177-4aaa-8126-2f84ad52d7ab	b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	en-us	Log Viewer
665430b0-7edc-428b-ac9d-016c5cb6f528	b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	es-cl	Visor de eventos
4270261c-c8c1-4d3e-bd20-e44a24e6bc6a	b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	es-mx	
67c857ea-ba31-4e1a-8107-ff85f36c6828	b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	de-de	
a414aecc-ca59-4605-9969-8f79a20b290d	b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	de-ch	
bca3f58f-1cee-4c63-86d9-f679a08ee3d4	b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	de-at	
16e8314c-62bc-4c8e-b9db-3ae45f54533c	b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	fr-fr	Voir les Logs
17128c99-eb5b-4031-9873-077c0ea7a20e	b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	fr-ca	
c6fb6398-3b33-47fb-9a95-98dfcdbc8a10	b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	fr-ch	
39df9f41-3aae-4a61-8506-7e9a0c2c1a82	b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	pt-pt	Visualizar Log
7f9aaae7-a5a9-4d6f-8545-55f1f60f90fa	b4750c3f-2a86-b00d-b7d0-345c14eca286	781ebbec-a55a-9d60-f7bb-f54ab2ee4e7e	pt-br	
898bc97f-f5e3-4874-b61b-47e34c4c82d7	b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	en-us	Login
ea9a346f-c4ba-4242-b30e-e68eed0ee8c8	b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	es-cl	Ingresar
1490c9a6-9f46-4109-a789-70d46d75de3b	b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	es-mx	
7f334ec3-c5cc-41ef-b42f-135bef13e6fb	b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	de-de	
2da94145-e39f-4e5e-8ba8-28bffbe7f2fe	b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	de-ch	
9f1eb022-6b6f-45c6-9116-a5d5338515ce	b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	de-at	
eb621636-ad9b-47a1-89bd-40bfff24b440	b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	fr-fr	Connexion
87075c71-996b-43ba-864e-c219baeeba30	b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	fr-ca	
b2eea7a3-8178-4020-bec3-eb9f30013d67	b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	fr-ch	
dd7e9195-d220-453e-b16f-b51d1c334728	b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	pt-pt	Entrar
2e974f91-51ab-4c3f-a37b-d583d7ef381d	b4750c3f-2a86-b00d-b7d0-345c14eca286	c85bf816-b88d-40fa-8634-11b456928afa	pt-br	
84ef9dc9-48ed-4881-a54e-83981c1862a5	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	en-us	Logout
60832fc4-8b64-4f0c-b1d9-a2fb1a978da5	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	es-cl	Salir
3f899ac6-66e0-4fe7-bb09-efd052bc83a8	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	es-mx	
06a16d8f-bc4e-4151-9de9-06a4c8229df8	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	de-de	
408347e3-c038-4702-8c03-b6ce1c77362a	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	de-ch	
15776654-a0f4-43f9-8732-17b0f29e001a	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	de-at	
cb35fd59-e80f-44a8-af8b-83555220ad20	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	fr-fr	Déconnexion
7e83b3a3-7727-41d9-9304-662444471265	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	fr-ca	
810a9a0f-2649-4f4a-a040-f67659cfdc81	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	fr-ch	
35353d14-1edc-4ff9-97be-6aa416559db5	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	pt-pt	Sair
f91efbb5-8991-4afc-81ac-8df69196e62d	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d29e9f4-0c9b-9d8d-cd2d-454899dc9bc4	pt-br	
8bf76945-06b4-4fb8-a9fc-2b0956a824bc	b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	en-us	Modules
a481b557-7db6-444f-b006-da586ea1e73d	b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	es-cl	Módulos
f8b4fba4-de13-482b-8412-a9c3498da185	b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	es-mx	
5d1f6bea-16f8-46ef-a151-368e88e1a24d	b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	de-de	
7070c466-3d49-46e0-8229-b47b4ac0775e	b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	de-ch	
13d4a043-e8f3-4257-b161-7ad911019ca1	b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	de-at	
5ada080f-d7c9-4bd1-9d05-3498e4dcd638	b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	fr-fr	Modules
fe55267e-d948-4205-8117-e686e32c9025	b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	fr-ca	
a63d523d-248d-402b-b52f-e5c4ef9f5e70	b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	fr-ch	
3bfeba60-2ad6-4d5a-9564-02acf4da8ed2	b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	pt-pt	Modulos
c131237e-ab97-4ef1-aad2-66657f471e41	b4750c3f-2a86-b00d-b7d0-345c14eca286	49fdb4e1-5417-0e7a-84b3-eb77f5263ea7	pt-br	
0a5f9916-16f7-4f57-bb15-c0a024fd3c7b	b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	en-us	Music on Hold
4ea5a9ac-7560-4156-ba44-90f4f03644dc	b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	es-cl	Música en espera
4464aa35-7281-4363-b8f9-3654f882d8d6	b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	es-mx	
2d48aa74-ff71-4ab0-a4b7-0acb93bcfae6	b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	de-de	
0956d3b1-3395-4442-867c-892d4f8f8997	b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	de-ch	
24bff121-83ce-4b9d-a117-fe8e7b17ee88	b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	de-at	
5816365a-90e6-460d-b31b-0fa0abc3bc4a	b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	fr-fr	Musique de Garde
fa2e4c5b-4cc0-4257-837a-c6132ca134cf	b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	fr-ca	
5b29369d-da95-4cf3-a355-1d7a2a80ab99	b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	fr-ch	
53c02217-49a2-4ab2-99ee-38988a21f2c1	b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	pt-pt	Musica em Espera
3bff5b23-5701-4f46-acdf-11be4527b9de	b4750c3f-2a86-b00d-b7d0-345c14eca286	1cd1d6cb-912d-db32-56c3-e0d5699feb9d	pt-br	
510fc6af-3d38-49ba-b23c-00b4ef2ff3e6	b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	en-us	Recordings
debbc365-d194-4099-ab83-f0db07be0322	b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	es-cl	Grabaciones
f0c57b26-3b4e-4771-aaf7-21f0533fdb99	b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	es-mx	
0f115521-2535-4bc8-a3e2-f3bbb7a90703	b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	de-de	
be28c248-a5d6-43d4-aa9a-d1487b70c92c	b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	de-ch	
bbf97654-5887-47f1-b4dd-b6acd46ce3be	b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	de-at	
f5818849-3a5d-4a0b-83c8-b3f9b2a9b3f4	b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	fr-fr	Guides Vocaux
6a007d78-a149-4793-af01-e3bdec35ab32	b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	fr-ca	
cb442006-4d57-4513-831c-ef5fe2835f20	b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	fr-ch	
177e923f-e45d-46ff-9ad2-70d96f7682fe	b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	pt-pt	Gravações
24c72136-618d-43c6-aedb-d7c53db6c94c	b4750c3f-2a86-b00d-b7d0-345c14eca286	e4290fd2-3ccc-a758-1714-660d38453104	pt-br	
2adcf36c-e16d-4408-9450-588b3b1b6fbf	b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	en-us	Registrations
9e47bd55-b21c-4ea7-bf0d-8a8d78b133b7	b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	es-cl	Registros
46d074b5-39b0-48bc-8f5d-d124f5eb32ef	b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	es-mx	
46f78d1a-afe9-4509-959f-8dd5aa955eb3	b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	de-de	
def6abf3-166b-4392-b2ff-56254b2b5f6b	b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	de-ch	
be0aa5da-1067-4c2f-b6e6-0f9d80def922	b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	de-at	
ec86afaf-68e5-4495-9923-6f599778e8e0	b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	fr-fr	Connexions
87efa334-72e3-4e2f-b1d7-f26dff6f1c57	b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	fr-ca	
7e97b614-bcf5-46cc-87ca-aa89487c78f3	b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	fr-ch	
2daa5b4e-5f34-4987-8e32-f10dcdd73674	b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	pt-pt	Registos
34af71f3-47a5-4ef7-a0b3-d8b642f0041d	b4750c3f-2a86-b00d-b7d0-345c14eca286	17dbfd56-291d-8c1c-bc43-713283a9dd5a	pt-br	
9b038cc4-a089-4875-aa49-6516bacf5c80	b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	en-us	Ring Groups
02e8dcc8-5762-465c-b600-5ebe95c31919	b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	es-cl	Grupo de llamados
4ef1a3ef-ad40-4c25-a85e-edd49161093b	b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	es-mx	
57fd15d2-0168-4745-806e-1152745315e9	b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	de-de	
759578d2-7f08-423d-bd87-63aaf447822d	b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	de-ch	
042d334e-dedc-42c8-aa01-c497a53bbd2e	b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	de-at	
694c4504-61bb-4cfd-b537-63ce9e0a3d35	b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	fr-fr	Groupes de sonnerie
c22d05d3-5e36-4a5f-ab2f-8ad7b3b5131c	b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	fr-ca	
f771f32f-26da-4cdc-a1c0-607bf38085e4	b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	fr-ch	
b3cac207-6889-4e62-8c5c-3606021815f7	b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	pt-pt	Grupos de Ring
b89c9d73-ceff-4585-b092-0521d44b70a1	b4750c3f-2a86-b00d-b7d0-345c14eca286	b30f085f-3ec6-2819-7e62-53dfba5cb8d5	pt-br	
c90c6e20-ff63-4b46-83b9-f8d903e95f66	b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	en-us	Schemas
0819224b-7861-40fd-af5b-2fc0ee8cbfa1	b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	es-cl	Esquemas
b20a2c14-63e4-4710-a5e9-49dce651605a	b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	es-mx	
2f4aea3c-beec-4f98-92de-2597e3ec1c54	b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	de-de	
4998405f-6b86-4d5d-b1c0-bc4517506bda	b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	de-ch	
4da5a934-0f57-48fb-8482-dafa847cda3e	b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	de-at	
47475771-0a93-4ec9-9dcc-ce849bb16033	b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	fr-fr	Schémas
a644e7de-3985-49a9-a257-f354539f329b	b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	fr-ca	
7a72964f-5d5d-4206-9564-42a733f87076	b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	fr-ch	
b0877750-7895-452f-9552-7e390fc68a2a	b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	pt-pt	Tabelas
6f374330-aaf3-4479-9c5a-ce4457397fae	b4750c3f-2a86-b00d-b7d0-345c14eca286	6be94b46-2126-947f-2365-0bea23651a6b	pt-br	
0e03ed40-0921-4e98-b0e7-b58f9321cd01	b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	en-us	Services
894b036e-fc1c-47bb-aff9-866dc1e06d6c	b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	es-cl	Servicios
4c1aaa79-38a6-47ac-807d-65ceeb01b544	b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	es-mx	
4c1ea74a-e0ba-4dbc-ac50-80c727714f4a	b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	de-de	
d99673eb-2501-484d-9711-16eefa87ad37	b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	de-ch	
1bb97b86-8877-41e4-aeae-df09ea618d6d	b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	de-at	
902511e5-b1f4-4a3e-8926-37ce7533a003	b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	fr-fr	Services
74094a07-a98a-4e86-90a0-163d135cb655	b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	fr-ca	
f41c4d63-0ad7-4cbc-8326-6490c8a3bd1c	b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	fr-ch	
06430cda-9d9f-4517-a416-4392e0e7d7f0	b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	pt-pt	Serviços
fd85ef5c-1f5c-43bc-9ade-5b4ff2de1ddb	b4750c3f-2a86-b00d-b7d0-345c14eca286	c28f14e9-e5ad-e992-0931-d5f5f0db6a79	pt-br	
8fe79258-6ce9-4f67-88fe-de32f3113e78	b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	en-us	Settings
daadb1e5-00b9-44cf-957e-6e3ff060409e	b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	es-cl	Configuraciones
4447365b-ffbb-4472-9ba4-4f1a7c34db1d	b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	es-mx	
6389f2d5-191c-4d9f-915f-2c5a6381ba1c	b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	de-de	
2da2a772-6c45-4c99-9a37-7a62f4e72e4e	b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	de-ch	
0c9b83b1-3d04-41dd-b3c2-1e49916dde32	b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	de-at	
0b860616-c1ca-4b15-943a-f4fd0431aa17	b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	fr-fr	Configuration
91c3864e-9916-4aa8-8955-cbf8358c780e	b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	fr-ca	
a6f6de1c-88cb-43eb-98ca-749e5cb6024f	b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	fr-ch	
f6a58535-dcb7-4338-bd71-c10b7b7485ff	b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	pt-pt	Definições
3f095703-3bf5-4f1d-8470-dc79eb7b72e0	b4750c3f-2a86-b00d-b7d0-345c14eca286	148ea42a-3711-3d64-181b-07a6a3c3ed60	pt-br	
a094ad58-3b9f-41bb-833e-9dce21aa4920	b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	en-us	SIP Profiles
02c8a211-01d0-4f1d-af91-9521eca0fba4	b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	es-cl	Perfiles SIP
157a7b9f-9d75-4fd7-b092-cdae9eeb07fe	b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	es-mx	
7edf5048-99ff-441c-889a-e85954fa81ef	b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	de-de	
d10f9999-6c94-4e4f-bff6-b5dcbebb5e3c	b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	de-ch	
ff9004c4-5050-4266-9383-0226b9c43818	b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	de-at	
f0bfaae3-b424-44d5-a981-e6612598ac9d	b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	fr-fr	Profiles SIP
3c2d01eb-e3b2-4589-922a-53165b5f6f4e	b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	fr-ca	
91dd3d7c-6262-4314-a248-aa9a084f9a6c	b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	fr-ch	
dce2c039-05b4-4970-85c6-3fc9f2b306d5	b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	pt-pt	Perfis SIP
9a2d3ad8-77e6-4d5b-a84b-605bc510a449	b4750c3f-2a86-b00d-b7d0-345c14eca286	47014b1d-13ad-921c-313d-ca42c0424b37	pt-br	
ba40f294-0b5c-4c92-a5e9-d2224fbbf427	b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	en-us	SIP Status
b1e3dc0c-7cd5-463a-b161-9b781abc4277	b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	es-cl	Estado de SIP
87605044-2ab2-41d5-94be-6c2e0462ff46	b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	es-mx	
182143a7-c84b-4bf9-9d81-2eb195fac144	b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	de-de	
809c1f20-f80f-44f4-9235-5d44dcd42516	b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	de-ch	
d08221bc-c93b-412e-9de8-90e9082a6e3c	b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	de-at	
c797c06d-3cdb-4884-b5ad-26a072d5e1c0	b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	fr-fr	Etat SIP
7f2da580-fc77-402a-b088-e2e70bce79a6	b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	fr-ca	
b002494f-20c6-4e15-a67a-25cc23ddb205	b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	fr-ch	
995f4843-484e-4254-867b-aa73ebca3bc9	b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	pt-pt	Estado do SIP
0486669f-8e91-4470-89ac-dbf59e4e5862	b4750c3f-2a86-b00d-b7d0-345c14eca286	b7aea9f7-d3cf-711f-828e-46e56e2e5328	pt-br	
2d398f6d-c0dd-4233-8135-56f2d8f064eb	b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	en-us	SQL Query
db2878b7-7c95-4c0f-a821-35ade9009a08	b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	es-cl	Coinsulta SQL
79c0a249-15ed-4ced-bfce-3fda5cee062a	b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	es-mx	
ec9c33af-b392-420a-9b8e-b92eef51e1c7	b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	de-de	
4db42d79-30db-4c9b-97a8-79bd5f1fe114	b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	de-ch	
1bb7e370-fa68-4807-b534-e909c1c7a151	b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	de-at	
a099a585-97a0-4d39-8b60-a42554a6c925	b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	fr-fr	Requête SQL
e6438901-665c-454d-9e29-a9a827cd48f5	b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	fr-ca	
71922312-adfe-4d00-9839-e40e6247fbf7	b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	fr-ch	
f7ce53ee-4014-4484-9c9c-954744e395ec	b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	pt-pt	Consultas SQL
ebe4d005-483a-43d4-b715-391a985554f4	b4750c3f-2a86-b00d-b7d0-345c14eca286	a894fed7-5a17-f695-c3de-e32ce58b3794	pt-br	
4a19abab-f5a2-4e22-9775-8ca7127e76a5	b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	en-us	System Status
63db8d45-dcf2-4796-8e49-77bf3df1591c	b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	es-cl	Estado de Sistema
9a13f029-a4d9-46e8-909e-6e08176de593	b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	es-mx	
2b8c0804-e5e7-4974-b4f1-b8bb862afd62	b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	de-de	
cc68455c-c72c-43c0-aa7d-17f8fd011877	b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	de-ch	
d68e0efd-640d-4f5b-9c88-8b3ad3b0a232	b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	de-at	
5bfaf6a3-2762-4876-abaf-36d7123f75ce	b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	fr-fr	Etat Système
3a944795-4cdb-4e1c-97b4-e0aae1ea80ff	b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	fr-ca	
f95dbeff-3b35-4355-86a2-2f955df04613	b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	fr-ch	
a4a227b2-e29c-4275-99c7-c18b92d42ab9	b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	pt-pt	Estado do Sistema
76337f36-9c08-46da-902c-6265f0de0878	b4750c3f-2a86-b00d-b7d0-345c14eca286	5243e0d2-0e8b-277a-912e-9d8b5fcdb41d	pt-br	
b09fb567-c1da-4895-b4c0-e539138e384c	b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	en-us	Time Conditions
779a0d2e-65c2-4125-ae7f-ec988033e677	b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	es-cl	Condic. de Tiempo
f069e5f1-2199-46cf-a93c-b7e16f54e98a	b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	es-mx	
dbe1f8c9-e291-41b8-86d8-d34188762351	b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	de-de	
82255423-d83f-412b-86f2-0af0bb064a87	b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	de-ch	
0468697f-bdc7-404e-8b11-d9c36b38f411	b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	de-at	
b3aed291-7a09-44c5-8c5d-0e313a76cdba	b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	fr-fr	Conditions Temporelles
6fbe510d-f696-44e1-94d4-c504e3c91678	b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	fr-ca	
525a1b58-c2bb-4253-b9f0-ecee712f702e	b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	fr-ch	
0952d546-d69a-44d9-8554-32ec71cc573f	b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	pt-pt	Condições Temporais
1f4dcd9e-796b-4f1a-b905-8bad0ddc0fc7	b4750c3f-2a86-b00d-b7d0-345c14eca286	67aede56-8623-df2d-6338-ecfbde5825f7	pt-br	
d7ed8d51-308e-4ebe-88e3-61bffab3416e	b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	en-us	Traffic Graph
48f0ea25-9db0-4e02-bde8-6f8896139e9c	b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	es-cl	Gráfico de Tráfico
1421d283-89d4-4182-a7c5-93c350b65910	b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	es-mx	
a371c7c7-ed41-41c0-b6f8-e1b7d10987c6	b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	de-de	
6fa1d6b8-6e83-45ff-82f3-7fc4bf940759	b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	de-ch	
380d6ab3-3a84-4ffb-92b0-9427caf9c346	b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	de-at	
9d51a10c-cd8e-481e-8a07-83772060bb2e	b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	fr-fr	Traffic Graphe
73a3d6fd-6316-4acf-84e2-5723d779b3ed	b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	fr-ca	
098b08d3-c5fe-4500-8e3b-14363ebdd519	b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	fr-ch	
96e0a8f6-b0f8-4b9d-9eb7-cca70316eb3a	b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	pt-pt	Grafico de Trafego
d09223d4-2737-43b2-8feb-7794bf97a600	b4750c3f-2a86-b00d-b7d0-345c14eca286	05ac3828-dc2b-c0e2-282c-79920f5349e0	pt-br	
ecc02d89-13dc-47d5-b91f-7b4b34e4455f	b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	en-us	Variables
c649e87a-a819-4082-8dbe-0118639386c4	b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	es-cl	Variables
69fea58c-a618-4b0d-b488-e37368693388	b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	es-mx	
2b607523-1af0-40c3-aa00-ae3ed1594da2	b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	de-de	
77292dd0-4c29-43c9-9b65-07e5935d8c2e	b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	de-ch	
06f93777-6548-4de6-b8cd-b9ee656809ef	b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	de-at	
642beb13-27c0-44d7-b277-27a8262642c5	b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	fr-fr	Variables
bc4034f2-a939-4973-9b51-b042caa219fe	b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	fr-ca	
676672ac-0327-4015-b4a9-0bd55c7c480c	b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	fr-ch	
2f3b1da3-c3aa-4dea-99ab-33755c66eea9	b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	pt-pt	Variáveis
a177c5b8-6d2f-4ea0-b3b6-d6a5577a0bd1	b4750c3f-2a86-b00d-b7d0-345c14eca286	7a4e9ec5-24b9-7200-89b8-d70bf8afdd8f	pt-br	
7583a64a-ae62-4328-a589-4e8864ec5f10	b4750c3f-2a86-b00d-b7d0-345c14eca286	0347f82a-62a0-49d0-bacd-511d080c46d5	en-us	Voicemail
847d8f23-e022-44b8-b6f4-e9e8ea993922	b4750c3f-2a86-b00d-b7d0-345c14eca286	0347f82a-62a0-49d0-bacd-511d080c46d5	fr-fr	Messagerie Vocale
a6b123e5-63e4-45b4-a81f-88c85f0df057	b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	en-us	Call Detail Records
bac99d81-596a-45cb-bcf1-b84622fa60bf	b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	es-cl	Registro de detalle de llamada
84923154-6ce8-48da-a973-9811a79b9dab	b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	es-mx	
5025a4f8-dafc-436f-aa3f-5d640628b146	b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	de-de	
581f73a2-4f20-4bf9-aba4-4679fd69cbd6	b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	de-ch	
573de91e-113d-4264-91ef-3fd6d8cf3d08	b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	de-at	
e189b461-ec8c-48ed-91b5-e2073d2ae713	b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	fr-fr	Historiques Appels
2aec27ab-401c-4680-add7-7a022c4fa4a4	b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	fr-ca	
5bff83ec-283f-4726-baa9-b6d6ea747159	b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	fr-ch	
969ccdba-dcd0-4453-9f15-625a5d1243ed	b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	pt-pt	Detalhes das Gravações de Voz
872320ad-fd05-42c5-afb6-f3434445a1b9	b4750c3f-2a86-b00d-b7d0-345c14eca286	8f80e71a-31a5-6432-47a0-7f5a7b271f05	pt-br	
f61cf664-ae78-46a8-90e1-2092f41588a8	b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	en-us	CDR Statistics
7955cc46-e959-4678-9a26-6c39a713a75e	b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	es-cl	Statistics CDR
35ce7bf0-4931-4634-b462-c9e115f172f8	b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	es-mx	
eac6c11f-3ef8-49e4-91ad-7c01cdd958ea	b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	de-de	
3c0bc28b-c987-45b8-9e8f-642f845055d3	b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	de-ch	
0c508bf3-9a5e-4d65-a250-9dfaf8843a7d	b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	de-at	
7e045f0e-8cf2-464e-a8a0-2b61871eb2ac	b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	fr-fr	Statistics CDR
54d3898d-66fb-4e5b-9ee8-3ad4daf063b8	b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	fr-ca	
5d18accd-c432-49f6-ab29-ea4471816da7	b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	fr-ch	
930c01c1-08f1-4882-b4c0-f89f3402efae	b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	pt-pt	Statistics CDR
314ff4ea-2dda-46e1-b686-899323075a41	b4750c3f-2a86-b00d-b7d0-345c14eca286	032887d2-2315-4e10-b3a2-8989f719c80c	pt-br	
a4ab8b3b-e78f-4d86-af62-02e44678270b	b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	en-us	Extension Summary
2aed69d2-d4fe-4383-9eef-9b53c6c9ff7c	b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	es-cl	Extension Summary
7d3240ae-08bf-4466-8815-1f5dc5d8a63d	b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	es-mx	
09bfaca5-14b7-41d1-98b4-f9c92fba2fee	b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	de-de	
29198cb9-18e1-4676-be50-b9e0c29fc73c	b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	de-ch	
36f2c8d7-5fc9-4f71-b503-b9794b2e8938	b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	de-at	
68dc20ad-0a5d-44f2-8b07-cd2f4fd6192b	b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	fr-fr	Extension Summary
242fe197-1bed-4a61-bddd-d218b003a869	b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	fr-ca	
1e74b72e-5c1a-4d70-9500-17d9d616f7ed	b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	fr-ch	
24bebd7c-a38c-4b8e-a2cf-b74ae8be7618	b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	pt-pt	Extension Summary
461b38e6-36d8-471f-9123-065a1bbec918	b4750c3f-2a86-b00d-b7d0-345c14eca286	4e45a3c1-6db5-417f-9abb-1d30a4fd0bf2	pt-br	
a9e3fba2-927d-4c3c-86db-5d9ed1a2e093	b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	en-us	XMPP Manager
d81b1452-002a-49d0-9670-316445ef2de8	b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	es-cl	Gestor XMPP
ec4f70dd-355e-4eee-ad27-181f8be8981e	b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	es-mx	
5841debf-7737-4984-9a60-f1e4330a8d57	b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	de-de	
3d5dfe0d-7a33-400c-b7ea-19a1bda04983	b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	de-ch	
06fddcce-51ab-4274-963a-088360155dd6	b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	de-at	
63669cbb-6c57-4d7e-86ef-78987c34a4d9	b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	fr-fr	Gestion XMPP
becccaae-e86c-41e7-b3bc-a2231640ea08	b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	fr-ca	
8b72f641-cec6-461c-819e-f8c7149bb6ec	b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	fr-ch	
f46c0ac4-9c40-4a76-8220-64c6f22cc055	b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	pt-pt	Gestor XMPP
49e6e4b9-2b61-4e94-a1c8-8da29ca066c1	b4750c3f-2a86-b00d-b7d0-345c14eca286	1808365b-0f7c-7555-89d0-31b3d9a75abb	pt-br	
438196d5-eaae-424c-ab5d-75bbdbd87d05	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	en-us	Apps
0b868f89-7589-49a3-a522-04767848a1bb	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	es-cl	Aplicaciones
21a0a4c1-f874-4ab2-8625-53d7d74b0a63	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	de-de	
d1f53150-1f2f-443f-af66-6ae5b8f8d0ac	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	de-ch	
89b00cd8-3025-43ac-80a4-37c465d5bd59	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	de-at	
4df68d62-6f00-46a8-b03b-b90a922269fc	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	fr-fr	Apps
408b3d26-d1f2-4a34-81a1-a37cb8385d58	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	fr-ca	
cf467552-25d1-4da6-b2c8-3fccde49c022	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	fr-ch	
75678850-354d-4189-8825-232c34149e3f	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	pt-pt	Aplicações
00f6da0c-160f-408b-9388-96365d35e3f2	b4750c3f-2a86-b00d-b7d0-345c14eca286	fd29e39c-c936-f5fc-8e2b-611681b266b5	pt-br	
1cbd6ec6-9ead-4c85-931b-0fea1dfe55dd	b4750c3f-2a86-b00d-b7d0-345c14eca286	ef00f229-7890-00c2-bf23-fed5b8fa9fe7	en-us	App Manager
143d0334-02ef-4438-8d89-c348703d61dd	b4750c3f-2a86-b00d-b7d0-345c14eca286	ef00f229-7890-00c2-bf23-fed5b8fa9fe7	es-cl	Administrador de Aplicaciones
a55636ae-0534-4ee0-b449-d5cab9a9da8b	b4750c3f-2a86-b00d-b7d0-345c14eca286	ef00f229-7890-00c2-bf23-fed5b8fa9fe7	de-de	
0571c660-90f0-49eb-b745-91894886908c	b4750c3f-2a86-b00d-b7d0-345c14eca286	ef00f229-7890-00c2-bf23-fed5b8fa9fe7	de-ch	
1d19d363-50bb-4b49-bf4e-833a6cef0408	b4750c3f-2a86-b00d-b7d0-345c14eca286	ef00f229-7890-00c2-bf23-fed5b8fa9fe7	de-at	
1f3a523d-5b53-489b-9b3e-183b53e5d4c2	b4750c3f-2a86-b00d-b7d0-345c14eca286	ef00f229-7890-00c2-bf23-fed5b8fa9fe7	fr-fr	Gestion App
64a7dded-9177-4cee-bd55-d870ecd8e4c7	b4750c3f-2a86-b00d-b7d0-345c14eca286	ef00f229-7890-00c2-bf23-fed5b8fa9fe7	fr-ca	
b703529d-75f6-4c91-bebf-07dd3720ba34	b4750c3f-2a86-b00d-b7d0-345c14eca286	ef00f229-7890-00c2-bf23-fed5b8fa9fe7	fr-ch	
d2604357-57bd-450a-a13d-f93cff28a30e	b4750c3f-2a86-b00d-b7d0-345c14eca286	ef00f229-7890-00c2-bf23-fed5b8fa9fe7	pt-pt	Gestor de Aplicações
50f24dd9-0ea4-49fa-8a4a-000f48a0dffc	b4750c3f-2a86-b00d-b7d0-345c14eca286	ef00f229-7890-00c2-bf23-fed5b8fa9fe7	pt-br	
8572f738-e03d-4bee-b413-4f3c575dc1ba	b4750c3f-2a86-b00d-b7d0-345c14eca286	ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	en-us	Databases
c874d7db-5df2-4b5f-bfbb-1802c6f7646d	b4750c3f-2a86-b00d-b7d0-345c14eca286	ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	es-cl	Bases de datos
245ba873-49a4-42be-985e-a6db30623aa0	b4750c3f-2a86-b00d-b7d0-345c14eca286	ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	de-de	
18e0dc78-2bd5-4688-a996-d8e3d1f91c48	b4750c3f-2a86-b00d-b7d0-345c14eca286	ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	de-ch	
3cc50ca5-cb26-4e73-a9f8-94d826fd0b7a	b4750c3f-2a86-b00d-b7d0-345c14eca286	ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	de-at	
b8346dd4-7abb-4a57-9347-d8b4c8849e2f	b4750c3f-2a86-b00d-b7d0-345c14eca286	ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	fr-fr	BDD
50d42041-47ff-4b70-a3dd-c72889c3605e	b4750c3f-2a86-b00d-b7d0-345c14eca286	ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	fr-ca	
cae22696-1e88-4761-a102-37bce1316540	b4750c3f-2a86-b00d-b7d0-345c14eca286	ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	fr-ch	
a17ccab3-1fbd-4fae-8bfb-d369aaf3f870	b4750c3f-2a86-b00d-b7d0-345c14eca286	ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	pt-pt	Base de Dados
e7e9626b-07a0-42f5-91d3-de26c24857bc	b4750c3f-2a86-b00d-b7d0-345c14eca286	ebbd754d-ca74-d5b1-a77e-9206ba3ecc3f	pt-br	
589f4ce5-d417-4eef-a846-3f1e75ccff4c	b4750c3f-2a86-b00d-b7d0-345c14eca286	834b2739-9e99-4345-9b0b-7ec3ca332b67	en-us	Default Settings
2eccd060-c084-421c-9c25-7866dde15376	b4750c3f-2a86-b00d-b7d0-345c14eca286	834b2739-9e99-4345-9b0b-7ec3ca332b67	es-cl	Conf. Predeterminada
2b78b16c-cc6c-43ca-b2ca-4749fccf2492	b4750c3f-2a86-b00d-b7d0-345c14eca286	834b2739-9e99-4345-9b0b-7ec3ca332b67	de-de	
45548b06-d62a-4c8f-b26a-1c6847e95aee	b4750c3f-2a86-b00d-b7d0-345c14eca286	834b2739-9e99-4345-9b0b-7ec3ca332b67	de-ch	
f974e8c3-23d4-43e5-8dcd-f08ce919cfa8	b4750c3f-2a86-b00d-b7d0-345c14eca286	834b2739-9e99-4345-9b0b-7ec3ca332b67	de-at	
7c5d050c-1334-4f5c-9b43-a2212f10f74d	b4750c3f-2a86-b00d-b7d0-345c14eca286	834b2739-9e99-4345-9b0b-7ec3ca332b67	fr-fr	Confs par Défaut
29667768-58e1-4143-9207-fd5562246580	b4750c3f-2a86-b00d-b7d0-345c14eca286	834b2739-9e99-4345-9b0b-7ec3ca332b67	fr-ca	
5eafffa0-7385-4511-8eaf-d9eba2f7231a	b4750c3f-2a86-b00d-b7d0-345c14eca286	834b2739-9e99-4345-9b0b-7ec3ca332b67	fr-ch	
d73d825b-1674-471b-a5af-ae79d4dfbc8d	b4750c3f-2a86-b00d-b7d0-345c14eca286	834b2739-9e99-4345-9b0b-7ec3ca332b67	pt-pt	Predefinições
542da85d-cede-4960-9cf0-f5040640f9d5	b4750c3f-2a86-b00d-b7d0-345c14eca286	834b2739-9e99-4345-9b0b-7ec3ca332b67	pt-br	
23a1621c-518a-4385-b223-ae223a79bd8f	b4750c3f-2a86-b00d-b7d0-345c14eca286	4fa7e90b-6d6c-12d4-712f-62857402b801	en-us	Domains
c51d32c8-5cbc-4a06-968d-ef8a681096b1	b4750c3f-2a86-b00d-b7d0-345c14eca286	4fa7e90b-6d6c-12d4-712f-62857402b801	es-cl	Dominios
fdc5f2b8-8d2e-4d5e-997d-38411af19821	b4750c3f-2a86-b00d-b7d0-345c14eca286	4fa7e90b-6d6c-12d4-712f-62857402b801	de-de	
e0f3c22b-5f2c-4978-bc3d-d7f8bd31f869	b4750c3f-2a86-b00d-b7d0-345c14eca286	4fa7e90b-6d6c-12d4-712f-62857402b801	de-ch	
dd7d8e4c-bff2-490e-a869-aa9b7994dc2a	b4750c3f-2a86-b00d-b7d0-345c14eca286	4fa7e90b-6d6c-12d4-712f-62857402b801	de-at	
8afccd92-0efa-41d0-ae66-c349d4fa8638	b4750c3f-2a86-b00d-b7d0-345c14eca286	4fa7e90b-6d6c-12d4-712f-62857402b801	fr-fr	domaines
95858b97-a232-4159-9675-fb575e351610	b4750c3f-2a86-b00d-b7d0-345c14eca286	4fa7e90b-6d6c-12d4-712f-62857402b801	fr-ca	
74651efe-12af-4ebc-8edc-00bdd4f4e176	b4750c3f-2a86-b00d-b7d0-345c14eca286	4fa7e90b-6d6c-12d4-712f-62857402b801	fr-ch	
8e15b98c-1214-4e91-92f8-1fef2d6f221b	b4750c3f-2a86-b00d-b7d0-345c14eca286	4fa7e90b-6d6c-12d4-712f-62857402b801	pt-pt	Domínios
c7da8f88-f7e5-4d93-851a-4ecedf5e577e	b4750c3f-2a86-b00d-b7d0-345c14eca286	4fa7e90b-6d6c-12d4-712f-62857402b801	pt-br	
41d4172d-125a-4606-b4f9-7668f3259a99	b4750c3f-2a86-b00d-b7d0-345c14eca286	da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	en-us	Menu Manager
49cd26c7-b496-427a-ae4a-e4fffc49ab59	b4750c3f-2a86-b00d-b7d0-345c14eca286	da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	es-cl	Gestor de Menú
535d5777-6501-4e2f-9b91-00a75fa24313	b4750c3f-2a86-b00d-b7d0-345c14eca286	da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	de-de	
9ee5d114-be1d-455d-841e-3bda664d82b9	b4750c3f-2a86-b00d-b7d0-345c14eca286	da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	de-ch	
884503ed-d001-4990-85f4-dbf4265d0278	b4750c3f-2a86-b00d-b7d0-345c14eca286	da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	de-at	
79e776fe-5e44-4556-8513-9408e9034e50	b4750c3f-2a86-b00d-b7d0-345c14eca286	da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	fr-fr	Gestion des Menus
87aa635b-998f-44b7-88cb-1bd60ec13e3f	b4750c3f-2a86-b00d-b7d0-345c14eca286	da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	fr-ca	
925a5566-68cd-4ab1-a491-8091cd339f3b	b4750c3f-2a86-b00d-b7d0-345c14eca286	da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	fr-ch	
0371d050-f8e1-4c20-8890-08d88cfe201d	b4750c3f-2a86-b00d-b7d0-345c14eca286	da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	pt-pt	Gestor de Menus
8b8406d3-f780-4264-8da0-ed9a649d1e9e	b4750c3f-2a86-b00d-b7d0-345c14eca286	da3a9ab4-c28e-ea8d-50cc-e8405ac8e76e	pt-br	
e044ebe9-40ad-449b-be25-63b1d239192b	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	en-us	System
f72018e3-6cdb-4a9e-9783-348a10b3aa86	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	es-cl	Sistema
52ae31fb-c21e-40eb-90a5-a995d5f1e286	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	de-de	
4bb4b1e3-0435-4ce3-8b0a-32df540880b1	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	de-ch	
70f22a8c-9af4-4c90-a9e5-fd1709aa3dbd	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	de-at	
df384338-3d4d-4379-9642-146ffa67f00e	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	fr-fr	Système
486559aa-7c26-4365-a68c-98b65d69c31c	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	fr-ca	
2de60f42-a815-4f32-8bb9-2a0965fad145	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	fr-ch	
ef23119f-aa10-43b8-be92-4fbc8b91ae33	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	pt-pt	Sistema
7d1efb2a-e89f-4519-8d5f-c74c31735e72	b4750c3f-2a86-b00d-b7d0-345c14eca286	02194288-6d56-6d3e-0b1a-d53a2bc10788	pt-br	
2a268e66-001f-4320-a143-0ef34f7651ad	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	en-us	Accounts
6cf5e541-84f4-4627-bf39-4739eb13eefe	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	es-cl	Cuentas
df880ada-8d00-4069-8079-d4902ba6cd4c	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	de-de	
ae4edab9-7e03-4694-85c4-0186e172c18e	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	de-ch	
7975eb44-081b-4b14-993a-d18e371c104f	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	de-at	
f0ad6a4a-d646-494a-800d-9caed0c4794e	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	fr-fr	Comptes
3d6d5e7a-b614-4b90-ab8f-fddfccf79322	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	fr-ca	
b7d08572-05a2-413b-82da-a1ae0f629cab	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	fr-ch	
49ef3b07-3a9d-41df-a1ec-cbe398f78e0a	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	pt-pt	Contas
b58901ce-7403-4e60-91a5-9a360ec7f713	b4750c3f-2a86-b00d-b7d0-345c14eca286	bc96d773-ee57-0cdd-c3ac-2d91aba61b55	pt-br	
72197473-8c4f-4d63-b8b7-eb171cc81945	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	en-us	Status
aa85e373-64e6-4e5b-b452-a8b4f4ced53a	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	es-cl	Estado
8f8767f5-81d4-42ad-b2a7-3dd13d97b006	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	de-de	
36ccdd1c-cf28-4b32-9ed6-3333874bd35e	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	de-ch	
434388cd-5523-4e31-bc81-6f7b6cea3970	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	de-at	
84a6428b-f0c3-4331-b643-0778624764a0	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	fr-fr	Etat
0b7298d0-9af2-47b3-b27b-3656ce5bc4f9	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	fr-ca	
54428e27-db78-4df3-9fdf-7c8b2070a4aa	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	fr-ch	
982832d0-a267-4f44-8ac5-f7f1820d580e	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	pt-pt	Estado
d55f876d-a74f-45fa-bf4a-6335bec9dc69	b4750c3f-2a86-b00d-b7d0-345c14eca286	0438b504-8613-7887-c420-c837ffb20cb1	pt-br	
8ef3114c-97fe-4ac0-858a-6c6b586c06b7	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	en-us	Advanced
85878bb3-b197-428c-8a55-6734bb8d8957	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	es-cl	Avanzado
462276ab-3faa-48c6-95be-2d1586c22791	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	de-de	
b6009cdf-a600-4e52-889a-e99ad0191f33	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	de-ch	
d325b13d-bf65-4a59-9eb4-e44e5f700212	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	de-at	
4854a6a3-1693-44cf-9e81-40202ddb895a	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	fr-fr	Avancé
7b5039b1-5534-4bd5-8cde-6cf70d32f18a	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	fr-ca	
fb22c33e-00a1-4961-bd33-c004e3bf0d65	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	fr-ch	
33faf17c-bf6d-478d-a16c-de7540243ed8	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	pt-pt	Avançado
4a2f38c9-9500-40f5-8fe9-71f28196173e	b4750c3f-2a86-b00d-b7d0-345c14eca286	594d99c5-6128-9c88-ca35-4b33392cec0f	pt-br	
44cfdbdf-e089-4315-8ef9-f84d6a406aff	b4750c3f-2a86-b00d-b7d0-345c14eca286	f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	en-us	Notifications
2c8d771c-7500-4b2b-86e9-72c76daefb0f	b4750c3f-2a86-b00d-b7d0-345c14eca286	f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	es-cl	Notificaciones
fab676df-7cc4-4923-ada4-929405137f26	b4750c3f-2a86-b00d-b7d0-345c14eca286	f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	de-de	
1b0546cf-8b82-4a93-96d7-88e2c1611fe6	b4750c3f-2a86-b00d-b7d0-345c14eca286	f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	de-ch	
12798493-9378-4799-8f05-fac513f7f268	b4750c3f-2a86-b00d-b7d0-345c14eca286	f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	de-at	
1bf3dfbc-29ec-479f-bf4c-dd2db26d8506	b4750c3f-2a86-b00d-b7d0-345c14eca286	f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	fr-fr	Notifications
165e6c52-ca83-4883-8faf-afc4752e6c21	b4750c3f-2a86-b00d-b7d0-345c14eca286	f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	fr-ca	Notifications
bcc0a4e6-b04f-427e-af9e-52e296228b5b	b4750c3f-2a86-b00d-b7d0-345c14eca286	f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	fr-ch	
0ca23286-5c52-4485-817c-cc6da7c803d2	b4750c3f-2a86-b00d-b7d0-345c14eca286	f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	pt-pt	
01935e93-9b23-43c1-9c4d-a1d22c3fe4ce	b4750c3f-2a86-b00d-b7d0-345c14eca286	f8d65f91-0f4a-405a-b5ac-24cb3c4f10ba	pt-br	
3493e9f0-5ac2-4f3c-a6fa-0ec93a01de32	b4750c3f-2a86-b00d-b7d0-345c14eca286	8c826e92-be3c-0944-669a-24e5b915d562	en-us	Upgrade
3250f735-f270-4a43-87f7-b105b8795326	b4750c3f-2a86-b00d-b7d0-345c14eca286	8c826e92-be3c-0944-669a-24e5b915d562	es-cl	Actualizar
de734625-cf07-4d93-8639-8b0c4da6ee4c	b4750c3f-2a86-b00d-b7d0-345c14eca286	8c826e92-be3c-0944-669a-24e5b915d562	de-de	
435e3df5-c8a7-4fc0-965c-90377051b7bc	b4750c3f-2a86-b00d-b7d0-345c14eca286	8c826e92-be3c-0944-669a-24e5b915d562	de-ch	
f0a1860f-2f26-421f-b2d8-a6855a64360a	b4750c3f-2a86-b00d-b7d0-345c14eca286	8c826e92-be3c-0944-669a-24e5b915d562	de-at	
9472c7b3-fbac-4695-bcf8-21a72634bed3	b4750c3f-2a86-b00d-b7d0-345c14eca286	8c826e92-be3c-0944-669a-24e5b915d562	fr-fr	Mise à jour
af7e0ca0-4d01-4cff-9efc-e0492060abdf	b4750c3f-2a86-b00d-b7d0-345c14eca286	8c826e92-be3c-0944-669a-24e5b915d562	fr-ca	
43ac7f64-be9f-4552-985c-8afc24d4bb3b	b4750c3f-2a86-b00d-b7d0-345c14eca286	8c826e92-be3c-0944-669a-24e5b915d562	fr-ch	
00141692-a9ca-488a-8d50-c9152331c2ea	b4750c3f-2a86-b00d-b7d0-345c14eca286	8c826e92-be3c-0944-669a-24e5b915d562	pt-pt	Actualizar BD
0b66becd-222e-4567-bd5a-2a8323430720	b4750c3f-2a86-b00d-b7d0-345c14eca286	8c826e92-be3c-0944-669a-24e5b915d562	pt-br	
3a3151bb-a565-41e9-bb21-a1cdf6bce634	b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	en-us	Account Settings
2c5a97f8-ce3a-47e4-b302-21a8085561aa	b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	es-cl	Config de Cuenta
7eda2307-3a9d-430d-8210-a6f8a3eb9f15	b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	de-de	
3c109b9c-aaba-45d6-a941-f33f2cda0077	b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	de-ch	
20bdbd65-344a-4f28-b029-11def60e050a	b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	de-at	
6761d220-1e6e-4ddb-ad83-0ce516de7a67	b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	fr-fr	Confs du Compte
f515809d-3a60-48c2-9c4e-376a85273458	b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	fr-ca	
85b94720-aead-4aee-89d8-acff35cf8400	b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	fr-ch	
5decda2b-2802-4ca6-a32a-b519c6021b11	b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	pt-pt	Configurações da Conta
0beb55a6-4da7-4804-917f-1db02da080e2	b4750c3f-2a86-b00d-b7d0-345c14eca286	4d532f0b-c206-c39d-ff33-fc67d668fb69	pt-br	
e1ae3ed6-14ea-46b4-a8cd-523f59b78404	b4750c3f-2a86-b00d-b7d0-345c14eca286	92c8ffdb-3c82-4f08-aec0-82421ec41bb5	en-us	User Dashboard
895199d3-7b08-4ec4-9548-bbee26e8125a	b4750c3f-2a86-b00d-b7d0-345c14eca286	92c8ffdb-3c82-4f08-aec0-82421ec41bb5	es-cl	Dashboard Usuario
4c1e1e32-e031-48d3-bce4-c50bb74fd511	b4750c3f-2a86-b00d-b7d0-345c14eca286	92c8ffdb-3c82-4f08-aec0-82421ec41bb5	de-de	
cb95a087-06a8-42ff-b486-af3f23490b20	b4750c3f-2a86-b00d-b7d0-345c14eca286	92c8ffdb-3c82-4f08-aec0-82421ec41bb5	de-ch	
8d43f175-91e1-416a-a064-f76fc0a5cd9d	b4750c3f-2a86-b00d-b7d0-345c14eca286	92c8ffdb-3c82-4f08-aec0-82421ec41bb5	de-at	
1ff78714-8188-4a8e-b101-1481eb2cf996	b4750c3f-2a86-b00d-b7d0-345c14eca286	92c8ffdb-3c82-4f08-aec0-82421ec41bb5	fr-fr	Tableau de bord de l'utilisateur
7783895c-1cf5-4e5a-b854-b14a7c87914b	b4750c3f-2a86-b00d-b7d0-345c14eca286	92c8ffdb-3c82-4f08-aec0-82421ec41bb5	fr-ca	
023b9e46-8e35-40e8-ac60-387ce5c19be5	b4750c3f-2a86-b00d-b7d0-345c14eca286	92c8ffdb-3c82-4f08-aec0-82421ec41bb5	fr-ch	
004f88bc-93d9-4bac-9226-443082ecdfac	b4750c3f-2a86-b00d-b7d0-345c14eca286	92c8ffdb-3c82-4f08-aec0-82421ec41bb5	pt-pt	Painel de Controle do Usuário
bc0d18c4-94a9-4623-9ab5-a3c48c606983	b4750c3f-2a86-b00d-b7d0-345c14eca286	92c8ffdb-3c82-4f08-aec0-82421ec41bb5	pt-br	
f8240040-682a-486d-9b96-b8401b60e899	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	en-us	User Manager
b469e669-3690-4f52-b98b-d238d4051c88	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	es-cl	Gestor de Usuarios
f7eb143f-171b-442f-88b0-5a0dadedc5fd	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	de-de	
bcace55f-0171-4d22-b27b-ce79dc146f9d	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	de-ch	
ea2b2f62-b87e-43cc-b064-4876fa349dd7	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	de-at	
52070d34-6620-4df2-934e-2701a3fe41ac	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	fr-fr	Gestion Usagers
c58feb84-c333-4b8f-97ca-6b303f005d68	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	fr-ca	
236abfb6-b942-4641-99e5-d3733adbf08a	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	fr-ch	
099b83b2-ed25-4caf-923a-b5186081b6c8	b4750c3f-2a86-b00d-b7d0-345c14eca286	0d57cc1e-1874-47b9-7ddd-fe1f57cec99b	pt-br	
4581c599-824f-4ab5-8e2f-5b926bed66fc	b4750c3f-2a86-b00d-b7d0-345c14eca286	3b4acc6d-827b-f537-bf21-0093d94ffec7	en-us	Group Manager
4895e52d-fa8a-49f2-9159-c6d0718270bc	b4750c3f-2a86-b00d-b7d0-345c14eca286	3b4acc6d-827b-f537-bf21-0093d94ffec7	de-de	
4fceb63b-52bf-4d61-a27d-615aa40951bb	b4750c3f-2a86-b00d-b7d0-345c14eca286	3b4acc6d-827b-f537-bf21-0093d94ffec7	de-ch	
d22880c0-3125-44fc-9d19-be8d117f72ae	b4750c3f-2a86-b00d-b7d0-345c14eca286	3b4acc6d-827b-f537-bf21-0093d94ffec7	de-at	
d061c271-83f2-4578-92f5-7c6e9c496719	b4750c3f-2a86-b00d-b7d0-345c14eca286	3b4acc6d-827b-f537-bf21-0093d94ffec7	fr-fr	Gestion Groupes
4a68fd80-4db1-49e1-b0a7-cbdb93eedd53	b4750c3f-2a86-b00d-b7d0-345c14eca286	3b4acc6d-827b-f537-bf21-0093d94ffec7	fr-ca	
fe8adb45-bee2-4c5b-b5f2-529d36256d12	b4750c3f-2a86-b00d-b7d0-345c14eca286	3b4acc6d-827b-f537-bf21-0093d94ffec7	fr-ch	
c2d6c633-3990-42bf-aebe-bcc4dfa9bd16	b4750c3f-2a86-b00d-b7d0-345c14eca286	3b4acc6d-827b-f537-bf21-0093d94ffec7	pt-br	
\.


--
-- Data for Name: v_menus; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_menus (menu_uuid, menu_name, menu_language, menu_description) FROM stdin;
b4750c3f-2a86-b00d-b7d0-345c14eca286	default	en-us	
\.


--
-- Data for Name: v_modules; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_modules (module_uuid, module_label, module_name, module_category, module_enabled, module_default_enabled, module_description) FROM stdin;
0df1c086-67bb-4288-8ce4-6bc0850d2106	FIFO	mod_fifo	Applications	true	true	FIFO provides custom call queues including call park.
2fa8a3d6-1b63-43d8-a2fd-c18df5d384c0	Local Stream	mod_local_stream	Streams / Files	true	true	For local streams (play all the files in a directory).
6b732f64-c380-456b-bb09-e4bd4053e3cd	Event Socket	mod_event_socket	Event Handlers	true	true	Sends events via a single socket.
9c3aa3e2-d633-4bfc-b2e4-a84323e6f3f1	HT-TAPI	mod_httapi	Applications	false	false	HT-TAPI Hypertext Telephony API
43f02042-002d-446e-93d1-bffa4a7f238f	Spanish	mod_say_es	Say	true	true	
efca8e12-8b2a-41a5-9d31-96896f462e3b	Commands	mod_commands	Applications	true	true	API interface commands.
dd954672-34db-4f80-bca7-35fded0e85b6	DB	mod_db	Applications	true	true	Database key / value storage functionality, dialing and limit backend.
a14452e6-8c74-442e-b01b-95849990ec75	Dingaling	mod_dingaling	Auto	false	false	
79305beb-f27e-4f32-bed6-4063452fb029	Loopback	mod_loopback	Endpoints	true	true	A loopback channel driver to make an outbound call as an inbound call.
ef476c2e-dc11-4cd0-a9a4-87d13cd43998	Log File	mod_logfile	Loggers	true	true	Send logs to the local file system.
df1bdfb6-0017-44ab-829b-427167b813d4	BV	mod_bv	Codecs	true	true	BroadVoice16 and BroadVoice32 audio codecs.
589abf1c-f8bb-4037-81d8-874747b288fc	Rtc	mod_rtc	Auto	false	false	
2e368485-ef97-49f9-9eb8-a85998162c4c	Native File	mod_native_file	File Format Interfaces	true	true	File interface for codec specific file formats.
db18a20c-0498-4499-9af2-cb8282cde162	CDR SQLite	mod_cdr_sqlite	Event Handlers	false	false	SQLite call detail record handler.
57a344c5-f2a6-427b-8cb8-da7f988984eb	XML CDR	mod_xml_cdr	XML Interfaces	true	true	XML based call detail record handler.
112b8288-72d2-4cae-a874-7c401444639b	ESF	mod_esf	Applications	true	true	Holds the multi cast paging application for SIP.
1e69a767-476e-4f99-8112-21a53f0e6815	Verto	mod_verto	Auto	false	false	
51f867bc-b72e-4b31-82ae-a3d21d4a6d73	Valet Parking	mod_valet_parking	Applications	false	false	Call parking
2bbef687-a8ac-49c1-96f9-2241cdc3ea45	Dialplan XML	mod_dialplan_xml	Dialplan Interfaces	true	true	Provides dialplan functionality in XML.
d96f4db7-55ee-4e16-886f-5a023b5f2276	Vp8	mod_vp8	Auto	false	false	
0b25f722-dabf-422e-bfe2-9acdcb3b05c1	SMS	mod_sms	Applications	true	true	Chat messages
ef521076-0d59-4bf7-9342-09fa68a65f00	AMR	mod_amr	Codecs	true	true	AMR codec.
2b3575f6-a2ae-47a1-938a-fa3f7709c6dd	Hash	mod_hash	Applications	true	true	Resource limitation.
e06c7815-3809-484f-abd0-740831305d1d	SpanDSP	mod_spandsp	Applications	true	true	FAX provides fax send and receive.
9e4357e2-946a-49f3-a5d3-04b82bab16c2	Codec2	mod_codec2	Auto	false	false	
740bd6d7-db1d-4fea-bf81-b5613f2e3ed2	Call Center	mod_callcenter	Applications	true	true	Call queuing with agents and tiers for call centers.
112dba99-9c22-48ef-b84c-c60492803fa4	Amrwb	mod_amrwb	Auto	false	false	
f006b893-ea4e-474e-8ec0-59c97dbf534b	Expr	mod_expr	Applications	true	true	Expression evaluation library.
3de7399c-8e07-42fb-919d-c30c80c04170	Sound File	mod_sndfile	File Format Interfaces	true	true	Multi-format file format transcoder (WAV, etc).
2d0f15db-957b-42c1-8f7f-dfa30eaa6f82	Skinny	mod_skinny	Auto	false	false	
5e978407-dd5a-42c7-af7f-e564f36a568e	CDR CSV	mod_cdr_csv	Event Handlers	false	false	CSV call detail record handler.
aa20bb23-77d9-4421-8fb2-c9c9fb61f206	LCR	mod_lcr	Applications	false	false	Least cost routing.
2539c689-d8ab-4c3e-8fb0-ab30794e20c1	Opus	mod_opus	Codecs	false	false	OPUS ultra-low delay audio codec
f505f66e-fef2-4112-81ad-d348785bd7ff	G.723.1	mod_g723_1	Codecs	true	true	G.723.1 codec.
008ff751-e68c-492f-a2f0-3ee73c683c4f	Console	mod_console	Loggers	true	true	Send logs to the console.
32a1956b-1686-4f59-8bc3-221cbee64095	PocketSphinx	mod_pocketsphinx	Speech Recognition / Text to Speech	true	true	Speech Recognition.
aa1e0d0a-dc45-44cd-b129-995b53ae1f30	Sofia	mod_sofia	Endpoints	true	true	SIP module.
c947a75e-04d4-42d3-96c6-a6b261eb871d	Lua	mod_lua	Languages	true	true	Lua script.
d1aacadb-3f1f-459d-8a4a-b635fb8ed2c6	English	mod_say_en	Say	true	true	
dddf4040-fde4-4a75-adef-8a29eb9d9570	Voicemail	mod_voicemail	Applications	false	false	Full featured voicemail module.
c7dfc0ac-b1cc-4929-8322-5b9e40a5ffde	Xml Scgi	mod_xml_scgi	Auto	false	false	
109c60da-2e11-4d10-a879-182b37f99526	Dialplan Asterisk	mod_dialplan_asterisk	Dialplan Interfaces	false	false	Allows Asterisk dialplans.
704e250c-f89e-4a96-9a4b-dab447462881	CID Lookup	mod_cidlookup	Applications	false	false	Lookup caller id info.
40a09ec8-f969-405c-b0de-8ef6a3e3e485	G.729	mod_g729	Codecs	true	true	G729 codec supports passthrough mode
deae2ac7-c3a9-4935-8672-ae975cfe6871	Conference	mod_conference	Applications	true	true	Conference room module.
08306fb7-f2a1-436e-81b0-d57444dbb650	Dialplan Plan Tools	mod_dptools	Applications	true	true	Provides a number of apps and utilities for the dialplan.
24f47d20-2fa8-445c-b239-f88e28b926aa	H26x	mod_h26x	Codecs	true	true	Video codecs
5b0435f8-c6d6-4aa1-99d1-7cffea1204d7	XML RPC	mod_xml_rpc	XML Interfaces	false	false	XML Remote Procedure Calls. Issue commands from your web application.
2d5bba16-d49e-4b15-b63f-5913e40c4953	Tone Stream	mod_tone_stream	Streams / Files	true	true	Generate tone streams.
b0b5ac16-bf2b-4f80-8931-22bbe9700698	ENUM	mod_enum	Applications	true	true	Route PSTN numbers over internet according to ENUM servers, such as e164.org.
c275fa87-531d-40a3-90d7-2a4fdc54dec7	B64	mod_b64	Auto	false	false	
ebc8bca6-b89f-4913-a7e6-4819d128e29f	Syslog	mod_syslog	Loggers	true	true	Send logs to a remote syslog server.
cf6ed7df-c560-4bef-8e51-a4c2c8118c17	Memcached	mod_memcache	Applications	true	true	API for memcached.
68a10cd2-b22a-4eaf-843f-2a5ec9f2c997	Cluechoo	mod_cluechoo	Applications	false	false	A framework demo module.
d83fbca6-f2e4-4e64-8611-2757eb0e95a9	FSV	mod_fsv	Applications	true	true	Video application (Recording and playback).
c24f1fd7-f0b6-450b-970b-5c996ab926cd	Nibblebill	mod_nibblebill	Applications	false	false	Billing module.
94feb69d-924f-4ed6-a52f-7c5ee530ea9c	Python	mod_python	Auto	false	false	
f79b52c6-d5cd-46cd-904a-d9c9b2fac531	V8	mod_v8	Auto	false	false	
81c67d8b-4254-4df9-a42e-f67f5ab5ccda	Say Pl	mod_say_pl	Auto	false	false	
\.


--
-- Data for Name: v_notifications; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_notifications (notification_uuid, project_notifications) FROM stdin;
c517aaa2-65d6-455c-a6f2-c8eb9c75c773	false
\.


--
-- Data for Name: v_recordings; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_recordings (recording_uuid, domain_uuid, recording_filename, recording_name, recording_description) FROM stdin;
\.


--
-- Data for Name: v_ring_group_destinations; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_ring_group_destinations (ring_group_destination_uuid, domain_uuid, ring_group_uuid, destination_number, destination_delay, destination_timeout, destination_prompt) FROM stdin;
\.


--
-- Data for Name: v_ring_group_users; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_ring_group_users (ring_group_user_uuid, domain_uuid, ring_group_uuid, user_uuid) FROM stdin;
\.


--
-- Data for Name: v_ring_groups; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_ring_groups (domain_uuid, ring_group_uuid, ring_group_name, ring_group_extension, ring_group_context, ring_group_forward_destination, ring_group_forward_enabled, ring_group_cid_name_prefix, ring_group_strategy, ring_group_timeout_app, ring_group_timeout_data, ring_group_ringback, ring_group_skip_active, ring_group_enabled, ring_group_description, dialplan_uuid) FROM stdin;
\.


--
-- Data for Name: v_rss; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_rss (rss_uuid, domain_uuid, rss_language, rss_category, rss_sub_category, rss_title, rss_link, rss_description, rss_img, rss_optional_1, rss_optional_2, rss_optional_3, rss_optional_4, rss_optional_5, rss_add_date, rss_add_user, rss_del_date, rss_del_user, rss_order, rss_content, rss_group) FROM stdin;
\.


--
-- Data for Name: v_rss_sub; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_rss_sub (rss_sub_uuid, domain_uuid, rss_uuid, rss_sub_language, rss_sub_title, rss_sub_link, rss_sub_description, rss_sub_optional_1, rss_sub_optional_2, rss_sub_optional_3, rss_sub_optional_4, rss_sub_optional_5, rss_sub_add_date, rss_sub_add_user, rss_sub_del_user, rss_sub_del_date) FROM stdin;
\.


--
-- Data for Name: v_rss_sub_category; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_rss_sub_category (rss_sub_category_uuid, domain_uuid, rss_sub_category_language, rss_category, rss_sub_category, rss_sub_category_description, rss_sub_add_user, rss_sub_add_date) FROM stdin;
\.


--
-- Data for Name: v_schema_data; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_schema_data (schema_data_uuid, domain_uuid, schema_uuid, data_row_uuid, field_name, data_field_value, data_add_user, data_add_date, data_del_user, data_del_date, schema_parent_uuid, data_parent_row_uuid) FROM stdin;
\.


--
-- Data for Name: v_schema_fields; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_schema_fields (schema_field_uuid, domain_uuid, schema_uuid, field_label, field_name, field_type, field_list_hidden, field_search_by, field_column, field_required, field_order, field_order_tab, field_value, field_description) FROM stdin;
\.


--
-- Data for Name: v_schema_name_values; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_schema_name_values (schema_name_value_uuid, domain_uuid, schema_uuid, schema_field_uuid, data_types_name, data_types_value) FROM stdin;
\.


--
-- Data for Name: v_schemas; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_schemas (schema_uuid, domain_uuid, schema_category, schema_label, schema_name, schema_auth, schema_captcha, schema_parent_uuid, schema_description) FROM stdin;
\.


--
-- Data for Name: v_services; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_services (service_uuid, domain_uuid, service_name, service_type, service_data, service_cmd_start, service_cmd_stop, service_cmd_restart, service_description) FROM stdin;
\.


--
-- Data for Name: v_settings; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_settings (numbering_plan, event_socket_ip_address, event_socket_port, event_socket_password, xml_rpc_http_port, xml_rpc_auth_realm, xml_rpc_auth_user, xml_rpc_auth_pass, admin_pin, smtp_host, smtp_secure, smtp_auth, smtp_username, smtp_password, smtp_from, smtp_from_name, mod_shout_decoder, mod_shout_volume) FROM stdin;
US	127.0.0.1	8021	ClueCon	8787	localhost	xmlrpc	7e4d3i	1234		none					Voicemail		0.3
\.


--
-- Data for Name: v_sip_profile_settings; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_sip_profile_settings (sip_profile_setting_uuid, sip_profile_uuid, sip_profile_setting_name, sip_profile_setting_value, sip_profile_setting_enabled, sip_profile_setting_description) FROM stdin;
0ac776bf-f398-49f3-b938-390f2e525618	9c44d3d6-9877-463f-ac78-af834ef990fc	debug	0	true	\N
de113e4c-86ab-4e13-88cb-2a6a274c7f41	9c44d3d6-9877-463f-ac78-af834ef990fc	sip-trace	no	true	\N
ff1d3643-207f-4d20-b694-2adfd9f004bd	9c44d3d6-9877-463f-ac78-af834ef990fc	sip-capture	no	true	\N
4a0754c9-d895-43ff-8a84-929f9f7a4215	9c44d3d6-9877-463f-ac78-af834ef990fc	rfc2833-pt	101	true	\N
08079f0b-6b5a-4fc4-9302-3783691cd33f	9c44d3d6-9877-463f-ac78-af834ef990fc	sip-port	$${external_sip_port}	true	\N
abba5358-1149-4fdf-9ff0-e0eeb91c9d85	9c44d3d6-9877-463f-ac78-af834ef990fc	dialplan	XML	true	\N
c9a968b6-8b43-4f63-ac76-91d5cfaa49c5	9c44d3d6-9877-463f-ac78-af834ef990fc	context	public	true	\N
205c2b7c-68dd-4c08-b9fb-c919a2ecc51d	9c44d3d6-9877-463f-ac78-af834ef990fc	dtmf-duration	2000	true	\N
4bec597d-54f7-49f2-af58-90f7f9728fc3	9c44d3d6-9877-463f-ac78-af834ef990fc	inbound-codec-prefs	$${global_codec_prefs}	true	\N
c0eba4c0-bba0-4cff-a836-5eb15f50e3b1	9c44d3d6-9877-463f-ac78-af834ef990fc	outbound-codec-prefs	$${outbound_codec_prefs}	true	\N
c9c1a13f-b233-4cfe-82f8-0cda8875a90d	9c44d3d6-9877-463f-ac78-af834ef990fc	hold-music	$${hold_music}	true	\N
ce87d18d-27c5-4002-9a54-f7b403b36ffd	9c44d3d6-9877-463f-ac78-af834ef990fc	zrtp-passthru	true	true	\N
745ac895-af50-4864-89de-b8e04e5a577d	9c44d3d6-9877-463f-ac78-af834ef990fc	rtp-timer-name	soft	true	\N
f97944d4-9488-440d-8216-365e1191d825	9c44d3d6-9877-463f-ac78-af834ef990fc	local-network-acl	localnet.auto	true	\N
cba5e465-5153-4564-9b64-9d375d8ad6ff	9c44d3d6-9877-463f-ac78-af834ef990fc	manage-presence	false	true	\N
e8fb9cc5-8bc8-417e-bbb0-60657616c1be	9c44d3d6-9877-463f-ac78-af834ef990fc	inbound-codec-negotiation	generous	true	\N
c75e2e23-6dcd-4044-bb38-006f3565e893	9c44d3d6-9877-463f-ac78-af834ef990fc	nonce-ttl	60	true	\N
f7aea320-3979-44d2-a882-15cbd2e3a868	9c44d3d6-9877-463f-ac78-af834ef990fc	auth-calls	false	true	\N
e1048089-b05c-43e6-b592-0450bcc9dcba	9c44d3d6-9877-463f-ac78-af834ef990fc	rtp-ip	$${local_ip_v4}	true	\N
ad533f4b-6990-4058-b5b1-9ef66063dd56	9c44d3d6-9877-463f-ac78-af834ef990fc	sip-ip	$${local_ip_v4}	true	\N
6a814d06-9b2c-4711-948f-24251c8a92bb	9c44d3d6-9877-463f-ac78-af834ef990fc	ext-rtp-ip	$${external_rtp_ip}	true	\N
1adb4292-9c94-4efc-a895-cb237d23364d	9c44d3d6-9877-463f-ac78-af834ef990fc	ext-sip-ip	$${external_sip_ip}	true	\N
3de066dc-cd4a-4273-b3ae-fad12bfb7289	9c44d3d6-9877-463f-ac78-af834ef990fc	rtp-timeout-sec	300	true	\N
b24856f4-e6b1-465e-a701-f8fa962771ce	9c44d3d6-9877-463f-ac78-af834ef990fc	rtp-hold-timeout-sec	1800	true	\N
481705e2-9a0d-47ed-9783-dbd932dcec07	9c44d3d6-9877-463f-ac78-af834ef990fc	tls	$${external_ssl_enable}	true	\N
c891edff-45d6-4d1a-9dc8-13b047043fe1	9c44d3d6-9877-463f-ac78-af834ef990fc	tls-only	false	true	\N
12db6094-874d-4905-9b0c-0e505e74b57e	9c44d3d6-9877-463f-ac78-af834ef990fc	tls-bind-params	transport=tls	true	\N
7e1d552c-6d5e-4f45-98c0-3e3b7a91f378	9c44d3d6-9877-463f-ac78-af834ef990fc	tls-sip-port	$${external_tls_port}	true	\N
5faf0630-068c-451b-8f92-898a5f2419df	9c44d3d6-9877-463f-ac78-af834ef990fc	tls-cert-dir	$${external_ssl_dir}	true	\N
61713671-362b-4bc0-8910-976afaddfc2b	9c44d3d6-9877-463f-ac78-af834ef990fc	tls-passphrase		true	\N
e0582713-51dd-4baa-93f7-cea79b226c1a	9c44d3d6-9877-463f-ac78-af834ef990fc	tls-verify-date	true	true	\N
fa7ee701-a4bb-4b1e-979c-6b70b450686d	9c44d3d6-9877-463f-ac78-af834ef990fc	tls-verify-policy	none	true	\N
9b6cf8f9-900c-45b6-a61d-b9ea34160dd3	9c44d3d6-9877-463f-ac78-af834ef990fc	tls-verify-depth	2	true	\N
48ede1af-7769-4167-ba93-b4a2889929e0	9c44d3d6-9877-463f-ac78-af834ef990fc	tls-verify-in-subjects		true	\N
cf9a744b-c9b9-4302-bdc1-e4f14532692c	9c44d3d6-9877-463f-ac78-af834ef990fc	tls-version	$${sip_tls_version}	true	\N
b7232787-0907-45e0-bc22-8ef6ce63f547	65f6a243-5a64-436e-8297-cfa4d2604932	debug	0	true	\N
ced85339-d149-4647-8ca4-633a80d3ef6f	65f6a243-5a64-436e-8297-cfa4d2604932	sip-trace	no	true	\N
1cc046cd-d701-475d-8d85-162f6e3f4ec2	65f6a243-5a64-436e-8297-cfa4d2604932	context	public	true	\N
c81e7e6a-073f-4550-b680-5b22f0186f50	65f6a243-5a64-436e-8297-cfa4d2604932	rfc2833-pt	101	true	\N
55f789f1-c643-4c55-a9bf-31309a1309db	65f6a243-5a64-436e-8297-cfa4d2604932	sip-port	$${internal_sip_port}	true	\N
b814b4b9-e810-42bc-a227-6411eef8835d	65f6a243-5a64-436e-8297-cfa4d2604932	dialplan	XML	true	\N
9509fec6-d3f5-4bc8-80c9-982529d1648d	65f6a243-5a64-436e-8297-cfa4d2604932	dtmf-duration	2000	true	\N
1ec5a1d2-7972-4f17-adef-5406edd3aa22	65f6a243-5a64-436e-8297-cfa4d2604932	inbound-codec-prefs	$${global_codec_prefs}	true	\N
643ac49a-5a6b-494e-80ba-d030e4dfd713	65f6a243-5a64-436e-8297-cfa4d2604932	outbound-codec-prefs	$${global_codec_prefs}	true	\N
6709d139-11ef-4df9-8bde-d6f8ec51c38a	65f6a243-5a64-436e-8297-cfa4d2604932	use-rtp-timer	true	true	\N
f61cb7ef-5f1d-4d72-b8b3-6b09ddb5fdfb	65f6a243-5a64-436e-8297-cfa4d2604932	rtp-timer-name	soft	true	\N
c5da2f60-f907-4468-ba8d-28520dd99a92	65f6a243-5a64-436e-8297-cfa4d2604932	rtp-ip	$${local_ip_v6}	true	\N
dcf5c5d7-1f6b-4d57-8a70-dc7e86c20ea8	65f6a243-5a64-436e-8297-cfa4d2604932	sip-ip	$${local_ip_v6}	true	\N
88fae1bb-e054-4e55-8871-aa76d544ad10	65f6a243-5a64-436e-8297-cfa4d2604932	hold-music	$${hold_music}	true	\N
3e49c904-1954-41ee-a9a9-b3f898e78d09	65f6a243-5a64-436e-8297-cfa4d2604932	apply-inbound-acl	domains	true	\N
01427522-b9a7-4af7-8f73-9d950b7e0e32	65f6a243-5a64-436e-8297-cfa4d2604932	record-template	$${recordings_dir}/${caller_id_number}.${strftime(%Y-%m-%d-%H-%M-%S)}.wav	true	\N
cd3995ec-a281-4098-a75f-d437b57fc93f	65f6a243-5a64-436e-8297-cfa4d2604932	manage-presence	true	true	\N
aee27246-0fd1-437e-ac5b-9dd30ed276a8	65f6a243-5a64-436e-8297-cfa4d2604932	inbound-codec-negotiation	generous	true	\N
caef0e80-0bb6-4a8d-ab0e-55e88a618dec	65f6a243-5a64-436e-8297-cfa4d2604932	tls	$${internal_ssl_enable}	true	\N
5a16e361-3929-4e11-9151-3cfb0732fd1b	65f6a243-5a64-436e-8297-cfa4d2604932	tls-bind-params	transport=tls	true	\N
e9d15364-1fbf-41d9-b3b4-cd073ea837b0	65f6a243-5a64-436e-8297-cfa4d2604932	tls-sip-port	$${internal_tls_port}	true	\N
55b74f22-e5ca-4b50-9e91-72fffb91fb32	65f6a243-5a64-436e-8297-cfa4d2604932	tls-cert-dir	$${internal_ssl_dir}	true	\N
c4e541ce-2a8c-42a3-a7b2-b078a7a8ee4c	65f6a243-5a64-436e-8297-cfa4d2604932	tls-version	$${sip_tls_version}	true	\N
3b925d90-9820-4bb0-9bf6-14e6e4390742	65f6a243-5a64-436e-8297-cfa4d2604932	nonce-ttl	60	true	\N
f2e0e823-f21a-478f-839c-7ee1165bf26b	65f6a243-5a64-436e-8297-cfa4d2604932	auth-calls	$${internal_auth_calls}	true	\N
34f54aed-fb48-4c87-8248-8c2ed7f6cfe5	65f6a243-5a64-436e-8297-cfa4d2604932	auth-all-packets	false	true	\N
4e2e59b0-81a2-4467-85a9-1a13b330b577	65f6a243-5a64-436e-8297-cfa4d2604932	rtp-timeout-sec	300	true	\N
cfd06856-7d9c-4b94-af68-3f1de6f2cf3f	65f6a243-5a64-436e-8297-cfa4d2604932	rtp-hold-timeout-sec	1800	true	\N
76a494c9-4018-46c1-a8bd-7e056ea5637c	67944b9f-4237-4363-a1d1-67c213952215	debug	0	true	\N
5ab8485d-f971-4d70-8119-ba7710cdbd15	67944b9f-4237-4363-a1d1-67c213952215	sip-trace	no	true	\N
05f5bac2-e2ad-430a-b3d3-35ac9c7c8e60	67944b9f-4237-4363-a1d1-67c213952215	sip-capture	no	true	\N
8a4e24ae-ca6c-4f78-b7c0-e8a32af1c7cc	67944b9f-4237-4363-a1d1-67c213952215	watchdog-enabled	no	true	\N
30269c2c-9bf5-4026-9734-457ba5d8bfa5	67944b9f-4237-4363-a1d1-67c213952215	watchdog-step-timeout	30000	true	\N
119b2758-796f-4996-8bff-9778d06d22e7	67944b9f-4237-4363-a1d1-67c213952215	watchdog-event-timeout	30000	true	\N
417448f4-9c5d-4bc4-8c4d-2c5f50a26dc5	67944b9f-4237-4363-a1d1-67c213952215	log-auth-failures	true	true	\N
47d5f44c-6ef9-4443-a54f-b7fbdee16bcd	67944b9f-4237-4363-a1d1-67c213952215	forward-unsolicited-mwi-notify	false	true	\N
e6fe1171-ec1e-4645-bf08-c0591ba1aef1	67944b9f-4237-4363-a1d1-67c213952215	context	public	true	\N
089f5ff6-9650-4544-a268-9b076fb0d298	67944b9f-4237-4363-a1d1-67c213952215	rfc2833-pt	101	true	\N
4f54ceb6-5acf-4e53-98aa-77fabda5c414	67944b9f-4237-4363-a1d1-67c213952215	dialplan	XML	true	\N
90a28e56-5747-4e15-bab4-8c4a6aa639b9	67944b9f-4237-4363-a1d1-67c213952215	dtmf-duration	2000	true	\N
8d13d639-9dc9-404f-8a85-f765c4fccf33	67944b9f-4237-4363-a1d1-67c213952215	inbound-codec-prefs	$${global_codec_prefs}	true	\N
e51e12d3-52ce-43d5-8d67-d8ddcbfb7189	67944b9f-4237-4363-a1d1-67c213952215	outbound-codec-prefs	$${global_codec_prefs}	true	\N
ed5d1d73-fa5c-4661-9760-42ac4be9b378	67944b9f-4237-4363-a1d1-67c213952215	rtp-timer-name	soft	true	\N
a293bdfe-f28b-424c-841e-3424e2fbb545	67944b9f-4237-4363-a1d1-67c213952215	rtp-ip	$${local_ip_v4}	true	\N
e5e05273-6883-44bb-a75b-30fc6a3358ce	67944b9f-4237-4363-a1d1-67c213952215	sip-ip	$${local_ip_v4}	true	\N
325c0438-7919-4214-bd59-4363a69dbd86	67944b9f-4237-4363-a1d1-67c213952215	hold-music	$${hold_music}	true	\N
a13c6bf9-00f2-4bee-92af-db9fe3a33441	67944b9f-4237-4363-a1d1-67c213952215	apply-nat-acl	nat.auto	true	\N
f41a85ab-e286-41a8-a8da-d76d545448cb	67944b9f-4237-4363-a1d1-67c213952215	apply-inbound-acl	domains	true	\N
6d0f2067-fac0-4229-9135-9e3d17734dea	67944b9f-4237-4363-a1d1-67c213952215	local-network-acl	localnet.auto	true	\N
5d808912-1939-4293-a577-75521110bfd0	67944b9f-4237-4363-a1d1-67c213952215	sip-port	5566	true	
32860285-2dd9-481e-98fa-95915c894637	67944b9f-4237-4363-a1d1-67c213952215	send-presence-on-register	true	true	\N
df1c6faf-b1fa-4051-9e26-020ee31dae58	67944b9f-4237-4363-a1d1-67c213952215	record-path	$${recordings_dir}	true	\N
e859ce5d-7ee9-4fb2-b2ce-dddf639e8a3a	67944b9f-4237-4363-a1d1-67c213952215	record-template	${domain_name}/archive/${strftime(%Y)}/${strftime(%b)}/${strftime(%d)}/${uuid}.wav	true	\N
0e768114-307d-44fd-ba5a-80270f672ebd	67944b9f-4237-4363-a1d1-67c213952215	manage-presence	true	true	\N
890efecc-70ef-4460-9a58-de41688db4dd	67944b9f-4237-4363-a1d1-67c213952215	presence-hosts	$${domain},$${local_ip_v4}	true	\N
e3d63c1b-7e90-48a6-ac47-016300ae6f00	67944b9f-4237-4363-a1d1-67c213952215	presence-privacy	$${presence_privacy}	true	\N
418e6e4b-f162-441e-933a-a4040c3f96bf	67944b9f-4237-4363-a1d1-67c213952215	inbound-codec-negotiation	generous	true	\N
2eee4283-77df-4e2b-8f75-374acc0de864	67944b9f-4237-4363-a1d1-67c213952215	tls	$${internal_ssl_enable}	true	\N
45bbafa1-88c5-43cc-b2df-62c158265949	67944b9f-4237-4363-a1d1-67c213952215	tls-only	false	true	\N
5f2260d5-c657-4516-8121-94820407ce39	67944b9f-4237-4363-a1d1-67c213952215	tls-bind-params	transport=tls	true	\N
ebcb8fc6-bb49-4d59-8959-5a86c0bec034	67944b9f-4237-4363-a1d1-67c213952215	tls-sip-port	$${internal_tls_port}	true	\N
8eee8147-9369-4993-bc7a-205a7aced5be	67944b9f-4237-4363-a1d1-67c213952215	tls-cert-dir	$${internal_ssl_dir}	true	\N
b20512ad-801f-44ad-a49e-e7a1ceba5e5a	67944b9f-4237-4363-a1d1-67c213952215	tls-passphrase		true	\N
3f7665f7-fa72-4d6c-900f-8965b7f7a4fc	67944b9f-4237-4363-a1d1-67c213952215	tls-verify-date	true	true	\N
9fcd18e6-fc11-41e1-872e-819fba933643	67944b9f-4237-4363-a1d1-67c213952215	tls-verify-policy	none	true	\N
bdf4395d-b0ae-4271-b8a8-2655b781675f	67944b9f-4237-4363-a1d1-67c213952215	tls-verify-depth	2	true	\N
6f8a81e6-f40f-4ed7-86a1-4165568d0b77	67944b9f-4237-4363-a1d1-67c213952215	tls-verify-in-subjects		true	\N
353dd066-fbe3-464e-a62d-ab9076036e8d	67944b9f-4237-4363-a1d1-67c213952215	tls-version	$${sip_tls_version}	true	\N
05ebfdf5-5069-47bc-b88a-8679e4fda2f2	67944b9f-4237-4363-a1d1-67c213952215	nonce-ttl	60	true	\N
afdc5679-cb02-4192-9362-5707323061b9	67944b9f-4237-4363-a1d1-67c213952215	auth-calls	$${internal_auth_calls}	true	\N
7b4d259c-c473-4e44-b617-656e9211fb51	67944b9f-4237-4363-a1d1-67c213952215	inbound-reg-force-matching-username	true	true	\N
dd379044-b8b8-43af-ac67-e99102e4ee02	67944b9f-4237-4363-a1d1-67c213952215	auth-all-packets	false	true	\N
222fc5f1-ed0e-4fda-b3b4-7f34bb75dd48	67944b9f-4237-4363-a1d1-67c213952215	ext-rtp-ip	$${external_rtp_ip}	true	\N
f777d3bc-8e00-4034-af6a-de8aceee6516	67944b9f-4237-4363-a1d1-67c213952215	ext-sip-ip	$${external_sip_ip}	true	\N
eae4bfcd-77b8-48e8-868e-11b593c6c06e	67944b9f-4237-4363-a1d1-67c213952215	rtp-timeout-sec	300	true	\N
dab568ef-66bc-40aa-8df9-72093dcc3a5a	67944b9f-4237-4363-a1d1-67c213952215	rtp-hold-timeout-sec	1800	true	\N
9a64db98-9850-48c5-b583-70a6674e788f	67944b9f-4237-4363-a1d1-67c213952215	challenge-realm	auto_from	true	\N
7dbf286c-f766-433a-8d67-a4bf1fd72355	65f6a243-5a64-436e-8297-cfa4d2604932	force-register-domain	$${domain}	false	\N
61968b3e-4efe-4d1b-8d53-2e3e050726dd	65f6a243-5a64-436e-8297-cfa4d2604932	force-register-db-domain	$${domain}	false	\N
3a871aac-c6c5-403d-8440-fadba7261cf5	67944b9f-4237-4363-a1d1-67c213952215	force-register-domain	$${domain}	false	\N
d719889e-7b85-4259-b9bb-38332eefd74f	67944b9f-4237-4363-a1d1-67c213952215	force-subscription-domain	$${domain}	false	\N
75826dd1-1f6b-4761-89d9-f7ff14874a20	67944b9f-4237-4363-a1d1-67c213952215	force-register-db-domain	$${domain}	false	\N
\.


--
-- Data for Name: v_sip_profiles; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_sip_profiles (sip_profile_uuid, sip_profile_name, sip_profile_hostname, sip_profile_description) FROM stdin;
9c44d3d6-9877-463f-ac78-af834ef990fc	external	\N	The External profile external provides anonymous calling in the public context. By default the External profile binds to port 5080. Calls can be sent using a SIP URL "voip.domain.com:5080"
65f6a243-5a64-436e-8297-cfa4d2604932	internal-ipv6	\N	The Internal IPV6 profile binds to the IP version 6 address and is similar to the Internal profile.
67944b9f-4237-4363-a1d1-67c213952215	internal	\N	The Internal profile by default requires registration which is used by the endpoints. By default the Internal profile binds to port 5060.
\.


--
-- Data for Name: v_software; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_software (software_uuid, software_name, software_url, software_version) FROM stdin;
03c08904-e09e-4c02-b05d-1e2c1f409e9b	FusionPBX	www.fusionpbx.com	3.7.1
\.


--
-- Data for Name: v_user_settings; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_user_settings (user_setting_uuid, user_uuid, domain_uuid, user_setting_category, user_setting_subcategory, user_setting_name, user_setting_value, user_setting_order, user_setting_enabled, user_setting_description) FROM stdin;
b8e84fde-1ec5-4198-ba9e-4c24002184aa	5ee2ef7d-35b2-4d11-8917-894fdfb32a57	60d6731b-e548-4175-ab3e-3fbb261e60f5	domain	language	code		\N	true	\N
91e8cacd-d8c2-4c01-bc12-29576bbb9ca7	5ee2ef7d-35b2-4d11-8917-894fdfb32a57	60d6731b-e548-4175-ab3e-3fbb261e60f5	domain	time_zone	name		\N	true	\N
\.


--
-- Data for Name: v_users; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_users (user_uuid, domain_uuid, username, password, salt, contact_uuid, user_status, api_key, user_enabled, add_user, add_date) FROM stdin;
037f1bf1-d664-4be5-920f-a3ff35a2d336	60d6731b-e548-4175-ab3e-3fbb261e60f5	ccalleu	28e976cae52f7d76bf2a8218123dfde6	XiU5PCVUhrBi?T13.iH^	cae1bea5-18f1-4cae-9c51-a46239da8e9b	\N	\N	true	ccalleu	2014-08-24 10:03:09.741742+02
3af1a190-2327-4c03-822e-5dfbb93f1235	60d6731b-e548-4175-ab3e-3fbb261e60f5	ophiel	b0c1119215a44784f6d2a309849f094b	sVfpqiE$4jHcNE4Zg!4U	9f808502-6109-410d-958c-9f7e445ce190	\N	\N	true	ccalleu	2014-08-24 12:19:45.31768+02
39c49bfa-f8ec-4831-9bfc-1a0489456839	60d6731b-e548-4175-ab3e-3fbb261e60f5	lukaszb	4086b2ce5d155f35710fe830efa70529	.D!ZNR!!YCS7coug^!z8	49b0e27c-182d-4e35-8682-0e872feed5df		\N	true	ccalleu	2014-08-25 23:12:53.15962+02
\.


--
-- Data for Name: v_vars; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_vars (var_uuid, var_name, var_value, var_cat, var_enabled, var_order, var_description) FROM stdin;
f4f4aae7-de25-43aa-a078-ebed3ea0f297	domain_name	$${domain}	Domain	true	2	
58fbdfa3-f1ca-4a99-b7da-95d7be94aa3f	sound_prefix	$${sounds_dir}/en/us/callie	Sound	true	3	U2V0cyB0aGUgc291bmQgZGlyZWN0b3J5Lg==
124c9ee9-caf2-449f-a66c-efae71f2a0f3	hold_music	local_stream://default	Music on Hold	true	4	
bea232ec-e60f-42c2-ac80-16dd61478c97	global_codec_prefs	G7221@32000h,G7221@16000h,G722,PCMU,PCMA,GSM	Codecs	true	5	RzcyMjFAMzIwMDBoLEc3MjIxQDE2MDAwaCxHNzIyLFBDTVUsUENNQSxpTEJDLEdTTSxIMjYzLEgyNjQ=
3b596a3d-56c6-41e1-bbad-1a0adcbf0065	outbound_codec_prefs	PCMU,PCMA,GSM	Codecs	true	6	ZGVmYXVsdDogUENNVSxQQ01BLEdTTQ==
b4737a0f-d53e-4b28-aa6b-97caf459f71c	xmpp_client_profile	xmppc	Dingaling	true	7	eG1wcF9jbGllbnRfcHJvZmlsZSBhbmQgeG1wcF9zZXJ2ZXJfcHJvZmlsZSB4bXBwX2NsaWVudF9wcm9maWxlIGNhbiBiZSBhbnkgc3RyaW5nLiB4bXBwX3NlcnZlcl9wcm9maWxlIGlzIGFwcGVuZGVkIHRvICJkaW5nYWxpbmdfIiB0byBmb3JtIHRoZSBkYXRhYmFzZSBuYW1lIGNvbnRhaW5pbmcgdGhlICJzdWJzY3JpcHRpb25zIiB0YWJsZS4gdXNlZCBieTogZGluZ2FsaW5nLmNvbmYueG1sIGVudW0uY29uZi54bWw=
fe6873eb-05bf-4fd1-b412-55064c6774d1	xmpp_server_profile	xmpps	Dingaling	true	8	
87aebb1a-f4c0-428a-9505-baf10a0fc046	bind_server_ip	auto	Dingaling	true	9	Q2FuIGJlIGFuIGlwIGFkZHJlc3MsIGEgZG5zIG5hbWUsIG9yICJhdXRvIi4gVGhpcyBkZXRlcm1pbmVzIGFuIGlwIGFkZHJlc3MgYXZhaWxhYmxlIG9uIHRoaXMgaG9zdCB0byBiaW5kLiBJZiB5b3UgYXJlIHNlcGFyYXRpbmcgUlRQIGFuZCBTSVAgdHJhZmZpYywgeW91IHdpbGwgd2FudCB0byBoYXZlIHVzZSBkaWZmZXJlbnQgYWRkcmVzc2VzIHdoZXJlIHRoaXMgdmFyaWFibGUgYXBwZWFycy4gVXNlZCBieTogZGluZ2FsaW5nLmNvbmYueG1s
559be35a-7718-4bfa-ab02-a4cf9dcd8d01	external_rtp_ip	$${local_ip_v4}	IP Address	true	10	KElmIHlvdScncmUgZ29pbmcgdG8gbG9hZCB0ZXN0IHRoZW4gcGxlYXNlIGlucHV0IHJlYWwgSVAgYWRkcmVzc2VzIGZvciBleHRlcm5hbF9ydHBfaXAgYW5kIGV4dGVybmFsX3NpcF9pcCkNCg0KQ2FuIGJlIGFuIG9uZSBvZjoNCiAgIGlwIGFkZHJlc3M6ICIxMi4zNC41Ni43OCINCiAgIGEgc3R1biBzZXJ2ZXIgbG9va3VwOiAic3R1bjpzdHVuLnNlcnZlci5jb20iDQogICBhIEROUyBuYW1lOiAiaG9zdDpob3N0LnNlcnZlci5jb20iDQoNCndoZXJlIGZzLm15ZG9tYWluLmNvbSBpcyBhIEROUyBBIHJlY29yZC11c2VmdWwgd2hlbiBmcyBpcyBvbiBhIGR5bmFtaWMgSVAgYWRkcmVzcywgYW5kIHVzZXMgYSBkeW5hbWljIEROUyB1cGRhdGVyLiBJZiB1bnNwZWNpZmllZCwgdGhlIGJpbmRfc2VydmVyX2lwIHZhbHVlIGlzIHVzZWQuIFVzZWQgYnk6IHNvZmlhLmNvbmYueG1sIGRpbmdhbGluZy5jb25mLnhtbA==
90eedfaf-4725-48ee-9bc3-61f552b47e8c	external_sip_ip	$${local_ip_v4}	IP Address	true	11	VXNlZCBhcyB0aGUgcHVibGljIElQIGFkZHJlc3MgZm9yIFNEUC4NCg0KQ2FuIGJlIGFuIG9uZSBvZjoNCiAgIGlwIGFkZHJlc3M6ICIxMi4zNC41Ni43OCINCiAgIGEgc3R1biBzZXJ2ZXIgbG9va3VwOiAic3R1bjpzdHVuLnNlcnZlci5jb20iDQogICBhIEROUyBuYW1lOiAiaG9zdDpob3N0LnNlcnZlci5jb20iDQoNCndoZXJlIGZzLm15ZG9tYWluLmNvbSBpcyBhIEROUyBBIHJlY29yZC11c2VmdWwgd2hlbiBmcyBpcyBvbiBhIGR5bmFtaWMgSVAgYWRkcmVzcywgYW5kIHVzZXMgYSBkeW5hbWljIEROUyB1cGRhdGVyLiBJZiB1bnNwZWNpZmllZCwgdGhlIGJpbmRfc2VydmVyX2lwIHZhbHVlIGlzIHVzZWQuIFVzZWQgYnk6IHNvZmlhLmNvbmYueG1sIGRpbmdhbGluZy5jb25mLnhtbA==
6d7799e3-2491-4509-960f-2300d7966fc1	unroll_loops	true	SIP	true	12	VXNlZCB0byB0dXJuIG9uIHNpcCBsb29wYmFjayB1bnJvbGxpbmcu
b8c6df9f-f7f3-456d-a865-b782c95075c4	call_debug	false	Defaults	true	13	
b7fc81ff-b82a-4740-b7fe-1315bdd4c809	console_loglevel	info	Defaults	true	14	
4638d88f-3d70-4832-8436-68a837ab3110	default_areacode	208	Defaults	true	15	
4ada303a-be25-4151-aa5e-88bc319c007e	uk-ring	%(400,200,400,450);%(400,2200,400,450)	Defaults	true	16	
152c2741-7968-4cdb-ad91-a0b622e433b3	us-ring	%(2000, 4000, 440.0, 480.0)	Defaults	true	17	
eef39e21-918e-46d1-a699-7f1264b818f7	pt-ring	%(1000, 5000, 400.0, 0.0)	Defaults	true	18	
2ed478ae-1021-4c2e-b93e-ed4f95f33034	fr-ring	%(1500, 3500, 440.0, 0.0)	Defaults	true	19	
cf7dc717-eb56-455d-9103-f6a0e45cfd11	rs-ring	%(1000, 4000, 425.0, 0.0)	Defaults	true	20	
cbabc950-049d-4be0-8db5-e80fbbe637f6	bong-ring	v=-7;%(100,0,941.0,1477.0);v=-7;>=2;+=.1;%(1400,0,350,440)	Defaults	true	21	
1a4776d2-5ee3-4747-9dc0-2c518bf3ddce	sit	%(274,0,913.8);%(274,0,1370.6);%(380,0,1776.7)	Defaults	true	22	
5330b9b7-1cc6-44e0-b3f8-fc3b647d7906	sip_tls_version	tlsv1	SIP	true	23	U0lQIGFuZCBUTFMgc2V0dGluZ3Mu
b3dddaa0-96c8-4cdf-8ba2-3bafe40ea8ab	internal_auth_calls	true	SIP Profile: Internal	true	24	
d4fff28e-9072-44ff-b0a8-9264a790ade3	internal_sip_port	5060	SIP Profile: Internal	true	25	
3c1fe7db-bbdb-4d22-b0a8-fd917811a865	internal_tls_port	5061	SIP Profile: Internal	true	26	
b2ab6e76-c039-47b2-94c1-74cd7e237cf9	internal_ssl_enable	false	SIP Profile: Internal	true	27	
be39d5cd-5098-4546-9c70-de22b3d3f963	internal_ssl_dir	$${base_dir}/conf/ssl	SIP Profile: Internal	true	28	
51660c85-f98a-4934-8f4a-c25e0148abff	external_auth_calls	false	SIP Profile: External	true	29	
523850d0-d82e-4313-a2e1-24c627b45551	external_sip_port	5080	SIP Profile: External	true	30	
e5a83abb-3168-48dc-a038-6c638657bb5e	external_tls_port	5081	SIP Profile: External	true	31	
e0ab6323-db7d-4056-98cb-4f2e8ce69684	external_ssl_enable	false	SIP Profile: External	true	32	
3df1b495-1e33-4b3a-a753-cf7f093956c2	external_ssl_dir	$${base_dir}/conf/ssl	SIP Profile: External	true	33	
0377ae99-20f3-4304-9ff2-23fa03d1e831	use_profile	internal	Defaults	true	34	
e117e30c-9720-4174-ac13-a7d720382a87	default_language	en	Defaults	true	35	
5d63daec-0e7c-4783-b008-84934a44cf84	default_dialect	us	Defaults	true	36	
d867aee1-c98e-4ece-a44b-eaf28ace5dd1	default_voice	callie	Defaults	true	37	
8a94ff06-775d-402c-9ba4-2fe0db8ae37b	ajax_refresh_rate	3000	Defaults	true	38	
6f300fed-d415-465c-a5db-fa7c2190e216	format_phone	Rxxx-xxx-xxxx	Defaults	true	39	
4263c6a2-fd4e-4fc8-837c-024688f04d2c	format_phone	xxx-xxx-xxxx	Defaults	true	40	
1d73d27f-cab4-458a-8b8b-951e363a3b20	xml_cdr_archive	dir	Defaults	true	41	
133d503d-9344-4660-a6f9-3726a659f24e	ringback	$${us-ring}	Defaults	true	42	
04a521ae-b195-439b-9263-c295661035b2	transfer_ringback	$${us-ring}	Defaults	true	43	
405f4572-c1c9-497a-8b97-627e89d1da6f	domain	sip.c-call.eu	Domain	false	1	U2V0cyB0aGUgZGVmYXVsdCBkb21haW4u
08343869-5d93-485a-838e-9e73a7afa15d	domain_uuid	60d6731b-e548-4175-ab3e-3fbb261e60f5	Defaults	false	999	
\.


--
-- Data for Name: v_voicemail_destinations; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_voicemail_destinations (voicemail_destination_uuid, voicemail_uuid, voicemail_uuid_copy) FROM stdin;
\.


--
-- Data for Name: v_voicemail_greetings; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_voicemail_greetings (voicemail_greeting_uuid, domain_uuid, voicemail_id, greeting_name, greeting_description) FROM stdin;
\.


--
-- Data for Name: v_voicemail_messages; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_voicemail_messages (domain_uuid, voicemail_message_uuid, voicemail_uuid, created_epoch, read_epoch, caller_id_name, caller_id_number, message_length, message_status, message_priority) FROM stdin;
\.


--
-- Data for Name: v_voicemails; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_voicemails (domain_uuid, voicemail_uuid, voicemail_id, voicemail_password, greeting_id, voicemail_mail_to, voicemail_attach_file, voicemail_local_after_email, voicemail_enabled, voicemail_description) FROM stdin;
60d6731b-e548-4175-ab3e-3fbb261e60f5	f66126eb-5fd2-45da-9cb6-2046617b51db	118	77877787	\N		true	true	true	
60d6731b-e548-4175-ab3e-3fbb261e60f5	9ee6fc4c-556d-43c0-8179-f82c2cd51b41	106	200629752	\N		true	true	false	
60d6731b-e548-4175-ab3e-3fbb261e60f5	b88d270a-0ec6-415c-833e-9ad6f771f400	834	12341234	\N		true	true	false	
\.


--
-- Data for Name: v_xml_cdr; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_xml_cdr (uuid, domain_uuid, extension_uuid, domain_name, accountcode, direction, default_language, context, xml, json, caller_id_name, caller_id_number, destination_number, start_epoch, start_stamp, answer_stamp, answer_epoch, end_epoch, end_stamp, duration, mduration, billsec, billmsec, bridge_uuid, read_codec, read_rate, write_codec, write_rate, remote_media_ip, network_addr, recording_file, leg, pdd_ms, rtp_audio_in_mos, last_app, last_arg, cc_side, cc_member_uuid, cc_queue_joined_epoch, cc_queue, cc_member_session_uuid, cc_agent, cc_agent_type, waitsec, conference_name, conference_uuid, conference_member_id, digits_dialed, pin_number, hangup_cause, hangup_cause_q850, sip_hangup_disposition) FROM stdin;
\.


--
-- Data for Name: v_xmpp; Type: TABLE DATA; Schema: public; Owner: fusionpbx
--

COPY v_xmpp (xmpp_profile_uuid, domain_uuid, profile_name, username, password, dialplan, context, rtp_ip, ext_rtp_ip, auto_login, sasl_type, xmpp_server, tls_enable, use_rtp_timer, default_exten, vad, avatar, candidate_acl, local_network_acl, enabled, description) FROM stdin;
\.


--
-- Name: v_apps_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_apps
    ADD CONSTRAINT v_apps_pkey PRIMARY KEY (app_uuid);


--
-- Name: v_call_block_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_call_block
    ADD CONSTRAINT v_call_block_pkey PRIMARY KEY (call_block_uuid);


--
-- Name: v_call_broadcasts_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_call_broadcasts
    ADD CONSTRAINT v_call_broadcasts_pkey PRIMARY KEY (call_broadcast_uuid);


--
-- Name: v_call_center_agents_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_call_center_agents
    ADD CONSTRAINT v_call_center_agents_pkey PRIMARY KEY (call_center_agent_uuid);


--
-- Name: v_call_center_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_call_center_logs
    ADD CONSTRAINT v_call_center_logs_pkey PRIMARY KEY (cc_uuid);


--
-- Name: v_call_center_queues_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_call_center_queues
    ADD CONSTRAINT v_call_center_queues_pkey PRIMARY KEY (call_center_queue_uuid);


--
-- Name: v_call_center_tiers_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_call_center_tiers
    ADD CONSTRAINT v_call_center_tiers_pkey PRIMARY KEY (call_center_tier_uuid);


--
-- Name: v_call_flows_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_call_flows
    ADD CONSTRAINT v_call_flows_pkey PRIMARY KEY (call_flow_uuid);


--
-- Name: v_clips_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_clips
    ADD CONSTRAINT v_clips_pkey PRIMARY KEY (clip_uuid);


--
-- Name: v_conference_centers_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_conference_centers
    ADD CONSTRAINT v_conference_centers_pkey PRIMARY KEY (conference_center_uuid);


--
-- Name: v_conference_rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_conference_rooms
    ADD CONSTRAINT v_conference_rooms_pkey PRIMARY KEY (conference_room_uuid);


--
-- Name: v_conference_session_details_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_conference_session_details
    ADD CONSTRAINT v_conference_session_details_pkey PRIMARY KEY (conference_session_detail_uuid);


--
-- Name: v_conference_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_conference_sessions
    ADD CONSTRAINT v_conference_sessions_pkey PRIMARY KEY (conference_session_uuid);


--
-- Name: v_conference_users_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_conference_users
    ADD CONSTRAINT v_conference_users_pkey PRIMARY KEY (conference_user_uuid);


--
-- Name: v_conferences_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_conferences
    ADD CONSTRAINT v_conferences_pkey PRIMARY KEY (conference_uuid);


--
-- Name: v_contact_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_contact_addresses
    ADD CONSTRAINT v_contact_addresses_pkey PRIMARY KEY (contact_address_uuid);


--
-- Name: v_contact_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_contact_groups
    ADD CONSTRAINT v_contact_groups_pkey PRIMARY KEY (contact_groups_uuid);


--
-- Name: v_contact_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_contact_notes
    ADD CONSTRAINT v_contact_notes_pkey PRIMARY KEY (contact_note_uuid);


--
-- Name: v_contact_phones_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_contact_phones
    ADD CONSTRAINT v_contact_phones_pkey PRIMARY KEY (contact_phone_uuid);


--
-- Name: v_contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_contacts
    ADD CONSTRAINT v_contacts_pkey PRIMARY KEY (contact_uuid);


--
-- Name: v_databases_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_databases
    ADD CONSTRAINT v_databases_pkey PRIMARY KEY (database_uuid);


--
-- Name: v_default_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_default_settings
    ADD CONSTRAINT v_default_settings_pkey PRIMARY KEY (default_setting_uuid);


--
-- Name: v_destinations_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_destinations
    ADD CONSTRAINT v_destinations_pkey PRIMARY KEY (destination_uuid);


--
-- Name: v_device_keys_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_device_keys
    ADD CONSTRAINT v_device_keys_pkey PRIMARY KEY (device_key_uuid);


--
-- Name: v_device_lines_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_device_lines
    ADD CONSTRAINT v_device_lines_pkey PRIMARY KEY (device_line_uuid);


--
-- Name: v_device_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_device_settings
    ADD CONSTRAINT v_device_settings_pkey PRIMARY KEY (device_setting_uuid);


--
-- Name: v_devices_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_devices
    ADD CONSTRAINT v_devices_pkey PRIMARY KEY (device_uuid);


--
-- Name: v_dialplan_details_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_dialplan_details
    ADD CONSTRAINT v_dialplan_details_pkey PRIMARY KEY (dialplan_detail_uuid);


--
-- Name: v_dialplans_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_dialplans
    ADD CONSTRAINT v_dialplans_pkey PRIMARY KEY (dialplan_uuid);


--
-- Name: v_domain_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_domain_settings
    ADD CONSTRAINT v_domain_settings_pkey PRIMARY KEY (domain_setting_uuid);


--
-- Name: v_domains_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_domains
    ADD CONSTRAINT v_domains_pkey PRIMARY KEY (domain_uuid);


--
-- Name: v_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_emails
    ADD CONSTRAINT v_emails_pkey PRIMARY KEY (email_uuid);


--
-- Name: v_extension_users_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_extension_users
    ADD CONSTRAINT v_extension_users_pkey PRIMARY KEY (extension_user_uuid);


--
-- Name: v_extensions_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_extensions
    ADD CONSTRAINT v_extensions_pkey PRIMARY KEY (extension_uuid);


--
-- Name: v_fax_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_fax
    ADD CONSTRAINT v_fax_pkey PRIMARY KEY (fax_uuid);


--
-- Name: v_fax_users_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_fax_users
    ADD CONSTRAINT v_fax_users_pkey PRIMARY KEY (fax_user_uuid);


--
-- Name: v_follow_me_destinations_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_follow_me_destinations
    ADD CONSTRAINT v_follow_me_destinations_pkey PRIMARY KEY (follow_me_destination_uuid);


--
-- Name: v_follow_me_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_follow_me
    ADD CONSTRAINT v_follow_me_pkey PRIMARY KEY (follow_me_uuid);


--
-- Name: v_gateways_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_gateways
    ADD CONSTRAINT v_gateways_pkey PRIMARY KEY (gateway_uuid);


--
-- Name: v_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_group_permissions
    ADD CONSTRAINT v_group_permissions_pkey PRIMARY KEY (group_permission_uuid);


--
-- Name: v_group_users_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_group_users
    ADD CONSTRAINT v_group_users_pkey PRIMARY KEY (group_user_uuid);


--
-- Name: v_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_groups
    ADD CONSTRAINT v_groups_pkey PRIMARY KEY (group_uuid);


--
-- Name: v_ivr_menu_options_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_ivr_menu_options
    ADD CONSTRAINT v_ivr_menu_options_pkey PRIMARY KEY (ivr_menu_option_uuid);


--
-- Name: v_ivr_menus_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_ivr_menus
    ADD CONSTRAINT v_ivr_menus_pkey PRIMARY KEY (ivr_menu_uuid);


--
-- Name: v_meeting_users_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_meeting_users
    ADD CONSTRAINT v_meeting_users_pkey PRIMARY KEY (meeting_user_uuid);


--
-- Name: v_menus_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_menus
    ADD CONSTRAINT v_menus_pkey PRIMARY KEY (menu_uuid);


--
-- Name: v_modules_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_modules
    ADD CONSTRAINT v_modules_pkey PRIMARY KEY (module_uuid);


--
-- Name: v_notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_notifications
    ADD CONSTRAINT v_notifications_pkey PRIMARY KEY (notification_uuid);


--
-- Name: v_recordings_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_recordings
    ADD CONSTRAINT v_recordings_pkey PRIMARY KEY (recording_uuid);


--
-- Name: v_ring_group_destinations_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_ring_group_destinations
    ADD CONSTRAINT v_ring_group_destinations_pkey PRIMARY KEY (ring_group_destination_uuid);


--
-- Name: v_ring_group_users_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_ring_group_users
    ADD CONSTRAINT v_ring_group_users_pkey PRIMARY KEY (ring_group_user_uuid);


--
-- Name: v_ring_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_ring_groups
    ADD CONSTRAINT v_ring_groups_pkey PRIMARY KEY (ring_group_uuid);


--
-- Name: v_rss_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_rss
    ADD CONSTRAINT v_rss_pkey PRIMARY KEY (rss_uuid);


--
-- Name: v_rss_sub_category_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_rss_sub_category
    ADD CONSTRAINT v_rss_sub_category_pkey PRIMARY KEY (rss_sub_category_uuid);


--
-- Name: v_rss_sub_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_rss_sub
    ADD CONSTRAINT v_rss_sub_pkey PRIMARY KEY (rss_sub_uuid);


--
-- Name: v_schema_data_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_schema_data
    ADD CONSTRAINT v_schema_data_pkey PRIMARY KEY (schema_data_uuid);


--
-- Name: v_schema_fields_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_schema_fields
    ADD CONSTRAINT v_schema_fields_pkey PRIMARY KEY (schema_field_uuid);


--
-- Name: v_schema_name_values_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_schema_name_values
    ADD CONSTRAINT v_schema_name_values_pkey PRIMARY KEY (schema_name_value_uuid);


--
-- Name: v_schemas_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_schemas
    ADD CONSTRAINT v_schemas_pkey PRIMARY KEY (schema_uuid);


--
-- Name: v_services_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_services
    ADD CONSTRAINT v_services_pkey PRIMARY KEY (service_uuid);


--
-- Name: v_sip_profile_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_sip_profile_settings
    ADD CONSTRAINT v_sip_profile_settings_pkey PRIMARY KEY (sip_profile_setting_uuid);


--
-- Name: v_sip_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_sip_profiles
    ADD CONSTRAINT v_sip_profiles_pkey PRIMARY KEY (sip_profile_uuid);


--
-- Name: v_software_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_software
    ADD CONSTRAINT v_software_pkey PRIMARY KEY (software_uuid);


--
-- Name: v_user_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_user_settings
    ADD CONSTRAINT v_user_settings_pkey PRIMARY KEY (user_setting_uuid);


--
-- Name: v_users_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_users
    ADD CONSTRAINT v_users_pkey PRIMARY KEY (user_uuid);


--
-- Name: v_vars_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_vars
    ADD CONSTRAINT v_vars_pkey PRIMARY KEY (var_uuid);


--
-- Name: v_voicemail_destinations_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_voicemail_destinations
    ADD CONSTRAINT v_voicemail_destinations_pkey PRIMARY KEY (voicemail_destination_uuid);


--
-- Name: v_voicemail_greetings_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_voicemail_greetings
    ADD CONSTRAINT v_voicemail_greetings_pkey PRIMARY KEY (voicemail_greeting_uuid);


--
-- Name: v_voicemail_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_voicemail_messages
    ADD CONSTRAINT v_voicemail_messages_pkey PRIMARY KEY (voicemail_message_uuid);


--
-- Name: v_voicemails_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_voicemails
    ADD CONSTRAINT v_voicemails_pkey PRIMARY KEY (voicemail_uuid);


--
-- Name: v_xml_cdr_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_xml_cdr
    ADD CONSTRAINT v_xml_cdr_pkey PRIMARY KEY (uuid);


--
-- Name: v_xmpp_pkey; Type: CONSTRAINT; Schema: public; Owner: fusionpbx; Tablespace: 
--

ALTER TABLE ONLY v_xmpp
    ADD CONSTRAINT v_xmpp_pkey PRIMARY KEY (xmpp_profile_uuid);


--
-- Name: index_billsec; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX index_billsec ON v_xml_cdr USING btree (billsec);


--
-- Name: index_caller_id_name; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX index_caller_id_name ON v_xml_cdr USING btree (caller_id_name);


--
-- Name: index_destination_number; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX index_destination_number ON v_xml_cdr USING btree (destination_number);


--
-- Name: index_duration; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX index_duration ON v_xml_cdr USING btree (duration);


--
-- Name: index_hangup_cause; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX index_hangup_cause ON v_xml_cdr USING btree (hangup_cause);


--
-- Name: index_start_stamp; Type: INDEX; Schema: public; Owner: fusionpbx; Tablespace: 
--

CREATE INDEX index_start_stamp ON v_xml_cdr USING btree (start_stamp);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

